#!/bin/python

import random as rnd
import sys
import string
from llvm import *
from llvm.core import *
import trashengine as trash

def obfuscate_module(module):
    target_func = module.get_function_named("compare")

    for block in target_func.basic_blocks:
        trash.fill_block_with_trash(block)

    print(module.get_function_named("compare"))

if __name__ == '__main__':
    ll_filename = sys.argv[1]

    ll_file = file(ll_filename)
    ll_module = Module.from_assembly(ll_file)

    obfuscate_module(ll_module)

    obfuscated_bitcode_file = file(sys.argv[2], "w")
    ll_module.to_bitcode(obfuscated_bitcode_file)
