; ModuleID = 'shell.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-redhat-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.rusage = type { %struct.timeval, %struct.timeval, %union.anon, %union.anon.0, %union.anon.1, %union.anon.2, %union.anon.3, %union.anon.4, %union.anon.5, %union.anon.6, %union.anon.7, %union.anon.8, %union.anon.9, %union.anon.10, %union.anon.11, %union.anon.12 }
%struct.timeval = type { i64, i64 }
%union.anon = type { i64 }
%union.anon.0 = type { i64 }
%union.anon.1 = type { i64 }
%union.anon.2 = type { i64 }
%union.anon.3 = type { i64 }
%union.anon.4 = type { i64 }
%union.anon.5 = type { i64 }
%union.anon.6 = type { i64 }
%union.anon.7 = type { i64 }
%union.anon.8 = type { i64 }
%union.anon.9 = type { i64 }
%union.anon.10 = type { i64 }
%union.anon.11 = type { i64 }
%union.anon.12 = type { i64 }
%struct.sqlite3_vfs = type { i32, i32, i32, %struct.sqlite3_vfs*, i8*, i8*, i32 (%struct.sqlite3_vfs*, i8*, %struct.sqlite3_file*, i32, i32*)*, i32 (%struct.sqlite3_vfs*, i8*, i32)*, i32 (%struct.sqlite3_vfs*, i8*, i32, i32*)*, i32 (%struct.sqlite3_vfs*, i8*, i32, i8*)*, i8* (%struct.sqlite3_vfs*, i8*)*, void (%struct.sqlite3_vfs*, i32, i8*)*, void ()* (%struct.sqlite3_vfs*, i8*, i8*)*, void (%struct.sqlite3_vfs*, i8*)*, i32 (%struct.sqlite3_vfs*, i32, i8*)*, i32 (%struct.sqlite3_vfs*, i32)*, i32 (%struct.sqlite3_vfs*, double*)*, i32 (%struct.sqlite3_vfs*, i32, i8*)*, i32 (%struct.sqlite3_vfs*, i64*)*, i32 (%struct.sqlite3_vfs*, i8*, void ()*)*, void ()* (%struct.sqlite3_vfs*, i8*)*, i8* (%struct.sqlite3_vfs*, i8*)* }
%struct.sqlite3_file = type { %struct.sqlite3_io_methods* }
%struct.sqlite3_io_methods = type { i32, i32 (%struct.sqlite3_file*)*, i32 (%struct.sqlite3_file*, i8*, i32, i64)*, i32 (%struct.sqlite3_file*, i8*, i32, i64)*, i32 (%struct.sqlite3_file*, i64)*, i32 (%struct.sqlite3_file*, i32)*, i32 (%struct.sqlite3_file*, i64*)*, i32 (%struct.sqlite3_file*, i32)*, i32 (%struct.sqlite3_file*, i32)*, i32 (%struct.sqlite3_file*, i32*)*, i32 (%struct.sqlite3_file*, i32, i8*)*, i32 (%struct.sqlite3_file*)*, i32 (%struct.sqlite3_file*)*, i32 (%struct.sqlite3_file*, i32, i32, i32, i8**)*, i32 (%struct.sqlite3_file*, i32, i32, i32)*, void (%struct.sqlite3_file*)*, i32 (%struct.sqlite3_file*, i32)*, i32 (%struct.sqlite3_file*, i64, i32, i8**)*, i32 (%struct.sqlite3_file*, i64, i8*)* }
%struct.anon.15 = type { i8*, i8* }
%struct.sqlite3 = type opaque
%struct.ShellState = type { %struct.sqlite3*, i32, i32, i32, i32, i32, i32, i32, %struct._IO_FILE*, %struct._IO_FILE*, i32, i32, i32, i32, i32, i8*, [20 x i8], [20 x i8], [100 x i32], [100 x i32], [20 x i8], %struct.SavedModeInfo, [4096 x i8], i8*, i8*, i8*, %struct.sqlite3_stmt*, %struct._IO_FILE*, i32*, i32, i32 }
%struct.SavedModeInfo = type { i32, i32, i32, [100 x i32] }
%struct.sqlite3_stmt = type opaque
%struct.anon.16 = type { i8*, i32 }
%struct.sqlite3_context = type opaque
%struct.Mem = type opaque
%struct.sqlite3_backup = type opaque
%struct.ImportCtx = type { i8*, %struct._IO_FILE*, i8*, i32, i32, i32, i32, i32, i32 }
%struct.anon = type { i8*, i32 }
%struct.anon.13 = type { i8*, i32 }
%struct.passwd = type { i8*, i8*, i32, i32, i8*, i8*, i8* }
%struct.anon.14 = type { i8*, i32 }

@.str = private unnamed_addr constant [61 x i8] c"2015-05-30 22:57:49 c864ff912db8bc0a3c3ecc1ceac61a25332e76c5\00", align 1
@stderr = external global %struct._IO_FILE*
@.str1 = private unnamed_addr constant [49 x i8] c"SQLite header and source version mismatch\0A%s\0A%s\0A\00", align 1
@Argv0 = internal global i8* null, align 8
@stdin_is_interactive = internal global i32 1, align 4
@.str2 = private unnamed_addr constant [15 x i8] c"out of memory\0A\00", align 1
@.str3 = private unnamed_addr constant [11 x i8] c"-separator\00", align 1
@.str4 = private unnamed_addr constant [11 x i8] c"-nullvalue\00", align 1
@.str5 = private unnamed_addr constant [9 x i8] c"-newline\00", align 1
@.str6 = private unnamed_addr constant [5 x i8] c"-cmd\00", align 1
@.str7 = private unnamed_addr constant [6 x i8] c"-init\00", align 1
@.str8 = private unnamed_addr constant [7 x i8] c"-batch\00", align 1
@.str9 = private unnamed_addr constant [6 x i8] c"-heap\00", align 1
@.str10 = private unnamed_addr constant [9 x i8] c"-scratch\00", align 1
@.str11 = private unnamed_addr constant [11 x i8] c"-pagecache\00", align 1
@.str12 = private unnamed_addr constant [11 x i8] c"-lookaside\00", align 1
@.str13 = private unnamed_addr constant [6 x i8] c"-mmap\00", align 1
@.str14 = private unnamed_addr constant [5 x i8] c"-vfs\00", align 1
@.str15 = private unnamed_addr constant [19 x i8] c"no such VFS: \22%s\22\0A\00", align 1
@.str16 = private unnamed_addr constant [9 x i8] c":memory:\00", align 1
@stdout = external global %struct._IO_FILE*
@.str17 = private unnamed_addr constant [6 x i8] c"-html\00", align 1
@.str18 = private unnamed_addr constant [6 x i8] c"-list\00", align 1
@.str19 = private unnamed_addr constant [6 x i8] c"-line\00", align 1
@.str20 = private unnamed_addr constant [8 x i8] c"-column\00", align 1
@.str21 = private unnamed_addr constant [5 x i8] c"-csv\00", align 1
@.str22 = private unnamed_addr constant [2 x i8] c",\00", align 1
@.str23 = private unnamed_addr constant [7 x i8] c"-ascii\00", align 1
@.str24 = private unnamed_addr constant [2 x i8] c"\1F\00", align 1
@.str25 = private unnamed_addr constant [2 x i8] c"\1E\00", align 1
@.str26 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str27 = private unnamed_addr constant [8 x i8] c"-header\00", align 1
@.str28 = private unnamed_addr constant [10 x i8] c"-noheader\00", align 1
@.str29 = private unnamed_addr constant [6 x i8] c"-echo\00", align 1
@.str30 = private unnamed_addr constant [5 x i8] c"-eqp\00", align 1
@.str31 = private unnamed_addr constant [7 x i8] c"-stats\00", align 1
@.str32 = private unnamed_addr constant [11 x i8] c"-scanstats\00", align 1
@.str33 = private unnamed_addr constant [11 x i8] c"-backslash\00", align 1
@.str34 = private unnamed_addr constant [6 x i8] c"-bail\00", align 1
@bail_on_error = internal global i32 0, align 4
@.str35 = private unnamed_addr constant [9 x i8] c"-version\00", align 1
@.str36 = private unnamed_addr constant [7 x i8] c"%s %s\0A\00", align 1
@.str37 = private unnamed_addr constant [13 x i8] c"-interactive\00", align 1
@.str38 = private unnamed_addr constant [6 x i8] c"-help\00", align 1
@.str39 = private unnamed_addr constant [11 x i8] c"Error: %s\0A\00", align 1
@.str40 = private unnamed_addr constant [35 x i8] c"Error: unable to process SQL \22%s\22\0A\00", align 1
@.str41 = private unnamed_addr constant [31 x i8] c"%s: Error: unknown option: %s\0A\00", align 1
@.str42 = private unnamed_addr constant [34 x i8] c"Use -help for a list of options.\0A\00", align 1
@.str43 = private unnamed_addr constant [34 x i8] c"Error: unable to process SQL: %s\0A\00", align 1
@.str44 = private unnamed_addr constant [56 x i8] c"SQLite version %s %.19s\0AEnter \22.help\22 for usage hints.\0A\00", align 1
@.str45 = private unnamed_addr constant [16 x i8] c"Connected to a \00", align 1
@.str46 = private unnamed_addr constant [29 x i8] c"transient in-memory database\00", align 1
@.str47 = private unnamed_addr constant [60 x i8] c".\0AUse \22.open FILENAME\22 to reopen on a persistent database.\0A\00", align 1
@.str48 = private unnamed_addr constant [19 x i8] c"%s/.sqlite_history\00", align 1
@stdin = external global %struct._IO_FILE*
@.str49 = private unnamed_addr constant [22 x i8] c"Error: out of memory\0A\00", align 1
@.str50 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@seenInterrupt = internal global i32 0, align 4
@.str51 = private unnamed_addr constant [4 x i8] c"%s\0A\00", align 1
@.str52 = private unnamed_addr constant [2 x i8] c";\00", align 1
@.str53 = private unnamed_addr constant [20 x i8] c"nAlloc>0 && zSql!=0\00", align 1
@.str54 = private unnamed_addr constant [8 x i8] c"shell.c\00", align 1
@__PRETTY_FUNCTION__.process_input = private unnamed_addr constant [40 x i8] c"int process_input(ShellState *, FILE *)\00", align 1
@.str55 = private unnamed_addr constant [21 x i8] c"Error: near line %d:\00", align 1
@.str56 = private unnamed_addr constant [7 x i8] c"Error:\00", align 1
@.str57 = private unnamed_addr constant [27 x i8] c"Error: incomplete SQL: %s\0A\00", align 1
@enableTimer = internal global i32 0, align 4
@.str58 = private unnamed_addr constant [36 x i8] c"Run Time: real %.3f user %f sys %f\0A\00", align 1
@iBegin = internal global i64 0, align 8
@sBegin = internal global %struct.rusage zeroinitializer, align 8
@timeOfDay.clockVfs = internal global %struct.sqlite3_vfs* null, align 8
@continuePrompt = internal global [20 x i8] zeroinitializer, align 16
@mainPrompt = internal global [20 x i8] zeroinitializer, align 16
@find_home_dir.home_dir = internal global i8* null, align 8
@.str59 = private unnamed_addr constant [5 x i8] c"HOME\00", align 1
@.str60 = private unnamed_addr constant [11 x i8] c"\1B[1m%s\1B[0m\00", align 1
@.str61 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@.str62 = private unnamed_addr constant [11 x i8] c"%*s = %s%s\00", align 1
@.str63 = private unnamed_addr constant [8 x i8] c"%*.*s%s\00", align 1
@.str64 = private unnamed_addr constant [3 x i8] c"  \00", align 1
@.str65 = private unnamed_addr constant [9 x i8] c"%-*.*s%s\00", align 1
@.str66 = private unnamed_addr constant [94 x i8] c"---------------------------------------------------------------------------------------------\00", align 1
@.str67 = private unnamed_addr constant [5 x i8] c"%*.s\00", align 1
@.str68 = private unnamed_addr constant [5 x i8] c"%s%s\00", align 1
@.str69 = private unnamed_addr constant [4 x i8] c";%s\00", align 1
@.str70 = private unnamed_addr constant [5 x i8] c"<TR>\00", align 1
@.str71 = private unnamed_addr constant [5 x i8] c"<TH>\00", align 1
@.str72 = private unnamed_addr constant [7 x i8] c"</TH>\0A\00", align 1
@.str73 = private unnamed_addr constant [7 x i8] c"</TR>\0A\00", align 1
@.str74 = private unnamed_addr constant [5 x i8] c"<TD>\00", align 1
@.str75 = private unnamed_addr constant [7 x i8] c"</TD>\0A\00", align 1
@.str76 = private unnamed_addr constant [15 x i8] c"INSERT INTO %s\00", align 1
@.str77 = private unnamed_addr constant [2 x i8] c"(\00", align 1
@.str78 = private unnamed_addr constant [2 x i8] c")\00", align 1
@.str79 = private unnamed_addr constant [9 x i8] c" VALUES(\00", align 1
@.str80 = private unnamed_addr constant [7 x i8] c"%sNULL\00", align 1
@.str81 = private unnamed_addr constant [4 x i8] c");\0A\00", align 1
@.str82 = private unnamed_addr constant [3 x i8] c"X'\00", align 1
@.str83 = private unnamed_addr constant [5 x i8] c"%02x\00", align 1
@.str84 = private unnamed_addr constant [2 x i8] c"'\00", align 1
@.str85 = private unnamed_addr constant [5 x i8] c"'%s'\00", align 1
@.str86 = private unnamed_addr constant [3 x i8] c"''\00", align 1
@.str87 = private unnamed_addr constant [7 x i8] c"%.*s''\00", align 1
@needCsvQuote = internal constant [256 x i8] c"\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\00\01\00\00\00\00\01\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01\01", align 16
@.str88 = private unnamed_addr constant [6 x i8] c"\5C%03o\00", align 1
@.str89 = private unnamed_addr constant [5 x i8] c"%.*s\00", align 1
@.str90 = private unnamed_addr constant [5 x i8] c"&lt;\00", align 1
@.str91 = private unnamed_addr constant [6 x i8] c"&amp;\00", align 1
@.str92 = private unnamed_addr constant [5 x i8] c"&gt;\00", align 1
@.str93 = private unnamed_addr constant [7 x i8] c"&quot;\00", align 1
@.str94 = private unnamed_addr constant [6 x i8] c"&#39;\00", align 1
@.str95 = private unnamed_addr constant [22 x i8] c"EXPLAIN QUERY PLAN %s\00", align 1
@.str96 = private unnamed_addr constant [12 x i8] c"--EQP-- %d,\00", align 1
@.str97 = private unnamed_addr constant [4 x i8] c"%d,\00", align 1
@.str98 = private unnamed_addr constant [56 x i8] c"Memory Used:                         %d (max %d) bytes\0A\00", align 1
@.str99 = private unnamed_addr constant [50 x i8] c"Number of Outstanding Allocations:   %d (max %d)\0A\00", align 1
@.str100 = private unnamed_addr constant [56 x i8] c"Number of Pcache Pages Used:         %d (max %d) pages\0A\00", align 1
@.str101 = private unnamed_addr constant [56 x i8] c"Number of Pcache Overflow Bytes:     %d (max %d) bytes\0A\00", align 1
@.str102 = private unnamed_addr constant [50 x i8] c"Number of Scratch Allocations Used:  %d (max %d)\0A\00", align 1
@.str103 = private unnamed_addr constant [56 x i8] c"Number of Scratch Overflow Bytes:    %d (max %d) bytes\0A\00", align 1
@.str104 = private unnamed_addr constant [47 x i8] c"Largest Allocation:                  %d bytes\0A\00", align 1
@.str105 = private unnamed_addr constant [47 x i8] c"Largest Pcache Allocation:           %d bytes\0A\00", align 1
@.str106 = private unnamed_addr constant [47 x i8] c"Largest Scratch Allocation:          %d bytes\0A\00", align 1
@.str107 = private unnamed_addr constant [50 x i8] c"Lookaside Slots Used:                %d (max %d)\0A\00", align 1
@.str108 = private unnamed_addr constant [41 x i8] c"Successful lookaside attempts:       %d\0A\00", align 1
@.str109 = private unnamed_addr constant [41 x i8] c"Lookaside failures due to size:      %d\0A\00", align 1
@.str110 = private unnamed_addr constant [41 x i8] c"Lookaside failures due to OOM:       %d\0A\00", align 1
@.str111 = private unnamed_addr constant [47 x i8] c"Pager Heap Usage:                    %d bytes\0A\00", align 1
@.str112 = private unnamed_addr constant [41 x i8] c"Page cache hits:                     %d\0A\00", align 1
@.str113 = private unnamed_addr constant [41 x i8] c"Page cache misses:                   %d\0A\00", align 1
@.str114 = private unnamed_addr constant [41 x i8] c"Page cache writes:                   %d\0A\00", align 1
@.str115 = private unnamed_addr constant [47 x i8] c"Schema Heap Usage:                   %d bytes\0A\00", align 1
@.str116 = private unnamed_addr constant [47 x i8] c"Statement Heap/Lookaside Usage:      %d bytes\0A\00", align 1
@.str117 = private unnamed_addr constant [41 x i8] c"Fullscan Steps:                      %d\0A\00", align 1
@.str118 = private unnamed_addr constant [41 x i8] c"Sort Operations:                     %d\0A\00", align 1
@.str119 = private unnamed_addr constant [41 x i8] c"Autoindex Inserts:                   %d\0A\00", align 1
@.str120 = private unnamed_addr constant [41 x i8] c"Virtual Machine Steps:               %d\0A\00", align 1
@.str121 = private unnamed_addr constant [5 x i8] c"Next\00", align 1
@.str122 = private unnamed_addr constant [5 x i8] c"Prev\00", align 1
@.str123 = private unnamed_addr constant [6 x i8] c"VPrev\00", align 1
@.str124 = private unnamed_addr constant [6 x i8] c"VNext\00", align 1
@.str125 = private unnamed_addr constant [11 x i8] c"SorterNext\00", align 1
@.str126 = private unnamed_addr constant [11 x i8] c"NextIfOpen\00", align 1
@.str127 = private unnamed_addr constant [11 x i8] c"PrevIfOpen\00", align 1
@explain_data_prepare.azNext = private unnamed_addr constant [8 x i8*] [i8* getelementptr inbounds ([5 x i8]* @.str121, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str122, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str123, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str124, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8]* @.str125, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8]* @.str126, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8]* @.str127, i32 0, i32 0), i8* null], align 16
@.str128 = private unnamed_addr constant [6 x i8] c"Yield\00", align 1
@.str129 = private unnamed_addr constant [7 x i8] c"SeekLT\00", align 1
@.str130 = private unnamed_addr constant [7 x i8] c"SeekGT\00", align 1
@.str131 = private unnamed_addr constant [11 x i8] c"RowSetRead\00", align 1
@.str132 = private unnamed_addr constant [7 x i8] c"Rewind\00", align 1
@.str133 = private unnamed_addr constant [5 x i8] c"Goto\00", align 1
@explain_data_prepare.azGoto = private unnamed_addr constant [2 x i8*] [i8* getelementptr inbounds ([5 x i8]* @.str133, i32 0, i32 0), i8* null], align 16
@.str134 = private unnamed_addr constant [8 x i8] c"explain\00", align 1
@.str135 = private unnamed_addr constant [7 x i8] c"backup\00", align 1
@.str136 = private unnamed_addr constant [5 x i8] c"save\00", align 1
@.str137 = private unnamed_addr constant [20 x i8] c"unknown option: %s\0A\00", align 1
@.str138 = private unnamed_addr constant [31 x i8] c"too many arguments to .backup\0A\00", align 1
@.str139 = private unnamed_addr constant [38 x i8] c"missing FILENAME argument on .backup\0A\00", align 1
@.str140 = private unnamed_addr constant [5 x i8] c"main\00", align 1
@.str141 = private unnamed_addr constant [25 x i8] c"Error: cannot open \22%s\22\0A\00", align 1
@.str142 = private unnamed_addr constant [5 x i8] c"bail\00", align 1
@.str143 = private unnamed_addr constant [21 x i8] c"Usage: .bail on|off\0A\00", align 1
@.str144 = private unnamed_addr constant [7 x i8] c"binary\00", align 1
@.str145 = private unnamed_addr constant [23 x i8] c"Usage: .binary on|off\0A\00", align 1
@.str146 = private unnamed_addr constant [11 x i8] c"breakpoint\00", align 1
@.str147 = private unnamed_addr constant [6 x i8] c"clone\00", align 1
@.str148 = private unnamed_addr constant [24 x i8] c"Usage: .clone FILENAME\0A\00", align 1
@.str149 = private unnamed_addr constant [10 x i8] c"databases\00", align 1
@.str150 = private unnamed_addr constant [23 x i8] c"PRAGMA database_list; \00", align 1
@.str151 = private unnamed_addr constant [7 x i8] c"dbinfo\00", align 1
@.str152 = private unnamed_addr constant [5 x i8] c"dump\00", align 1
@.str153 = private unnamed_addr constant [29 x i8] c"Usage: .dump ?LIKE-PATTERN?\0A\00", align 1
@.str154 = private unnamed_addr constant [26 x i8] c"PRAGMA foreign_keys=OFF;\0A\00", align 1
@.str155 = private unnamed_addr constant [20 x i8] c"BEGIN TRANSACTION;\0A\00", align 1
@.str156 = private unnamed_addr constant [42 x i8] c"SAVEPOINT dump; PRAGMA writable_schema=ON\00", align 1
@.str157 = private unnamed_addr constant [107 x i8] c"SELECT name, type, sql FROM sqlite_master WHERE sql NOT NULL AND type=='table' AND name!='sqlite_sequence'\00", align 1
@.str158 = private unnamed_addr constant [72 x i8] c"SELECT name, type, sql FROM sqlite_master WHERE name=='sqlite_sequence'\00", align 1
@.str159 = private unnamed_addr constant [88 x i8] c"SELECT sql FROM sqlite_master WHERE sql NOT NULL AND type IN ('index','trigger','view')\00", align 1
@zShellStatic = internal global i8* null, align 8
@.str160 = private unnamed_addr constant [112 x i8] c"SELECT name, type, sql FROM sqlite_master WHERE tbl_name LIKE shellstatic() AND type=='table'  AND sql NOT NULL\00", align 1
@.str161 = private unnamed_addr constant [122 x i8] c"SELECT sql FROM sqlite_master WHERE sql NOT NULL  AND type IN ('index','trigger','view')  AND tbl_name LIKE shellstatic()\00", align 1
@.str162 = private unnamed_addr constant [29 x i8] c"PRAGMA writable_schema=OFF;\0A\00", align 1
@.str163 = private unnamed_addr constant [28 x i8] c"PRAGMA writable_schema=OFF;\00", align 1
@.str164 = private unnamed_addr constant [14 x i8] c"RELEASE dump;\00", align 1
@.str165 = private unnamed_addr constant [28 x i8] c"ROLLBACK; -- due to errors\0A\00", align 1
@.str166 = private unnamed_addr constant [9 x i8] c"COMMIT;\0A\00", align 1
@.str167 = private unnamed_addr constant [5 x i8] c"echo\00", align 1
@.str168 = private unnamed_addr constant [21 x i8] c"Usage: .echo on|off\0A\00", align 1
@.str169 = private unnamed_addr constant [4 x i8] c"eqp\00", align 1
@.str170 = private unnamed_addr constant [20 x i8] c"Usage: .eqp on|off\0A\00", align 1
@.str171 = private unnamed_addr constant [5 x i8] c"exit\00", align 1
@.str172 = private unnamed_addr constant [11 x i8] c"fullschema\00", align 1
@.str173 = private unnamed_addr constant [20 x i8] c"Usage: .fullschema\0A\00", align 1
@.str174 = private unnamed_addr constant [262 x i8] c"SELECT sql FROM  (SELECT sql sql, type type, tbl_name tbl_name, name name, rowid x     FROM sqlite_master UNION ALL   SELECT sql, type, tbl_name, name, rowid FROM sqlite_temp_master) WHERE type!='meta' AND sql NOTNULL AND name NOT LIKE 'sqlite_%' ORDER BY rowid\00", align 1
@.str175 = private unnamed_addr constant [67 x i8] c"SELECT rowid FROM sqlite_master WHERE name GLOB 'sqlite_stat[134]'\00", align 1
@.str176 = private unnamed_addr constant [32 x i8] c"/* No STAT tables available */\0A\00", align 1
@.str177 = private unnamed_addr constant [24 x i8] c"ANALYZE sqlite_master;\0A\00", align 1
@.str178 = private unnamed_addr constant [31 x i8] c"SELECT 'ANALYZE sqlite_master'\00", align 1
@.str179 = private unnamed_addr constant [13 x i8] c"sqlite_stat1\00", align 1
@.str180 = private unnamed_addr constant [27 x i8] c"SELECT * FROM sqlite_stat1\00", align 1
@.str181 = private unnamed_addr constant [13 x i8] c"sqlite_stat3\00", align 1
@.str182 = private unnamed_addr constant [27 x i8] c"SELECT * FROM sqlite_stat3\00", align 1
@.str183 = private unnamed_addr constant [13 x i8] c"sqlite_stat4\00", align 1
@.str184 = private unnamed_addr constant [27 x i8] c"SELECT * FROM sqlite_stat4\00", align 1
@.str185 = private unnamed_addr constant [8 x i8] c"headers\00", align 1
@.str186 = private unnamed_addr constant [24 x i8] c"Usage: .headers on|off\0A\00", align 1
@.str187 = private unnamed_addr constant [5 x i8] c"help\00", align 1
@zHelp = internal global [3763 x i8] c".backup ?DB? FILE      Backup DB (default \22main\22) to FILE\0A.bail on|off           Stop after hitting an error.  Default OFF\0A.binary on|off         Turn binary output on or off.  Default OFF\0A.clone NEWDB           Clone data into NEWDB from the existing database\0A.databases             List names and files of attached databases\0A.dbinfo ?DB?           Show status information about the database\0A.dump ?TABLE? ...      Dump the database in an SQL text format\0A                         If TABLE specified, only dump tables matching\0A                         LIKE pattern TABLE.\0A.echo on|off           Turn command echo on or off\0A.eqp on|off            Enable or disable automatic EXPLAIN QUERY PLAN\0A.exit                  Exit this program\0A.explain ?on|off?      Turn output mode suitable for EXPLAIN on or off.\0A                         With no args, it turns EXPLAIN on.\0A.fullschema            Show schema and the content of sqlite_stat tables\0A.headers on|off        Turn display of headers on or off\0A.help                  Show this message\0A.import FILE TABLE     Import data from FILE into TABLE\0A.indexes ?TABLE?       Show names of all indexes\0A                         If TABLE specified, only show indexes for tables\0A                         matching LIKE pattern TABLE.\0A.limit ?LIMIT? ?VAL?   Display or change the value of an SQLITE_LIMIT\0A.load FILE ?ENTRY?     Load an extension library\0A.log FILE|off          Turn logging on or off.  FILE can be stderr/stdout\0A.mode MODE ?TABLE?     Set output mode where MODE is one of:\0A                         ascii    Columns/rows delimited by 0x1F and 0x1E\0A                         csv      Comma-separated values\0A                         column   Left-aligned columns.  (See .width)\0A                         html     HTML <table> code\0A                         insert   SQL insert statements for TABLE\0A                         line     One value per line\0A                         list     Values delimited by .separator strings\0A                         tabs     Tab-separated values\0A                         tcl      TCL list elements\0A.nullvalue STRING      Use STRING in place of NULL values\0A.once FILENAME         Output for the next SQL command only to FILENAME\0A.open ?FILENAME?       Close existing database and reopen FILENAME\0A.output ?FILENAME?     Send output to FILENAME or stdout\0A.print STRING...       Print literal STRING\0A.prompt MAIN CONTINUE  Replace the standard prompts\0A.quit                  Exit this program\0A.read FILENAME         Execute SQL in FILENAME\0A.restore ?DB? FILE     Restore content of DB (default \22main\22) from FILE\0A.save FILE             Write in-memory database into FILE\0A.scanstats on|off      Turn sqlite3_stmt_scanstatus() metrics on or off\0A.schema ?TABLE?        Show the CREATE statements\0A                         If TABLE specified, only show tables matching\0A                         LIKE pattern TABLE.\0A.separator COL ?ROW?   Change the column separator and optionally the row\0A                         separator for both the output mode and .import\0A.shell CMD ARGS...     Run CMD ARGS... in a system shell\0A.show                  Show the current values for various settings\0A.stats on|off          Turn stats on or off\0A.system CMD ARGS...    Run CMD ARGS... in a system shell\0A.tables ?TABLE?        List names of tables\0A                         If TABLE specified, only list tables matching\0A                         LIKE pattern TABLE.\0A.timeout MS            Try opening locked tables for MS milliseconds\0A.timer on|off          Turn SQL timer on or off\0A.trace FILE|off        Output each SQL statement as it is run\0A.vfsname ?AUX?         Print the name of the VFS stack\0A.width NUM1 NUM2 ...   Set column widths for \22column\22 mode\0A                         Negative values right-justify\0A\00", align 16
@.str188 = private unnamed_addr constant [7 x i8] c"import\00", align 1
@.str189 = private unnamed_addr constant [27 x i8] c"Usage: .import FILE TABLE\0A\00", align 1
@.str190 = private unnamed_addr constant [54 x i8] c"Error: non-null column separator required for import\0A\00", align 1
@.str191 = private unnamed_addr constant [65 x i8] c"Error: multi-character column separators not allowed for import\0A\00", align 1
@.str192 = private unnamed_addr constant [51 x i8] c"Error: non-null row separator required for import\0A\00", align 1
@.str193 = private unnamed_addr constant [3 x i8] c"\0D\0A\00", align 1
@.str194 = private unnamed_addr constant [62 x i8] c"Error: multi-character row separators not allowed for import\0A\00", align 1
@.str195 = private unnamed_addr constant [2 x i8] c"r\00", align 1
@.str196 = private unnamed_addr constant [7 x i8] c"<pipe>\00", align 1
@.str197 = private unnamed_addr constant [3 x i8] c"rb\00", align 1
@.str198 = private unnamed_addr constant [17 x i8] c"SELECT * FROM %s\00", align 1
@.str199 = private unnamed_addr constant [17 x i8] c"no such table: *\00", align 1
@.str200 = private unnamed_addr constant [16 x i8] c"CREATE TABLE %s\00", align 1
@.str201 = private unnamed_addr constant [17 x i8] c"%z%c\0A  \22%s\22 TEXT\00", align 1
@.str202 = private unnamed_addr constant [16 x i8] c"%s: empty file\0A\00", align 1
@.str203 = private unnamed_addr constant [5 x i8] c"%z\0A)\00", align 1
@.str204 = private unnamed_addr constant [33 x i8] c"CREATE TABLE %s(...) failed: %s\0A\00", align 1
@.str205 = private unnamed_addr constant [26 x i8] c"INSERT INTO \22%w\22 VALUES(?\00", align 1
@.str206 = private unnamed_addr constant [6 x i8] c"BEGIN\00", align 1
@.str207 = private unnamed_addr constant [70 x i8] c"%s:%d: expected %d columns but found %d - filling the rest with NULL\0A\00", align 1
@.str208 = private unnamed_addr constant [58 x i8] c"%s:%d: expected %d columns but found %d - extras ignored\0A\00", align 1
@.str209 = private unnamed_addr constant [26 x i8] c"%s:%d: INSERT failed: %s\0A\00", align 1
@.str210 = private unnamed_addr constant [7 x i8] c"COMMIT\00", align 1
@.str211 = private unnamed_addr constant [8 x i8] c"indices\00", align 1
@.str212 = private unnamed_addr constant [8 x i8] c"indexes\00", align 1
@.str213 = private unnamed_addr constant [155 x i8] c"SELECT name FROM sqlite_master WHERE type='index' AND name NOT LIKE 'sqlite_%' UNION ALL SELECT name FROM sqlite_temp_master WHERE type='index' ORDER BY 1\00", align 1
@.str214 = private unnamed_addr constant [190 x i8] c"SELECT name FROM sqlite_master WHERE type='index' AND tbl_name LIKE shellstatic() UNION ALL SELECT name FROM sqlite_temp_master WHERE type='index' AND tbl_name LIKE shellstatic() ORDER BY 1\00", align 1
@.str215 = private unnamed_addr constant [32 x i8] c"Usage: .indexes ?LIKE-PATTERN?\0A\00", align 1
@.str216 = private unnamed_addr constant [54 x i8] c"Error: querying sqlite_master and sqlite_temp_master\0A\00", align 1
@.str217 = private unnamed_addr constant [7 x i8] c"limits\00", align 1
@do_meta_command.aLimit = internal constant <{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }> <{ { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([7 x i8]* @.str218, i32 0, i32 0), i32 0, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([11 x i8]* @.str219, i32 0, i32 0), i32 1, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([7 x i8]* @.str220, i32 0, i32 0), i32 2, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([11 x i8]* @.str221, i32 0, i32 0), i32 3, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([16 x i8]* @.str222, i32 0, i32 0), i32 4, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([8 x i8]* @.str223, i32 0, i32 0), i32 5, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([13 x i8]* @.str224, i32 0, i32 0), i32 6, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([9 x i8]* @.str225, i32 0, i32 0), i32 7, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([20 x i8]* @.str226, i32 0, i32 0), i32 8, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([16 x i8]* @.str227, i32 0, i32 0), i32 9, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([14 x i8]* @.str228, i32 0, i32 0), i32 10, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([15 x i8]* @.str229, i32 0, i32 0), i32 11, [4 x i8] undef } }>, align 16
@.str218 = private unnamed_addr constant [7 x i8] c"length\00", align 1
@.str219 = private unnamed_addr constant [11 x i8] c"sql_length\00", align 1
@.str220 = private unnamed_addr constant [7 x i8] c"column\00", align 1
@.str221 = private unnamed_addr constant [11 x i8] c"expr_depth\00", align 1
@.str222 = private unnamed_addr constant [16 x i8] c"compound_select\00", align 1
@.str223 = private unnamed_addr constant [8 x i8] c"vdbe_op\00", align 1
@.str224 = private unnamed_addr constant [13 x i8] c"function_arg\00", align 1
@.str225 = private unnamed_addr constant [9 x i8] c"attached\00", align 1
@.str226 = private unnamed_addr constant [20 x i8] c"like_pattern_length\00", align 1
@.str227 = private unnamed_addr constant [16 x i8] c"variable_number\00", align 1
@.str228 = private unnamed_addr constant [14 x i8] c"trigger_depth\00", align 1
@.str229 = private unnamed_addr constant [15 x i8] c"worker_threads\00", align 1
@.str230 = private unnamed_addr constant [9 x i8] c"%20s %d\0A\00", align 1
@.str231 = private unnamed_addr constant [32 x i8] c"Usage: .limit NAME ?NEW-VALUE?\0A\00", align 1
@.str232 = private unnamed_addr constant [23 x i8] c"ambiguous limit: \22%s\22\0A\00", align 1
@.str233 = private unnamed_addr constant [67 x i8] c"unknown limit: \22%s\22\0Aenter \22.limits\22 with no arguments for a list.\0A\00", align 1
@.str234 = private unnamed_addr constant [5 x i8] c"load\00", align 1
@.str235 = private unnamed_addr constant [32 x i8] c"Usage: .load FILE ?ENTRYPOINT?\0A\00", align 1
@.str236 = private unnamed_addr constant [4 x i8] c"log\00", align 1
@.str237 = private unnamed_addr constant [22 x i8] c"Usage: .log FILENAME\0A\00", align 1
@.str238 = private unnamed_addr constant [5 x i8] c"mode\00", align 1
@.str239 = private unnamed_addr constant [6 x i8] c"lines\00", align 1
@.str240 = private unnamed_addr constant [8 x i8] c"columns\00", align 1
@.str241 = private unnamed_addr constant [5 x i8] c"list\00", align 1
@.str242 = private unnamed_addr constant [5 x i8] c"html\00", align 1
@.str243 = private unnamed_addr constant [4 x i8] c"tcl\00", align 1
@.str244 = private unnamed_addr constant [2 x i8] c" \00", align 1
@.str245 = private unnamed_addr constant [4 x i8] c"csv\00", align 1
@.str246 = private unnamed_addr constant [5 x i8] c"tabs\00", align 1
@.str247 = private unnamed_addr constant [2 x i8] c"\09\00", align 1
@.str248 = private unnamed_addr constant [7 x i8] c"insert\00", align 1
@.str249 = private unnamed_addr constant [6 x i8] c"table\00", align 1
@.str250 = private unnamed_addr constant [6 x i8] c"ascii\00", align 1
@.str251 = private unnamed_addr constant [79 x i8] c"Error: mode should be one of: ascii column csv html insert line list tabs tcl\0A\00", align 1
@.str252 = private unnamed_addr constant [10 x i8] c"nullvalue\00", align 1
@.str253 = private unnamed_addr constant [26 x i8] c"Usage: .nullvalue STRING\0A\00", align 1
@.str254 = private unnamed_addr constant [5 x i8] c"open\00", align 1
@.str255 = private unnamed_addr constant [7 x i8] c"output\00", align 1
@.str256 = private unnamed_addr constant [5 x i8] c"once\00", align 1
@.str257 = private unnamed_addr constant [7 x i8] c"stdout\00", align 1
@.str258 = private unnamed_addr constant [17 x i8] c"Usage: .%s FILE\0A\00", align 1
@.str259 = private unnamed_addr constant [19 x i8] c"Usage: .once FILE\0A\00", align 1
@.str260 = private unnamed_addr constant [2 x i8] c"w\00", align 1
@.str261 = private unnamed_addr constant [30 x i8] c"Error: cannot open pipe \22%s\22\0A\00", align 1
@.str262 = private unnamed_addr constant [4 x i8] c"off\00", align 1
@.str263 = private unnamed_addr constant [29 x i8] c"Error: cannot write to \22%s\22\0A\00", align 1
@.str264 = private unnamed_addr constant [6 x i8] c"print\00", align 1
@.str265 = private unnamed_addr constant [7 x i8] c"prompt\00", align 1
@.str266 = private unnamed_addr constant [5 x i8] c"quit\00", align 1
@.str267 = private unnamed_addr constant [5 x i8] c"read\00", align 1
@.str268 = private unnamed_addr constant [19 x i8] c"Usage: .read FILE\0A\00", align 1
@.str269 = private unnamed_addr constant [8 x i8] c"restore\00", align 1
@.str270 = private unnamed_addr constant [27 x i8] c"Usage: .restore ?DB? FILE\0A\00", align 1
@.str271 = private unnamed_addr constant [32 x i8] c"Error: source database is busy\0A\00", align 1
@.str272 = private unnamed_addr constant [10 x i8] c"scanstats\00", align 1
@.str273 = private unnamed_addr constant [50 x i8] c"Warning: .scanstats not available in this build.\0A\00", align 1
@.str274 = private unnamed_addr constant [26 x i8] c"Usage: .scanstats on|off\0A\00", align 1
@.str275 = private unnamed_addr constant [7 x i8] c"schema\00", align 1
@.str276 = private unnamed_addr constant [14 x i8] c"sqlite_master\00", align 1
@.str277 = private unnamed_addr constant [105 x i8] c"CREATE TABLE sqlite_master (\0A  type text,\0A  name text,\0A  tbl_name text,\0A  rootpage integer,\0A  sql text\0A)\00", align 1
@.str278 = private unnamed_addr constant [4 x i8] c"sql\00", align 1
@.str279 = private unnamed_addr constant [19 x i8] c"sqlite_temp_master\00", align 1
@.str280 = private unnamed_addr constant [115 x i8] c"CREATE TEMP TABLE sqlite_temp_master (\0A  type text,\0A  name text,\0A  tbl_name text,\0A  rootpage integer,\0A  sql text\0A)\00", align 1
@.str281 = private unnamed_addr constant [274 x i8] c"SELECT sql FROM   (SELECT sql sql, type type, tbl_name tbl_name, name name, rowid x     FROM sqlite_master UNION ALL   SELECT sql, type, tbl_name, name, rowid FROM sqlite_temp_master) WHERE lower(tbl_name) LIKE shellstatic()  AND type!='meta' AND sql NOTNULL ORDER BY rowid\00", align 1
@.str282 = private unnamed_addr constant [263 x i8] c"SELECT sql FROM   (SELECT sql sql, type type, tbl_name tbl_name, name name, rowid x     FROM sqlite_master UNION ALL   SELECT sql, type, tbl_name, name, rowid FROM sqlite_temp_master) WHERE type!='meta' AND sql NOTNULL AND name NOT LIKE 'sqlite_%' ORDER BY rowid\00", align 1
@.str283 = private unnamed_addr constant [31 x i8] c"Usage: .schema ?LIKE-PATTERN?\0A\00", align 1
@.str284 = private unnamed_addr constant [36 x i8] c"Error: querying schema information\0A\00", align 1
@.str285 = private unnamed_addr constant [10 x i8] c"separator\00", align 1
@.str286 = private unnamed_addr constant [29 x i8] c"Usage: .separator COL ?ROW?\0A\00", align 1
@.str287 = private unnamed_addr constant [6 x i8] c"shell\00", align 1
@.str288 = private unnamed_addr constant [7 x i8] c"system\00", align 1
@.str289 = private unnamed_addr constant [24 x i8] c"Usage: .system COMMAND\0A\00", align 1
@.str290 = private unnamed_addr constant [5 x i8] c"\22%s\22\00", align 1
@.str291 = private unnamed_addr constant [6 x i8] c"%z %s\00", align 1
@.str292 = private unnamed_addr constant [8 x i8] c"%z \22%s\22\00", align 1
@.str293 = private unnamed_addr constant [27 x i8] c"System command returns %d\0A\00", align 1
@.str294 = private unnamed_addr constant [5 x i8] c"show\00", align 1
@.str295 = private unnamed_addr constant [14 x i8] c"Usage: .show\0A\00", align 1
@.str296 = private unnamed_addr constant [13 x i8] c"%12.12s: %s\0A\00", align 1
@.str297 = private unnamed_addr constant [3 x i8] c"on\00", align 1
@.str298 = private unnamed_addr constant [11 x i8] c"%9.9s: %s\0A\00", align 1
@modeDescr = internal global [10 x i8*] [i8* getelementptr inbounds ([5 x i8]* @.str352, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8]* @.str220, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str241, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str353, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str242, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8]* @.str248, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str243, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str245, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str134, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str250, i32 0, i32 0)], align 16
@.str299 = private unnamed_addr constant [10 x i8] c"%12.12s: \00", align 1
@.str300 = private unnamed_addr constant [13 x i8] c"colseparator\00", align 1
@.str301 = private unnamed_addr constant [13 x i8] c"rowseparator\00", align 1
@.str302 = private unnamed_addr constant [6 x i8] c"stats\00", align 1
@.str303 = private unnamed_addr constant [6 x i8] c"width\00", align 1
@.str304 = private unnamed_addr constant [4 x i8] c"%d \00", align 1
@.str305 = private unnamed_addr constant [22 x i8] c"Usage: .stats on|off\0A\00", align 1
@.str306 = private unnamed_addr constant [7 x i8] c"tables\00", align 1
@.str307 = private unnamed_addr constant [21 x i8] c"PRAGMA database_list\00", align 1
@.str308 = private unnamed_addr constant [113 x i8] c"SELECT name FROM sqlite_master WHERE type IN ('table','view')   AND name NOT LIKE 'sqlite_%%'   AND name LIKE ?1\00", align 1
@.str309 = private unnamed_addr constant [5 x i8] c"temp\00", align 1
@.str310 = private unnamed_addr constant [142 x i8] c"%z UNION ALL SELECT 'temp.' || name FROM sqlite_temp_master WHERE type IN ('table','view')   AND name NOT LIKE 'sqlite_%%'   AND name LIKE ?1\00", align 1
@.str311 = private unnamed_addr constant [140 x i8] c"%z UNION ALL SELECT '%q.' || name FROM \22%w\22.sqlite_master WHERE type IN ('table','view')   AND name NOT LIKE 'sqlite_%%'   AND name LIKE ?1\00", align 1
@.str312 = private unnamed_addr constant [14 x i8] c"%z ORDER BY 1\00", align 1
@.str313 = private unnamed_addr constant [2 x i8] c"%\00", align 1
@.str314 = private unnamed_addr constant [7 x i8] c"%s%-*s\00", align 1
@.str315 = private unnamed_addr constant [9 x i8] c"testctrl\00", align 1
@do_meta_command.aCtrl = internal constant <{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }> <{ { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([10 x i8]* @.str316, i32 0, i32 0), i32 5, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([13 x i8]* @.str317, i32 0, i32 0), i32 6, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([11 x i8]* @.str318, i32 0, i32 0), i32 7, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([12 x i8]* @.str319, i32 0, i32 0), i32 8, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([14 x i8]* @.str320, i32 0, i32 0), i32 9, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([20 x i8]* @.str321, i32 0, i32 0), i32 10, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([13 x i8]* @.str322, i32 0, i32 0), i32 11, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([7 x i8]* @.str323, i32 0, i32 0), i32 12, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([7 x i8]* @.str324, i32 0, i32 0), i32 13, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([8 x i8]* @.str325, i32 0, i32 0), i32 14, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([14 x i8]* @.str326, i32 0, i32 0), i32 15, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([10 x i8]* @.str327, i32 0, i32 0), i32 16, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([14 x i8]* @.str328, i32 0, i32 0), i32 17, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([10 x i8]* @.str329, i32 0, i32 0), i32 22, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([14 x i8]* @.str330, i32 0, i32 0), i32 20, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([9 x i8]* @.str331, i32 0, i32 0), i32 25, [4 x i8] undef } }>, align 16
@.str316 = private unnamed_addr constant [10 x i8] c"prng_save\00", align 1
@.str317 = private unnamed_addr constant [13 x i8] c"prng_restore\00", align 1
@.str318 = private unnamed_addr constant [11 x i8] c"prng_reset\00", align 1
@.str319 = private unnamed_addr constant [12 x i8] c"bitvec_test\00", align 1
@.str320 = private unnamed_addr constant [14 x i8] c"fault_install\00", align 1
@.str321 = private unnamed_addr constant [20 x i8] c"benign_malloc_hooks\00", align 1
@.str322 = private unnamed_addr constant [13 x i8] c"pending_byte\00", align 1
@.str323 = private unnamed_addr constant [7 x i8] c"assert\00", align 1
@.str324 = private unnamed_addr constant [7 x i8] c"always\00", align 1
@.str325 = private unnamed_addr constant [8 x i8] c"reserve\00", align 1
@.str326 = private unnamed_addr constant [14 x i8] c"optimizations\00", align 1
@.str327 = private unnamed_addr constant [10 x i8] c"iskeyword\00", align 1
@.str328 = private unnamed_addr constant [14 x i8] c"scratchmalloc\00", align 1
@.str329 = private unnamed_addr constant [10 x i8] c"byteorder\00", align 1
@.str330 = private unnamed_addr constant [14 x i8] c"never_corrupt\00", align 1
@.str331 = private unnamed_addr constant [9 x i8] c"imposter\00", align 1
@.str332 = private unnamed_addr constant [29 x i8] c"ambiguous option name: \22%s\22\0A\00", align 1
@.str333 = private unnamed_addr constant [36 x i8] c"Error: invalid testctrl option: %s\0A\00", align 1
@.str334 = private unnamed_addr constant [13 x i8] c"%d (0x%08x)\0A\00", align 1
@.str335 = private unnamed_addr constant [46 x i8] c"Error: testctrl %s takes a single int option\0A\00", align 1
@.str336 = private unnamed_addr constant [37 x i8] c"Error: testctrl %s takes no options\0A\00", align 1
@.str337 = private unnamed_addr constant [55 x i8] c"Error: testctrl %s takes a single unsigned int option\0A\00", align 1
@.str338 = private unnamed_addr constant [45 x i8] c"Usage: .testctrl imposter dbName onoff tnum\0A\00", align 1
@.str339 = private unnamed_addr constant [52 x i8] c"Error: CLI support for testctrl %s not implemented\0A\00", align 1
@.str340 = private unnamed_addr constant [8 x i8] c"timeout\00", align 1
@.str341 = private unnamed_addr constant [6 x i8] c"timer\00", align 1
@.str342 = private unnamed_addr constant [44 x i8] c"Error: timer not available on this system.\0A\00", align 1
@.str343 = private unnamed_addr constant [22 x i8] c"Usage: .timer on|off\0A\00", align 1
@.str344 = private unnamed_addr constant [6 x i8] c"trace\00", align 1
@.str345 = private unnamed_addr constant [24 x i8] c"Usage: .trace FILE|off\0A\00", align 1
@.str346 = private unnamed_addr constant [8 x i8] c"version\00", align 1
@.str347 = private unnamed_addr constant [14 x i8] c"SQLite %s %s\0A\00", align 1
@.str348 = private unnamed_addr constant [8 x i8] c"vfsname\00", align 1
@.str349 = private unnamed_addr constant [44 x i8] c"nArg<=(int)(sizeof(azArg)/sizeof(azArg[0]))\00", align 1
@__PRETTY_FUNCTION__.do_meta_command = private unnamed_addr constant [42 x i8] c"int do_meta_command(char *, ShellState *)\00", align 1
@.str350 = private unnamed_addr constant [76 x i8] c"Error: unknown command or invalid arguments:  \22%s\22. Enter \22.help\22 for help\0A\00", align 1
@.str351 = private unnamed_addr constant [7 x i8] c"%.*s;\0A\00", align 1
@.str352 = private unnamed_addr constant [5 x i8] c"line\00", align 1
@.str353 = private unnamed_addr constant [5 x i8] c"semi\00", align 1
@.str354 = private unnamed_addr constant [7 x i8] c"stderr\00", align 1
@.str355 = private unnamed_addr constant [3 x i8] c"wb\00", align 1
@.str356 = private unnamed_addr constant [31 x i8] c"%s:%d: unescaped %c character\0A\00", align 1
@.str357 = private unnamed_addr constant [37 x i8] c"%s:%d: unterminated %c-quoted field\0A\00", align 1
@.str358 = private unnamed_addr constant [29 x i8] c"/**** ERROR: (%d) %s *****/\0A\00", align 1
@.str359 = private unnamed_addr constant [4 x i8] c",%s\00", align 1
@.str360 = private unnamed_addr constant [4 x i8] c"\0A;\0A\00", align 1
@.str361 = private unnamed_addr constant [3 x i8] c";\0A\00", align 1
@.str362 = private unnamed_addr constant [35 x i8] c"/****** CORRUPTION ERROR *******/\0A\00", align 1
@.str363 = private unnamed_addr constant [20 x i8] c"/****** %s ******/\0A\00", align 1
@.str364 = private unnamed_addr constant [23 x i8] c"%s ORDER BY rowid DESC\00", align 1
@.str365 = private unnamed_addr constant [27 x i8] c"/****** ERROR: %s ******/\0A\00", align 1
@.str366 = private unnamed_addr constant [16 x i8] c"sqlite_sequence\00", align 1
@.str367 = private unnamed_addr constant [30 x i8] c"DELETE FROM sqlite_sequence;\0A\00", align 1
@.str368 = private unnamed_addr constant [13 x i8] c"sqlite_stat?\00", align 1
@.str369 = private unnamed_addr constant [8 x i8] c"sqlite_\00", align 1
@.str370 = private unnamed_addr constant [21 x i8] c"CREATE VIRTUAL TABLE\00", align 1
@.str371 = private unnamed_addr constant [28 x i8] c"PRAGMA writable_schema=ON;\0A\00", align 1
@.str372 = private unnamed_addr constant [92 x i8] c"INSERT INTO sqlite_master(type,name,tbl_name,rootpage,sql)VALUES('table','%q','%q',0,'%q');\00", align 1
@.str373 = private unnamed_addr constant [5 x i8] c"%s;\0A\00", align 1
@.str374 = private unnamed_addr constant [19 x i8] c"PRAGMA table_info(\00", align 1
@.str375 = private unnamed_addr constant [3 x i8] c");\00", align 1
@.str376 = private unnamed_addr constant [26 x i8] c"SELECT 'INSERT INTO ' || \00", align 1
@.str377 = private unnamed_addr constant [19 x i8] c" || ' VALUES(' || \00", align 1
@.str378 = private unnamed_addr constant [7 x i8] c"quote(\00", align 1
@.str379 = private unnamed_addr constant [4 x i8] c"), \00", align 1
@.str380 = private unnamed_addr constant [3 x i8] c") \00", align 1
@.str381 = private unnamed_addr constant [14 x i8] c"|| ')' FROM  \00", align 1
@.str382 = private unnamed_addr constant [21 x i8] c" ORDER BY rowid DESC\00", align 1
@.str383 = private unnamed_addr constant [16 x i8] c"(zCsr-zIn)==len\00", align 1
@__PRETTY_FUNCTION__.appendText = private unnamed_addr constant [45 x i8] c"char *appendText(char *, const char *, char)\00", align 1
@shell_dbinfo_command.aField = internal constant <{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }> <{ { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([21 x i8]* @.str384, i32 0, i32 0), i32 24, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([21 x i8]* @.str385, i32 0, i32 0), i32 28, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([21 x i8]* @.str386, i32 0, i32 0), i32 36, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([15 x i8]* @.str387, i32 0, i32 0), i32 40, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([15 x i8]* @.str388, i32 0, i32 0), i32 44, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([20 x i8]* @.str389, i32 0, i32 0), i32 48, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([21 x i8]* @.str390, i32 0, i32 0), i32 52, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([20 x i8]* @.str391, i32 0, i32 0), i32 64, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([15 x i8]* @.str392, i32 0, i32 0), i32 56, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([14 x i8]* @.str393, i32 0, i32 0), i32 60, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([16 x i8]* @.str394, i32 0, i32 0), i32 68, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([18 x i8]* @.str395, i32 0, i32 0), i32 96, [4 x i8] undef } }>, align 16
@.str384 = private unnamed_addr constant [21 x i8] c"file change counter:\00", align 1
@.str385 = private unnamed_addr constant [21 x i8] c"database page count:\00", align 1
@.str386 = private unnamed_addr constant [21 x i8] c"freelist page count:\00", align 1
@.str387 = private unnamed_addr constant [15 x i8] c"schema cookie:\00", align 1
@.str388 = private unnamed_addr constant [15 x i8] c"schema format:\00", align 1
@.str389 = private unnamed_addr constant [20 x i8] c"default cache size:\00", align 1
@.str390 = private unnamed_addr constant [21 x i8] c"autovacuum top root:\00", align 1
@.str391 = private unnamed_addr constant [20 x i8] c"incremental vacuum:\00", align 1
@.str392 = private unnamed_addr constant [15 x i8] c"text encoding:\00", align 1
@.str393 = private unnamed_addr constant [14 x i8] c"user version:\00", align 1
@.str394 = private unnamed_addr constant [16 x i8] c"application id:\00", align 1
@.str395 = private unnamed_addr constant [18 x i8] c"software version:\00", align 1
@shell_dbinfo_command.aQuery = internal constant [5 x %struct.anon.15] [%struct.anon.15 { i8* getelementptr inbounds ([18 x i8]* @.str396, i32 0, i32 0), i8* getelementptr inbounds ([43 x i8]* @.str397, i32 0, i32 0) }, %struct.anon.15 { i8* getelementptr inbounds ([19 x i8]* @.str398, i32 0, i32 0), i8* getelementptr inbounds ([43 x i8]* @.str399, i32 0, i32 0) }, %struct.anon.15 { i8* getelementptr inbounds ([20 x i8]* @.str400, i32 0, i32 0), i8* getelementptr inbounds ([45 x i8]* @.str401, i32 0, i32 0) }, %struct.anon.15 { i8* getelementptr inbounds ([17 x i8]* @.str402, i32 0, i32 0), i8* getelementptr inbounds ([42 x i8]* @.str403, i32 0, i32 0) }, %struct.anon.15 { i8* getelementptr inbounds ([13 x i8]* @.str404, i32 0, i32 0), i8* getelementptr inbounds ([34 x i8]* @.str405, i32 0, i32 0) }], align 16
@.str396 = private unnamed_addr constant [18 x i8] c"number of tables:\00", align 1
@.str397 = private unnamed_addr constant [43 x i8] c"SELECT count(*) FROM %s WHERE type='table'\00", align 1
@.str398 = private unnamed_addr constant [19 x i8] c"number of indexes:\00", align 1
@.str399 = private unnamed_addr constant [43 x i8] c"SELECT count(*) FROM %s WHERE type='index'\00", align 1
@.str400 = private unnamed_addr constant [20 x i8] c"number of triggers:\00", align 1
@.str401 = private unnamed_addr constant [45 x i8] c"SELECT count(*) FROM %s WHERE type='trigger'\00", align 1
@.str402 = private unnamed_addr constant [17 x i8] c"number of views:\00", align 1
@.str403 = private unnamed_addr constant [42 x i8] c"SELECT count(*) FROM %s WHERE type='view'\00", align 1
@.str404 = private unnamed_addr constant [13 x i8] c"schema size:\00", align 1
@.str405 = private unnamed_addr constant [34 x i8] c"SELECT total(length(sql)) FROM %s\00", align 1
@.str406 = private unnamed_addr constant [32 x i8] c"unable to read database header\0A\00", align 1
@.str407 = private unnamed_addr constant [10 x i8] c"%-20s %d\0A\00", align 1
@.str408 = private unnamed_addr constant [20 x i8] c"database page size:\00", align 1
@.str409 = private unnamed_addr constant [14 x i8] c"write format:\00", align 1
@.str410 = private unnamed_addr constant [13 x i8] c"read format:\00", align 1
@.str411 = private unnamed_addr constant [16 x i8] c"reserved bytes:\00", align 1
@.str412 = private unnamed_addr constant [9 x i8] c"%-20s %u\00", align 1
@.str413 = private unnamed_addr constant [8 x i8] c" (utf8)\00", align 1
@.str414 = private unnamed_addr constant [11 x i8] c" (utf16le)\00", align 1
@.str415 = private unnamed_addr constant [11 x i8] c" (utf16be)\00", align 1
@.str416 = private unnamed_addr constant [19 x i8] c"main.sqlite_master\00", align 1
@.str417 = private unnamed_addr constant [19 x i8] c"\22%w\22.sqlite_master\00", align 1
@.str418 = private unnamed_addr constant [27 x i8] c"File \22%s\22 already exists.\0A\00", align 1
@.str419 = private unnamed_addr constant [35 x i8] c"Cannot create output database: %s\0A\00", align 1
@.str420 = private unnamed_addr constant [27 x i8] c"PRAGMA writable_schema=ON;\00", align 1
@.str421 = private unnamed_addr constant [17 x i8] c"BEGIN EXCLUSIVE;\00", align 1
@.str422 = private unnamed_addr constant [13 x i8] c"type='table'\00", align 1
@.str423 = private unnamed_addr constant [14 x i8] c"type!='table'\00", align 1
@.str424 = private unnamed_addr constant [8 x i8] c"COMMIT;\00", align 1
@.str425 = private unnamed_addr constant [19 x i8] c"SELECT * FROM \22%w\22\00", align 1
@.str426 = private unnamed_addr constant [22 x i8] c"Error %d: %s on [%s]\0A\00", align 1
@.str427 = private unnamed_addr constant [36 x i8] c"INSERT OR IGNORE INTO \22%s\22 VALUES(?\00", align 1
@.str428 = private unnamed_addr constant [3 x i8] c",?\00", align 1
@.str429 = private unnamed_addr constant [14 x i8] c"Error %d: %s\0A\00", align 1
@.str430 = private unnamed_addr constant [4 x i8] c"%c\08\00", align 1
@.str431 = private unnamed_addr constant [5 x i8] c"|/-\5C\00", align 1
@.str432 = private unnamed_addr constant [40 x i8] c"SELECT * FROM \22%w\22 ORDER BY rowid DESC;\00", align 1
@.str433 = private unnamed_addr constant [36 x i8] c"Warning: cannot step \22%s\22 backwards\00", align 1
@.str434 = private unnamed_addr constant [45 x i8] c"SELECT name, sql FROM sqlite_master WHERE %s\00", align 1
@.str435 = private unnamed_addr constant [24 x i8] c"Error: (%d) %s on [%s]\0A\00", align 1
@.str436 = private unnamed_addr constant [7 x i8] c"%s... \00", align 1
@.str437 = private unnamed_addr constant [21 x i8] c"Error: %s\0ASQL: [%s]\0A\00", align 1
@.str438 = private unnamed_addr constant [6 x i8] c"done\0A\00", align 1
@.str439 = private unnamed_addr constant [65 x i8] c"SELECT name, sql FROM sqlite_master WHERE %s ORDER BY rowid DESC\00", align 1
@test_breakpoint.nCall = internal global i32 0, align 4
@.str440 = private unnamed_addr constant [4 x i8] c"yes\00", align 1
@.str441 = private unnamed_addr constant [3 x i8] c"no\00", align 1
@.str442 = private unnamed_addr constant [50 x i8] c"ERROR: Not a boolean value: \22%s\22. Assuming \22no\22.\0A\00", align 1
@.str443 = private unnamed_addr constant [145 x i8] c"Usage: %s [OPTIONS] FILENAME [SQL]\0AFILENAME is the name of an SQLite database. A new database is created\0Aif the file does not previously exist.\0A\00", align 1
@.str444 = private unnamed_addr constant [20 x i8] c"OPTIONS include:\0A%s\00", align 1
@zOptions = internal constant [1318 x i8] c"   -ascii               set output mode to 'ascii'\0A   -bail                stop after hitting an error\0A   -batch               force batch I/O\0A   -column              set output mode to 'column'\0A   -cmd COMMAND         run \22COMMAND\22 before reading stdin\0A   -csv                 set output mode to 'csv'\0A   -echo                print commands before execution\0A   -init FILENAME       read/process named file\0A   -[no]header          turn headers on or off\0A   -help                show this message\0A   -html                set output mode to HTML\0A   -interactive         force interactive I/O\0A   -line                set output mode to 'line'\0A   -list                set output mode to 'list'\0A   -lookaside SIZE N    use N entries of SZ bytes for lookaside memory\0A   -mmap N              default mmap size set to N\0A   -newline SEP         set output row separator. Default: '\5Cn'\0A   -nullvalue TEXT      set text string for NULL values. Default ''\0A   -pagecache SIZE N    use N slots of SZ bytes each for page cache memory\0A   -scratch SIZE N      use N slots of SZ bytes each for scratch memory\0A   -separator SEP       set output column separator. Default: '|'\0A   -stats               print memory stats before each finalize\0A   -version             show SQLite version\0A   -vfs NAME            use NAME as the default VFS\0A\00", align 16
@.str445 = private unnamed_addr constant [49 x i8] c"Use the -help option for additional information\0A\00", align 1
@.str446 = private unnamed_addr constant [65 x i8] c"-- warning: cannot find home directory; cannot read ~/.sqliterc\0A\00", align 1
@.str447 = private unnamed_addr constant [13 x i8] c"%s/.sqliterc\00", align 1
@.str448 = private unnamed_addr constant [30 x i8] c"-- Loading resources from %s\0A\00", align 1
@globalDb = internal global %struct.sqlite3* null, align 8
@.str449 = private unnamed_addr constant [12 x i8] c"shellstatic\00", align 1
@.str450 = private unnamed_addr constant [41 x i8] c"Error: unable to open database \22%s\22: %s\0A\00", align 1
@.str451 = private unnamed_addr constant [9 x i8] c"readfile\00", align 1
@.str452 = private unnamed_addr constant [10 x i8] c"writefile\00", align 1
@.str453 = private unnamed_addr constant [8 x i8] c"0==argc\00", align 1
@__PRETTY_FUNCTION__.shellstaticFunc = private unnamed_addr constant [63 x i8] c"void shellstaticFunc(sqlite3_context *, int, sqlite3_value **)\00", align 1
@.str454 = private unnamed_addr constant [13 x i8] c"zShellStatic\00", align 1
@integerValue.aMult = internal constant <{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }> <{ { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([4 x i8]* @.str455, i32 0, i32 0), i32 1024, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([4 x i8]* @.str456, i32 0, i32 0), i32 1048576, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([4 x i8]* @.str457, i32 0, i32 0), i32 1073741824, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([3 x i8]* @.str458, i32 0, i32 0), i32 1000, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([3 x i8]* @.str459, i32 0, i32 0), i32 1000000, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([3 x i8]* @.str460, i32 0, i32 0), i32 1000000000, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([2 x i8]* @.str461, i32 0, i32 0), i32 1000, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([2 x i8]* @.str462, i32 0, i32 0), i32 1000000, [4 x i8] undef }, { i8*, i32, [4 x i8] } { i8* getelementptr inbounds ([2 x i8]* @.str463, i32 0, i32 0), i32 1000000000, [4 x i8] undef } }>, align 16
@.str455 = private unnamed_addr constant [4 x i8] c"KiB\00", align 1
@.str456 = private unnamed_addr constant [4 x i8] c"MiB\00", align 1
@.str457 = private unnamed_addr constant [4 x i8] c"GiB\00", align 1
@.str458 = private unnamed_addr constant [3 x i8] c"KB\00", align 1
@.str459 = private unnamed_addr constant [3 x i8] c"MB\00", align 1
@.str460 = private unnamed_addr constant [3 x i8] c"GB\00", align 1
@.str461 = private unnamed_addr constant [2 x i8] c"K\00", align 1
@.str462 = private unnamed_addr constant [2 x i8] c"M\00", align 1
@.str463 = private unnamed_addr constant [2 x i8] c"G\00", align 1
@.str464 = private unnamed_addr constant [35 x i8] c"%s: Error: missing argument to %s\0A\00", align 1
@.str465 = private unnamed_addr constant [2 x i8] c"|\00", align 1
@.str466 = private unnamed_addr constant [9 x i8] c"sqlite> \00", align 1
@.str467 = private unnamed_addr constant [9 x i8] c"   ...> \00", align 1
@.str468 = private unnamed_addr constant [9 x i8] c"(%d) %s\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @get2byteInt(i8* %a) #0 {
  %1 = alloca i8*, align 8
  store i8* %a, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = getelementptr inbounds i8* %2, i64 0
  %4 = load i8* %3, align 1
  %5 = zext i8 %4 to i32
  %6 = shl i32 %5, 8
  %7 = load i8** %1, align 8
  %8 = getelementptr inbounds i8* %7, i64 1
  %9 = load i8* %8, align 1
  %10 = zext i8 %9 to i32
  %11 = add nsw i32 %6, %10
  ret i32 %11
}

; Function Attrs: nounwind uwtable
define i32 @get4byteInt(i8* %a) #0 {
  %1 = alloca i8*, align 8
  store i8* %a, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = getelementptr inbounds i8* %2, i64 0
  %4 = load i8* %3, align 1
  %5 = zext i8 %4 to i32
  %6 = shl i32 %5, 24
  %7 = load i8** %1, align 8
  %8 = getelementptr inbounds i8* %7, i64 1
  %9 = load i8* %8, align 1
  %10 = zext i8 %9 to i32
  %11 = shl i32 %10, 16
  %12 = add nsw i32 %6, %11
  %13 = load i8** %1, align 8
  %14 = getelementptr inbounds i8* %13, i64 2
  %15 = load i8* %14, align 1
  %16 = zext i8 %15 to i32
  %17 = shl i32 %16, 8
  %18 = add nsw i32 %12, %17
  %19 = load i8** %1, align 8
  %20 = getelementptr inbounds i8* %19, i64 3
  %21 = load i8* %20, align 1
  %22 = zext i8 %21 to i32
  %23 = add nsw i32 %18, %22
  ret i32 %23
}

; Function Attrs: nounwind uwtable
define i32 @main(i32 %argc, i8** %argv) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %zErrMsg = alloca i8*, align 8
  %data = alloca %struct.ShellState, align 8
  %zInitFile = alloca i8*, align 8
  %i = alloca i32, align 4
  %rc = alloca i32, align 4
  %warnInmemoryDb = alloca i32, align 4
  %readStdin = alloca i32, align 4
  %nCmd = alloca i32, align 4
  %azCmd = alloca i8**, align 8
  %z = alloca i8*, align 8
  %n = alloca i32, align 4
  %sz = alloca i32, align 4
  %n1 = alloca i32, align 4
  %sz2 = alloca i32, align 4
  %n3 = alloca i32, align 4
  %sz4 = alloca i32, align 4
  %sz5 = alloca i64, align 8
  %pVfs = alloca %struct.sqlite3_vfs*, align 8
  %z6 = alloca i8*, align 8
  %zHome = alloca i8*, align 8
  %zHistory = alloca i8*, align 8
  %nHistory = alloca i32, align 4
  store i32 0, i32* %1
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 8
  store i8* null, i8** %zErrMsg, align 8
  store i8* null, i8** %zInitFile, align 8
  store i32 0, i32* %rc, align 4
  store i32 0, i32* %warnInmemoryDb, align 4
  store i32 1, i32* %readStdin, align 4
  store i32 0, i32* %nCmd, align 4
  store i8** null, i8*** %azCmd, align 8
  %4 = call i8* @sqlite3_sourceid()
  %5 = call i32 @strcmp(i8* %4, i8* getelementptr inbounds ([61 x i8]* @.str, i32 0, i32 0)) #7
  %6 = icmp ne i32 %5, 0
  br i1 %6, label %7, label %11

; <label>:7                                       ; preds = %0
  %8 = load %struct._IO_FILE** @stderr, align 8
  %9 = call i8* @sqlite3_sourceid()
  %10 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([49 x i8]* @.str1, i32 0, i32 0), i8* %9, i8* getelementptr inbounds ([61 x i8]* @.str, i32 0, i32 0))
  call void @exit(i32 1) #8
  unreachable

; <label>:11                                      ; preds = %0
  %12 = load %struct._IO_FILE** @stderr, align 8
  %13 = call i32 @setvbuf(%struct._IO_FILE* %12, i8* null, i32 2, i64 0) #5
  %14 = load i8*** %3, align 8
  %15 = getelementptr inbounds i8** %14, i64 0
  %16 = load i8** %15, align 8
  store i8* %16, i8** @Argv0, align 8
  call void @main_init(%struct.ShellState* %data)
  %17 = call i32 @isatty(i32 0) #5
  store i32 %17, i32* @stdin_is_interactive, align 4
  %18 = call void (i32)* (i32, void (i32)*)* @signal(i32 2, void (i32)* @interrupt_handler) #5
  store i32 1, i32* %i, align 4
  br label %19

; <label>:19                                      ; preds = %292, %11
  %20 = load i32* %i, align 4
  %21 = load i32* %2, align 4
  %22 = icmp slt i32 %20, %21
  br i1 %22, label %23, label %295

; <label>:23                                      ; preds = %19
  %24 = load i32* %i, align 4
  %25 = sext i32 %24 to i64
  %26 = load i8*** %3, align 8
  %27 = getelementptr inbounds i8** %26, i64 %25
  %28 = load i8** %27, align 8
  store i8* %28, i8** %z, align 8
  %29 = load i8** %z, align 8
  %30 = getelementptr inbounds i8* %29, i64 0
  %31 = load i8* %30, align 1
  %32 = sext i8 %31 to i32
  %33 = icmp ne i32 %32, 45
  br i1 %33, label %34, label %64

; <label>:34                                      ; preds = %23
  %35 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 23
  %36 = load i8** %35, align 8
  %37 = icmp eq i8* %36, null
  br i1 %37, label %38, label %41

; <label>:38                                      ; preds = %34
  %39 = load i8** %z, align 8
  %40 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 23
  store i8* %39, i8** %40, align 8
  br label %63

; <label>:41                                      ; preds = %34
  store i32 0, i32* %readStdin, align 4
  %42 = load i32* %nCmd, align 4
  %43 = add nsw i32 %42, 1
  store i32 %43, i32* %nCmd, align 4
  %44 = load i8*** %azCmd, align 8
  %45 = bitcast i8** %44 to i8*
  %46 = load i32* %nCmd, align 4
  %47 = sext i32 %46 to i64
  %48 = mul i64 8, %47
  %49 = call i8* @realloc(i8* %45, i64 %48) #5
  %50 = bitcast i8* %49 to i8**
  store i8** %50, i8*** %azCmd, align 8
  %51 = load i8*** %azCmd, align 8
  %52 = icmp eq i8** %51, null
  br i1 %52, label %53, label %56

; <label>:53                                      ; preds = %41
  %54 = load %struct._IO_FILE** @stderr, align 8
  %55 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %54, i8* getelementptr inbounds ([15 x i8]* @.str2, i32 0, i32 0))
  call void @exit(i32 1) #8
  unreachable

; <label>:56                                      ; preds = %41
  %57 = load i8** %z, align 8
  %58 = load i32* %nCmd, align 4
  %59 = sub nsw i32 %58, 1
  %60 = sext i32 %59 to i64
  %61 = load i8*** %azCmd, align 8
  %62 = getelementptr inbounds i8** %61, i64 %60
  store i8* %57, i8** %62, align 8
  br label %63

; <label>:63                                      ; preds = %56, %38
  br label %64

; <label>:64                                      ; preds = %63, %23
  %65 = load i8** %z, align 8
  %66 = getelementptr inbounds i8* %65, i64 1
  %67 = load i8* %66, align 1
  %68 = sext i8 %67 to i32
  %69 = icmp eq i32 %68, 45
  br i1 %69, label %70, label %73

; <label>:70                                      ; preds = %64
  %71 = load i8** %z, align 8
  %72 = getelementptr inbounds i8* %71, i32 1
  store i8* %72, i8** %z, align 8
  br label %73

; <label>:73                                      ; preds = %70, %64
  %74 = load i8** %z, align 8
  %75 = call i32 @strcmp(i8* %74, i8* getelementptr inbounds ([11 x i8]* @.str3, i32 0, i32 0)) #7
  %76 = icmp eq i32 %75, 0
  br i1 %76, label %89, label %77

; <label>:77                                      ; preds = %73
  %78 = load i8** %z, align 8
  %79 = call i32 @strcmp(i8* %78, i8* getelementptr inbounds ([11 x i8]* @.str4, i32 0, i32 0)) #7
  %80 = icmp eq i32 %79, 0
  br i1 %80, label %89, label %81

; <label>:81                                      ; preds = %77
  %82 = load i8** %z, align 8
  %83 = call i32 @strcmp(i8* %82, i8* getelementptr inbounds ([9 x i8]* @.str5, i32 0, i32 0)) #7
  %84 = icmp eq i32 %83, 0
  br i1 %84, label %89, label %85

; <label>:85                                      ; preds = %81
  %86 = load i8** %z, align 8
  %87 = call i32 @strcmp(i8* %86, i8* getelementptr inbounds ([5 x i8]* @.str6, i32 0, i32 0)) #7
  %88 = icmp eq i32 %87, 0
  br i1 %88, label %89, label %95

; <label>:89                                      ; preds = %85, %81, %77, %73
  %90 = load i32* %2, align 4
  %91 = load i8*** %3, align 8
  %92 = load i32* %i, align 4
  %93 = add nsw i32 %92, 1
  store i32 %93, i32* %i, align 4
  %94 = call i8* @cmdline_option_value(i32 %90, i8** %91, i32 %93)
  br label %291

; <label>:95                                      ; preds = %85
  %96 = load i8** %z, align 8
  %97 = call i32 @strcmp(i8* %96, i8* getelementptr inbounds ([6 x i8]* @.str7, i32 0, i32 0)) #7
  %98 = icmp eq i32 %97, 0
  br i1 %98, label %99, label %105

; <label>:99                                      ; preds = %95
  %100 = load i32* %2, align 4
  %101 = load i8*** %3, align 8
  %102 = load i32* %i, align 4
  %103 = add nsw i32 %102, 1
  store i32 %103, i32* %i, align 4
  %104 = call i8* @cmdline_option_value(i32 %100, i8** %101, i32 %103)
  store i8* %104, i8** %zInitFile, align 8
  br label %290

; <label>:105                                     ; preds = %95
  %106 = load i8** %z, align 8
  %107 = call i32 @strcmp(i8* %106, i8* getelementptr inbounds ([7 x i8]* @.str8, i32 0, i32 0)) #7
  %108 = icmp eq i32 %107, 0
  br i1 %108, label %109, label %110

; <label>:109                                     ; preds = %105
  store i32 0, i32* @stdin_is_interactive, align 4
  br label %289

; <label>:110                                     ; preds = %105
  %111 = load i8** %z, align 8
  %112 = call i32 @strcmp(i8* %111, i8* getelementptr inbounds ([6 x i8]* @.str9, i32 0, i32 0)) #7
  %113 = icmp eq i32 %112, 0
  br i1 %113, label %114, label %115

; <label>:114                                     ; preds = %110
  br label %288

; <label>:115                                     ; preds = %110
  %116 = load i8** %z, align 8
  %117 = call i32 @strcmp(i8* %116, i8* getelementptr inbounds ([9 x i8]* @.str10, i32 0, i32 0)) #7
  %118 = icmp eq i32 %117, 0
  br i1 %118, label %119, label %162

; <label>:119                                     ; preds = %115
  %120 = load i32* %2, align 4
  %121 = load i8*** %3, align 8
  %122 = load i32* %i, align 4
  %123 = add nsw i32 %122, 1
  store i32 %123, i32* %i, align 4
  %124 = call i8* @cmdline_option_value(i32 %120, i8** %121, i32 %123)
  %125 = call i64 @integerValue(i8* %124)
  %126 = trunc i64 %125 to i32
  store i32 %126, i32* %sz, align 4
  %127 = load i32* %sz, align 4
  %128 = icmp sgt i32 %127, 400000
  br i1 %128, label %129, label %130

; <label>:129                                     ; preds = %119
  store i32 400000, i32* %sz, align 4
  br label %130

; <label>:130                                     ; preds = %129, %119
  %131 = load i32* %sz, align 4
  %132 = icmp slt i32 %131, 2500
  br i1 %132, label %133, label %134

; <label>:133                                     ; preds = %130
  store i32 2500, i32* %sz, align 4
  br label %134

; <label>:134                                     ; preds = %133, %130
  %135 = load i32* %2, align 4
  %136 = load i8*** %3, align 8
  %137 = load i32* %i, align 4
  %138 = add nsw i32 %137, 1
  store i32 %138, i32* %i, align 4
  %139 = call i8* @cmdline_option_value(i32 %135, i8** %136, i32 %138)
  %140 = call i64 @integerValue(i8* %139)
  %141 = trunc i64 %140 to i32
  store i32 %141, i32* %n, align 4
  %142 = load i32* %n, align 4
  %143 = icmp sgt i32 %142, 10
  br i1 %143, label %144, label %145

; <label>:144                                     ; preds = %134
  store i32 10, i32* %n, align 4
  br label %145

; <label>:145                                     ; preds = %144, %134
  %146 = load i32* %n, align 4
  %147 = icmp slt i32 %146, 1
  br i1 %147, label %148, label %149

; <label>:148                                     ; preds = %145
  store i32 1, i32* %n, align 4
  br label %149

; <label>:149                                     ; preds = %148, %145
  %150 = load i32* %n, align 4
  %151 = load i32* %sz, align 4
  %152 = mul nsw i32 %150, %151
  %153 = add nsw i32 %152, 1
  %154 = sext i32 %153 to i64
  %155 = call noalias i8* @malloc(i64 %154) #5
  %156 = load i32* %sz, align 4
  %157 = load i32* %n, align 4
  %158 = call i32 (i32, ...)* @sqlite3_config(i32 6, i8* %155, i32 %156, i32 %157)
  %159 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 14
  %160 = load i32* %159, align 4
  %161 = or i32 %160, 1
  store i32 %161, i32* %159, align 4
  br label %287

; <label>:162                                     ; preds = %115
  %163 = load i8** %z, align 8
  %164 = call i32 @strcmp(i8* %163, i8* getelementptr inbounds ([11 x i8]* @.str11, i32 0, i32 0)) #7
  %165 = icmp eq i32 %164, 0
  br i1 %165, label %166, label %205

; <label>:166                                     ; preds = %162
  %167 = load i32* %2, align 4
  %168 = load i8*** %3, align 8
  %169 = load i32* %i, align 4
  %170 = add nsw i32 %169, 1
  store i32 %170, i32* %i, align 4
  %171 = call i8* @cmdline_option_value(i32 %167, i8** %168, i32 %170)
  %172 = call i64 @integerValue(i8* %171)
  %173 = trunc i64 %172 to i32
  store i32 %173, i32* %sz2, align 4
  %174 = load i32* %sz2, align 4
  %175 = icmp sgt i32 %174, 70000
  br i1 %175, label %176, label %177

; <label>:176                                     ; preds = %166
  store i32 70000, i32* %sz2, align 4
  br label %177

; <label>:177                                     ; preds = %176, %166
  %178 = load i32* %sz2, align 4
  %179 = icmp slt i32 %178, 800
  br i1 %179, label %180, label %181

; <label>:180                                     ; preds = %177
  store i32 800, i32* %sz2, align 4
  br label %181

; <label>:181                                     ; preds = %180, %177
  %182 = load i32* %2, align 4
  %183 = load i8*** %3, align 8
  %184 = load i32* %i, align 4
  %185 = add nsw i32 %184, 1
  store i32 %185, i32* %i, align 4
  %186 = call i8* @cmdline_option_value(i32 %182, i8** %183, i32 %185)
  %187 = call i64 @integerValue(i8* %186)
  %188 = trunc i64 %187 to i32
  store i32 %188, i32* %n1, align 4
  %189 = load i32* %n1, align 4
  %190 = icmp slt i32 %189, 10
  br i1 %190, label %191, label %192

; <label>:191                                     ; preds = %181
  store i32 10, i32* %n1, align 4
  br label %192

; <label>:192                                     ; preds = %191, %181
  %193 = load i32* %n1, align 4
  %194 = load i32* %sz2, align 4
  %195 = mul nsw i32 %193, %194
  %196 = add nsw i32 %195, 1
  %197 = sext i32 %196 to i64
  %198 = call noalias i8* @malloc(i64 %197) #5
  %199 = load i32* %sz2, align 4
  %200 = load i32* %n1, align 4
  %201 = call i32 (i32, ...)* @sqlite3_config(i32 7, i8* %198, i32 %199, i32 %200)
  %202 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 14
  %203 = load i32* %202, align 4
  %204 = or i32 %203, 2
  store i32 %204, i32* %202, align 4
  br label %286

; <label>:205                                     ; preds = %162
  %206 = load i8** %z, align 8
  %207 = call i32 @strcmp(i8* %206, i8* getelementptr inbounds ([11 x i8]* @.str12, i32 0, i32 0)) #7
  %208 = icmp eq i32 %207, 0
  br i1 %208, label %209, label %244

; <label>:209                                     ; preds = %205
  %210 = load i32* %2, align 4
  %211 = load i8*** %3, align 8
  %212 = load i32* %i, align 4
  %213 = add nsw i32 %212, 1
  store i32 %213, i32* %i, align 4
  %214 = call i8* @cmdline_option_value(i32 %210, i8** %211, i32 %213)
  %215 = call i64 @integerValue(i8* %214)
  %216 = trunc i64 %215 to i32
  store i32 %216, i32* %sz4, align 4
  %217 = load i32* %sz4, align 4
  %218 = icmp slt i32 %217, 0
  br i1 %218, label %219, label %220

; <label>:219                                     ; preds = %209
  store i32 0, i32* %sz4, align 4
  br label %220

; <label>:220                                     ; preds = %219, %209
  %221 = load i32* %2, align 4
  %222 = load i8*** %3, align 8
  %223 = load i32* %i, align 4
  %224 = add nsw i32 %223, 1
  store i32 %224, i32* %i, align 4
  %225 = call i8* @cmdline_option_value(i32 %221, i8** %222, i32 %224)
  %226 = call i64 @integerValue(i8* %225)
  %227 = trunc i64 %226 to i32
  store i32 %227, i32* %n3, align 4
  %228 = load i32* %n3, align 4
  %229 = icmp slt i32 %228, 0
  br i1 %229, label %230, label %231

; <label>:230                                     ; preds = %220
  store i32 0, i32* %n3, align 4
  br label %231

; <label>:231                                     ; preds = %230, %220
  %232 = load i32* %sz4, align 4
  %233 = load i32* %n3, align 4
  %234 = call i32 (i32, ...)* @sqlite3_config(i32 13, i32 %232, i32 %233)
  %235 = load i32* %sz4, align 4
  %236 = load i32* %n3, align 4
  %237 = mul nsw i32 %235, %236
  %238 = icmp eq i32 %237, 0
  br i1 %238, label %239, label %243

; <label>:239                                     ; preds = %231
  %240 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 14
  %241 = load i32* %240, align 4
  %242 = and i32 %241, -5
  store i32 %242, i32* %240, align 4
  br label %243

; <label>:243                                     ; preds = %239, %231
  br label %285

; <label>:244                                     ; preds = %205
  %245 = load i8** %z, align 8
  %246 = call i32 @strcmp(i8* %245, i8* getelementptr inbounds ([6 x i8]* @.str13, i32 0, i32 0)) #7
  %247 = icmp eq i32 %246, 0
  br i1 %247, label %248, label %258

; <label>:248                                     ; preds = %244
  %249 = load i32* %2, align 4
  %250 = load i8*** %3, align 8
  %251 = load i32* %i, align 4
  %252 = add nsw i32 %251, 1
  store i32 %252, i32* %i, align 4
  %253 = call i8* @cmdline_option_value(i32 %249, i8** %250, i32 %252)
  %254 = call i64 @integerValue(i8* %253)
  store i64 %254, i64* %sz5, align 8
  %255 = load i64* %sz5, align 8
  %256 = load i64* %sz5, align 8
  %257 = call i32 (i32, ...)* @sqlite3_config(i32 22, i64 %255, i64 %256)
  br label %284

; <label>:258                                     ; preds = %244
  %259 = load i8** %z, align 8
  %260 = call i32 @strcmp(i8* %259, i8* getelementptr inbounds ([5 x i8]* @.str14, i32 0, i32 0)) #7
  %261 = icmp eq i32 %260, 0
  br i1 %261, label %262, label %283

; <label>:262                                     ; preds = %258
  %263 = load i32* %2, align 4
  %264 = load i8*** %3, align 8
  %265 = load i32* %i, align 4
  %266 = add nsw i32 %265, 1
  store i32 %266, i32* %i, align 4
  %267 = call i8* @cmdline_option_value(i32 %263, i8** %264, i32 %266)
  %268 = call %struct.sqlite3_vfs* @sqlite3_vfs_find(i8* %267)
  store %struct.sqlite3_vfs* %268, %struct.sqlite3_vfs** %pVfs, align 8
  %269 = load %struct.sqlite3_vfs** %pVfs, align 8
  %270 = icmp ne %struct.sqlite3_vfs* %269, null
  br i1 %270, label %271, label %274

; <label>:271                                     ; preds = %262
  %272 = load %struct.sqlite3_vfs** %pVfs, align 8
  %273 = call i32 @sqlite3_vfs_register(%struct.sqlite3_vfs* %272, i32 1)
  br label %282

; <label>:274                                     ; preds = %262
  %275 = load %struct._IO_FILE** @stderr, align 8
  %276 = load i32* %i, align 4
  %277 = sext i32 %276 to i64
  %278 = load i8*** %3, align 8
  %279 = getelementptr inbounds i8** %278, i64 %277
  %280 = load i8** %279, align 8
  %281 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %275, i8* getelementptr inbounds ([19 x i8]* @.str15, i32 0, i32 0), i8* %280)
  call void @exit(i32 1) #8
  unreachable

; <label>:282                                     ; preds = %271
  br label %283

; <label>:283                                     ; preds = %282, %258
  br label %284

; <label>:284                                     ; preds = %283, %248
  br label %285

; <label>:285                                     ; preds = %284, %243
  br label %286

; <label>:286                                     ; preds = %285, %192
  br label %287

; <label>:287                                     ; preds = %286, %149
  br label %288

; <label>:288                                     ; preds = %287, %114
  br label %289

; <label>:289                                     ; preds = %288, %109
  br label %290

; <label>:290                                     ; preds = %289, %99
  br label %291

; <label>:291                                     ; preds = %290, %89
  br label %292

; <label>:292                                     ; preds = %291
  %293 = load i32* %i, align 4
  %294 = add nsw i32 %293, 1
  store i32 %294, i32* %i, align 4
  br label %19

; <label>:295                                     ; preds = %19
  %296 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 23
  %297 = load i8** %296, align 8
  %298 = icmp eq i8* %297, null
  br i1 %298, label %299, label %304

; <label>:299                                     ; preds = %295
  %300 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 23
  store i8* getelementptr inbounds ([9 x i8]* @.str16, i32 0, i32 0), i8** %300, align 8
  %301 = load i32* %2, align 4
  %302 = icmp eq i32 %301, 1
  %303 = zext i1 %302 to i32
  store i32 %303, i32* %warnInmemoryDb, align 4
  br label %304

; <label>:304                                     ; preds = %299, %295
  %305 = load %struct._IO_FILE** @stdout, align 8
  %306 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 8
  store %struct._IO_FILE* %305, %struct._IO_FILE** %306, align 8
  %307 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 23
  %308 = load i8** %307, align 8
  %309 = call i32 @access(i8* %308, i32 0) #5
  %310 = icmp eq i32 %309, 0
  br i1 %310, label %311, label %312

; <label>:311                                     ; preds = %304
  call void @open_db(%struct.ShellState* %data, i32 0)
  br label %312

; <label>:312                                     ; preds = %311, %304
  %313 = load i8** %zInitFile, align 8
  call void @process_sqliterc(%struct.ShellState* %data, i8* %313)
  store i32 1, i32* %i, align 4
  br label %314

; <label>:314                                     ; preds = %652, %312
  %315 = load i32* %i, align 4
  %316 = load i32* %2, align 4
  %317 = icmp slt i32 %315, %316
  br i1 %317, label %318, label %655

; <label>:318                                     ; preds = %314
  %319 = load i32* %i, align 4
  %320 = sext i32 %319 to i64
  %321 = load i8*** %3, align 8
  %322 = getelementptr inbounds i8** %321, i64 %320
  %323 = load i8** %322, align 8
  store i8* %323, i8** %z6, align 8
  %324 = load i8** %z6, align 8
  %325 = getelementptr inbounds i8* %324, i64 0
  %326 = load i8* %325, align 1
  %327 = sext i8 %326 to i32
  %328 = icmp ne i32 %327, 45
  br i1 %328, label %329, label %330

; <label>:329                                     ; preds = %318
  br label %652

; <label>:330                                     ; preds = %318
  %331 = load i8** %z6, align 8
  %332 = getelementptr inbounds i8* %331, i64 1
  %333 = load i8* %332, align 1
  %334 = sext i8 %333 to i32
  %335 = icmp eq i32 %334, 45
  br i1 %335, label %336, label %339

; <label>:336                                     ; preds = %330
  %337 = load i8** %z6, align 8
  %338 = getelementptr inbounds i8* %337, i32 1
  store i8* %338, i8** %z6, align 8
  br label %339

; <label>:339                                     ; preds = %336, %330
  %340 = load i8** %z6, align 8
  %341 = call i32 @strcmp(i8* %340, i8* getelementptr inbounds ([6 x i8]* @.str7, i32 0, i32 0)) #7
  %342 = icmp eq i32 %341, 0
  br i1 %342, label %343, label %346

; <label>:343                                     ; preds = %339
  %344 = load i32* %i, align 4
  %345 = add nsw i32 %344, 1
  store i32 %345, i32* %i, align 4
  br label %651

; <label>:346                                     ; preds = %339
  %347 = load i8** %z6, align 8
  %348 = call i32 @strcmp(i8* %347, i8* getelementptr inbounds ([6 x i8]* @.str17, i32 0, i32 0)) #7
  %349 = icmp eq i32 %348, 0
  br i1 %349, label %350, label %352

; <label>:350                                     ; preds = %346
  %351 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 11
  store i32 4, i32* %351, align 4
  br label %650

; <label>:352                                     ; preds = %346
  %353 = load i8** %z6, align 8
  %354 = call i32 @strcmp(i8* %353, i8* getelementptr inbounds ([6 x i8]* @.str18, i32 0, i32 0)) #7
  %355 = icmp eq i32 %354, 0
  br i1 %355, label %356, label %358

; <label>:356                                     ; preds = %352
  %357 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 11
  store i32 2, i32* %357, align 4
  br label %649

; <label>:358                                     ; preds = %352
  %359 = load i8** %z6, align 8
  %360 = call i32 @strcmp(i8* %359, i8* getelementptr inbounds ([6 x i8]* @.str19, i32 0, i32 0)) #7
  %361 = icmp eq i32 %360, 0
  br i1 %361, label %362, label %364

; <label>:362                                     ; preds = %358
  %363 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 11
  store i32 0, i32* %363, align 4
  br label %648

; <label>:364                                     ; preds = %358
  %365 = load i8** %z6, align 8
  %366 = call i32 @strcmp(i8* %365, i8* getelementptr inbounds ([8 x i8]* @.str20, i32 0, i32 0)) #7
  %367 = icmp eq i32 %366, 0
  br i1 %367, label %368, label %370

; <label>:368                                     ; preds = %364
  %369 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 11
  store i32 1, i32* %369, align 4
  br label %647

; <label>:370                                     ; preds = %364
  %371 = load i8** %z6, align 8
  %372 = call i32 @strcmp(i8* %371, i8* getelementptr inbounds ([5 x i8]* @.str21, i32 0, i32 0)) #7
  %373 = icmp eq i32 %372, 0
  br i1 %373, label %374, label %378

; <label>:374                                     ; preds = %370
  %375 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 11
  store i32 7, i32* %375, align 4
  %376 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 16
  %377 = bitcast [20 x i8]* %376 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %377, i8* getelementptr inbounds ([2 x i8]* @.str22, i32 0, i32 0), i64 2, i32 1, i1 false)
  br label %646

; <label>:378                                     ; preds = %370
  %379 = load i8** %z6, align 8
  %380 = call i32 @strcmp(i8* %379, i8* getelementptr inbounds ([7 x i8]* @.str23, i32 0, i32 0)) #7
  %381 = icmp eq i32 %380, 0
  br i1 %381, label %382, label %390

; <label>:382                                     ; preds = %378
  %383 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 11
  store i32 9, i32* %383, align 4
  %384 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 16
  %385 = getelementptr inbounds [20 x i8]* %384, i32 0, i32 0
  %386 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %385, i8* getelementptr inbounds ([2 x i8]* @.str24, i32 0, i32 0))
  %387 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 17
  %388 = getelementptr inbounds [20 x i8]* %387, i32 0, i32 0
  %389 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %388, i8* getelementptr inbounds ([2 x i8]* @.str25, i32 0, i32 0))
  br label %645

; <label>:390                                     ; preds = %378
  %391 = load i8** %z6, align 8
  %392 = call i32 @strcmp(i8* %391, i8* getelementptr inbounds ([11 x i8]* @.str3, i32 0, i32 0)) #7
  %393 = icmp eq i32 %392, 0
  br i1 %393, label %394, label %403

; <label>:394                                     ; preds = %390
  %395 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 16
  %396 = getelementptr inbounds [20 x i8]* %395, i32 0, i32 0
  %397 = load i32* %2, align 4
  %398 = load i8*** %3, align 8
  %399 = load i32* %i, align 4
  %400 = add nsw i32 %399, 1
  store i32 %400, i32* %i, align 4
  %401 = call i8* @cmdline_option_value(i32 %397, i8** %398, i32 %400)
  %402 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %396, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %401)
  br label %644

; <label>:403                                     ; preds = %390
  %404 = load i8** %z6, align 8
  %405 = call i32 @strcmp(i8* %404, i8* getelementptr inbounds ([9 x i8]* @.str5, i32 0, i32 0)) #7
  %406 = icmp eq i32 %405, 0
  br i1 %406, label %407, label %416

; <label>:407                                     ; preds = %403
  %408 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 17
  %409 = getelementptr inbounds [20 x i8]* %408, i32 0, i32 0
  %410 = load i32* %2, align 4
  %411 = load i8*** %3, align 8
  %412 = load i32* %i, align 4
  %413 = add nsw i32 %412, 1
  store i32 %413, i32* %i, align 4
  %414 = call i8* @cmdline_option_value(i32 %410, i8** %411, i32 %413)
  %415 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %409, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %414)
  br label %643

; <label>:416                                     ; preds = %403
  %417 = load i8** %z6, align 8
  %418 = call i32 @strcmp(i8* %417, i8* getelementptr inbounds ([11 x i8]* @.str4, i32 0, i32 0)) #7
  %419 = icmp eq i32 %418, 0
  br i1 %419, label %420, label %429

; <label>:420                                     ; preds = %416
  %421 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 20
  %422 = getelementptr inbounds [20 x i8]* %421, i32 0, i32 0
  %423 = load i32* %2, align 4
  %424 = load i8*** %3, align 8
  %425 = load i32* %i, align 4
  %426 = add nsw i32 %425, 1
  store i32 %426, i32* %i, align 4
  %427 = call i8* @cmdline_option_value(i32 %423, i8** %424, i32 %426)
  %428 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %422, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %427)
  br label %642

; <label>:429                                     ; preds = %416
  %430 = load i8** %z6, align 8
  %431 = call i32 @strcmp(i8* %430, i8* getelementptr inbounds ([8 x i8]* @.str27, i32 0, i32 0)) #7
  %432 = icmp eq i32 %431, 0
  br i1 %432, label %433, label %435

; <label>:433                                     ; preds = %429
  %434 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 13
  store i32 1, i32* %434, align 4
  br label %641

; <label>:435                                     ; preds = %429
  %436 = load i8** %z6, align 8
  %437 = call i32 @strcmp(i8* %436, i8* getelementptr inbounds ([10 x i8]* @.str28, i32 0, i32 0)) #7
  %438 = icmp eq i32 %437, 0
  br i1 %438, label %439, label %441

; <label>:439                                     ; preds = %435
  %440 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 13
  store i32 0, i32* %440, align 4
  br label %640

; <label>:441                                     ; preds = %435
  %442 = load i8** %z6, align 8
  %443 = call i32 @strcmp(i8* %442, i8* getelementptr inbounds ([6 x i8]* @.str29, i32 0, i32 0)) #7
  %444 = icmp eq i32 %443, 0
  br i1 %444, label %445, label %447

; <label>:445                                     ; preds = %441
  %446 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 1
  store i32 1, i32* %446, align 4
  br label %639

; <label>:447                                     ; preds = %441
  %448 = load i8** %z6, align 8
  %449 = call i32 @strcmp(i8* %448, i8* getelementptr inbounds ([5 x i8]* @.str30, i32 0, i32 0)) #7
  %450 = icmp eq i32 %449, 0
  br i1 %450, label %451, label %453

; <label>:451                                     ; preds = %447
  %452 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 2
  store i32 1, i32* %452, align 4
  br label %638

; <label>:453                                     ; preds = %447
  %454 = load i8** %z6, align 8
  %455 = call i32 @strcmp(i8* %454, i8* getelementptr inbounds ([7 x i8]* @.str31, i32 0, i32 0)) #7
  %456 = icmp eq i32 %455, 0
  br i1 %456, label %457, label %459

; <label>:457                                     ; preds = %453
  %458 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 3
  store i32 1, i32* %458, align 4
  br label %637

; <label>:459                                     ; preds = %453
  %460 = load i8** %z6, align 8
  %461 = call i32 @strcmp(i8* %460, i8* getelementptr inbounds ([11 x i8]* @.str32, i32 0, i32 0)) #7
  %462 = icmp eq i32 %461, 0
  br i1 %462, label %463, label %465

; <label>:463                                     ; preds = %459
  %464 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 4
  store i32 1, i32* %464, align 4
  br label %636

; <label>:465                                     ; preds = %459
  %466 = load i8** %z6, align 8
  %467 = call i32 @strcmp(i8* %466, i8* getelementptr inbounds ([11 x i8]* @.str33, i32 0, i32 0)) #7
  %468 = icmp eq i32 %467, 0
  br i1 %468, label %469, label %471

; <label>:469                                     ; preds = %465
  %470 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 5
  store i32 1, i32* %470, align 4
  br label %635

; <label>:471                                     ; preds = %465
  %472 = load i8** %z6, align 8
  %473 = call i32 @strcmp(i8* %472, i8* getelementptr inbounds ([6 x i8]* @.str34, i32 0, i32 0)) #7
  %474 = icmp eq i32 %473, 0
  br i1 %474, label %475, label %476

; <label>:475                                     ; preds = %471
  store i32 1, i32* @bail_on_error, align 4
  br label %634

; <label>:476                                     ; preds = %471
  %477 = load i8** %z6, align 8
  %478 = call i32 @strcmp(i8* %477, i8* getelementptr inbounds ([9 x i8]* @.str35, i32 0, i32 0)) #7
  %479 = icmp eq i32 %478, 0
  br i1 %479, label %480, label %484

; <label>:480                                     ; preds = %476
  %481 = call i8* @sqlite3_libversion()
  %482 = call i8* @sqlite3_sourceid()
  %483 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([7 x i8]* @.str36, i32 0, i32 0), i8* %481, i8* %482)
  store i32 0, i32* %1
  br label %791

; <label>:484                                     ; preds = %476
  %485 = load i8** %z6, align 8
  %486 = call i32 @strcmp(i8* %485, i8* getelementptr inbounds ([13 x i8]* @.str37, i32 0, i32 0)) #7
  %487 = icmp eq i32 %486, 0
  br i1 %487, label %488, label %489

; <label>:488                                     ; preds = %484
  store i32 1, i32* @stdin_is_interactive, align 4
  br label %632

; <label>:489                                     ; preds = %484
  %490 = load i8** %z6, align 8
  %491 = call i32 @strcmp(i8* %490, i8* getelementptr inbounds ([7 x i8]* @.str8, i32 0, i32 0)) #7
  %492 = icmp eq i32 %491, 0
  br i1 %492, label %493, label %494

; <label>:493                                     ; preds = %489
  store i32 0, i32* @stdin_is_interactive, align 4
  br label %631

; <label>:494                                     ; preds = %489
  %495 = load i8** %z6, align 8
  %496 = call i32 @strcmp(i8* %495, i8* getelementptr inbounds ([6 x i8]* @.str9, i32 0, i32 0)) #7
  %497 = icmp eq i32 %496, 0
  br i1 %497, label %498, label %501

; <label>:498                                     ; preds = %494
  %499 = load i32* %i, align 4
  %500 = add nsw i32 %499, 1
  store i32 %500, i32* %i, align 4
  br label %630

; <label>:501                                     ; preds = %494
  %502 = load i8** %z6, align 8
  %503 = call i32 @strcmp(i8* %502, i8* getelementptr inbounds ([9 x i8]* @.str10, i32 0, i32 0)) #7
  %504 = icmp eq i32 %503, 0
  br i1 %504, label %505, label %508

; <label>:505                                     ; preds = %501
  %506 = load i32* %i, align 4
  %507 = add nsw i32 %506, 2
  store i32 %507, i32* %i, align 4
  br label %629

; <label>:508                                     ; preds = %501
  %509 = load i8** %z6, align 8
  %510 = call i32 @strcmp(i8* %509, i8* getelementptr inbounds ([11 x i8]* @.str11, i32 0, i32 0)) #7
  %511 = icmp eq i32 %510, 0
  br i1 %511, label %512, label %515

; <label>:512                                     ; preds = %508
  %513 = load i32* %i, align 4
  %514 = add nsw i32 %513, 2
  store i32 %514, i32* %i, align 4
  br label %628

; <label>:515                                     ; preds = %508
  %516 = load i8** %z6, align 8
  %517 = call i32 @strcmp(i8* %516, i8* getelementptr inbounds ([11 x i8]* @.str12, i32 0, i32 0)) #7
  %518 = icmp eq i32 %517, 0
  br i1 %518, label %519, label %522

; <label>:519                                     ; preds = %515
  %520 = load i32* %i, align 4
  %521 = add nsw i32 %520, 2
  store i32 %521, i32* %i, align 4
  br label %627

; <label>:522                                     ; preds = %515
  %523 = load i8** %z6, align 8
  %524 = call i32 @strcmp(i8* %523, i8* getelementptr inbounds ([6 x i8]* @.str13, i32 0, i32 0)) #7
  %525 = icmp eq i32 %524, 0
  br i1 %525, label %526, label %529

; <label>:526                                     ; preds = %522
  %527 = load i32* %i, align 4
  %528 = add nsw i32 %527, 1
  store i32 %528, i32* %i, align 4
  br label %626

; <label>:529                                     ; preds = %522
  %530 = load i8** %z6, align 8
  %531 = call i32 @strcmp(i8* %530, i8* getelementptr inbounds ([5 x i8]* @.str14, i32 0, i32 0)) #7
  %532 = icmp eq i32 %531, 0
  br i1 %532, label %533, label %536

; <label>:533                                     ; preds = %529
  %534 = load i32* %i, align 4
  %535 = add nsw i32 %534, 1
  store i32 %535, i32* %i, align 4
  br label %625

; <label>:536                                     ; preds = %529
  %537 = load i8** %z6, align 8
  %538 = call i32 @strcmp(i8* %537, i8* getelementptr inbounds ([6 x i8]* @.str38, i32 0, i32 0)) #7
  %539 = icmp eq i32 %538, 0
  br i1 %539, label %540, label %541

; <label>:540                                     ; preds = %536
  call void @usage(i32 1)
  br label %624

; <label>:541                                     ; preds = %536
  %542 = load i8** %z6, align 8
  %543 = call i32 @strcmp(i8* %542, i8* getelementptr inbounds ([5 x i8]* @.str6, i32 0, i32 0)) #7
  %544 = icmp eq i32 %543, 0
  br i1 %544, label %545, label %616

; <label>:545                                     ; preds = %541
  %546 = load i32* %i, align 4
  %547 = load i32* %2, align 4
  %548 = sub nsw i32 %547, 1
  %549 = icmp eq i32 %546, %548
  br i1 %549, label %550, label %551

; <label>:550                                     ; preds = %545
  br label %655

; <label>:551                                     ; preds = %545
  %552 = load i32* %2, align 4
  %553 = load i8*** %3, align 8
  %554 = load i32* %i, align 4
  %555 = add nsw i32 %554, 1
  store i32 %555, i32* %i, align 4
  %556 = call i8* @cmdline_option_value(i32 %552, i8** %553, i32 %555)
  store i8* %556, i8** %z6, align 8
  %557 = load i8** %z6, align 8
  %558 = getelementptr inbounds i8* %557, i64 0
  %559 = load i8* %558, align 1
  %560 = sext i8 %559 to i32
  %561 = icmp eq i32 %560, 46
  br i1 %561, label %562, label %579

; <label>:562                                     ; preds = %551
  %563 = load i8** %z6, align 8
  %564 = call i32 @do_meta_command(i8* %563, %struct.ShellState* %data)
  store i32 %564, i32* %rc, align 4
  %565 = load i32* %rc, align 4
  %566 = icmp ne i32 %565, 0
  br i1 %566, label %567, label %578

; <label>:567                                     ; preds = %562
  %568 = load i32* @bail_on_error, align 4
  %569 = icmp ne i32 %568, 0
  br i1 %569, label %570, label %578

; <label>:570                                     ; preds = %567
  %571 = load i32* %rc, align 4
  %572 = icmp eq i32 %571, 2
  br i1 %572, label %573, label %574

; <label>:573                                     ; preds = %570
  br label %576

; <label>:574                                     ; preds = %570
  %575 = load i32* %rc, align 4
  br label %576

; <label>:576                                     ; preds = %574, %573
  %577 = phi i32 [ 0, %573 ], [ %575, %574 ]
  store i32 %577, i32* %1
  br label %791

; <label>:578                                     ; preds = %567, %562
  br label %615

; <label>:579                                     ; preds = %551
  call void @open_db(%struct.ShellState* %data, i32 0)
  %580 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 0
  %581 = load %struct.sqlite3** %580, align 8
  %582 = load i8** %z6, align 8
  %583 = call i32 @shell_exec(%struct.sqlite3* %581, i8* %582, i32 (i8*, i32, i8**, i8**, i32*)* @shell_callback, %struct.ShellState* %data, i8** %zErrMsg)
  store i32 %583, i32* %rc, align 4
  %584 = load i8** %zErrMsg, align 8
  %585 = icmp ne i8* %584, null
  br i1 %585, label %586, label %601

; <label>:586                                     ; preds = %579
  %587 = load %struct._IO_FILE** @stderr, align 8
  %588 = load i8** %zErrMsg, align 8
  %589 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %587, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %588)
  %590 = load i32* @bail_on_error, align 4
  %591 = icmp ne i32 %590, 0
  br i1 %591, label %592, label %600

; <label>:592                                     ; preds = %586
  %593 = load i32* %rc, align 4
  %594 = icmp ne i32 %593, 0
  br i1 %594, label %595, label %597

; <label>:595                                     ; preds = %592
  %596 = load i32* %rc, align 4
  br label %598

; <label>:597                                     ; preds = %592
  br label %598

; <label>:598                                     ; preds = %597, %595
  %599 = phi i32 [ %596, %595 ], [ 1, %597 ]
  store i32 %599, i32* %1
  br label %791

; <label>:600                                     ; preds = %586
  br label %614

; <label>:601                                     ; preds = %579
  %602 = load i32* %rc, align 4
  %603 = icmp ne i32 %602, 0
  br i1 %603, label %604, label %613

; <label>:604                                     ; preds = %601
  %605 = load %struct._IO_FILE** @stderr, align 8
  %606 = load i8** %z6, align 8
  %607 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %605, i8* getelementptr inbounds ([35 x i8]* @.str40, i32 0, i32 0), i8* %606)
  %608 = load i32* @bail_on_error, align 4
  %609 = icmp ne i32 %608, 0
  br i1 %609, label %610, label %612

; <label>:610                                     ; preds = %604
  %611 = load i32* %rc, align 4
  store i32 %611, i32* %1
  br label %791

; <label>:612                                     ; preds = %604
  br label %613

; <label>:613                                     ; preds = %612, %601
  br label %614

; <label>:614                                     ; preds = %613, %600
  br label %615

; <label>:615                                     ; preds = %614, %578
  br label %623

; <label>:616                                     ; preds = %541
  %617 = load %struct._IO_FILE** @stderr, align 8
  %618 = load i8** @Argv0, align 8
  %619 = load i8** %z6, align 8
  %620 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %617, i8* getelementptr inbounds ([31 x i8]* @.str41, i32 0, i32 0), i8* %618, i8* %619)
  %621 = load %struct._IO_FILE** @stderr, align 8
  %622 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %621, i8* getelementptr inbounds ([34 x i8]* @.str42, i32 0, i32 0))
  store i32 1, i32* %1
  br label %791

; <label>:623                                     ; preds = %615
  br label %624

; <label>:624                                     ; preds = %623, %540
  br label %625

; <label>:625                                     ; preds = %624, %533
  br label %626

; <label>:626                                     ; preds = %625, %526
  br label %627

; <label>:627                                     ; preds = %626, %519
  br label %628

; <label>:628                                     ; preds = %627, %512
  br label %629

; <label>:629                                     ; preds = %628, %505
  br label %630

; <label>:630                                     ; preds = %629, %498
  br label %631

; <label>:631                                     ; preds = %630, %493
  br label %632

; <label>:632                                     ; preds = %631, %488
  br label %633

; <label>:633                                     ; preds = %632
  br label %634

; <label>:634                                     ; preds = %633, %475
  br label %635

; <label>:635                                     ; preds = %634, %469
  br label %636

; <label>:636                                     ; preds = %635, %463
  br label %637

; <label>:637                                     ; preds = %636, %457
  br label %638

; <label>:638                                     ; preds = %637, %451
  br label %639

; <label>:639                                     ; preds = %638, %445
  br label %640

; <label>:640                                     ; preds = %639, %439
  br label %641

; <label>:641                                     ; preds = %640, %433
  br label %642

; <label>:642                                     ; preds = %641, %420
  br label %643

; <label>:643                                     ; preds = %642, %407
  br label %644

; <label>:644                                     ; preds = %643, %394
  br label %645

; <label>:645                                     ; preds = %644, %382
  br label %646

; <label>:646                                     ; preds = %645, %374
  br label %647

; <label>:647                                     ; preds = %646, %368
  br label %648

; <label>:648                                     ; preds = %647, %362
  br label %649

; <label>:649                                     ; preds = %648, %356
  br label %650

; <label>:650                                     ; preds = %649, %350
  br label %651

; <label>:651                                     ; preds = %650, %343
  br label %652

; <label>:652                                     ; preds = %651, %329
  %653 = load i32* %i, align 4
  %654 = add nsw i32 %653, 1
  store i32 %654, i32* %i, align 4
  br label %314

; <label>:655                                     ; preds = %550, %314
  %656 = load i32* %readStdin, align 4
  %657 = icmp ne i32 %656, 0
  br i1 %657, label %734, label %658

; <label>:658                                     ; preds = %655
  store i32 0, i32* %i, align 4
  br label %659

; <label>:659                                     ; preds = %728, %658
  %660 = load i32* %i, align 4
  %661 = load i32* %nCmd, align 4
  %662 = icmp slt i32 %660, %661
  br i1 %662, label %663, label %731

; <label>:663                                     ; preds = %659
  %664 = load i32* %i, align 4
  %665 = sext i32 %664 to i64
  %666 = load i8*** %azCmd, align 8
  %667 = getelementptr inbounds i8** %666, i64 %665
  %668 = load i8** %667, align 8
  %669 = getelementptr inbounds i8* %668, i64 0
  %670 = load i8* %669, align 1
  %671 = sext i8 %670 to i32
  %672 = icmp eq i32 %671, 46
  br i1 %672, label %673, label %691

; <label>:673                                     ; preds = %663
  %674 = load i32* %i, align 4
  %675 = sext i32 %674 to i64
  %676 = load i8*** %azCmd, align 8
  %677 = getelementptr inbounds i8** %676, i64 %675
  %678 = load i8** %677, align 8
  %679 = call i32 @do_meta_command(i8* %678, %struct.ShellState* %data)
  store i32 %679, i32* %rc, align 4
  %680 = load i32* %rc, align 4
  %681 = icmp ne i32 %680, 0
  br i1 %681, label %682, label %690

; <label>:682                                     ; preds = %673
  %683 = load i32* %rc, align 4
  %684 = icmp eq i32 %683, 2
  br i1 %684, label %685, label %686

; <label>:685                                     ; preds = %682
  br label %688

; <label>:686                                     ; preds = %682
  %687 = load i32* %rc, align 4
  br label %688

; <label>:688                                     ; preds = %686, %685
  %689 = phi i32 [ 0, %685 ], [ %687, %686 ]
  store i32 %689, i32* %1
  br label %791

; <label>:690                                     ; preds = %673
  br label %727

; <label>:691                                     ; preds = %663
  call void @open_db(%struct.ShellState* %data, i32 0)
  %692 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 0
  %693 = load %struct.sqlite3** %692, align 8
  %694 = load i32* %i, align 4
  %695 = sext i32 %694 to i64
  %696 = load i8*** %azCmd, align 8
  %697 = getelementptr inbounds i8** %696, i64 %695
  %698 = load i8** %697, align 8
  %699 = call i32 @shell_exec(%struct.sqlite3* %693, i8* %698, i32 (i8*, i32, i8**, i8**, i32*)* @shell_callback, %struct.ShellState* %data, i8** %zErrMsg)
  store i32 %699, i32* %rc, align 4
  %700 = load i8** %zErrMsg, align 8
  %701 = icmp ne i8* %700, null
  br i1 %701, label %702, label %713

; <label>:702                                     ; preds = %691
  %703 = load %struct._IO_FILE** @stderr, align 8
  %704 = load i8** %zErrMsg, align 8
  %705 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %703, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %704)
  %706 = load i32* %rc, align 4
  %707 = icmp ne i32 %706, 0
  br i1 %707, label %708, label %710

; <label>:708                                     ; preds = %702
  %709 = load i32* %rc, align 4
  br label %711

; <label>:710                                     ; preds = %702
  br label %711

; <label>:711                                     ; preds = %710, %708
  %712 = phi i32 [ %709, %708 ], [ 1, %710 ]
  store i32 %712, i32* %1
  br label %791

; <label>:713                                     ; preds = %691
  %714 = load i32* %rc, align 4
  %715 = icmp ne i32 %714, 0
  br i1 %715, label %716, label %725

; <label>:716                                     ; preds = %713
  %717 = load %struct._IO_FILE** @stderr, align 8
  %718 = load i32* %i, align 4
  %719 = sext i32 %718 to i64
  %720 = load i8*** %azCmd, align 8
  %721 = getelementptr inbounds i8** %720, i64 %719
  %722 = load i8** %721, align 8
  %723 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %717, i8* getelementptr inbounds ([34 x i8]* @.str43, i32 0, i32 0), i8* %722)
  %724 = load i32* %rc, align 4
  store i32 %724, i32* %1
  br label %791

; <label>:725                                     ; preds = %713
  br label %726

; <label>:726                                     ; preds = %725
  br label %727

; <label>:727                                     ; preds = %726, %690
  br label %728

; <label>:728                                     ; preds = %727
  %729 = load i32* %i, align 4
  %730 = add nsw i32 %729, 1
  store i32 %730, i32* %i, align 4
  br label %659

; <label>:731                                     ; preds = %659
  %732 = load i8*** %azCmd, align 8
  %733 = bitcast i8** %732 to i8*
  call void @free(i8* %733) #5
  br label %779

; <label>:734                                     ; preds = %655
  %735 = load i32* @stdin_is_interactive, align 4
  %736 = icmp ne i32 %735, 0
  br i1 %736, label %737, label %775

; <label>:737                                     ; preds = %734
  store i8* null, i8** %zHistory, align 8
  %738 = call i8* @sqlite3_libversion()
  %739 = call i8* @sqlite3_sourceid()
  %740 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([56 x i8]* @.str44, i32 0, i32 0), i8* %738, i8* %739)
  %741 = load i32* %warnInmemoryDb, align 4
  %742 = icmp ne i32 %741, 0
  br i1 %742, label %743, label %746

; <label>:743                                     ; preds = %737
  %744 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([16 x i8]* @.str45, i32 0, i32 0))
  call void @printBold(i8* getelementptr inbounds ([29 x i8]* @.str46, i32 0, i32 0))
  %745 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([60 x i8]* @.str47, i32 0, i32 0))
  br label %746

; <label>:746                                     ; preds = %743, %737
  %747 = call i8* @find_home_dir()
  store i8* %747, i8** %zHome, align 8
  %748 = load i8** %zHome, align 8
  %749 = icmp ne i8* %748, null
  br i1 %749, label %750, label %764

; <label>:750                                     ; preds = %746
  %751 = load i8** %zHome, align 8
  %752 = call i32 @strlen30(i8* %751)
  %753 = add nsw i32 %752, 20
  store i32 %753, i32* %nHistory, align 4
  %754 = load i32* %nHistory, align 4
  %755 = sext i32 %754 to i64
  %756 = call noalias i8* @malloc(i64 %755) #5
  store i8* %756, i8** %zHistory, align 8
  %757 = icmp ne i8* %756, null
  br i1 %757, label %758, label %763

; <label>:758                                     ; preds = %750
  %759 = load i32* %nHistory, align 4
  %760 = load i8** %zHistory, align 8
  %761 = load i8** %zHome, align 8
  %762 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 %759, i8* %760, i8* getelementptr inbounds ([19 x i8]* @.str48, i32 0, i32 0), i8* %761)
  br label %763

; <label>:763                                     ; preds = %758, %750
  br label %764

; <label>:764                                     ; preds = %763, %746
  %765 = load i8** %zHistory, align 8
  %766 = icmp ne i8* %765, null
  br i1 %766, label %767, label %768

; <label>:767                                     ; preds = %764
  br label %768

; <label>:768                                     ; preds = %767, %764
  %769 = call i32 @process_input(%struct.ShellState* %data, %struct._IO_FILE* null)
  store i32 %769, i32* %rc, align 4
  %770 = load i8** %zHistory, align 8
  %771 = icmp ne i8* %770, null
  br i1 %771, label %772, label %774

; <label>:772                                     ; preds = %768
  %773 = load i8** %zHistory, align 8
  call void @free(i8* %773) #5
  br label %774

; <label>:774                                     ; preds = %772, %768
  br label %778

; <label>:775                                     ; preds = %734
  %776 = load %struct._IO_FILE** @stdin, align 8
  %777 = call i32 @process_input(%struct.ShellState* %data, %struct._IO_FILE* %776)
  store i32 %777, i32* %rc, align 4
  br label %778

; <label>:778                                     ; preds = %775, %774
  br label %779

; <label>:779                                     ; preds = %778, %731
  call void @set_table_name(%struct.ShellState* %data, i8* null)
  %780 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 0
  %781 = load %struct.sqlite3** %780, align 8
  %782 = icmp ne %struct.sqlite3* %781, null
  br i1 %782, label %783, label %787

; <label>:783                                     ; preds = %779
  %784 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 0
  %785 = load %struct.sqlite3** %784, align 8
  %786 = call i32 @sqlite3_close(%struct.sqlite3* %785)
  br label %787

; <label>:787                                     ; preds = %783, %779
  %788 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 24
  %789 = load i8** %788, align 8
  call void @sqlite3_free(i8* %789)
  %790 = load i32* %rc, align 4
  store i32 %790, i32* %1
  br label %791

; <label>:791                                     ; preds = %787, %716, %711, %688, %616, %610, %598, %576, %480
  %792 = load i32* %1
  ret i32 %792
}

; Function Attrs: nounwind readonly
declare i32 @strcmp(i8*, i8*) #1

declare i8* @sqlite3_sourceid() #2

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: noreturn nounwind
declare void @exit(i32) #3

; Function Attrs: nounwind
declare i32 @setvbuf(%struct._IO_FILE*, i8*, i32, i64) #4

; Function Attrs: nounwind uwtable
define internal void @main_init(%struct.ShellState* %data) #0 {
  %1 = alloca %struct.ShellState*, align 8
  store %struct.ShellState* %data, %struct.ShellState** %1, align 8
  %2 = load %struct.ShellState** %1, align 8
  %3 = bitcast %struct.ShellState* %2 to i8*
  call void @llvm.memset.p0i8.i64(i8* %3, i8 0, i64 5512, i32 8, i1 false)
  %4 = load %struct.ShellState** %1, align 8
  %5 = getelementptr inbounds %struct.ShellState* %4, i32 0, i32 11
  store i32 2, i32* %5, align 4
  %6 = load %struct.ShellState** %1, align 8
  %7 = getelementptr inbounds %struct.ShellState* %6, i32 0, i32 16
  %8 = bitcast [20 x i8]* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %8, i8* getelementptr inbounds ([2 x i8]* @.str465, i32 0, i32 0), i64 2, i32 1, i1 false)
  %9 = load %struct.ShellState** %1, align 8
  %10 = getelementptr inbounds %struct.ShellState* %9, i32 0, i32 17
  %11 = bitcast [20 x i8]* %10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %11, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0), i64 2, i32 1, i1 false)
  %12 = load %struct.ShellState** %1, align 8
  %13 = getelementptr inbounds %struct.ShellState* %12, i32 0, i32 13
  store i32 0, i32* %13, align 4
  %14 = load %struct.ShellState** %1, align 8
  %15 = getelementptr inbounds %struct.ShellState* %14, i32 0, i32 14
  store i32 4, i32* %15, align 4
  %16 = call i32 (i32, ...)* @sqlite3_config(i32 17, i32 1)
  %17 = load %struct.ShellState** %1, align 8
  %18 = call i32 (i32, ...)* @sqlite3_config(i32 16, void (i8*, i32, i8*)* @shellLog, %struct.ShellState* %17)
  %19 = call i32 (i32, ...)* @sqlite3_config(i32 2)
  %20 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* getelementptr inbounds ([20 x i8]* @mainPrompt, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8]* @.str466, i32 0, i32 0))
  %21 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* getelementptr inbounds ([20 x i8]* @continuePrompt, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8]* @.str467, i32 0, i32 0))
  ret void
}

; Function Attrs: nounwind
declare i32 @isatty(i32) #4

; Function Attrs: nounwind
declare void (i32)* @signal(i32, void (i32)*) #4

; Function Attrs: nounwind uwtable
define internal void @interrupt_handler(i32 %NotUsed) #0 {
  %1 = alloca i32, align 4
  store i32 %NotUsed, i32* %1, align 4
  %2 = load i32* %1, align 4
  %3 = load volatile i32* @seenInterrupt, align 4
  %4 = add nsw i32 %3, 1
  store volatile i32 %4, i32* @seenInterrupt, align 4
  %5 = load volatile i32* @seenInterrupt, align 4
  %6 = icmp sgt i32 %5, 2
  br i1 %6, label %7, label %8

; <label>:7                                       ; preds = %0
  call void @exit(i32 1) #8
  unreachable

; <label>:8                                       ; preds = %0
  %9 = load %struct.sqlite3** @globalDb, align 8
  %10 = icmp ne %struct.sqlite3* %9, null
  br i1 %10, label %11, label %13

; <label>:11                                      ; preds = %8
  %12 = load %struct.sqlite3** @globalDb, align 8
  call void @sqlite3_interrupt(%struct.sqlite3* %12)
  br label %13

; <label>:13                                      ; preds = %11, %8
  ret void
}

; Function Attrs: nounwind
declare i8* @realloc(i8*, i64) #4

; Function Attrs: nounwind uwtable
define internal i8* @cmdline_option_value(i32 %argc, i8** %argv, i32 %i) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8**, align 8
  %3 = alloca i32, align 4
  store i32 %argc, i32* %1, align 4
  store i8** %argv, i8*** %2, align 8
  store i32 %i, i32* %3, align 4
  %4 = load i32* %3, align 4
  %5 = load i32* %1, align 4
  %6 = icmp eq i32 %4, %5
  br i1 %6, label %7, label %19

; <label>:7                                       ; preds = %0
  %8 = load %struct._IO_FILE** @stderr, align 8
  %9 = load i8*** %2, align 8
  %10 = getelementptr inbounds i8** %9, i64 0
  %11 = load i8** %10, align 8
  %12 = load i32* %1, align 4
  %13 = sub nsw i32 %12, 1
  %14 = sext i32 %13 to i64
  %15 = load i8*** %2, align 8
  %16 = getelementptr inbounds i8** %15, i64 %14
  %17 = load i8** %16, align 8
  %18 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([35 x i8]* @.str464, i32 0, i32 0), i8* %11, i8* %17)
  call void @exit(i32 1) #8
  unreachable

; <label>:19                                      ; preds = %0
  %20 = load i32* %3, align 4
  %21 = sext i32 %20 to i64
  %22 = load i8*** %2, align 8
  %23 = getelementptr inbounds i8** %22, i64 %21
  %24 = load i8** %23, align 8
  ret i8* %24
}

; Function Attrs: nounwind uwtable
define internal i64 @integerValue(i8* %zArg) #0 {
  %1 = alloca i8*, align 8
  %v = alloca i64, align 8
  %i = alloca i32, align 4
  %isNeg = alloca i32, align 4
  %x = alloca i32, align 4
  store i8* %zArg, i8** %1, align 8
  store i64 0, i64* %v, align 8
  store i32 0, i32* %isNeg, align 4
  %2 = load i8** %1, align 8
  %3 = getelementptr inbounds i8* %2, i64 0
  %4 = load i8* %3, align 1
  %5 = sext i8 %4 to i32
  %6 = icmp eq i32 %5, 45
  br i1 %6, label %7, label %10

; <label>:7                                       ; preds = %0
  store i32 1, i32* %isNeg, align 4
  %8 = load i8** %1, align 8
  %9 = getelementptr inbounds i8* %8, i32 1
  store i8* %9, i8** %1, align 8
  br label %20

; <label>:10                                      ; preds = %0
  %11 = load i8** %1, align 8
  %12 = getelementptr inbounds i8* %11, i64 0
  %13 = load i8* %12, align 1
  %14 = sext i8 %13 to i32
  %15 = icmp eq i32 %14, 43
  br i1 %15, label %16, label %19

; <label>:16                                      ; preds = %10
  %17 = load i8** %1, align 8
  %18 = getelementptr inbounds i8* %17, i32 1
  store i8* %18, i8** %1, align 8
  br label %19

; <label>:19                                      ; preds = %16, %10
  br label %20

; <label>:20                                      ; preds = %19, %7
  %21 = load i8** %1, align 8
  %22 = getelementptr inbounds i8* %21, i64 0
  %23 = load i8* %22, align 1
  %24 = sext i8 %23 to i32
  %25 = icmp eq i32 %24, 48
  br i1 %25, label %26, label %50

; <label>:26                                      ; preds = %20
  %27 = load i8** %1, align 8
  %28 = getelementptr inbounds i8* %27, i64 1
  %29 = load i8* %28, align 1
  %30 = sext i8 %29 to i32
  %31 = icmp eq i32 %30, 120
  br i1 %31, label %32, label %50

; <label>:32                                      ; preds = %26
  %33 = load i8** %1, align 8
  %34 = getelementptr inbounds i8* %33, i64 2
  store i8* %34, i8** %1, align 8
  br label %35

; <label>:35                                      ; preds = %41, %32
  %36 = load i8** %1, align 8
  %37 = getelementptr inbounds i8* %36, i64 0
  %38 = load i8* %37, align 1
  %39 = call i32 @hexDigitValue(i8 signext %38)
  store i32 %39, i32* %x, align 4
  %40 = icmp sge i32 %39, 0
  br i1 %40, label %41, label %49

; <label>:41                                      ; preds = %35
  %42 = load i64* %v, align 8
  %43 = shl i64 %42, 4
  %44 = load i32* %x, align 4
  %45 = sext i32 %44 to i64
  %46 = add nsw i64 %43, %45
  store i64 %46, i64* %v, align 8
  %47 = load i8** %1, align 8
  %48 = getelementptr inbounds i8* %47, i32 1
  store i8* %48, i8** %1, align 8
  br label %35

; <label>:49                                      ; preds = %35
  br label %76

; <label>:50                                      ; preds = %26, %20
  br label %51

; <label>:51                                      ; preds = %64, %50
  %52 = load i8** %1, align 8
  %53 = getelementptr inbounds i8* %52, i64 0
  %54 = load i8* %53, align 1
  %55 = zext i8 %54 to i32
  %56 = sext i32 %55 to i64
  %57 = call i16** @__ctype_b_loc() #9
  %58 = load i16** %57, align 8
  %59 = getelementptr inbounds i16* %58, i64 %56
  %60 = load i16* %59, align 2
  %61 = zext i16 %60 to i32
  %62 = and i32 %61, 2048
  %63 = icmp ne i32 %62, 0
  br i1 %63, label %64, label %75

; <label>:64                                      ; preds = %51
  %65 = load i64* %v, align 8
  %66 = mul nsw i64 %65, 10
  %67 = load i8** %1, align 8
  %68 = getelementptr inbounds i8* %67, i64 0
  %69 = load i8* %68, align 1
  %70 = sext i8 %69 to i64
  %71 = add nsw i64 %66, %70
  %72 = sub nsw i64 %71, 48
  store i64 %72, i64* %v, align 8
  %73 = load i8** %1, align 8
  %74 = getelementptr inbounds i8* %73, i32 1
  store i8* %74, i8** %1, align 8
  br label %51

; <label>:75                                      ; preds = %51
  br label %76

; <label>:76                                      ; preds = %75, %49
  store i32 0, i32* %i, align 4
  br label %77

; <label>:77                                      ; preds = %99, %76
  %78 = load i32* %i, align 4
  %79 = icmp slt i32 %78, 9
  br i1 %79, label %80, label %102

; <label>:80                                      ; preds = %77
  %81 = load i32* %i, align 4
  %82 = sext i32 %81 to i64
  %83 = getelementptr inbounds [9 x %struct.anon.16]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @integerValue.aMult to [9 x %struct.anon.16]*), i32 0, i64 %82
  %84 = getelementptr inbounds %struct.anon.16* %83, i32 0, i32 0
  %85 = load i8** %84, align 8
  %86 = load i8** %1, align 8
  %87 = call i32 @sqlite3_stricmp(i8* %85, i8* %86)
  %88 = icmp eq i32 %87, 0
  br i1 %88, label %89, label %98

; <label>:89                                      ; preds = %80
  %90 = load i32* %i, align 4
  %91 = sext i32 %90 to i64
  %92 = getelementptr inbounds [9 x %struct.anon.16]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @integerValue.aMult to [9 x %struct.anon.16]*), i32 0, i64 %91
  %93 = getelementptr inbounds %struct.anon.16* %92, i32 0, i32 1
  %94 = load i32* %93, align 4
  %95 = sext i32 %94 to i64
  %96 = load i64* %v, align 8
  %97 = mul nsw i64 %96, %95
  store i64 %97, i64* %v, align 8
  br label %102

; <label>:98                                      ; preds = %80
  br label %99

; <label>:99                                      ; preds = %98
  %100 = load i32* %i, align 4
  %101 = add nsw i32 %100, 1
  store i32 %101, i32* %i, align 4
  br label %77

; <label>:102                                     ; preds = %89, %77
  %103 = load i32* %isNeg, align 4
  %104 = icmp ne i32 %103, 0
  br i1 %104, label %105, label %108

; <label>:105                                     ; preds = %102
  %106 = load i64* %v, align 8
  %107 = sub nsw i64 0, %106
  br label %110

; <label>:108                                     ; preds = %102
  %109 = load i64* %v, align 8
  br label %110

; <label>:110                                     ; preds = %108, %105
  %111 = phi i64 [ %107, %105 ], [ %109, %108 ]
  ret i64 %111
}

declare i32 @sqlite3_config(i32, ...) #2

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #4

declare %struct.sqlite3_vfs* @sqlite3_vfs_find(i8*) #2

declare i32 @sqlite3_vfs_register(%struct.sqlite3_vfs*, i32) #2

; Function Attrs: nounwind
declare i32 @access(i8*, i32) #4

; Function Attrs: nounwind uwtable
define internal void @open_db(%struct.ShellState* %p, i32 %keepAlive) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca i32, align 4
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store i32 %keepAlive, i32* %2, align 4
  %3 = load %struct.ShellState** %1, align 8
  %4 = getelementptr inbounds %struct.ShellState* %3, i32 0, i32 0
  %5 = load %struct.sqlite3** %4, align 8
  %6 = icmp eq %struct.sqlite3* %5, null
  br i1 %6, label %7, label %71

; <label>:7                                       ; preds = %0
  %8 = call i32 @sqlite3_initialize()
  %9 = load %struct.ShellState** %1, align 8
  %10 = getelementptr inbounds %struct.ShellState* %9, i32 0, i32 23
  %11 = load i8** %10, align 8
  %12 = load %struct.ShellState** %1, align 8
  %13 = getelementptr inbounds %struct.ShellState* %12, i32 0, i32 0
  %14 = call i32 @sqlite3_open(i8* %11, %struct.sqlite3** %13)
  %15 = load %struct.ShellState** %1, align 8
  %16 = getelementptr inbounds %struct.ShellState* %15, i32 0, i32 0
  %17 = load %struct.sqlite3** %16, align 8
  store %struct.sqlite3* %17, %struct.sqlite3** @globalDb, align 8
  %18 = load %struct.ShellState** %1, align 8
  %19 = getelementptr inbounds %struct.ShellState* %18, i32 0, i32 0
  %20 = load %struct.sqlite3** %19, align 8
  %21 = icmp ne %struct.sqlite3* %20, null
  br i1 %21, label %22, label %33

; <label>:22                                      ; preds = %7
  %23 = load %struct.ShellState** %1, align 8
  %24 = getelementptr inbounds %struct.ShellState* %23, i32 0, i32 0
  %25 = load %struct.sqlite3** %24, align 8
  %26 = call i32 @sqlite3_errcode(%struct.sqlite3* %25)
  %27 = icmp eq i32 %26, 0
  br i1 %27, label %28, label %33

; <label>:28                                      ; preds = %22
  %29 = load %struct.ShellState** %1, align 8
  %30 = getelementptr inbounds %struct.ShellState* %29, i32 0, i32 0
  %31 = load %struct.sqlite3** %30, align 8
  %32 = call i32 @sqlite3_create_function(%struct.sqlite3* %31, i8* getelementptr inbounds ([12 x i8]* @.str449, i32 0, i32 0), i32 0, i32 1, i8* null, void (%struct.sqlite3_context*, i32, %struct.Mem**)* @shellstaticFunc, void (%struct.sqlite3_context*, i32, %struct.Mem**)* null, void (%struct.sqlite3_context*)* null)
  br label %33

; <label>:33                                      ; preds = %28, %22, %7
  %34 = load %struct.ShellState** %1, align 8
  %35 = getelementptr inbounds %struct.ShellState* %34, i32 0, i32 0
  %36 = load %struct.sqlite3** %35, align 8
  %37 = icmp eq %struct.sqlite3* %36, null
  br i1 %37, label %44, label %38

; <label>:38                                      ; preds = %33
  %39 = load %struct.ShellState** %1, align 8
  %40 = getelementptr inbounds %struct.ShellState* %39, i32 0, i32 0
  %41 = load %struct.sqlite3** %40, align 8
  %42 = call i32 @sqlite3_errcode(%struct.sqlite3* %41)
  %43 = icmp ne i32 0, %42
  br i1 %43, label %44, label %58

; <label>:44                                      ; preds = %38, %33
  %45 = load %struct._IO_FILE** @stderr, align 8
  %46 = load %struct.ShellState** %1, align 8
  %47 = getelementptr inbounds %struct.ShellState* %46, i32 0, i32 23
  %48 = load i8** %47, align 8
  %49 = load %struct.ShellState** %1, align 8
  %50 = getelementptr inbounds %struct.ShellState* %49, i32 0, i32 0
  %51 = load %struct.sqlite3** %50, align 8
  %52 = call i8* @sqlite3_errmsg(%struct.sqlite3* %51)
  %53 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %45, i8* getelementptr inbounds ([41 x i8]* @.str450, i32 0, i32 0), i8* %48, i8* %52)
  %54 = load i32* %2, align 4
  %55 = icmp ne i32 %54, 0
  br i1 %55, label %56, label %57

; <label>:56                                      ; preds = %44
  br label %71

; <label>:57                                      ; preds = %44
  call void @exit(i32 1) #8
  unreachable

; <label>:58                                      ; preds = %38
  %59 = load %struct.ShellState** %1, align 8
  %60 = getelementptr inbounds %struct.ShellState* %59, i32 0, i32 0
  %61 = load %struct.sqlite3** %60, align 8
  %62 = call i32 @sqlite3_enable_load_extension(%struct.sqlite3* %61, i32 1)
  %63 = load %struct.ShellState** %1, align 8
  %64 = getelementptr inbounds %struct.ShellState* %63, i32 0, i32 0
  %65 = load %struct.sqlite3** %64, align 8
  %66 = call i32 @sqlite3_create_function(%struct.sqlite3* %65, i8* getelementptr inbounds ([9 x i8]* @.str451, i32 0, i32 0), i32 1, i32 1, i8* null, void (%struct.sqlite3_context*, i32, %struct.Mem**)* @readfileFunc, void (%struct.sqlite3_context*, i32, %struct.Mem**)* null, void (%struct.sqlite3_context*)* null)
  %67 = load %struct.ShellState** %1, align 8
  %68 = getelementptr inbounds %struct.ShellState* %67, i32 0, i32 0
  %69 = load %struct.sqlite3** %68, align 8
  %70 = call i32 @sqlite3_create_function(%struct.sqlite3* %69, i8* getelementptr inbounds ([10 x i8]* @.str452, i32 0, i32 0), i32 2, i32 1, i8* null, void (%struct.sqlite3_context*, i32, %struct.Mem**)* @writefileFunc, void (%struct.sqlite3_context*, i32, %struct.Mem**)* null, void (%struct.sqlite3_context*)* null)
  br label %71

; <label>:71                                      ; preds = %56, %58, %0
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @process_sqliterc(%struct.ShellState* %p, i8* %sqliterc_override) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca i8*, align 8
  %home_dir = alloca i8*, align 8
  %sqliterc = alloca i8*, align 8
  %zBuf = alloca i8*, align 8
  %in = alloca %struct._IO_FILE*, align 8
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store i8* %sqliterc_override, i8** %2, align 8
  store i8* null, i8** %home_dir, align 8
  %3 = load i8** %2, align 8
  store i8* %3, i8** %sqliterc, align 8
  store i8* null, i8** %zBuf, align 8
  store %struct._IO_FILE* null, %struct._IO_FILE** %in, align 8
  %4 = load i8** %sqliterc, align 8
  %5 = icmp eq i8* %4, null
  br i1 %5, label %6, label %18

; <label>:6                                       ; preds = %0
  %7 = call i8* @find_home_dir()
  store i8* %7, i8** %home_dir, align 8
  %8 = load i8** %home_dir, align 8
  %9 = icmp eq i8* %8, null
  br i1 %9, label %10, label %13

; <label>:10                                      ; preds = %6
  %11 = load %struct._IO_FILE** @stderr, align 8
  %12 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([65 x i8]* @.str446, i32 0, i32 0))
  br label %38

; <label>:13                                      ; preds = %6
  %14 = call i32 @sqlite3_initialize()
  %15 = load i8** %home_dir, align 8
  %16 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([13 x i8]* @.str447, i32 0, i32 0), i8* %15)
  store i8* %16, i8** %zBuf, align 8
  %17 = load i8** %zBuf, align 8
  store i8* %17, i8** %sqliterc, align 8
  br label %18

; <label>:18                                      ; preds = %13, %0
  %19 = load i8** %sqliterc, align 8
  %20 = call %struct._IO_FILE* @fopen64(i8* %19, i8* getelementptr inbounds ([3 x i8]* @.str197, i32 0, i32 0))
  store %struct._IO_FILE* %20, %struct._IO_FILE** %in, align 8
  %21 = load %struct._IO_FILE** %in, align 8
  %22 = icmp ne %struct._IO_FILE* %21, null
  br i1 %22, label %23, label %36

; <label>:23                                      ; preds = %18
  %24 = load i32* @stdin_is_interactive, align 4
  %25 = icmp ne i32 %24, 0
  br i1 %25, label %26, label %30

; <label>:26                                      ; preds = %23
  %27 = load %struct._IO_FILE** @stderr, align 8
  %28 = load i8** %sqliterc, align 8
  %29 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %27, i8* getelementptr inbounds ([30 x i8]* @.str448, i32 0, i32 0), i8* %28)
  br label %30

; <label>:30                                      ; preds = %26, %23
  %31 = load %struct.ShellState** %1, align 8
  %32 = load %struct._IO_FILE** %in, align 8
  %33 = call i32 @process_input(%struct.ShellState* %31, %struct._IO_FILE* %32)
  %34 = load %struct._IO_FILE** %in, align 8
  %35 = call i32 @fclose(%struct._IO_FILE* %34)
  br label %36

; <label>:36                                      ; preds = %30, %18
  %37 = load i8** %zBuf, align 8
  call void @sqlite3_free(i8* %37)
  br label %38

; <label>:38                                      ; preds = %36, %10
  ret void
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #5

declare i8* @sqlite3_snprintf(i32, i8*, i8*, ...) #2

declare i32 @printf(i8*, ...) #2

declare i8* @sqlite3_libversion() #2

; Function Attrs: nounwind uwtable
define internal void @usage(i32 %showDetail) #0 {
  %1 = alloca i32, align 4
  store i32 %showDetail, i32* %1, align 4
  %2 = load %struct._IO_FILE** @stderr, align 8
  %3 = load i8** @Argv0, align 8
  %4 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2, i8* getelementptr inbounds ([145 x i8]* @.str443, i32 0, i32 0), i8* %3)
  %5 = load i32* %1, align 4
  %6 = icmp ne i32 %5, 0
  br i1 %6, label %7, label %10

; <label>:7                                       ; preds = %0
  %8 = load %struct._IO_FILE** @stderr, align 8
  %9 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([20 x i8]* @.str444, i32 0, i32 0), i8* getelementptr inbounds ([1318 x i8]* @zOptions, i32 0, i32 0))
  br label %13

; <label>:10                                      ; preds = %0
  %11 = load %struct._IO_FILE** @stderr, align 8
  %12 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([49 x i8]* @.str445, i32 0, i32 0))
  br label %13

; <label>:13                                      ; preds = %10, %7
  call void @exit(i32 1) #8
  unreachable
                                                  ; No predecessors!
  ret void
}

; Function Attrs: nounwind uwtable
define internal i32 @do_meta_command(i8* %zLine, %struct.ShellState* %p) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %3 = alloca %struct.ShellState*, align 8
  %h = alloca i32, align 4
  %nArg = alloca i32, align 4
  %n = alloca i32, align 4
  %c = alloca i32, align 4
  %rc = alloca i32, align 4
  %azArg = alloca [50 x i8*], align 16
  %delim = alloca i32, align 4
  %zDestFile = alloca i8*, align 8
  %zDb = alloca i8*, align 8
  %pDest = alloca %struct.sqlite3*, align 8
  %pBackup = alloca %struct.sqlite3_backup*, align 8
  %j = alloca i32, align 4
  %z = alloca i8*, align 8
  %data = alloca %struct.ShellState, align 8
  %zErrMsg = alloca i8*, align 8
  %i = alloca i32, align 4
  %val = alloca i32, align 4
  %data1 = alloca %struct.ShellState, align 8
  %zErrMsg2 = alloca i8*, align 8
  %doStats = alloca i32, align 4
  %pStmt = alloca %struct.sqlite3_stmt*, align 8
  %zTable = alloca i8*, align 8
  %zFile = alloca i8*, align 8
  %pStmt3 = alloca %struct.sqlite3_stmt*, align 8
  %nCol = alloca i32, align 4
  %nByte = alloca i32, align 4
  %i4 = alloca i32, align 4
  %j5 = alloca i32, align 4
  %needCommit = alloca i32, align 4
  %nSep = alloca i32, align 4
  %zSql = alloca i8*, align 8
  %sCtx = alloca %struct.ImportCtx, align 8
  %xRead = alloca i8* (%struct.ImportCtx*)*, align 8
  %xCloser = alloca i32 (%struct._IO_FILE*)*, align 8
  %zCreate = alloca i8*, align 8
  %cSep = alloca i8, align 1
  %startLine = alloca i32, align 4
  %z6 = alloca i8*, align 8
  %data7 = alloca %struct.ShellState, align 8
  %zErrMsg8 = alloca i8*, align 8
  %i9 = alloca i32, align 4
  %n2 = alloca i32, align 4
  %iLimit = alloca i32, align 4
  %zFile10 = alloca i8*, align 8
  %zProc = alloca i8*, align 8
  %zErrMsg11 = alloca i8*, align 8
  %zFile12 = alloca i8*, align 8
  %zMode = alloca i8*, align 8
  %n213 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %savedDb = alloca %struct.sqlite3*, align 8
  %zSavedFilename = alloca i8*, align 8
  %zNewFilename = alloca i8*, align 8
  %zFile14 = alloca i8*, align 8
  %i15 = alloca i32, align 4
  %alt = alloca %struct._IO_FILE*, align 8
  %zSrcFile = alloca i8*, align 8
  %zDb16 = alloca i8*, align 8
  %pSrc = alloca %struct.sqlite3*, align 8
  %pBackup17 = alloca %struct.sqlite3_backup*, align 8
  %nTimeout = alloca i32, align 4
  %data18 = alloca %struct.ShellState, align 8
  %zErrMsg19 = alloca i8*, align 8
  %i20 = alloca i32, align 4
  %new_argv = alloca [2 x i8*], align 16
  %new_colv = alloca [2 x i8*], align 16
  %new_argv21 = alloca [2 x i8*], align 16
  %new_colv22 = alloca [2 x i8*], align 16
  %zCmd = alloca i8*, align 8
  %i23 = alloca i32, align 4
  %x = alloca i32, align 4
  %i24 = alloca i32, align 4
  %pStmt25 = alloca %struct.sqlite3_stmt*, align 8
  %azResult = alloca i8**, align 8
  %nRow = alloca i32, align 4
  %nAlloc = alloca i32, align 4
  %zSql26 = alloca i8*, align 8
  %ii = alloca i32, align 4
  %zDbName = alloca i8*, align 8
  %azNew = alloca i8**, align 8
  %n227 = alloca i32, align 4
  %len = alloca i32, align 4
  %maxlen = alloca i32, align 4
  %i28 = alloca i32, align 4
  %j29 = alloca i32, align 4
  %nPrintCol = alloca i32, align 4
  %nPrintRow = alloca i32, align 4
  %zSp = alloca i8*, align 8
  %testctrl = alloca i32, align 4
  %rc2 = alloca i32, align 4
  %i30 = alloca i32, align 4
  %n231 = alloca i32, align 4
  %opt = alloca i32, align 4
  %opt32 = alloca i32, align 4
  %opt33 = alloca i32, align 4
  %zDbName34 = alloca i8*, align 8
  %zVfsName = alloca i8*, align 8
  %j35 = alloca i32, align 4
  store i8* %zLine, i8** %2, align 8
  store %struct.ShellState* %p, %struct.ShellState** %3, align 8
  store i32 1, i32* %h, align 4
  store i32 0, i32* %nArg, align 4
  store i32 0, i32* %rc, align 4
  br label %4

; <label>:4                                       ; preds = %204, %0
  %5 = load i32* %h, align 4
  %6 = sext i32 %5 to i64
  %7 = load i8** %2, align 8
  %8 = getelementptr inbounds i8* %7, i64 %6
  %9 = load i8* %8, align 1
  %10 = sext i8 %9 to i32
  %11 = icmp ne i32 %10, 0
  br i1 %11, label %12, label %15

; <label>:12                                      ; preds = %4
  %13 = load i32* %nArg, align 4
  %14 = icmp slt i32 %13, 50
  br label %15

; <label>:15                                      ; preds = %12, %4
  %16 = phi i1 [ false, %4 ], [ %14, %12 ]
  br i1 %16, label %17, label %205

; <label>:17                                      ; preds = %15
  br label %18

; <label>:18                                      ; preds = %33, %17
  %19 = load i32* %h, align 4
  %20 = sext i32 %19 to i64
  %21 = load i8** %2, align 8
  %22 = getelementptr inbounds i8* %21, i64 %20
  %23 = load i8* %22, align 1
  %24 = zext i8 %23 to i32
  %25 = sext i32 %24 to i64
  %26 = call i16** @__ctype_b_loc() #9
  %27 = load i16** %26, align 8
  %28 = getelementptr inbounds i16* %27, i64 %25
  %29 = load i16* %28, align 2
  %30 = zext i16 %29 to i32
  %31 = and i32 %30, 8192
  %32 = icmp ne i32 %31, 0
  br i1 %32, label %33, label %36

; <label>:33                                      ; preds = %18
  %34 = load i32* %h, align 4
  %35 = add nsw i32 %34, 1
  store i32 %35, i32* %h, align 4
  br label %18

; <label>:36                                      ; preds = %18
  %37 = load i32* %h, align 4
  %38 = sext i32 %37 to i64
  %39 = load i8** %2, align 8
  %40 = getelementptr inbounds i8* %39, i64 %38
  %41 = load i8* %40, align 1
  %42 = sext i8 %41 to i32
  %43 = icmp eq i32 %42, 0
  br i1 %43, label %44, label %45

; <label>:44                                      ; preds = %36
  br label %205

; <label>:45                                      ; preds = %36
  %46 = load i32* %h, align 4
  %47 = sext i32 %46 to i64
  %48 = load i8** %2, align 8
  %49 = getelementptr inbounds i8* %48, i64 %47
  %50 = load i8* %49, align 1
  %51 = sext i8 %50 to i32
  %52 = icmp eq i32 %51, 39
  br i1 %52, label %61, label %53

; <label>:53                                      ; preds = %45
  %54 = load i32* %h, align 4
  %55 = sext i32 %54 to i64
  %56 = load i8** %2, align 8
  %57 = getelementptr inbounds i8* %56, i64 %55
  %58 = load i8* %57, align 1
  %59 = sext i8 %58 to i32
  %60 = icmp eq i32 %59, 34
  br i1 %60, label %61, label %147

; <label>:61                                      ; preds = %53, %45
  %62 = load i32* %h, align 4
  %63 = add nsw i32 %62, 1
  store i32 %63, i32* %h, align 4
  %64 = sext i32 %62 to i64
  %65 = load i8** %2, align 8
  %66 = getelementptr inbounds i8* %65, i64 %64
  %67 = load i8* %66, align 1
  %68 = sext i8 %67 to i32
  store i32 %68, i32* %delim, align 4
  %69 = load i32* %h, align 4
  %70 = sext i32 %69 to i64
  %71 = load i8** %2, align 8
  %72 = getelementptr inbounds i8* %71, i64 %70
  %73 = load i32* %nArg, align 4
  %74 = add nsw i32 %73, 1
  store i32 %74, i32* %nArg, align 4
  %75 = sext i32 %73 to i64
  %76 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %75
  store i8* %72, i8** %76, align 8
  br label %77

; <label>:77                                      ; preds = %119, %61
  %78 = load i32* %h, align 4
  %79 = sext i32 %78 to i64
  %80 = load i8** %2, align 8
  %81 = getelementptr inbounds i8* %80, i64 %79
  %82 = load i8* %81, align 1
  %83 = sext i8 %82 to i32
  %84 = icmp ne i32 %83, 0
  br i1 %84, label %85, label %94

; <label>:85                                      ; preds = %77
  %86 = load i32* %h, align 4
  %87 = sext i32 %86 to i64
  %88 = load i8** %2, align 8
  %89 = getelementptr inbounds i8* %88, i64 %87
  %90 = load i8* %89, align 1
  %91 = sext i8 %90 to i32
  %92 = load i32* %delim, align 4
  %93 = icmp ne i32 %91, %92
  br label %94

; <label>:94                                      ; preds = %85, %77
  %95 = phi i1 [ false, %77 ], [ %93, %85 ]
  br i1 %95, label %96, label %122

; <label>:96                                      ; preds = %94
  %97 = load i32* %h, align 4
  %98 = sext i32 %97 to i64
  %99 = load i8** %2, align 8
  %100 = getelementptr inbounds i8* %99, i64 %98
  %101 = load i8* %100, align 1
  %102 = sext i8 %101 to i32
  %103 = icmp eq i32 %102, 92
  br i1 %103, label %104, label %119

; <label>:104                                     ; preds = %96
  %105 = load i32* %delim, align 4
  %106 = icmp eq i32 %105, 34
  br i1 %106, label %107, label %119

; <label>:107                                     ; preds = %104
  %108 = load i32* %h, align 4
  %109 = add nsw i32 %108, 1
  %110 = sext i32 %109 to i64
  %111 = load i8** %2, align 8
  %112 = getelementptr inbounds i8* %111, i64 %110
  %113 = load i8* %112, align 1
  %114 = sext i8 %113 to i32
  %115 = icmp ne i32 %114, 0
  br i1 %115, label %116, label %119

; <label>:116                                     ; preds = %107
  %117 = load i32* %h, align 4
  %118 = add nsw i32 %117, 1
  store i32 %118, i32* %h, align 4
  br label %119

; <label>:119                                     ; preds = %116, %107, %104, %96
  %120 = load i32* %h, align 4
  %121 = add nsw i32 %120, 1
  store i32 %121, i32* %h, align 4
  br label %77

; <label>:122                                     ; preds = %94
  %123 = load i32* %h, align 4
  %124 = sext i32 %123 to i64
  %125 = load i8** %2, align 8
  %126 = getelementptr inbounds i8* %125, i64 %124
  %127 = load i8* %126, align 1
  %128 = sext i8 %127 to i32
  %129 = load i32* %delim, align 4
  %130 = icmp eq i32 %128, %129
  br i1 %130, label %131, label %137

; <label>:131                                     ; preds = %122
  %132 = load i32* %h, align 4
  %133 = add nsw i32 %132, 1
  store i32 %133, i32* %h, align 4
  %134 = sext i32 %132 to i64
  %135 = load i8** %2, align 8
  %136 = getelementptr inbounds i8* %135, i64 %134
  store i8 0, i8* %136, align 1
  br label %137

; <label>:137                                     ; preds = %131, %122
  %138 = load i32* %delim, align 4
  %139 = icmp eq i32 %138, 34
  br i1 %139, label %140, label %146

; <label>:140                                     ; preds = %137
  %141 = load i32* %nArg, align 4
  %142 = sub nsw i32 %141, 1
  %143 = sext i32 %142 to i64
  %144 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %143
  %145 = load i8** %144, align 8
  call void @resolve_backslashes(i8* %145)
  br label %146

; <label>:146                                     ; preds = %140, %137
  br label %204

; <label>:147                                     ; preds = %53
  %148 = load i32* %h, align 4
  %149 = sext i32 %148 to i64
  %150 = load i8** %2, align 8
  %151 = getelementptr inbounds i8* %150, i64 %149
  %152 = load i32* %nArg, align 4
  %153 = add nsw i32 %152, 1
  store i32 %153, i32* %nArg, align 4
  %154 = sext i32 %152 to i64
  %155 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %154
  store i8* %151, i8** %155, align 8
  br label %156

; <label>:156                                     ; preds = %182, %147
  %157 = load i32* %h, align 4
  %158 = sext i32 %157 to i64
  %159 = load i8** %2, align 8
  %160 = getelementptr inbounds i8* %159, i64 %158
  %161 = load i8* %160, align 1
  %162 = sext i8 %161 to i32
  %163 = icmp ne i32 %162, 0
  br i1 %163, label %164, label %180

; <label>:164                                     ; preds = %156
  %165 = load i32* %h, align 4
  %166 = sext i32 %165 to i64
  %167 = load i8** %2, align 8
  %168 = getelementptr inbounds i8* %167, i64 %166
  %169 = load i8* %168, align 1
  %170 = zext i8 %169 to i32
  %171 = sext i32 %170 to i64
  %172 = call i16** @__ctype_b_loc() #9
  %173 = load i16** %172, align 8
  %174 = getelementptr inbounds i16* %173, i64 %171
  %175 = load i16* %174, align 2
  %176 = zext i16 %175 to i32
  %177 = and i32 %176, 8192
  %178 = icmp ne i32 %177, 0
  %179 = xor i1 %178, true
  br label %180

; <label>:180                                     ; preds = %164, %156
  %181 = phi i1 [ false, %156 ], [ %179, %164 ]
  br i1 %181, label %182, label %185

; <label>:182                                     ; preds = %180
  %183 = load i32* %h, align 4
  %184 = add nsw i32 %183, 1
  store i32 %184, i32* %h, align 4
  br label %156

; <label>:185                                     ; preds = %180
  %186 = load i32* %h, align 4
  %187 = sext i32 %186 to i64
  %188 = load i8** %2, align 8
  %189 = getelementptr inbounds i8* %188, i64 %187
  %190 = load i8* %189, align 1
  %191 = icmp ne i8 %190, 0
  br i1 %191, label %192, label %198

; <label>:192                                     ; preds = %185
  %193 = load i32* %h, align 4
  %194 = add nsw i32 %193, 1
  store i32 %194, i32* %h, align 4
  %195 = sext i32 %193 to i64
  %196 = load i8** %2, align 8
  %197 = getelementptr inbounds i8* %196, i64 %195
  store i8 0, i8* %197, align 1
  br label %198

; <label>:198                                     ; preds = %192, %185
  %199 = load i32* %nArg, align 4
  %200 = sub nsw i32 %199, 1
  %201 = sext i32 %200 to i64
  %202 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %201
  %203 = load i8** %202, align 8
  call void @resolve_backslashes(i8* %203)
  br label %204

; <label>:204                                     ; preds = %198, %146
  br label %4

; <label>:205                                     ; preds = %44, %15
  %206 = load i32* %nArg, align 4
  %207 = icmp eq i32 %206, 0
  br i1 %207, label %208, label %209

; <label>:208                                     ; preds = %205
  store i32 0, i32* %1
  br label %3389

; <label>:209                                     ; preds = %205
  %210 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %211 = load i8** %210, align 8
  %212 = call i32 @strlen30(i8* %211)
  store i32 %212, i32* %n, align 4
  %213 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %214 = load i8** %213, align 8
  %215 = getelementptr inbounds i8* %214, i64 0
  %216 = load i8* %215, align 1
  %217 = sext i8 %216 to i32
  store i32 %217, i32* %c, align 4
  %218 = load i32* %c, align 4
  %219 = icmp eq i32 %218, 98
  br i1 %219, label %220, label %230

; <label>:220                                     ; preds = %209
  %221 = load i32* %n, align 4
  %222 = icmp sge i32 %221, 3
  br i1 %222, label %223, label %230

; <label>:223                                     ; preds = %220
  %224 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %225 = load i8** %224, align 8
  %226 = load i32* %n, align 4
  %227 = sext i32 %226 to i64
  %228 = call i32 @strncmp(i8* %225, i8* getelementptr inbounds ([7 x i8]* @.str135, i32 0, i32 0), i64 %227) #7
  %229 = icmp eq i32 %228, 0
  br i1 %229, label %243, label %230

; <label>:230                                     ; preds = %223, %220, %209
  %231 = load i32* %c, align 4
  %232 = icmp eq i32 %231, 115
  br i1 %232, label %233, label %359

; <label>:233                                     ; preds = %230
  %234 = load i32* %n, align 4
  %235 = icmp sge i32 %234, 3
  br i1 %235, label %236, label %359

; <label>:236                                     ; preds = %233
  %237 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %238 = load i8** %237, align 8
  %239 = load i32* %n, align 4
  %240 = sext i32 %239 to i64
  %241 = call i32 @strncmp(i8* %238, i8* getelementptr inbounds ([5 x i8]* @.str136, i32 0, i32 0), i64 %240) #7
  %242 = icmp eq i32 %241, 0
  br i1 %242, label %243, label %359

; <label>:243                                     ; preds = %236, %223
  store i8* null, i8** %zDestFile, align 8
  store i8* null, i8** %zDb, align 8
  store i32 1, i32* %j, align 4
  br label %244

; <label>:244                                     ; preds = %298, %243
  %245 = load i32* %j, align 4
  %246 = load i32* %nArg, align 4
  %247 = icmp slt i32 %245, %246
  br i1 %247, label %248, label %301

; <label>:248                                     ; preds = %244
  %249 = load i32* %j, align 4
  %250 = sext i32 %249 to i64
  %251 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %250
  %252 = load i8** %251, align 8
  store i8* %252, i8** %z, align 8
  %253 = load i8** %z, align 8
  %254 = getelementptr inbounds i8* %253, i64 0
  %255 = load i8* %254, align 1
  %256 = sext i8 %255 to i32
  %257 = icmp eq i32 %256, 45
  br i1 %257, label %258, label %275

; <label>:258                                     ; preds = %248
  br label %259

; <label>:259                                     ; preds = %265, %258
  %260 = load i8** %z, align 8
  %261 = getelementptr inbounds i8* %260, i64 0
  %262 = load i8* %261, align 1
  %263 = sext i8 %262 to i32
  %264 = icmp eq i32 %263, 45
  br i1 %264, label %265, label %268

; <label>:265                                     ; preds = %259
  %266 = load i8** %z, align 8
  %267 = getelementptr inbounds i8* %266, i32 1
  store i8* %267, i8** %z, align 8
  br label %259

; <label>:268                                     ; preds = %259
  %269 = load %struct._IO_FILE** @stderr, align 8
  %270 = load i32* %j, align 4
  %271 = sext i32 %270 to i64
  %272 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %271
  %273 = load i8** %272, align 8
  %274 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %269, i8* getelementptr inbounds ([20 x i8]* @.str137, i32 0, i32 0), i8* %273)
  store i32 1, i32* %1
  br label %3389

; <label>:275                                     ; preds = %248
  %276 = load i8** %zDestFile, align 8
  %277 = icmp eq i8* %276, null
  br i1 %277, label %278, label %283

; <label>:278                                     ; preds = %275
  %279 = load i32* %j, align 4
  %280 = sext i32 %279 to i64
  %281 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %280
  %282 = load i8** %281, align 8
  store i8* %282, i8** %zDestFile, align 8
  br label %296

; <label>:283                                     ; preds = %275
  %284 = load i8** %zDb, align 8
  %285 = icmp eq i8* %284, null
  br i1 %285, label %286, label %292

; <label>:286                                     ; preds = %283
  %287 = load i8** %zDestFile, align 8
  store i8* %287, i8** %zDb, align 8
  %288 = load i32* %j, align 4
  %289 = sext i32 %288 to i64
  %290 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %289
  %291 = load i8** %290, align 8
  store i8* %291, i8** %zDestFile, align 8
  br label %295

; <label>:292                                     ; preds = %283
  %293 = load %struct._IO_FILE** @stderr, align 8
  %294 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %293, i8* getelementptr inbounds ([31 x i8]* @.str138, i32 0, i32 0))
  store i32 1, i32* %1
  br label %3389

; <label>:295                                     ; preds = %286
  br label %296

; <label>:296                                     ; preds = %295, %278
  br label %297

; <label>:297                                     ; preds = %296
  br label %298

; <label>:298                                     ; preds = %297
  %299 = load i32* %j, align 4
  %300 = add nsw i32 %299, 1
  store i32 %300, i32* %j, align 4
  br label %244

; <label>:301                                     ; preds = %244
  %302 = load i8** %zDestFile, align 8
  %303 = icmp eq i8* %302, null
  br i1 %303, label %304, label %307

; <label>:304                                     ; preds = %301
  %305 = load %struct._IO_FILE** @stderr, align 8
  %306 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %305, i8* getelementptr inbounds ([38 x i8]* @.str139, i32 0, i32 0))
  store i32 1, i32* %1
  br label %3389

; <label>:307                                     ; preds = %301
  %308 = load i8** %zDb, align 8
  %309 = icmp eq i8* %308, null
  br i1 %309, label %310, label %311

; <label>:310                                     ; preds = %307
  store i8* getelementptr inbounds ([5 x i8]* @.str140, i32 0, i32 0), i8** %zDb, align 8
  br label %311

; <label>:311                                     ; preds = %310, %307
  %312 = load i8** %zDestFile, align 8
  %313 = call i32 @sqlite3_open(i8* %312, %struct.sqlite3** %pDest)
  store i32 %313, i32* %rc, align 4
  %314 = load i32* %rc, align 4
  %315 = icmp ne i32 %314, 0
  br i1 %315, label %316, label %322

; <label>:316                                     ; preds = %311
  %317 = load %struct._IO_FILE** @stderr, align 8
  %318 = load i8** %zDestFile, align 8
  %319 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %317, i8* getelementptr inbounds ([25 x i8]* @.str141, i32 0, i32 0), i8* %318)
  %320 = load %struct.sqlite3** %pDest, align 8
  %321 = call i32 @sqlite3_close(%struct.sqlite3* %320)
  store i32 1, i32* %1
  br label %3389

; <label>:322                                     ; preds = %311
  %323 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %323, i32 0)
  %324 = load %struct.sqlite3** %pDest, align 8
  %325 = load %struct.ShellState** %3, align 8
  %326 = getelementptr inbounds %struct.ShellState* %325, i32 0, i32 0
  %327 = load %struct.sqlite3** %326, align 8
  %328 = load i8** %zDb, align 8
  %329 = call %struct.sqlite3_backup* @sqlite3_backup_init(%struct.sqlite3* %324, i8* getelementptr inbounds ([5 x i8]* @.str140, i32 0, i32 0), %struct.sqlite3* %327, i8* %328)
  store %struct.sqlite3_backup* %329, %struct.sqlite3_backup** %pBackup, align 8
  %330 = load %struct.sqlite3_backup** %pBackup, align 8
  %331 = icmp eq %struct.sqlite3_backup* %330, null
  br i1 %331, label %332, label %339

; <label>:332                                     ; preds = %322
  %333 = load %struct._IO_FILE** @stderr, align 8
  %334 = load %struct.sqlite3** %pDest, align 8
  %335 = call i8* @sqlite3_errmsg(%struct.sqlite3* %334)
  %336 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %333, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %335)
  %337 = load %struct.sqlite3** %pDest, align 8
  %338 = call i32 @sqlite3_close(%struct.sqlite3* %337)
  store i32 1, i32* %1
  br label %3389

; <label>:339                                     ; preds = %322
  br label %340

; <label>:340                                     ; preds = %344, %339
  %341 = load %struct.sqlite3_backup** %pBackup, align 8
  %342 = call i32 @sqlite3_backup_step(%struct.sqlite3_backup* %341, i32 100)
  store i32 %342, i32* %rc, align 4
  %343 = icmp eq i32 %342, 0
  br i1 %343, label %344, label %345

; <label>:344                                     ; preds = %340
  br label %340

; <label>:345                                     ; preds = %340
  %346 = load %struct.sqlite3_backup** %pBackup, align 8
  %347 = call i32 @sqlite3_backup_finish(%struct.sqlite3_backup* %346)
  %348 = load i32* %rc, align 4
  %349 = icmp eq i32 %348, 101
  br i1 %349, label %350, label %351

; <label>:350                                     ; preds = %345
  store i32 0, i32* %rc, align 4
  br label %356

; <label>:351                                     ; preds = %345
  %352 = load %struct._IO_FILE** @stderr, align 8
  %353 = load %struct.sqlite3** %pDest, align 8
  %354 = call i8* @sqlite3_errmsg(%struct.sqlite3* %353)
  %355 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %352, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %354)
  store i32 1, i32* %rc, align 4
  br label %356

; <label>:356                                     ; preds = %351, %350
  %357 = load %struct.sqlite3** %pDest, align 8
  %358 = call i32 @sqlite3_close(%struct.sqlite3* %357)
  br label %3369

; <label>:359                                     ; preds = %236, %233, %230
  %360 = load i32* %c, align 4
  %361 = icmp eq i32 %360, 98
  br i1 %361, label %362, label %383

; <label>:362                                     ; preds = %359
  %363 = load i32* %n, align 4
  %364 = icmp sge i32 %363, 3
  br i1 %364, label %365, label %383

; <label>:365                                     ; preds = %362
  %366 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %367 = load i8** %366, align 8
  %368 = load i32* %n, align 4
  %369 = sext i32 %368 to i64
  %370 = call i32 @strncmp(i8* %367, i8* getelementptr inbounds ([5 x i8]* @.str142, i32 0, i32 0), i64 %369) #7
  %371 = icmp eq i32 %370, 0
  br i1 %371, label %372, label %383

; <label>:372                                     ; preds = %365
  %373 = load i32* %nArg, align 4
  %374 = icmp eq i32 %373, 2
  br i1 %374, label %375, label %379

; <label>:375                                     ; preds = %372
  %376 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %377 = load i8** %376, align 8
  %378 = call i32 @booleanValue(i8* %377)
  store i32 %378, i32* @bail_on_error, align 4
  br label %382

; <label>:379                                     ; preds = %372
  %380 = load %struct._IO_FILE** @stderr, align 8
  %381 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %380, i8* getelementptr inbounds ([21 x i8]* @.str143, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %382

; <label>:382                                     ; preds = %379, %375
  br label %3368

; <label>:383                                     ; preds = %365, %362, %359
  %384 = load i32* %c, align 4
  %385 = icmp eq i32 %384, 98
  br i1 %385, label %386, label %411

; <label>:386                                     ; preds = %383
  %387 = load i32* %n, align 4
  %388 = icmp sge i32 %387, 3
  br i1 %388, label %389, label %411

; <label>:389                                     ; preds = %386
  %390 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %391 = load i8** %390, align 8
  %392 = load i32* %n, align 4
  %393 = sext i32 %392 to i64
  %394 = call i32 @strncmp(i8* %391, i8* getelementptr inbounds ([7 x i8]* @.str144, i32 0, i32 0), i64 %393) #7
  %395 = icmp eq i32 %394, 0
  br i1 %395, label %396, label %411

; <label>:396                                     ; preds = %389
  %397 = load i32* %nArg, align 4
  %398 = icmp eq i32 %397, 2
  br i1 %398, label %399, label %407

; <label>:399                                     ; preds = %396
  %400 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %401 = load i8** %400, align 8
  %402 = call i32 @booleanValue(i8* %401)
  %403 = icmp ne i32 %402, 0
  br i1 %403, label %404, label %405

; <label>:404                                     ; preds = %399
  br label %406

; <label>:405                                     ; preds = %399
  br label %406

; <label>:406                                     ; preds = %405, %404
  br label %410

; <label>:407                                     ; preds = %396
  %408 = load %struct._IO_FILE** @stderr, align 8
  %409 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %408, i8* getelementptr inbounds ([23 x i8]* @.str145, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %410

; <label>:410                                     ; preds = %407, %406
  br label %3367

; <label>:411                                     ; preds = %389, %386, %383
  %412 = load i32* %c, align 4
  %413 = icmp eq i32 %412, 98
  br i1 %413, label %414, label %425

; <label>:414                                     ; preds = %411
  %415 = load i32* %n, align 4
  %416 = icmp sge i32 %415, 3
  br i1 %416, label %417, label %425

; <label>:417                                     ; preds = %414
  %418 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %419 = load i8** %418, align 8
  %420 = load i32* %n, align 4
  %421 = sext i32 %420 to i64
  %422 = call i32 @strncmp(i8* %419, i8* getelementptr inbounds ([11 x i8]* @.str146, i32 0, i32 0), i64 %421) #7
  %423 = icmp eq i32 %422, 0
  br i1 %423, label %424, label %425

; <label>:424                                     ; preds = %417
  call void @test_breakpoint()
  br label %3366

; <label>:425                                     ; preds = %417, %414, %411
  %426 = load i32* %c, align 4
  %427 = icmp eq i32 %426, 99
  br i1 %427, label %428, label %446

; <label>:428                                     ; preds = %425
  %429 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %430 = load i8** %429, align 8
  %431 = load i32* %n, align 4
  %432 = sext i32 %431 to i64
  %433 = call i32 @strncmp(i8* %430, i8* getelementptr inbounds ([6 x i8]* @.str147, i32 0, i32 0), i64 %432) #7
  %434 = icmp eq i32 %433, 0
  br i1 %434, label %435, label %446

; <label>:435                                     ; preds = %428
  %436 = load i32* %nArg, align 4
  %437 = icmp eq i32 %436, 2
  br i1 %437, label %438, label %442

; <label>:438                                     ; preds = %435
  %439 = load %struct.ShellState** %3, align 8
  %440 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %441 = load i8** %440, align 8
  call void @tryToClone(%struct.ShellState* %439, i8* %441)
  br label %445

; <label>:442                                     ; preds = %435
  %443 = load %struct._IO_FILE** @stderr, align 8
  %444 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %443, i8* getelementptr inbounds ([24 x i8]* @.str148, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %445

; <label>:445                                     ; preds = %442, %438
  br label %3365

; <label>:446                                     ; preds = %428, %425
  %447 = load i32* %c, align 4
  %448 = icmp eq i32 %447, 100
  br i1 %448, label %449, label %486

; <label>:449                                     ; preds = %446
  %450 = load i32* %n, align 4
  %451 = icmp sgt i32 %450, 1
  br i1 %451, label %452, label %486

; <label>:452                                     ; preds = %449
  %453 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %454 = load i8** %453, align 8
  %455 = load i32* %n, align 4
  %456 = sext i32 %455 to i64
  %457 = call i32 @strncmp(i8* %454, i8* getelementptr inbounds ([10 x i8]* @.str149, i32 0, i32 0), i64 %456) #7
  %458 = icmp eq i32 %457, 0
  br i1 %458, label %459, label %486

; <label>:459                                     ; preds = %452
  store i8* null, i8** %zErrMsg, align 8
  %460 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %460, i32 0)
  %461 = bitcast %struct.ShellState* %data to i8*
  %462 = load %struct.ShellState** %3, align 8
  %463 = bitcast %struct.ShellState* %462 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %461, i8* %463, i64 5512, i32 8, i1 false)
  %464 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 13
  store i32 1, i32* %464, align 4
  %465 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 11
  store i32 1, i32* %465, align 4
  %466 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 18
  %467 = getelementptr inbounds [100 x i32]* %466, i32 0, i64 0
  store i32 3, i32* %467, align 4
  %468 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 18
  %469 = getelementptr inbounds [100 x i32]* %468, i32 0, i64 1
  store i32 15, i32* %469, align 4
  %470 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 18
  %471 = getelementptr inbounds [100 x i32]* %470, i32 0, i64 2
  store i32 58, i32* %471, align 4
  %472 = getelementptr inbounds %struct.ShellState* %data, i32 0, i32 7
  store i32 0, i32* %472, align 4
  %473 = load %struct.ShellState** %3, align 8
  %474 = getelementptr inbounds %struct.ShellState* %473, i32 0, i32 0
  %475 = load %struct.sqlite3** %474, align 8
  %476 = bitcast %struct.ShellState* %data to i8*
  %477 = call i32 @sqlite3_exec(%struct.sqlite3* %475, i8* getelementptr inbounds ([23 x i8]* @.str150, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* @callback, i8* %476, i8** %zErrMsg)
  %478 = load i8** %zErrMsg, align 8
  %479 = icmp ne i8* %478, null
  br i1 %479, label %480, label %485

; <label>:480                                     ; preds = %459
  %481 = load %struct._IO_FILE** @stderr, align 8
  %482 = load i8** %zErrMsg, align 8
  %483 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %481, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %482)
  %484 = load i8** %zErrMsg, align 8
  call void @sqlite3_free(i8* %484)
  store i32 1, i32* %rc, align 4
  br label %485

; <label>:485                                     ; preds = %480, %459
  br label %3364

; <label>:486                                     ; preds = %452, %449, %446
  %487 = load i32* %c, align 4
  %488 = icmp eq i32 %487, 100
  br i1 %488, label %489, label %501

; <label>:489                                     ; preds = %486
  %490 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %491 = load i8** %490, align 8
  %492 = load i32* %n, align 4
  %493 = sext i32 %492 to i64
  %494 = call i32 @strncmp(i8* %491, i8* getelementptr inbounds ([7 x i8]* @.str151, i32 0, i32 0), i64 %493) #7
  %495 = icmp eq i32 %494, 0
  br i1 %495, label %496, label %501

; <label>:496                                     ; preds = %489
  %497 = load %struct.ShellState** %3, align 8
  %498 = load i32* %nArg, align 4
  %499 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i32 0
  %500 = call i32 @shell_dbinfo_command(%struct.ShellState* %497, i32 %498, i8** %499)
  store i32 %500, i32* %rc, align 4
  br label %3363

; <label>:501                                     ; preds = %489, %486
  %502 = load i32* %c, align 4
  %503 = icmp eq i32 %502, 100
  br i1 %503, label %504, label %595

; <label>:504                                     ; preds = %501
  %505 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %506 = load i8** %505, align 8
  %507 = load i32* %n, align 4
  %508 = sext i32 %507 to i64
  %509 = call i32 @strncmp(i8* %506, i8* getelementptr inbounds ([5 x i8]* @.str152, i32 0, i32 0), i64 %508) #7
  %510 = icmp eq i32 %509, 0
  br i1 %510, label %511, label %595

; <label>:511                                     ; preds = %504
  %512 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %512, i32 0)
  %513 = load i32* %nArg, align 4
  %514 = icmp ne i32 %513, 1
  br i1 %514, label %515, label %521

; <label>:515                                     ; preds = %511
  %516 = load i32* %nArg, align 4
  %517 = icmp ne i32 %516, 2
  br i1 %517, label %518, label %521

; <label>:518                                     ; preds = %515
  %519 = load %struct._IO_FILE** @stderr, align 8
  %520 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %519, i8* getelementptr inbounds ([29 x i8]* @.str153, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:521                                     ; preds = %515, %511
  %522 = load %struct.ShellState** %3, align 8
  %523 = getelementptr inbounds %struct.ShellState* %522, i32 0, i32 8
  %524 = load %struct._IO_FILE** %523, align 8
  %525 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %524, i8* getelementptr inbounds ([26 x i8]* @.str154, i32 0, i32 0))
  %526 = load %struct.ShellState** %3, align 8
  %527 = getelementptr inbounds %struct.ShellState* %526, i32 0, i32 8
  %528 = load %struct._IO_FILE** %527, align 8
  %529 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %528, i8* getelementptr inbounds ([20 x i8]* @.str155, i32 0, i32 0))
  %530 = load %struct.ShellState** %3, align 8
  %531 = getelementptr inbounds %struct.ShellState* %530, i32 0, i32 12
  store i32 0, i32* %531, align 4
  %532 = load %struct.ShellState** %3, align 8
  %533 = getelementptr inbounds %struct.ShellState* %532, i32 0, i32 0
  %534 = load %struct.sqlite3** %533, align 8
  %535 = call i32 @sqlite3_exec(%struct.sqlite3* %534, i8* getelementptr inbounds ([42 x i8]* @.str156, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  %536 = load %struct.ShellState** %3, align 8
  %537 = getelementptr inbounds %struct.ShellState* %536, i32 0, i32 10
  store i32 0, i32* %537, align 4
  %538 = load i32* %nArg, align 4
  %539 = icmp eq i32 %538, 1
  br i1 %539, label %540, label %547

; <label>:540                                     ; preds = %521
  %541 = load %struct.ShellState** %3, align 8
  %542 = call i32 @run_schema_dump_query(%struct.ShellState* %541, i8* getelementptr inbounds ([107 x i8]* @.str157, i32 0, i32 0))
  %543 = load %struct.ShellState** %3, align 8
  %544 = call i32 @run_schema_dump_query(%struct.ShellState* %543, i8* getelementptr inbounds ([72 x i8]* @.str158, i32 0, i32 0))
  %545 = load %struct.ShellState** %3, align 8
  %546 = call i32 @run_table_dump_query(%struct.ShellState* %545, i8* getelementptr inbounds ([88 x i8]* @.str159, i32 0, i32 0), i8* null)
  br label %565

; <label>:547                                     ; preds = %521
  store i32 1, i32* %i, align 4
  br label %548

; <label>:548                                     ; preds = %561, %547
  %549 = load i32* %i, align 4
  %550 = load i32* %nArg, align 4
  %551 = icmp slt i32 %549, %550
  br i1 %551, label %552, label %564

; <label>:552                                     ; preds = %548
  %553 = load i32* %i, align 4
  %554 = sext i32 %553 to i64
  %555 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %554
  %556 = load i8** %555, align 8
  store i8* %556, i8** @zShellStatic, align 8
  %557 = load %struct.ShellState** %3, align 8
  %558 = call i32 @run_schema_dump_query(%struct.ShellState* %557, i8* getelementptr inbounds ([112 x i8]* @.str160, i32 0, i32 0))
  %559 = load %struct.ShellState** %3, align 8
  %560 = call i32 @run_table_dump_query(%struct.ShellState* %559, i8* getelementptr inbounds ([122 x i8]* @.str161, i32 0, i32 0), i8* null)
  store i8* null, i8** @zShellStatic, align 8
  br label %561

; <label>:561                                     ; preds = %552
  %562 = load i32* %i, align 4
  %563 = add nsw i32 %562, 1
  store i32 %563, i32* %i, align 4
  br label %548

; <label>:564                                     ; preds = %548
  br label %565

; <label>:565                                     ; preds = %564, %540
  %566 = load %struct.ShellState** %3, align 8
  %567 = getelementptr inbounds %struct.ShellState* %566, i32 0, i32 12
  %568 = load i32* %567, align 4
  %569 = icmp ne i32 %568, 0
  br i1 %569, label %570, label %577

; <label>:570                                     ; preds = %565
  %571 = load %struct.ShellState** %3, align 8
  %572 = getelementptr inbounds %struct.ShellState* %571, i32 0, i32 8
  %573 = load %struct._IO_FILE** %572, align 8
  %574 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %573, i8* getelementptr inbounds ([29 x i8]* @.str162, i32 0, i32 0))
  %575 = load %struct.ShellState** %3, align 8
  %576 = getelementptr inbounds %struct.ShellState* %575, i32 0, i32 12
  store i32 0, i32* %576, align 4
  br label %577

; <label>:577                                     ; preds = %570, %565
  %578 = load %struct.ShellState** %3, align 8
  %579 = getelementptr inbounds %struct.ShellState* %578, i32 0, i32 0
  %580 = load %struct.sqlite3** %579, align 8
  %581 = call i32 @sqlite3_exec(%struct.sqlite3* %580, i8* getelementptr inbounds ([28 x i8]* @.str163, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  %582 = load %struct.ShellState** %3, align 8
  %583 = getelementptr inbounds %struct.ShellState* %582, i32 0, i32 0
  %584 = load %struct.sqlite3** %583, align 8
  %585 = call i32 @sqlite3_exec(%struct.sqlite3* %584, i8* getelementptr inbounds ([14 x i8]* @.str164, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  %586 = load %struct.ShellState** %3, align 8
  %587 = getelementptr inbounds %struct.ShellState* %586, i32 0, i32 8
  %588 = load %struct._IO_FILE** %587, align 8
  %589 = load %struct.ShellState** %3, align 8
  %590 = getelementptr inbounds %struct.ShellState* %589, i32 0, i32 10
  %591 = load i32* %590, align 4
  %592 = icmp ne i32 %591, 0
  %593 = select i1 %592, i8* getelementptr inbounds ([28 x i8]* @.str165, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8]* @.str166, i32 0, i32 0)
  %594 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %588, i8* %593)
  br label %3362

; <label>:595                                     ; preds = %504, %501
  %596 = load i32* %c, align 4
  %597 = icmp eq i32 %596, 101
  br i1 %597, label %598, label %618

; <label>:598                                     ; preds = %595
  %599 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %600 = load i8** %599, align 8
  %601 = load i32* %n, align 4
  %602 = sext i32 %601 to i64
  %603 = call i32 @strncmp(i8* %600, i8* getelementptr inbounds ([5 x i8]* @.str167, i32 0, i32 0), i64 %602) #7
  %604 = icmp eq i32 %603, 0
  br i1 %604, label %605, label %618

; <label>:605                                     ; preds = %598
  %606 = load i32* %nArg, align 4
  %607 = icmp eq i32 %606, 2
  br i1 %607, label %608, label %614

; <label>:608                                     ; preds = %605
  %609 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %610 = load i8** %609, align 8
  %611 = call i32 @booleanValue(i8* %610)
  %612 = load %struct.ShellState** %3, align 8
  %613 = getelementptr inbounds %struct.ShellState* %612, i32 0, i32 1
  store i32 %611, i32* %613, align 4
  br label %617

; <label>:614                                     ; preds = %605
  %615 = load %struct._IO_FILE** @stderr, align 8
  %616 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %615, i8* getelementptr inbounds ([21 x i8]* @.str168, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %617

; <label>:617                                     ; preds = %614, %608
  br label %3361

; <label>:618                                     ; preds = %598, %595
  %619 = load i32* %c, align 4
  %620 = icmp eq i32 %619, 101
  br i1 %620, label %621, label %641

; <label>:621                                     ; preds = %618
  %622 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %623 = load i8** %622, align 8
  %624 = load i32* %n, align 4
  %625 = sext i32 %624 to i64
  %626 = call i32 @strncmp(i8* %623, i8* getelementptr inbounds ([4 x i8]* @.str169, i32 0, i32 0), i64 %625) #7
  %627 = icmp eq i32 %626, 0
  br i1 %627, label %628, label %641

; <label>:628                                     ; preds = %621
  %629 = load i32* %nArg, align 4
  %630 = icmp eq i32 %629, 2
  br i1 %630, label %631, label %637

; <label>:631                                     ; preds = %628
  %632 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %633 = load i8** %632, align 8
  %634 = call i32 @booleanValue(i8* %633)
  %635 = load %struct.ShellState** %3, align 8
  %636 = getelementptr inbounds %struct.ShellState* %635, i32 0, i32 2
  store i32 %634, i32* %636, align 4
  br label %640

; <label>:637                                     ; preds = %628
  %638 = load %struct._IO_FILE** @stderr, align 8
  %639 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %638, i8* getelementptr inbounds ([20 x i8]* @.str170, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %640

; <label>:640                                     ; preds = %637, %631
  br label %3360

; <label>:641                                     ; preds = %621, %618
  %642 = load i32* %c, align 4
  %643 = icmp eq i32 %642, 101
  br i1 %643, label %644, label %663

; <label>:644                                     ; preds = %641
  %645 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %646 = load i8** %645, align 8
  %647 = load i32* %n, align 4
  %648 = sext i32 %647 to i64
  %649 = call i32 @strncmp(i8* %646, i8* getelementptr inbounds ([5 x i8]* @.str171, i32 0, i32 0), i64 %648) #7
  %650 = icmp eq i32 %649, 0
  br i1 %650, label %651, label %663

; <label>:651                                     ; preds = %644
  %652 = load i32* %nArg, align 4
  %653 = icmp sgt i32 %652, 1
  br i1 %653, label %654, label %662

; <label>:654                                     ; preds = %651
  %655 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %656 = load i8** %655, align 8
  %657 = call i64 @integerValue(i8* %656)
  %658 = trunc i64 %657 to i32
  store i32 %658, i32* %rc, align 4
  %659 = icmp ne i32 %658, 0
  br i1 %659, label %660, label %662

; <label>:660                                     ; preds = %654
  %661 = load i32* %rc, align 4
  call void @exit(i32 %661) #8
  unreachable

; <label>:662                                     ; preds = %654, %651
  store i32 2, i32* %rc, align 4
  br label %3359

; <label>:663                                     ; preds = %644, %641
  %664 = load i32* %c, align 4
  %665 = icmp eq i32 %664, 101
  br i1 %665, label %666, label %777

; <label>:666                                     ; preds = %663
  %667 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %668 = load i8** %667, align 8
  %669 = load i32* %n, align 4
  %670 = sext i32 %669 to i64
  %671 = call i32 @strncmp(i8* %668, i8* getelementptr inbounds ([8 x i8]* @.str134, i32 0, i32 0), i64 %670) #7
  %672 = icmp eq i32 %671, 0
  br i1 %672, label %673, label %777

; <label>:673                                     ; preds = %666
  %674 = load i32* %nArg, align 4
  %675 = icmp sge i32 %674, 2
  br i1 %675, label %676, label %680

; <label>:676                                     ; preds = %673
  %677 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %678 = load i8** %677, align 8
  %679 = call i32 @booleanValue(i8* %678)
  br label %681

; <label>:680                                     ; preds = %673
  br label %681

; <label>:681                                     ; preds = %680, %676
  %682 = phi i32 [ %679, %676 ], [ 1, %680 ]
  store i32 %682, i32* %val, align 4
  %683 = load i32* %val, align 4
  %684 = icmp eq i32 %683, 1
  br i1 %684, label %685, label %746

; <label>:685                                     ; preds = %681
  %686 = load %struct.ShellState** %3, align 8
  %687 = getelementptr inbounds %struct.ShellState* %686, i32 0, i32 21
  %688 = getelementptr inbounds %struct.SavedModeInfo* %687, i32 0, i32 0
  %689 = load i32* %688, align 4
  %690 = icmp ne i32 %689, 0
  br i1 %690, label %714, label %691

; <label>:691                                     ; preds = %685
  %692 = load %struct.ShellState** %3, align 8
  %693 = getelementptr inbounds %struct.ShellState* %692, i32 0, i32 21
  %694 = getelementptr inbounds %struct.SavedModeInfo* %693, i32 0, i32 0
  store i32 1, i32* %694, align 4
  %695 = load %struct.ShellState** %3, align 8
  %696 = getelementptr inbounds %struct.ShellState* %695, i32 0, i32 11
  %697 = load i32* %696, align 4
  %698 = load %struct.ShellState** %3, align 8
  %699 = getelementptr inbounds %struct.ShellState* %698, i32 0, i32 21
  %700 = getelementptr inbounds %struct.SavedModeInfo* %699, i32 0, i32 1
  store i32 %697, i32* %700, align 4
  %701 = load %struct.ShellState** %3, align 8
  %702 = getelementptr inbounds %struct.ShellState* %701, i32 0, i32 13
  %703 = load i32* %702, align 4
  %704 = load %struct.ShellState** %3, align 8
  %705 = getelementptr inbounds %struct.ShellState* %704, i32 0, i32 21
  %706 = getelementptr inbounds %struct.SavedModeInfo* %705, i32 0, i32 2
  store i32 %703, i32* %706, align 4
  %707 = load %struct.ShellState** %3, align 8
  %708 = getelementptr inbounds %struct.ShellState* %707, i32 0, i32 21
  %709 = getelementptr inbounds %struct.SavedModeInfo* %708, i32 0, i32 3
  %710 = bitcast [100 x i32]* %709 to i8*
  %711 = load %struct.ShellState** %3, align 8
  %712 = getelementptr inbounds %struct.ShellState* %711, i32 0, i32 18
  %713 = bitcast [100 x i32]* %712 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %710, i8* %713, i64 400, i32 4, i1 false)
  br label %714

; <label>:714                                     ; preds = %691, %685
  %715 = load %struct.ShellState** %3, align 8
  %716 = getelementptr inbounds %struct.ShellState* %715, i32 0, i32 11
  store i32 8, i32* %716, align 4
  %717 = load %struct.ShellState** %3, align 8
  %718 = getelementptr inbounds %struct.ShellState* %717, i32 0, i32 13
  store i32 1, i32* %718, align 4
  %719 = load %struct.ShellState** %3, align 8
  %720 = getelementptr inbounds %struct.ShellState* %719, i32 0, i32 18
  %721 = bitcast [100 x i32]* %720 to i8*
  call void @llvm.memset.p0i8.i64(i8* %721, i8 0, i64 400, i32 8, i1 false)
  %722 = load %struct.ShellState** %3, align 8
  %723 = getelementptr inbounds %struct.ShellState* %722, i32 0, i32 18
  %724 = getelementptr inbounds [100 x i32]* %723, i32 0, i64 0
  store i32 4, i32* %724, align 4
  %725 = load %struct.ShellState** %3, align 8
  %726 = getelementptr inbounds %struct.ShellState* %725, i32 0, i32 18
  %727 = getelementptr inbounds [100 x i32]* %726, i32 0, i64 1
  store i32 13, i32* %727, align 4
  %728 = load %struct.ShellState** %3, align 8
  %729 = getelementptr inbounds %struct.ShellState* %728, i32 0, i32 18
  %730 = getelementptr inbounds [100 x i32]* %729, i32 0, i64 2
  store i32 4, i32* %730, align 4
  %731 = load %struct.ShellState** %3, align 8
  %732 = getelementptr inbounds %struct.ShellState* %731, i32 0, i32 18
  %733 = getelementptr inbounds [100 x i32]* %732, i32 0, i64 3
  store i32 4, i32* %733, align 4
  %734 = load %struct.ShellState** %3, align 8
  %735 = getelementptr inbounds %struct.ShellState* %734, i32 0, i32 18
  %736 = getelementptr inbounds [100 x i32]* %735, i32 0, i64 4
  store i32 4, i32* %736, align 4
  %737 = load %struct.ShellState** %3, align 8
  %738 = getelementptr inbounds %struct.ShellState* %737, i32 0, i32 18
  %739 = getelementptr inbounds [100 x i32]* %738, i32 0, i64 5
  store i32 13, i32* %739, align 4
  %740 = load %struct.ShellState** %3, align 8
  %741 = getelementptr inbounds %struct.ShellState* %740, i32 0, i32 18
  %742 = getelementptr inbounds [100 x i32]* %741, i32 0, i64 6
  store i32 2, i32* %742, align 4
  %743 = load %struct.ShellState** %3, align 8
  %744 = getelementptr inbounds %struct.ShellState* %743, i32 0, i32 18
  %745 = getelementptr inbounds [100 x i32]* %744, i32 0, i64 7
  store i32 13, i32* %745, align 4
  br label %776

; <label>:746                                     ; preds = %681
  %747 = load %struct.ShellState** %3, align 8
  %748 = getelementptr inbounds %struct.ShellState* %747, i32 0, i32 21
  %749 = getelementptr inbounds %struct.SavedModeInfo* %748, i32 0, i32 0
  %750 = load i32* %749, align 4
  %751 = icmp ne i32 %750, 0
  br i1 %751, label %752, label %775

; <label>:752                                     ; preds = %746
  %753 = load %struct.ShellState** %3, align 8
  %754 = getelementptr inbounds %struct.ShellState* %753, i32 0, i32 21
  %755 = getelementptr inbounds %struct.SavedModeInfo* %754, i32 0, i32 0
  store i32 0, i32* %755, align 4
  %756 = load %struct.ShellState** %3, align 8
  %757 = getelementptr inbounds %struct.ShellState* %756, i32 0, i32 21
  %758 = getelementptr inbounds %struct.SavedModeInfo* %757, i32 0, i32 1
  %759 = load i32* %758, align 4
  %760 = load %struct.ShellState** %3, align 8
  %761 = getelementptr inbounds %struct.ShellState* %760, i32 0, i32 11
  store i32 %759, i32* %761, align 4
  %762 = load %struct.ShellState** %3, align 8
  %763 = getelementptr inbounds %struct.ShellState* %762, i32 0, i32 21
  %764 = getelementptr inbounds %struct.SavedModeInfo* %763, i32 0, i32 2
  %765 = load i32* %764, align 4
  %766 = load %struct.ShellState** %3, align 8
  %767 = getelementptr inbounds %struct.ShellState* %766, i32 0, i32 13
  store i32 %765, i32* %767, align 4
  %768 = load %struct.ShellState** %3, align 8
  %769 = getelementptr inbounds %struct.ShellState* %768, i32 0, i32 18
  %770 = bitcast [100 x i32]* %769 to i8*
  %771 = load %struct.ShellState** %3, align 8
  %772 = getelementptr inbounds %struct.ShellState* %771, i32 0, i32 21
  %773 = getelementptr inbounds %struct.SavedModeInfo* %772, i32 0, i32 3
  %774 = bitcast [100 x i32]* %773 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %770, i8* %774, i64 400, i32 4, i1 false)
  br label %775

; <label>:775                                     ; preds = %752, %746
  br label %776

; <label>:776                                     ; preds = %775, %714
  br label %3358

; <label>:777                                     ; preds = %666, %663
  %778 = load i32* %c, align 4
  %779 = icmp eq i32 %778, 102
  br i1 %779, label %780, label %857

; <label>:780                                     ; preds = %777
  %781 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %782 = load i8** %781, align 8
  %783 = load i32* %n, align 4
  %784 = sext i32 %783 to i64
  %785 = call i32 @strncmp(i8* %782, i8* getelementptr inbounds ([11 x i8]* @.str172, i32 0, i32 0), i64 %784) #7
  %786 = icmp eq i32 %785, 0
  br i1 %786, label %787, label %857

; <label>:787                                     ; preds = %780
  store i8* null, i8** %zErrMsg2, align 8
  store i32 0, i32* %doStats, align 4
  %788 = load i32* %nArg, align 4
  %789 = icmp ne i32 %788, 1
  br i1 %789, label %790, label %793

; <label>:790                                     ; preds = %787
  %791 = load %struct._IO_FILE** @stderr, align 8
  %792 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %791, i8* getelementptr inbounds ([20 x i8]* @.str173, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:793                                     ; preds = %787
  %794 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %794, i32 0)
  %795 = bitcast %struct.ShellState* %data1 to i8*
  %796 = load %struct.ShellState** %3, align 8
  %797 = bitcast %struct.ShellState* %796 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %795, i8* %797, i64 5512, i32 8, i1 false)
  %798 = getelementptr inbounds %struct.ShellState* %data1, i32 0, i32 13
  store i32 0, i32* %798, align 4
  %799 = getelementptr inbounds %struct.ShellState* %data1, i32 0, i32 11
  store i32 3, i32* %799, align 4
  %800 = load %struct.ShellState** %3, align 8
  %801 = getelementptr inbounds %struct.ShellState* %800, i32 0, i32 0
  %802 = load %struct.sqlite3** %801, align 8
  %803 = bitcast %struct.ShellState* %data1 to i8*
  %804 = call i32 @sqlite3_exec(%struct.sqlite3* %802, i8* getelementptr inbounds ([262 x i8]* @.str174, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* @callback, i8* %803, i8** %zErrMsg2)
  store i32 %804, i32* %rc, align 4
  %805 = load i32* %rc, align 4
  %806 = icmp eq i32 %805, 0
  br i1 %806, label %807, label %818

; <label>:807                                     ; preds = %793
  %808 = load %struct.ShellState** %3, align 8
  %809 = getelementptr inbounds %struct.ShellState* %808, i32 0, i32 0
  %810 = load %struct.sqlite3** %809, align 8
  %811 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %810, i8* getelementptr inbounds ([67 x i8]* @.str175, i32 0, i32 0), i32 -1, %struct.sqlite3_stmt** %pStmt, i8** null)
  store i32 %811, i32* %rc, align 4
  %812 = load %struct.sqlite3_stmt** %pStmt, align 8
  %813 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %812)
  %814 = icmp eq i32 %813, 100
  %815 = zext i1 %814 to i32
  store i32 %815, i32* %doStats, align 4
  %816 = load %struct.sqlite3_stmt** %pStmt, align 8
  %817 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %816)
  br label %818

; <label>:818                                     ; preds = %807, %793
  %819 = load i32* %doStats, align 4
  %820 = icmp eq i32 %819, 0
  br i1 %820, label %821, label %826

; <label>:821                                     ; preds = %818
  %822 = load %struct.ShellState** %3, align 8
  %823 = getelementptr inbounds %struct.ShellState* %822, i32 0, i32 8
  %824 = load %struct._IO_FILE** %823, align 8
  %825 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %824, i8* getelementptr inbounds ([32 x i8]* @.str176, i32 0, i32 0))
  br label %856

; <label>:826                                     ; preds = %818
  %827 = load %struct.ShellState** %3, align 8
  %828 = getelementptr inbounds %struct.ShellState* %827, i32 0, i32 8
  %829 = load %struct._IO_FILE** %828, align 8
  %830 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %829, i8* getelementptr inbounds ([24 x i8]* @.str177, i32 0, i32 0))
  %831 = load %struct.ShellState** %3, align 8
  %832 = getelementptr inbounds %struct.ShellState* %831, i32 0, i32 0
  %833 = load %struct.sqlite3** %832, align 8
  %834 = bitcast %struct.ShellState* %data1 to i8*
  %835 = call i32 @sqlite3_exec(%struct.sqlite3* %833, i8* getelementptr inbounds ([31 x i8]* @.str178, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* @callback, i8* %834, i8** %zErrMsg2)
  %836 = getelementptr inbounds %struct.ShellState* %data1, i32 0, i32 11
  store i32 5, i32* %836, align 4
  %837 = getelementptr inbounds %struct.ShellState* %data1, i32 0, i32 15
  store i8* getelementptr inbounds ([13 x i8]* @.str179, i32 0, i32 0), i8** %837, align 8
  %838 = load %struct.ShellState** %3, align 8
  %839 = getelementptr inbounds %struct.ShellState* %838, i32 0, i32 0
  %840 = load %struct.sqlite3** %839, align 8
  %841 = call i32 @shell_exec(%struct.sqlite3* %840, i8* getelementptr inbounds ([27 x i8]* @.str180, i32 0, i32 0), i32 (i8*, i32, i8**, i8**, i32*)* @shell_callback, %struct.ShellState* %data1, i8** %zErrMsg2)
  %842 = getelementptr inbounds %struct.ShellState* %data1, i32 0, i32 15
  store i8* getelementptr inbounds ([13 x i8]* @.str181, i32 0, i32 0), i8** %842, align 8
  %843 = load %struct.ShellState** %3, align 8
  %844 = getelementptr inbounds %struct.ShellState* %843, i32 0, i32 0
  %845 = load %struct.sqlite3** %844, align 8
  %846 = call i32 @shell_exec(%struct.sqlite3* %845, i8* getelementptr inbounds ([27 x i8]* @.str182, i32 0, i32 0), i32 (i8*, i32, i8**, i8**, i32*)* @shell_callback, %struct.ShellState* %data1, i8** %zErrMsg2)
  %847 = getelementptr inbounds %struct.ShellState* %data1, i32 0, i32 15
  store i8* getelementptr inbounds ([13 x i8]* @.str183, i32 0, i32 0), i8** %847, align 8
  %848 = load %struct.ShellState** %3, align 8
  %849 = getelementptr inbounds %struct.ShellState* %848, i32 0, i32 0
  %850 = load %struct.sqlite3** %849, align 8
  %851 = call i32 @shell_exec(%struct.sqlite3* %850, i8* getelementptr inbounds ([27 x i8]* @.str184, i32 0, i32 0), i32 (i8*, i32, i8**, i8**, i32*)* @shell_callback, %struct.ShellState* %data1, i8** %zErrMsg2)
  %852 = load %struct.ShellState** %3, align 8
  %853 = getelementptr inbounds %struct.ShellState* %852, i32 0, i32 8
  %854 = load %struct._IO_FILE** %853, align 8
  %855 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %854, i8* getelementptr inbounds ([24 x i8]* @.str177, i32 0, i32 0))
  br label %856

; <label>:856                                     ; preds = %826, %821
  br label %3357

; <label>:857                                     ; preds = %780, %777
  %858 = load i32* %c, align 4
  %859 = icmp eq i32 %858, 104
  br i1 %859, label %860, label %880

; <label>:860                                     ; preds = %857
  %861 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %862 = load i8** %861, align 8
  %863 = load i32* %n, align 4
  %864 = sext i32 %863 to i64
  %865 = call i32 @strncmp(i8* %862, i8* getelementptr inbounds ([8 x i8]* @.str185, i32 0, i32 0), i64 %864) #7
  %866 = icmp eq i32 %865, 0
  br i1 %866, label %867, label %880

; <label>:867                                     ; preds = %860
  %868 = load i32* %nArg, align 4
  %869 = icmp eq i32 %868, 2
  br i1 %869, label %870, label %876

; <label>:870                                     ; preds = %867
  %871 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %872 = load i8** %871, align 8
  %873 = call i32 @booleanValue(i8* %872)
  %874 = load %struct.ShellState** %3, align 8
  %875 = getelementptr inbounds %struct.ShellState* %874, i32 0, i32 13
  store i32 %873, i32* %875, align 4
  br label %879

; <label>:876                                     ; preds = %867
  %877 = load %struct._IO_FILE** @stderr, align 8
  %878 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %877, i8* getelementptr inbounds ([24 x i8]* @.str186, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %879

; <label>:879                                     ; preds = %876, %870
  br label %3356

; <label>:880                                     ; preds = %860, %857
  %881 = load i32* %c, align 4
  %882 = icmp eq i32 %881, 104
  br i1 %882, label %883, label %895

; <label>:883                                     ; preds = %880
  %884 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %885 = load i8** %884, align 8
  %886 = load i32* %n, align 4
  %887 = sext i32 %886 to i64
  %888 = call i32 @strncmp(i8* %885, i8* getelementptr inbounds ([5 x i8]* @.str187, i32 0, i32 0), i64 %887) #7
  %889 = icmp eq i32 %888, 0
  br i1 %889, label %890, label %895

; <label>:890                                     ; preds = %883
  %891 = load %struct.ShellState** %3, align 8
  %892 = getelementptr inbounds %struct.ShellState* %891, i32 0, i32 8
  %893 = load %struct._IO_FILE** %892, align 8
  %894 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %893, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* getelementptr inbounds ([3763 x i8]* @zHelp, i32 0, i32 0))
  br label %3355

; <label>:895                                     ; preds = %883, %880
  %896 = load i32* %c, align 4
  %897 = icmp eq i32 %896, 105
  br i1 %897, label %898, label %1385

; <label>:898                                     ; preds = %895
  %899 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %900 = load i8** %899, align 8
  %901 = load i32* %n, align 4
  %902 = sext i32 %901 to i64
  %903 = call i32 @strncmp(i8* %900, i8* getelementptr inbounds ([7 x i8]* @.str188, i32 0, i32 0), i64 %902) #7
  %904 = icmp eq i32 %903, 0
  br i1 %904, label %905, label %1385

; <label>:905                                     ; preds = %898
  store %struct.sqlite3_stmt* null, %struct.sqlite3_stmt** %pStmt3, align 8
  %906 = load i32* %nArg, align 4
  %907 = icmp ne i32 %906, 3
  br i1 %907, label %908, label %911

; <label>:908                                     ; preds = %905
  %909 = load %struct._IO_FILE** @stderr, align 8
  %910 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %909, i8* getelementptr inbounds ([27 x i8]* @.str189, i32 0, i32 0))
  br label %3370

; <label>:911                                     ; preds = %905
  %912 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %913 = load i8** %912, align 8
  store i8* %913, i8** %zFile, align 8
  %914 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %915 = load i8** %914, align 8
  store i8* %915, i8** %zTable, align 8
  store volatile i32 0, i32* @seenInterrupt, align 4
  %916 = bitcast %struct.ImportCtx* %sCtx to i8*
  call void @llvm.memset.p0i8.i64(i8* %916, i8 0, i64 48, i32 8, i1 false)
  %917 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %917, i32 0)
  %918 = load %struct.ShellState** %3, align 8
  %919 = getelementptr inbounds %struct.ShellState* %918, i32 0, i32 16
  %920 = getelementptr inbounds [20 x i8]* %919, i32 0, i32 0
  %921 = call i32 @strlen30(i8* %920)
  store i32 %921, i32* %nSep, align 4
  %922 = load i32* %nSep, align 4
  %923 = icmp eq i32 %922, 0
  br i1 %923, label %924, label %927

; <label>:924                                     ; preds = %911
  %925 = load %struct._IO_FILE** @stderr, align 8
  %926 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %925, i8* getelementptr inbounds ([54 x i8]* @.str190, i32 0, i32 0))
  store i32 1, i32* %1
  br label %3389

; <label>:927                                     ; preds = %911
  %928 = load i32* %nSep, align 4
  %929 = icmp sgt i32 %928, 1
  br i1 %929, label %930, label %933

; <label>:930                                     ; preds = %927
  %931 = load %struct._IO_FILE** @stderr, align 8
  %932 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %931, i8* getelementptr inbounds ([65 x i8]* @.str191, i32 0, i32 0))
  store i32 1, i32* %1
  br label %3389

; <label>:933                                     ; preds = %927
  %934 = load %struct.ShellState** %3, align 8
  %935 = getelementptr inbounds %struct.ShellState* %934, i32 0, i32 17
  %936 = getelementptr inbounds [20 x i8]* %935, i32 0, i32 0
  %937 = call i32 @strlen30(i8* %936)
  store i32 %937, i32* %nSep, align 4
  %938 = load i32* %nSep, align 4
  %939 = icmp eq i32 %938, 0
  br i1 %939, label %940, label %943

; <label>:940                                     ; preds = %933
  %941 = load %struct._IO_FILE** @stderr, align 8
  %942 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %941, i8* getelementptr inbounds ([51 x i8]* @.str192, i32 0, i32 0))
  store i32 1, i32* %1
  br label %3389

; <label>:943                                     ; preds = %933
  %944 = load i32* %nSep, align 4
  %945 = icmp eq i32 %944, 2
  br i1 %945, label %946, label %966

; <label>:946                                     ; preds = %943
  %947 = load %struct.ShellState** %3, align 8
  %948 = getelementptr inbounds %struct.ShellState* %947, i32 0, i32 11
  %949 = load i32* %948, align 4
  %950 = icmp eq i32 %949, 7
  br i1 %950, label %951, label %966

; <label>:951                                     ; preds = %946
  %952 = load %struct.ShellState** %3, align 8
  %953 = getelementptr inbounds %struct.ShellState* %952, i32 0, i32 17
  %954 = getelementptr inbounds [20 x i8]* %953, i32 0, i32 0
  %955 = call i32 @strcmp(i8* %954, i8* getelementptr inbounds ([3 x i8]* @.str193, i32 0, i32 0)) #7
  %956 = icmp eq i32 %955, 0
  br i1 %956, label %957, label %966

; <label>:957                                     ; preds = %951
  %958 = load %struct.ShellState** %3, align 8
  %959 = getelementptr inbounds %struct.ShellState* %958, i32 0, i32 17
  %960 = getelementptr inbounds [20 x i8]* %959, i32 0, i32 0
  %961 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %960, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  %962 = load %struct.ShellState** %3, align 8
  %963 = getelementptr inbounds %struct.ShellState* %962, i32 0, i32 17
  %964 = getelementptr inbounds [20 x i8]* %963, i32 0, i32 0
  %965 = call i32 @strlen30(i8* %964)
  store i32 %965, i32* %nSep, align 4
  br label %966

; <label>:966                                     ; preds = %957, %951, %946, %943
  %967 = load i32* %nSep, align 4
  %968 = icmp sgt i32 %967, 1
  br i1 %968, label %969, label %972

; <label>:969                                     ; preds = %966
  %970 = load %struct._IO_FILE** @stderr, align 8
  %971 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %970, i8* getelementptr inbounds ([62 x i8]* @.str194, i32 0, i32 0))
  store i32 1, i32* %1
  br label %3389

; <label>:972                                     ; preds = %966
  %973 = load i8** %zFile, align 8
  %974 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  store i8* %973, i8** %974, align 8
  %975 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 5
  store i32 1, i32* %975, align 4
  %976 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  %977 = load i8** %976, align 8
  %978 = getelementptr inbounds i8* %977, i64 0
  %979 = load i8* %978, align 1
  %980 = sext i8 %979 to i32
  %981 = icmp eq i32 %980, 124
  br i1 %981, label %982, label %989

; <label>:982                                     ; preds = %972
  %983 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  %984 = load i8** %983, align 8
  %985 = getelementptr inbounds i8* %984, i64 1
  %986 = call %struct._IO_FILE* @popen(i8* %985, i8* getelementptr inbounds ([2 x i8]* @.str195, i32 0, i32 0))
  %987 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  store %struct._IO_FILE* %986, %struct._IO_FILE** %987, align 8
  %988 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  store i8* getelementptr inbounds ([7 x i8]* @.str196, i32 0, i32 0), i8** %988, align 8
  store i32 (%struct._IO_FILE*)* @pclose, i32 (%struct._IO_FILE*)** %xCloser, align 8
  br label %994

; <label>:989                                     ; preds = %972
  %990 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  %991 = load i8** %990, align 8
  %992 = call %struct._IO_FILE* @fopen64(i8* %991, i8* getelementptr inbounds ([3 x i8]* @.str197, i32 0, i32 0))
  %993 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  store %struct._IO_FILE* %992, %struct._IO_FILE** %993, align 8
  store i32 (%struct._IO_FILE*)* @fclose, i32 (%struct._IO_FILE*)** %xCloser, align 8
  br label %994

; <label>:994                                     ; preds = %989, %982
  %995 = load %struct.ShellState** %3, align 8
  %996 = getelementptr inbounds %struct.ShellState* %995, i32 0, i32 11
  %997 = load i32* %996, align 4
  %998 = icmp eq i32 %997, 9
  br i1 %998, label %999, label %1000

; <label>:999                                     ; preds = %994
  store i8* (%struct.ImportCtx*)* @ascii_read_one_field, i8* (%struct.ImportCtx*)** %xRead, align 8
  br label %1001

; <label>:1000                                    ; preds = %994
  store i8* (%struct.ImportCtx*)* @csv_read_one_field, i8* (%struct.ImportCtx*)** %xRead, align 8
  br label %1001

; <label>:1001                                    ; preds = %1000, %999
  %1002 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  %1003 = load %struct._IO_FILE** %1002, align 8
  %1004 = icmp eq %struct._IO_FILE* %1003, null
  br i1 %1004, label %1005, label %1009

; <label>:1005                                    ; preds = %1001
  %1006 = load %struct._IO_FILE** @stderr, align 8
  %1007 = load i8** %zFile, align 8
  %1008 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1006, i8* getelementptr inbounds ([25 x i8]* @.str141, i32 0, i32 0), i8* %1007)
  store i32 1, i32* %1
  br label %3389

; <label>:1009                                    ; preds = %1001
  %1010 = load %struct.ShellState** %3, align 8
  %1011 = getelementptr inbounds %struct.ShellState* %1010, i32 0, i32 16
  %1012 = getelementptr inbounds [20 x i8]* %1011, i32 0, i64 0
  %1013 = load i8* %1012, align 1
  %1014 = sext i8 %1013 to i32
  %1015 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 7
  store i32 %1014, i32* %1015, align 4
  %1016 = load %struct.ShellState** %3, align 8
  %1017 = getelementptr inbounds %struct.ShellState* %1016, i32 0, i32 17
  %1018 = getelementptr inbounds [20 x i8]* %1017, i32 0, i64 0
  %1019 = load i8* %1018, align 1
  %1020 = sext i8 %1019 to i32
  %1021 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 8
  store i32 %1020, i32* %1021, align 4
  %1022 = load i8** %zTable, align 8
  %1023 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([17 x i8]* @.str198, i32 0, i32 0), i8* %1022)
  store i8* %1023, i8** %zSql, align 8
  %1024 = load i8** %zSql, align 8
  %1025 = icmp eq i8* %1024, null
  br i1 %1025, label %1026, label %1033

; <label>:1026                                    ; preds = %1009
  %1027 = load %struct._IO_FILE** @stderr, align 8
  %1028 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1027, i8* getelementptr inbounds ([22 x i8]* @.str49, i32 0, i32 0))
  %1029 = load i32 (%struct._IO_FILE*)** %xCloser, align 8
  %1030 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  %1031 = load %struct._IO_FILE** %1030, align 8
  %1032 = call i32 %1029(%struct._IO_FILE* %1031)
  store i32 1, i32* %1
  br label %3389

; <label>:1033                                    ; preds = %1009
  %1034 = load i8** %zSql, align 8
  %1035 = call i32 @strlen30(i8* %1034)
  store i32 %1035, i32* %nByte, align 4
  %1036 = load %struct.ShellState** %3, align 8
  %1037 = getelementptr inbounds %struct.ShellState* %1036, i32 0, i32 0
  %1038 = load %struct.sqlite3** %1037, align 8
  %1039 = load i8** %zSql, align 8
  %1040 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %1038, i8* %1039, i32 -1, %struct.sqlite3_stmt** %pStmt3, i8** null)
  store i32 %1040, i32* %rc, align 4
  call void @import_append_char(%struct.ImportCtx* %sCtx, i32 0)
  %1041 = load i32* %rc, align 4
  %1042 = icmp ne i32 %1041, 0
  br i1 %1042, label %1043, label %1118

; <label>:1043                                    ; preds = %1033
  %1044 = load %struct.ShellState** %3, align 8
  %1045 = getelementptr inbounds %struct.ShellState* %1044, i32 0, i32 0
  %1046 = load %struct.sqlite3** %1045, align 8
  %1047 = call i8* @sqlite3_errmsg(%struct.sqlite3* %1046)
  %1048 = call i32 @sqlite3_strglob(i8* getelementptr inbounds ([17 x i8]* @.str199, i32 0, i32 0), i8* %1047)
  %1049 = icmp eq i32 %1048, 0
  br i1 %1049, label %1050, label %1118

; <label>:1050                                    ; preds = %1043
  %1051 = load i8** %zTable, align 8
  %1052 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([16 x i8]* @.str200, i32 0, i32 0), i8* %1051)
  store i8* %1052, i8** %zCreate, align 8
  store i8 40, i8* %cSep, align 1
  br label %1053

; <label>:1053                                    ; preds = %1070, %1050
  %1054 = load i8* (%struct.ImportCtx*)** %xRead, align 8
  %1055 = call i8* %1054(%struct.ImportCtx* %sCtx)
  %1056 = icmp ne i8* %1055, null
  br i1 %1056, label %1057, label %1071

; <label>:1057                                    ; preds = %1053
  %1058 = load i8** %zCreate, align 8
  %1059 = load i8* %cSep, align 1
  %1060 = sext i8 %1059 to i32
  %1061 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 2
  %1062 = load i8** %1061, align 8
  %1063 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([17 x i8]* @.str201, i32 0, i32 0), i8* %1058, i32 %1060, i8* %1062)
  store i8* %1063, i8** %zCreate, align 8
  store i8 44, i8* %cSep, align 1
  %1064 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 6
  %1065 = load i32* %1064, align 4
  %1066 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 7
  %1067 = load i32* %1066, align 4
  %1068 = icmp ne i32 %1065, %1067
  br i1 %1068, label %1069, label %1070

; <label>:1069                                    ; preds = %1057
  br label %1071

; <label>:1070                                    ; preds = %1057
  br label %1053

; <label>:1071                                    ; preds = %1069, %1053
  %1072 = load i8* %cSep, align 1
  %1073 = sext i8 %1072 to i32
  %1074 = icmp eq i32 %1073, 40
  br i1 %1074, label %1075, label %1087

; <label>:1075                                    ; preds = %1071
  %1076 = load i8** %zCreate, align 8
  call void @sqlite3_free(i8* %1076)
  %1077 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 2
  %1078 = load i8** %1077, align 8
  call void @sqlite3_free(i8* %1078)
  %1079 = load i32 (%struct._IO_FILE*)** %xCloser, align 8
  %1080 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  %1081 = load %struct._IO_FILE** %1080, align 8
  %1082 = call i32 %1079(%struct._IO_FILE* %1081)
  %1083 = load %struct._IO_FILE** @stderr, align 8
  %1084 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  %1085 = load i8** %1084, align 8
  %1086 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1083, i8* getelementptr inbounds ([16 x i8]* @.str202, i32 0, i32 0), i8* %1085)
  store i32 1, i32* %1
  br label %3389

; <label>:1087                                    ; preds = %1071
  %1088 = load i8** %zCreate, align 8
  %1089 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([5 x i8]* @.str203, i32 0, i32 0), i8* %1088)
  store i8* %1089, i8** %zCreate, align 8
  %1090 = load %struct.ShellState** %3, align 8
  %1091 = getelementptr inbounds %struct.ShellState* %1090, i32 0, i32 0
  %1092 = load %struct.sqlite3** %1091, align 8
  %1093 = load i8** %zCreate, align 8
  %1094 = call i32 @sqlite3_exec(%struct.sqlite3* %1092, i8* %1093, i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  store i32 %1094, i32* %rc, align 4
  %1095 = load i8** %zCreate, align 8
  call void @sqlite3_free(i8* %1095)
  %1096 = load i32* %rc, align 4
  %1097 = icmp ne i32 %1096, 0
  br i1 %1097, label %1098, label %1112

; <label>:1098                                    ; preds = %1087
  %1099 = load %struct._IO_FILE** @stderr, align 8
  %1100 = load i8** %zTable, align 8
  %1101 = load %struct.ShellState** %3, align 8
  %1102 = getelementptr inbounds %struct.ShellState* %1101, i32 0, i32 0
  %1103 = load %struct.sqlite3** %1102, align 8
  %1104 = call i8* @sqlite3_errmsg(%struct.sqlite3* %1103)
  %1105 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1099, i8* getelementptr inbounds ([33 x i8]* @.str204, i32 0, i32 0), i8* %1100, i8* %1104)
  %1106 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 2
  %1107 = load i8** %1106, align 8
  call void @sqlite3_free(i8* %1107)
  %1108 = load i32 (%struct._IO_FILE*)** %xCloser, align 8
  %1109 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  %1110 = load %struct._IO_FILE** %1109, align 8
  %1111 = call i32 %1108(%struct._IO_FILE* %1110)
  store i32 1, i32* %1
  br label %3389

; <label>:1112                                    ; preds = %1087
  %1113 = load %struct.ShellState** %3, align 8
  %1114 = getelementptr inbounds %struct.ShellState* %1113, i32 0, i32 0
  %1115 = load %struct.sqlite3** %1114, align 8
  %1116 = load i8** %zSql, align 8
  %1117 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %1115, i8* %1116, i32 -1, %struct.sqlite3_stmt** %pStmt3, i8** null)
  store i32 %1117, i32* %rc, align 4
  br label %1118

; <label>:1118                                    ; preds = %1112, %1043, %1033
  %1119 = load i8** %zSql, align 8
  call void @sqlite3_free(i8* %1119)
  %1120 = load i32* %rc, align 4
  %1121 = icmp ne i32 %1120, 0
  br i1 %1121, label %1122, label %1139

; <label>:1122                                    ; preds = %1118
  %1123 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1124 = icmp ne %struct.sqlite3_stmt* %1123, null
  br i1 %1124, label %1125, label %1128

; <label>:1125                                    ; preds = %1122
  %1126 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1127 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %1126)
  br label %1128

; <label>:1128                                    ; preds = %1125, %1122
  %1129 = load %struct._IO_FILE** @stderr, align 8
  %1130 = load %struct.ShellState** %3, align 8
  %1131 = getelementptr inbounds %struct.ShellState* %1130, i32 0, i32 0
  %1132 = load %struct.sqlite3** %1131, align 8
  %1133 = call i8* @sqlite3_errmsg(%struct.sqlite3* %1132)
  %1134 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1129, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %1133)
  %1135 = load i32 (%struct._IO_FILE*)** %xCloser, align 8
  %1136 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  %1137 = load %struct._IO_FILE** %1136, align 8
  %1138 = call i32 %1135(%struct._IO_FILE* %1137)
  store i32 1, i32* %1
  br label %3389

; <label>:1139                                    ; preds = %1118
  %1140 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1141 = call i32 @sqlite3_column_count(%struct.sqlite3_stmt* %1140)
  store i32 %1141, i32* %nCol, align 4
  %1142 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1143 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %1142)
  store %struct.sqlite3_stmt* null, %struct.sqlite3_stmt** %pStmt3, align 8
  %1144 = load i32* %nCol, align 4
  %1145 = icmp eq i32 %1144, 0
  br i1 %1145, label %1146, label %1147

; <label>:1146                                    ; preds = %1139
  store i32 0, i32* %1
  br label %3389

; <label>:1147                                    ; preds = %1139
  %1148 = load i32* %nByte, align 4
  %1149 = mul nsw i32 %1148, 2
  %1150 = add nsw i32 %1149, 20
  %1151 = load i32* %nCol, align 4
  %1152 = mul nsw i32 %1151, 2
  %1153 = add nsw i32 %1150, %1152
  %1154 = sext i32 %1153 to i64
  %1155 = call i8* @sqlite3_malloc64(i64 %1154)
  store i8* %1155, i8** %zSql, align 8
  %1156 = load i8** %zSql, align 8
  %1157 = icmp eq i8* %1156, null
  br i1 %1157, label %1158, label %1165

; <label>:1158                                    ; preds = %1147
  %1159 = load %struct._IO_FILE** @stderr, align 8
  %1160 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1159, i8* getelementptr inbounds ([22 x i8]* @.str49, i32 0, i32 0))
  %1161 = load i32 (%struct._IO_FILE*)** %xCloser, align 8
  %1162 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  %1163 = load %struct._IO_FILE** %1162, align 8
  %1164 = call i32 %1161(%struct._IO_FILE* %1163)
  store i32 1, i32* %1
  br label %3389

; <label>:1165                                    ; preds = %1147
  %1166 = load i32* %nByte, align 4
  %1167 = add nsw i32 %1166, 20
  %1168 = load i8** %zSql, align 8
  %1169 = load i8** %zTable, align 8
  %1170 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 %1167, i8* %1168, i8* getelementptr inbounds ([26 x i8]* @.str205, i32 0, i32 0), i8* %1169)
  %1171 = load i8** %zSql, align 8
  %1172 = call i32 @strlen30(i8* %1171)
  store i32 %1172, i32* %j5, align 4
  store i32 1, i32* %i4, align 4
  br label %1173

; <label>:1173                                    ; preds = %1188, %1165
  %1174 = load i32* %i4, align 4
  %1175 = load i32* %nCol, align 4
  %1176 = icmp slt i32 %1174, %1175
  br i1 %1176, label %1177, label %1191

; <label>:1177                                    ; preds = %1173
  %1178 = load i32* %j5, align 4
  %1179 = add nsw i32 %1178, 1
  store i32 %1179, i32* %j5, align 4
  %1180 = sext i32 %1178 to i64
  %1181 = load i8** %zSql, align 8
  %1182 = getelementptr inbounds i8* %1181, i64 %1180
  store i8 44, i8* %1182, align 1
  %1183 = load i32* %j5, align 4
  %1184 = add nsw i32 %1183, 1
  store i32 %1184, i32* %j5, align 4
  %1185 = sext i32 %1183 to i64
  %1186 = load i8** %zSql, align 8
  %1187 = getelementptr inbounds i8* %1186, i64 %1185
  store i8 63, i8* %1187, align 1
  br label %1188

; <label>:1188                                    ; preds = %1177
  %1189 = load i32* %i4, align 4
  %1190 = add nsw i32 %1189, 1
  store i32 %1190, i32* %i4, align 4
  br label %1173

; <label>:1191                                    ; preds = %1173
  %1192 = load i32* %j5, align 4
  %1193 = add nsw i32 %1192, 1
  store i32 %1193, i32* %j5, align 4
  %1194 = sext i32 %1192 to i64
  %1195 = load i8** %zSql, align 8
  %1196 = getelementptr inbounds i8* %1195, i64 %1194
  store i8 41, i8* %1196, align 1
  %1197 = load i32* %j5, align 4
  %1198 = sext i32 %1197 to i64
  %1199 = load i8** %zSql, align 8
  %1200 = getelementptr inbounds i8* %1199, i64 %1198
  store i8 0, i8* %1200, align 1
  %1201 = load %struct.ShellState** %3, align 8
  %1202 = getelementptr inbounds %struct.ShellState* %1201, i32 0, i32 0
  %1203 = load %struct.sqlite3** %1202, align 8
  %1204 = load i8** %zSql, align 8
  %1205 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %1203, i8* %1204, i32 -1, %struct.sqlite3_stmt** %pStmt3, i8** null)
  store i32 %1205, i32* %rc, align 4
  %1206 = load i8** %zSql, align 8
  call void @sqlite3_free(i8* %1206)
  %1207 = load i32* %rc, align 4
  %1208 = icmp ne i32 %1207, 0
  br i1 %1208, label %1209, label %1226

; <label>:1209                                    ; preds = %1191
  %1210 = load %struct._IO_FILE** @stderr, align 8
  %1211 = load %struct.ShellState** %3, align 8
  %1212 = getelementptr inbounds %struct.ShellState* %1211, i32 0, i32 0
  %1213 = load %struct.sqlite3** %1212, align 8
  %1214 = call i8* @sqlite3_errmsg(%struct.sqlite3* %1213)
  %1215 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1210, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %1214)
  %1216 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1217 = icmp ne %struct.sqlite3_stmt* %1216, null
  br i1 %1217, label %1218, label %1221

; <label>:1218                                    ; preds = %1209
  %1219 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1220 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %1219)
  br label %1221

; <label>:1221                                    ; preds = %1218, %1209
  %1222 = load i32 (%struct._IO_FILE*)** %xCloser, align 8
  %1223 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  %1224 = load %struct._IO_FILE** %1223, align 8
  %1225 = call i32 %1222(%struct._IO_FILE* %1224)
  store i32 1, i32* %1
  br label %3389

; <label>:1226                                    ; preds = %1191
  %1227 = load %struct.ShellState** %3, align 8
  %1228 = getelementptr inbounds %struct.ShellState* %1227, i32 0, i32 0
  %1229 = load %struct.sqlite3** %1228, align 8
  %1230 = call i32 @sqlite3_get_autocommit(%struct.sqlite3* %1229)
  store i32 %1230, i32* %needCommit, align 4
  %1231 = load i32* %needCommit, align 4
  %1232 = icmp ne i32 %1231, 0
  br i1 %1232, label %1233, label %1238

; <label>:1233                                    ; preds = %1226
  %1234 = load %struct.ShellState** %3, align 8
  %1235 = getelementptr inbounds %struct.ShellState* %1234, i32 0, i32 0
  %1236 = load %struct.sqlite3** %1235, align 8
  %1237 = call i32 @sqlite3_exec(%struct.sqlite3* %1236, i8* getelementptr inbounds ([6 x i8]* @.str206, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  br label %1238

; <label>:1238                                    ; preds = %1233, %1226
  br label %1239

; <label>:1239                                    ; preds = %1364, %1238
  %1240 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 5
  %1241 = load i32* %1240, align 4
  store i32 %1241, i32* %startLine, align 4
  store i32 0, i32* %i4, align 4
  br label %1242

; <label>:1242                                    ; preds = %1312, %1239
  %1243 = load i32* %i4, align 4
  %1244 = load i32* %nCol, align 4
  %1245 = icmp slt i32 %1243, %1244
  br i1 %1245, label %1246, label %1315

; <label>:1246                                    ; preds = %1242
  %1247 = load i8* (%struct.ImportCtx*)** %xRead, align 8
  %1248 = call i8* %1247(%struct.ImportCtx* %sCtx)
  store i8* %1248, i8** %z6, align 8
  %1249 = load i8** %z6, align 8
  %1250 = icmp eq i8* %1249, null
  br i1 %1250, label %1251, label %1255

; <label>:1251                                    ; preds = %1246
  %1252 = load i32* %i4, align 4
  %1253 = icmp eq i32 %1252, 0
  br i1 %1253, label %1254, label %1255

; <label>:1254                                    ; preds = %1251
  br label %1315

; <label>:1255                                    ; preds = %1251, %1246
  %1256 = load %struct.ShellState** %3, align 8
  %1257 = getelementptr inbounds %struct.ShellState* %1256, i32 0, i32 11
  %1258 = load i32* %1257, align 4
  %1259 = icmp eq i32 %1258, 9
  br i1 %1259, label %1260, label %1273

; <label>:1260                                    ; preds = %1255
  %1261 = load i8** %z6, align 8
  %1262 = icmp eq i8* %1261, null
  br i1 %1262, label %1269, label %1263

; <label>:1263                                    ; preds = %1260
  %1264 = load i8** %z6, align 8
  %1265 = getelementptr inbounds i8* %1264, i64 0
  %1266 = load i8* %1265, align 1
  %1267 = sext i8 %1266 to i32
  %1268 = icmp eq i32 %1267, 0
  br i1 %1268, label %1269, label %1273

; <label>:1269                                    ; preds = %1263, %1260
  %1270 = load i32* %i4, align 4
  %1271 = icmp eq i32 %1270, 0
  br i1 %1271, label %1272, label %1273

; <label>:1272                                    ; preds = %1269
  br label %1315

; <label>:1273                                    ; preds = %1269, %1263, %1255
  %1274 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1275 = load i32* %i4, align 4
  %1276 = add nsw i32 %1275, 1
  %1277 = load i8** %z6, align 8
  %1278 = call i32 @sqlite3_bind_text(%struct.sqlite3_stmt* %1274, i32 %1276, i8* %1277, i32 -1, void (i8*)* inttoptr (i64 -1 to void (i8*)*))
  %1279 = load i32* %i4, align 4
  %1280 = load i32* %nCol, align 4
  %1281 = sub nsw i32 %1280, 1
  %1282 = icmp slt i32 %1279, %1281
  br i1 %1282, label %1283, label %1311

; <label>:1283                                    ; preds = %1273
  %1284 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 6
  %1285 = load i32* %1284, align 4
  %1286 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 7
  %1287 = load i32* %1286, align 4
  %1288 = icmp ne i32 %1285, %1287
  br i1 %1288, label %1289, label %1311

; <label>:1289                                    ; preds = %1283
  %1290 = load %struct._IO_FILE** @stderr, align 8
  %1291 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  %1292 = load i8** %1291, align 8
  %1293 = load i32* %startLine, align 4
  %1294 = load i32* %nCol, align 4
  %1295 = load i32* %i4, align 4
  %1296 = add nsw i32 %1295, 1
  %1297 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1290, i8* getelementptr inbounds ([70 x i8]* @.str207, i32 0, i32 0), i8* %1292, i32 %1293, i32 %1294, i32 %1296)
  %1298 = load i32* %i4, align 4
  %1299 = add nsw i32 %1298, 2
  store i32 %1299, i32* %i4, align 4
  br label %1300

; <label>:1300                                    ; preds = %1304, %1289
  %1301 = load i32* %i4, align 4
  %1302 = load i32* %nCol, align 4
  %1303 = icmp sle i32 %1301, %1302
  br i1 %1303, label %1304, label %1310

; <label>:1304                                    ; preds = %1300
  %1305 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1306 = load i32* %i4, align 4
  %1307 = call i32 @sqlite3_bind_null(%struct.sqlite3_stmt* %1305, i32 %1306)
  %1308 = load i32* %i4, align 4
  %1309 = add nsw i32 %1308, 1
  store i32 %1309, i32* %i4, align 4
  br label %1300

; <label>:1310                                    ; preds = %1300
  br label %1311

; <label>:1311                                    ; preds = %1310, %1283, %1273
  br label %1312

; <label>:1312                                    ; preds = %1311
  %1313 = load i32* %i4, align 4
  %1314 = add nsw i32 %1313, 1
  store i32 %1314, i32* %i4, align 4
  br label %1242

; <label>:1315                                    ; preds = %1272, %1254, %1242
  %1316 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 6
  %1317 = load i32* %1316, align 4
  %1318 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 7
  %1319 = load i32* %1318, align 4
  %1320 = icmp eq i32 %1317, %1319
  br i1 %1320, label %1321, label %1341

; <label>:1321                                    ; preds = %1315
  br label %1322

; <label>:1322                                    ; preds = %1327, %1321
  %1323 = load i8* (%struct.ImportCtx*)** %xRead, align 8
  %1324 = call i8* %1323(%struct.ImportCtx* %sCtx)
  %1325 = load i32* %i4, align 4
  %1326 = add nsw i32 %1325, 1
  store i32 %1326, i32* %i4, align 4
  br label %1327

; <label>:1327                                    ; preds = %1322
  %1328 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 6
  %1329 = load i32* %1328, align 4
  %1330 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 7
  %1331 = load i32* %1330, align 4
  %1332 = icmp eq i32 %1329, %1331
  br i1 %1332, label %1322, label %1333

; <label>:1333                                    ; preds = %1327
  %1334 = load %struct._IO_FILE** @stderr, align 8
  %1335 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  %1336 = load i8** %1335, align 8
  %1337 = load i32* %startLine, align 4
  %1338 = load i32* %nCol, align 4
  %1339 = load i32* %i4, align 4
  %1340 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1334, i8* getelementptr inbounds ([58 x i8]* @.str208, i32 0, i32 0), i8* %1336, i32 %1337, i32 %1338, i32 %1339)
  br label %1341

; <label>:1341                                    ; preds = %1333, %1315
  %1342 = load i32* %i4, align 4
  %1343 = load i32* %nCol, align 4
  %1344 = icmp sge i32 %1342, %1343
  br i1 %1344, label %1345, label %1363

; <label>:1345                                    ; preds = %1341
  %1346 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1347 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %1346)
  %1348 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1349 = call i32 @sqlite3_reset(%struct.sqlite3_stmt* %1348)
  store i32 %1349, i32* %rc, align 4
  %1350 = load i32* %rc, align 4
  %1351 = icmp ne i32 %1350, 0
  br i1 %1351, label %1352, label %1362

; <label>:1352                                    ; preds = %1345
  %1353 = load %struct._IO_FILE** @stderr, align 8
  %1354 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 0
  %1355 = load i8** %1354, align 8
  %1356 = load i32* %startLine, align 4
  %1357 = load %struct.ShellState** %3, align 8
  %1358 = getelementptr inbounds %struct.ShellState* %1357, i32 0, i32 0
  %1359 = load %struct.sqlite3** %1358, align 8
  %1360 = call i8* @sqlite3_errmsg(%struct.sqlite3* %1359)
  %1361 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1353, i8* getelementptr inbounds ([26 x i8]* @.str209, i32 0, i32 0), i8* %1355, i32 %1356, i8* %1360)
  br label %1362

; <label>:1362                                    ; preds = %1352, %1345
  br label %1363

; <label>:1363                                    ; preds = %1362, %1341
  br label %1364

; <label>:1364                                    ; preds = %1363
  %1365 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 6
  %1366 = load i32* %1365, align 4
  %1367 = icmp ne i32 %1366, -1
  br i1 %1367, label %1239, label %1368

; <label>:1368                                    ; preds = %1364
  %1369 = load i32 (%struct._IO_FILE*)** %xCloser, align 8
  %1370 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 1
  %1371 = load %struct._IO_FILE** %1370, align 8
  %1372 = call i32 %1369(%struct._IO_FILE* %1371)
  %1373 = getelementptr inbounds %struct.ImportCtx* %sCtx, i32 0, i32 2
  %1374 = load i8** %1373, align 8
  call void @sqlite3_free(i8* %1374)
  %1375 = load %struct.sqlite3_stmt** %pStmt3, align 8
  %1376 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %1375)
  %1377 = load i32* %needCommit, align 4
  %1378 = icmp ne i32 %1377, 0
  br i1 %1378, label %1379, label %1384

; <label>:1379                                    ; preds = %1368
  %1380 = load %struct.ShellState** %3, align 8
  %1381 = getelementptr inbounds %struct.ShellState* %1380, i32 0, i32 0
  %1382 = load %struct.sqlite3** %1381, align 8
  %1383 = call i32 @sqlite3_exec(%struct.sqlite3* %1382, i8* getelementptr inbounds ([7 x i8]* @.str210, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  br label %1384

; <label>:1384                                    ; preds = %1379, %1368
  br label %3354

; <label>:1385                                    ; preds = %898, %895
  %1386 = load i32* %c, align 4
  %1387 = icmp eq i32 %1386, 105
  br i1 %1387, label %1388, label %1448

; <label>:1388                                    ; preds = %1385
  %1389 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1390 = load i8** %1389, align 8
  %1391 = load i32* %n, align 4
  %1392 = sext i32 %1391 to i64
  %1393 = call i32 @strncmp(i8* %1390, i8* getelementptr inbounds ([8 x i8]* @.str211, i32 0, i32 0), i64 %1392) #7
  %1394 = icmp eq i32 %1393, 0
  br i1 %1394, label %1402, label %1395

; <label>:1395                                    ; preds = %1388
  %1396 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1397 = load i8** %1396, align 8
  %1398 = load i32* %n, align 4
  %1399 = sext i32 %1398 to i64
  %1400 = call i32 @strncmp(i8* %1397, i8* getelementptr inbounds ([8 x i8]* @.str212, i32 0, i32 0), i64 %1399) #7
  %1401 = icmp eq i32 %1400, 0
  br i1 %1401, label %1402, label %1448

; <label>:1402                                    ; preds = %1395, %1388
  store i8* null, i8** %zErrMsg8, align 8
  %1403 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %1403, i32 0)
  %1404 = bitcast %struct.ShellState* %data7 to i8*
  %1405 = load %struct.ShellState** %3, align 8
  %1406 = bitcast %struct.ShellState* %1405 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %1404, i8* %1406, i64 5512, i32 8, i1 false)
  %1407 = getelementptr inbounds %struct.ShellState* %data7, i32 0, i32 13
  store i32 0, i32* %1407, align 4
  %1408 = getelementptr inbounds %struct.ShellState* %data7, i32 0, i32 11
  store i32 2, i32* %1408, align 4
  %1409 = load i32* %nArg, align 4
  %1410 = icmp eq i32 %1409, 1
  br i1 %1410, label %1411, label %1417

; <label>:1411                                    ; preds = %1402
  %1412 = load %struct.ShellState** %3, align 8
  %1413 = getelementptr inbounds %struct.ShellState* %1412, i32 0, i32 0
  %1414 = load %struct.sqlite3** %1413, align 8
  %1415 = bitcast %struct.ShellState* %data7 to i8*
  %1416 = call i32 @sqlite3_exec(%struct.sqlite3* %1414, i8* getelementptr inbounds ([155 x i8]* @.str213, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* @callback, i8* %1415, i8** %zErrMsg8)
  store i32 %1416, i32* %rc, align 4
  br label %1432

; <label>:1417                                    ; preds = %1402
  %1418 = load i32* %nArg, align 4
  %1419 = icmp eq i32 %1418, 2
  br i1 %1419, label %1420, label %1428

; <label>:1420                                    ; preds = %1417
  %1421 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1422 = load i8** %1421, align 8
  store i8* %1422, i8** @zShellStatic, align 8
  %1423 = load %struct.ShellState** %3, align 8
  %1424 = getelementptr inbounds %struct.ShellState* %1423, i32 0, i32 0
  %1425 = load %struct.sqlite3** %1424, align 8
  %1426 = bitcast %struct.ShellState* %data7 to i8*
  %1427 = call i32 @sqlite3_exec(%struct.sqlite3* %1425, i8* getelementptr inbounds ([190 x i8]* @.str214, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* @callback, i8* %1426, i8** %zErrMsg8)
  store i32 %1427, i32* %rc, align 4
  store i8* null, i8** @zShellStatic, align 8
  br label %1431

; <label>:1428                                    ; preds = %1417
  %1429 = load %struct._IO_FILE** @stderr, align 8
  %1430 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1429, i8* getelementptr inbounds ([32 x i8]* @.str215, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:1431                                    ; preds = %1420
  br label %1432

; <label>:1432                                    ; preds = %1431, %1411
  %1433 = load i8** %zErrMsg8, align 8
  %1434 = icmp ne i8* %1433, null
  br i1 %1434, label %1435, label %1440

; <label>:1435                                    ; preds = %1432
  %1436 = load %struct._IO_FILE** @stderr, align 8
  %1437 = load i8** %zErrMsg8, align 8
  %1438 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1436, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %1437)
  %1439 = load i8** %zErrMsg8, align 8
  call void @sqlite3_free(i8* %1439)
  store i32 1, i32* %rc, align 4
  br label %1447

; <label>:1440                                    ; preds = %1432
  %1441 = load i32* %rc, align 4
  %1442 = icmp ne i32 %1441, 0
  br i1 %1442, label %1443, label %1446

; <label>:1443                                    ; preds = %1440
  %1444 = load %struct._IO_FILE** @stderr, align 8
  %1445 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1444, i8* getelementptr inbounds ([54 x i8]* @.str216, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %1446

; <label>:1446                                    ; preds = %1443, %1440
  br label %1447

; <label>:1447                                    ; preds = %1446, %1435
  br label %3353

; <label>:1448                                    ; preds = %1395, %1385
  %1449 = load i32* %c, align 4
  %1450 = icmp eq i32 %1449, 108
  br i1 %1450, label %1451, label %1573

; <label>:1451                                    ; preds = %1448
  %1452 = load i32* %n, align 4
  %1453 = icmp sge i32 %1452, 5
  br i1 %1453, label %1454, label %1573

; <label>:1454                                    ; preds = %1451
  %1455 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1456 = load i8** %1455, align 8
  %1457 = load i32* %n, align 4
  %1458 = sext i32 %1457 to i64
  %1459 = call i32 @strncmp(i8* %1456, i8* getelementptr inbounds ([7 x i8]* @.str217, i32 0, i32 0), i64 %1458) #7
  %1460 = icmp eq i32 %1459, 0
  br i1 %1460, label %1461, label %1573

; <label>:1461                                    ; preds = %1454
  %1462 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %1462, i32 0)
  %1463 = load i32* %nArg, align 4
  %1464 = icmp eq i32 %1463, 1
  br i1 %1464, label %1465, label %1490

; <label>:1465                                    ; preds = %1461
  store i32 0, i32* %i9, align 4
  br label %1466

; <label>:1466                                    ; preds = %1486, %1465
  %1467 = load i32* %i9, align 4
  %1468 = sext i32 %1467 to i64
  %1469 = icmp ult i64 %1468, 12
  br i1 %1469, label %1470, label %1489

; <label>:1470                                    ; preds = %1466
  %1471 = load i32* %i9, align 4
  %1472 = sext i32 %1471 to i64
  %1473 = getelementptr inbounds [12 x %struct.anon]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @do_meta_command.aLimit to [12 x %struct.anon]*), i32 0, i64 %1472
  %1474 = getelementptr inbounds %struct.anon* %1473, i32 0, i32 0
  %1475 = load i8** %1474, align 8
  %1476 = load %struct.ShellState** %3, align 8
  %1477 = getelementptr inbounds %struct.ShellState* %1476, i32 0, i32 0
  %1478 = load %struct.sqlite3** %1477, align 8
  %1479 = load i32* %i9, align 4
  %1480 = sext i32 %1479 to i64
  %1481 = getelementptr inbounds [12 x %struct.anon]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @do_meta_command.aLimit to [12 x %struct.anon]*), i32 0, i64 %1480
  %1482 = getelementptr inbounds %struct.anon* %1481, i32 0, i32 1
  %1483 = load i32* %1482, align 4
  %1484 = call i32 @sqlite3_limit(%struct.sqlite3* %1478, i32 %1483, i32 -1)
  %1485 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([9 x i8]* @.str230, i32 0, i32 0), i8* %1475, i32 %1484)
  br label %1486

; <label>:1486                                    ; preds = %1470
  %1487 = load i32* %i9, align 4
  %1488 = add nsw i32 %1487, 1
  store i32 %1488, i32* %i9, align 4
  br label %1466

; <label>:1489                                    ; preds = %1466
  br label %1572

; <label>:1490                                    ; preds = %1461
  %1491 = load i32* %nArg, align 4
  %1492 = icmp sgt i32 %1491, 3
  br i1 %1492, label %1493, label %1496

; <label>:1493                                    ; preds = %1490
  %1494 = load %struct._IO_FILE** @stderr, align 8
  %1495 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1494, i8* getelementptr inbounds ([32 x i8]* @.str231, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:1496                                    ; preds = %1490
  store i32 -1, i32* %iLimit, align 4
  %1497 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1498 = load i8** %1497, align 8
  %1499 = call i32 @strlen30(i8* %1498)
  store i32 %1499, i32* %n2, align 4
  store i32 0, i32* %i9, align 4
  br label %1500

; <label>:1500                                    ; preds = %1527, %1496
  %1501 = load i32* %i9, align 4
  %1502 = sext i32 %1501 to i64
  %1503 = icmp ult i64 %1502, 12
  br i1 %1503, label %1504, label %1530

; <label>:1504                                    ; preds = %1500
  %1505 = load i32* %i9, align 4
  %1506 = sext i32 %1505 to i64
  %1507 = getelementptr inbounds [12 x %struct.anon]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @do_meta_command.aLimit to [12 x %struct.anon]*), i32 0, i64 %1506
  %1508 = getelementptr inbounds %struct.anon* %1507, i32 0, i32 0
  %1509 = load i8** %1508, align 8
  %1510 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1511 = load i8** %1510, align 8
  %1512 = load i32* %n2, align 4
  %1513 = call i32 @sqlite3_strnicmp(i8* %1509, i8* %1511, i32 %1512)
  %1514 = icmp eq i32 %1513, 0
  br i1 %1514, label %1515, label %1526

; <label>:1515                                    ; preds = %1504
  %1516 = load i32* %iLimit, align 4
  %1517 = icmp slt i32 %1516, 0
  br i1 %1517, label %1518, label %1520

; <label>:1518                                    ; preds = %1515
  %1519 = load i32* %i9, align 4
  store i32 %1519, i32* %iLimit, align 4
  br label %1525

; <label>:1520                                    ; preds = %1515
  %1521 = load %struct._IO_FILE** @stderr, align 8
  %1522 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1523 = load i8** %1522, align 8
  %1524 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1521, i8* getelementptr inbounds ([23 x i8]* @.str232, i32 0, i32 0), i8* %1523)
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:1525                                    ; preds = %1518
  br label %1526

; <label>:1526                                    ; preds = %1525, %1504
  br label %1527

; <label>:1527                                    ; preds = %1526
  %1528 = load i32* %i9, align 4
  %1529 = add nsw i32 %1528, 1
  store i32 %1529, i32* %i9, align 4
  br label %1500

; <label>:1530                                    ; preds = %1500
  %1531 = load i32* %iLimit, align 4
  %1532 = icmp slt i32 %1531, 0
  br i1 %1532, label %1533, label %1538

; <label>:1533                                    ; preds = %1530
  %1534 = load %struct._IO_FILE** @stderr, align 8
  %1535 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1536 = load i8** %1535, align 8
  %1537 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1534, i8* getelementptr inbounds ([67 x i8]* @.str233, i32 0, i32 0), i8* %1536)
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:1538                                    ; preds = %1530
  %1539 = load i32* %nArg, align 4
  %1540 = icmp eq i32 %1539, 3
  br i1 %1540, label %1541, label %1555

; <label>:1541                                    ; preds = %1538
  %1542 = load %struct.ShellState** %3, align 8
  %1543 = getelementptr inbounds %struct.ShellState* %1542, i32 0, i32 0
  %1544 = load %struct.sqlite3** %1543, align 8
  %1545 = load i32* %iLimit, align 4
  %1546 = sext i32 %1545 to i64
  %1547 = getelementptr inbounds [12 x %struct.anon]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @do_meta_command.aLimit to [12 x %struct.anon]*), i32 0, i64 %1546
  %1548 = getelementptr inbounds %struct.anon* %1547, i32 0, i32 1
  %1549 = load i32* %1548, align 4
  %1550 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %1551 = load i8** %1550, align 8
  %1552 = call i64 @integerValue(i8* %1551)
  %1553 = trunc i64 %1552 to i32
  %1554 = call i32 @sqlite3_limit(%struct.sqlite3* %1544, i32 %1549, i32 %1553)
  br label %1555

; <label>:1555                                    ; preds = %1541, %1538
  %1556 = load i32* %iLimit, align 4
  %1557 = sext i32 %1556 to i64
  %1558 = getelementptr inbounds [12 x %struct.anon]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @do_meta_command.aLimit to [12 x %struct.anon]*), i32 0, i64 %1557
  %1559 = getelementptr inbounds %struct.anon* %1558, i32 0, i32 0
  %1560 = load i8** %1559, align 8
  %1561 = load %struct.ShellState** %3, align 8
  %1562 = getelementptr inbounds %struct.ShellState* %1561, i32 0, i32 0
  %1563 = load %struct.sqlite3** %1562, align 8
  %1564 = load i32* %iLimit, align 4
  %1565 = sext i32 %1564 to i64
  %1566 = getelementptr inbounds [12 x %struct.anon]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @do_meta_command.aLimit to [12 x %struct.anon]*), i32 0, i64 %1565
  %1567 = getelementptr inbounds %struct.anon* %1566, i32 0, i32 1
  %1568 = load i32* %1567, align 4
  %1569 = call i32 @sqlite3_limit(%struct.sqlite3* %1563, i32 %1568, i32 -1)
  %1570 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([9 x i8]* @.str230, i32 0, i32 0), i8* %1560, i32 %1569)
  br label %1571

; <label>:1571                                    ; preds = %1555
  br label %1572

; <label>:1572                                    ; preds = %1571, %1489
  br label %3352

; <label>:1573                                    ; preds = %1454, %1451, %1448
  %1574 = load i32* %c, align 4
  %1575 = icmp eq i32 %1574, 108
  br i1 %1575, label %1576, label %1615

; <label>:1576                                    ; preds = %1573
  %1577 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1578 = load i8** %1577, align 8
  %1579 = load i32* %n, align 4
  %1580 = sext i32 %1579 to i64
  %1581 = call i32 @strncmp(i8* %1578, i8* getelementptr inbounds ([5 x i8]* @.str234, i32 0, i32 0), i64 %1580) #7
  %1582 = icmp eq i32 %1581, 0
  br i1 %1582, label %1583, label %1615

; <label>:1583                                    ; preds = %1576
  store i8* null, i8** %zErrMsg11, align 8
  %1584 = load i32* %nArg, align 4
  %1585 = icmp slt i32 %1584, 2
  br i1 %1585, label %1586, label %1589

; <label>:1586                                    ; preds = %1583
  %1587 = load %struct._IO_FILE** @stderr, align 8
  %1588 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1587, i8* getelementptr inbounds ([32 x i8]* @.str235, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:1589                                    ; preds = %1583
  %1590 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1591 = load i8** %1590, align 8
  store i8* %1591, i8** %zFile10, align 8
  %1592 = load i32* %nArg, align 4
  %1593 = icmp sge i32 %1592, 3
  br i1 %1593, label %1594, label %1597

; <label>:1594                                    ; preds = %1589
  %1595 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %1596 = load i8** %1595, align 8
  br label %1598

; <label>:1597                                    ; preds = %1589
  br label %1598

; <label>:1598                                    ; preds = %1597, %1594
  %1599 = phi i8* [ %1596, %1594 ], [ null, %1597 ]
  store i8* %1599, i8** %zProc, align 8
  %1600 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %1600, i32 0)
  %1601 = load %struct.ShellState** %3, align 8
  %1602 = getelementptr inbounds %struct.ShellState* %1601, i32 0, i32 0
  %1603 = load %struct.sqlite3** %1602, align 8
  %1604 = load i8** %zFile10, align 8
  %1605 = load i8** %zProc, align 8
  %1606 = call i32 @sqlite3_load_extension(%struct.sqlite3* %1603, i8* %1604, i8* %1605, i8** %zErrMsg11)
  store i32 %1606, i32* %rc, align 4
  %1607 = load i32* %rc, align 4
  %1608 = icmp ne i32 %1607, 0
  br i1 %1608, label %1609, label %1614

; <label>:1609                                    ; preds = %1598
  %1610 = load %struct._IO_FILE** @stderr, align 8
  %1611 = load i8** %zErrMsg11, align 8
  %1612 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1610, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %1611)
  %1613 = load i8** %zErrMsg11, align 8
  call void @sqlite3_free(i8* %1613)
  store i32 1, i32* %rc, align 4
  br label %1614

; <label>:1614                                    ; preds = %1609, %1598
  br label %3351

; <label>:1615                                    ; preds = %1576, %1573
  %1616 = load i32* %c, align 4
  %1617 = icmp eq i32 %1616, 108
  br i1 %1617, label %1618, label %1642

; <label>:1618                                    ; preds = %1615
  %1619 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1620 = load i8** %1619, align 8
  %1621 = load i32* %n, align 4
  %1622 = sext i32 %1621 to i64
  %1623 = call i32 @strncmp(i8* %1620, i8* getelementptr inbounds ([4 x i8]* @.str236, i32 0, i32 0), i64 %1622) #7
  %1624 = icmp eq i32 %1623, 0
  br i1 %1624, label %1625, label %1642

; <label>:1625                                    ; preds = %1618
  %1626 = load i32* %nArg, align 4
  %1627 = icmp ne i32 %1626, 2
  br i1 %1627, label %1628, label %1631

; <label>:1628                                    ; preds = %1625
  %1629 = load %struct._IO_FILE** @stderr, align 8
  %1630 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1629, i8* getelementptr inbounds ([22 x i8]* @.str237, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %1641

; <label>:1631                                    ; preds = %1625
  %1632 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1633 = load i8** %1632, align 8
  store i8* %1633, i8** %zFile12, align 8
  %1634 = load %struct.ShellState** %3, align 8
  %1635 = getelementptr inbounds %struct.ShellState* %1634, i32 0, i32 27
  %1636 = load %struct._IO_FILE** %1635, align 8
  call void @output_file_close(%struct._IO_FILE* %1636)
  %1637 = load i8** %zFile12, align 8
  %1638 = call %struct._IO_FILE* @output_file_open(i8* %1637)
  %1639 = load %struct.ShellState** %3, align 8
  %1640 = getelementptr inbounds %struct.ShellState* %1639, i32 0, i32 27
  store %struct._IO_FILE* %1638, %struct._IO_FILE** %1640, align 8
  br label %1641

; <label>:1641                                    ; preds = %1631, %1628
  br label %3350

; <label>:1642                                    ; preds = %1618, %1615
  %1643 = load i32* %c, align 4
  %1644 = icmp eq i32 %1643, 109
  br i1 %1644, label %1645, label %1835

; <label>:1645                                    ; preds = %1642
  %1646 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1647 = load i8** %1646, align 8
  %1648 = load i32* %n, align 4
  %1649 = sext i32 %1648 to i64
  %1650 = call i32 @strncmp(i8* %1647, i8* getelementptr inbounds ([5 x i8]* @.str238, i32 0, i32 0), i64 %1649) #7
  %1651 = icmp eq i32 %1650, 0
  br i1 %1651, label %1652, label %1835

; <label>:1652                                    ; preds = %1645
  %1653 = load i32* %nArg, align 4
  %1654 = icmp sge i32 %1653, 2
  br i1 %1654, label %1655, label %1658

; <label>:1655                                    ; preds = %1652
  %1656 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1657 = load i8** %1656, align 8
  br label %1659

; <label>:1658                                    ; preds = %1652
  br label %1659

; <label>:1659                                    ; preds = %1658, %1655
  %1660 = phi i8* [ %1657, %1655 ], [ getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), %1658 ]
  store i8* %1660, i8** %zMode, align 8
  %1661 = load i8** %zMode, align 8
  %1662 = call i64 @strlen(i8* %1661) #7
  %1663 = trunc i64 %1662 to i32
  store i32 %1663, i32* %n213, align 4
  %1664 = load i8** %zMode, align 8
  %1665 = getelementptr inbounds i8* %1664, i64 0
  %1666 = load i8* %1665, align 1
  %1667 = sext i8 %1666 to i32
  store i32 %1667, i32* %c2, align 4
  %1668 = load i32* %c2, align 4
  %1669 = icmp eq i32 %1668, 108
  br i1 %1669, label %1670, label %1683

; <label>:1670                                    ; preds = %1659
  %1671 = load i32* %n213, align 4
  %1672 = icmp sgt i32 %1671, 2
  br i1 %1672, label %1673, label %1683

; <label>:1673                                    ; preds = %1670
  %1674 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1675 = load i8** %1674, align 8
  %1676 = load i32* %n213, align 4
  %1677 = sext i32 %1676 to i64
  %1678 = call i32 @strncmp(i8* %1675, i8* getelementptr inbounds ([6 x i8]* @.str239, i32 0, i32 0), i64 %1677) #7
  %1679 = icmp eq i32 %1678, 0
  br i1 %1679, label %1680, label %1683

; <label>:1680                                    ; preds = %1673
  %1681 = load %struct.ShellState** %3, align 8
  %1682 = getelementptr inbounds %struct.ShellState* %1681, i32 0, i32 11
  store i32 0, i32* %1682, align 4
  br label %1834

; <label>:1683                                    ; preds = %1673, %1670, %1659
  %1684 = load i32* %c2, align 4
  %1685 = icmp eq i32 %1684, 99
  br i1 %1685, label %1686, label %1696

; <label>:1686                                    ; preds = %1683
  %1687 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1688 = load i8** %1687, align 8
  %1689 = load i32* %n213, align 4
  %1690 = sext i32 %1689 to i64
  %1691 = call i32 @strncmp(i8* %1688, i8* getelementptr inbounds ([8 x i8]* @.str240, i32 0, i32 0), i64 %1690) #7
  %1692 = icmp eq i32 %1691, 0
  br i1 %1692, label %1693, label %1696

; <label>:1693                                    ; preds = %1686
  %1694 = load %struct.ShellState** %3, align 8
  %1695 = getelementptr inbounds %struct.ShellState* %1694, i32 0, i32 11
  store i32 1, i32* %1695, align 4
  br label %1833

; <label>:1696                                    ; preds = %1686, %1683
  %1697 = load i32* %c2, align 4
  %1698 = icmp eq i32 %1697, 108
  br i1 %1698, label %1699, label %1712

; <label>:1699                                    ; preds = %1696
  %1700 = load i32* %n213, align 4
  %1701 = icmp sgt i32 %1700, 2
  br i1 %1701, label %1702, label %1712

; <label>:1702                                    ; preds = %1699
  %1703 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1704 = load i8** %1703, align 8
  %1705 = load i32* %n213, align 4
  %1706 = sext i32 %1705 to i64
  %1707 = call i32 @strncmp(i8* %1704, i8* getelementptr inbounds ([5 x i8]* @.str241, i32 0, i32 0), i64 %1706) #7
  %1708 = icmp eq i32 %1707, 0
  br i1 %1708, label %1709, label %1712

; <label>:1709                                    ; preds = %1702
  %1710 = load %struct.ShellState** %3, align 8
  %1711 = getelementptr inbounds %struct.ShellState* %1710, i32 0, i32 11
  store i32 2, i32* %1711, align 4
  br label %1832

; <label>:1712                                    ; preds = %1702, %1699, %1696
  %1713 = load i32* %c2, align 4
  %1714 = icmp eq i32 %1713, 104
  br i1 %1714, label %1715, label %1725

; <label>:1715                                    ; preds = %1712
  %1716 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1717 = load i8** %1716, align 8
  %1718 = load i32* %n213, align 4
  %1719 = sext i32 %1718 to i64
  %1720 = call i32 @strncmp(i8* %1717, i8* getelementptr inbounds ([5 x i8]* @.str242, i32 0, i32 0), i64 %1719) #7
  %1721 = icmp eq i32 %1720, 0
  br i1 %1721, label %1722, label %1725

; <label>:1722                                    ; preds = %1715
  %1723 = load %struct.ShellState** %3, align 8
  %1724 = getelementptr inbounds %struct.ShellState* %1723, i32 0, i32 11
  store i32 4, i32* %1724, align 4
  br label %1831

; <label>:1725                                    ; preds = %1715, %1712
  %1726 = load i32* %c2, align 4
  %1727 = icmp eq i32 %1726, 116
  br i1 %1727, label %1728, label %1742

; <label>:1728                                    ; preds = %1725
  %1729 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1730 = load i8** %1729, align 8
  %1731 = load i32* %n213, align 4
  %1732 = sext i32 %1731 to i64
  %1733 = call i32 @strncmp(i8* %1730, i8* getelementptr inbounds ([4 x i8]* @.str243, i32 0, i32 0), i64 %1732) #7
  %1734 = icmp eq i32 %1733, 0
  br i1 %1734, label %1735, label %1742

; <label>:1735                                    ; preds = %1728
  %1736 = load %struct.ShellState** %3, align 8
  %1737 = getelementptr inbounds %struct.ShellState* %1736, i32 0, i32 11
  store i32 6, i32* %1737, align 4
  %1738 = load %struct.ShellState** %3, align 8
  %1739 = getelementptr inbounds %struct.ShellState* %1738, i32 0, i32 16
  %1740 = getelementptr inbounds [20 x i8]* %1739, i32 0, i32 0
  %1741 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %1740, i8* getelementptr inbounds ([2 x i8]* @.str244, i32 0, i32 0))
  br label %1830

; <label>:1742                                    ; preds = %1728, %1725
  %1743 = load i32* %c2, align 4
  %1744 = icmp eq i32 %1743, 99
  br i1 %1744, label %1745, label %1763

; <label>:1745                                    ; preds = %1742
  %1746 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1747 = load i8** %1746, align 8
  %1748 = load i32* %n213, align 4
  %1749 = sext i32 %1748 to i64
  %1750 = call i32 @strncmp(i8* %1747, i8* getelementptr inbounds ([4 x i8]* @.str245, i32 0, i32 0), i64 %1749) #7
  %1751 = icmp eq i32 %1750, 0
  br i1 %1751, label %1752, label %1763

; <label>:1752                                    ; preds = %1745
  %1753 = load %struct.ShellState** %3, align 8
  %1754 = getelementptr inbounds %struct.ShellState* %1753, i32 0, i32 11
  store i32 7, i32* %1754, align 4
  %1755 = load %struct.ShellState** %3, align 8
  %1756 = getelementptr inbounds %struct.ShellState* %1755, i32 0, i32 16
  %1757 = getelementptr inbounds [20 x i8]* %1756, i32 0, i32 0
  %1758 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %1757, i8* getelementptr inbounds ([2 x i8]* @.str22, i32 0, i32 0))
  %1759 = load %struct.ShellState** %3, align 8
  %1760 = getelementptr inbounds %struct.ShellState* %1759, i32 0, i32 17
  %1761 = getelementptr inbounds [20 x i8]* %1760, i32 0, i32 0
  %1762 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %1761, i8* getelementptr inbounds ([3 x i8]* @.str193, i32 0, i32 0))
  br label %1829

; <label>:1763                                    ; preds = %1745, %1742
  %1764 = load i32* %c2, align 4
  %1765 = icmp eq i32 %1764, 116
  br i1 %1765, label %1766, label %1780

; <label>:1766                                    ; preds = %1763
  %1767 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1768 = load i8** %1767, align 8
  %1769 = load i32* %n213, align 4
  %1770 = sext i32 %1769 to i64
  %1771 = call i32 @strncmp(i8* %1768, i8* getelementptr inbounds ([5 x i8]* @.str246, i32 0, i32 0), i64 %1770) #7
  %1772 = icmp eq i32 %1771, 0
  br i1 %1772, label %1773, label %1780

; <label>:1773                                    ; preds = %1766
  %1774 = load %struct.ShellState** %3, align 8
  %1775 = getelementptr inbounds %struct.ShellState* %1774, i32 0, i32 11
  store i32 2, i32* %1775, align 4
  %1776 = load %struct.ShellState** %3, align 8
  %1777 = getelementptr inbounds %struct.ShellState* %1776, i32 0, i32 16
  %1778 = getelementptr inbounds [20 x i8]* %1777, i32 0, i32 0
  %1779 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %1778, i8* getelementptr inbounds ([2 x i8]* @.str247, i32 0, i32 0))
  br label %1828

; <label>:1780                                    ; preds = %1766, %1763
  %1781 = load i32* %c2, align 4
  %1782 = icmp eq i32 %1781, 105
  br i1 %1782, label %1783, label %1802

; <label>:1783                                    ; preds = %1780
  %1784 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1785 = load i8** %1784, align 8
  %1786 = load i32* %n213, align 4
  %1787 = sext i32 %1786 to i64
  %1788 = call i32 @strncmp(i8* %1785, i8* getelementptr inbounds ([7 x i8]* @.str248, i32 0, i32 0), i64 %1787) #7
  %1789 = icmp eq i32 %1788, 0
  br i1 %1789, label %1790, label %1802

; <label>:1790                                    ; preds = %1783
  %1791 = load %struct.ShellState** %3, align 8
  %1792 = getelementptr inbounds %struct.ShellState* %1791, i32 0, i32 11
  store i32 5, i32* %1792, align 4
  %1793 = load %struct.ShellState** %3, align 8
  %1794 = load i32* %nArg, align 4
  %1795 = icmp sge i32 %1794, 3
  br i1 %1795, label %1796, label %1799

; <label>:1796                                    ; preds = %1790
  %1797 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %1798 = load i8** %1797, align 8
  br label %1800

; <label>:1799                                    ; preds = %1790
  br label %1800

; <label>:1800                                    ; preds = %1799, %1796
  %1801 = phi i8* [ %1798, %1796 ], [ getelementptr inbounds ([6 x i8]* @.str249, i32 0, i32 0), %1799 ]
  call void @set_table_name(%struct.ShellState* %1793, i8* %1801)
  br label %1827

; <label>:1802                                    ; preds = %1783, %1780
  %1803 = load i32* %c2, align 4
  %1804 = icmp eq i32 %1803, 97
  br i1 %1804, label %1805, label %1823

; <label>:1805                                    ; preds = %1802
  %1806 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1807 = load i8** %1806, align 8
  %1808 = load i32* %n213, align 4
  %1809 = sext i32 %1808 to i64
  %1810 = call i32 @strncmp(i8* %1807, i8* getelementptr inbounds ([6 x i8]* @.str250, i32 0, i32 0), i64 %1809) #7
  %1811 = icmp eq i32 %1810, 0
  br i1 %1811, label %1812, label %1823

; <label>:1812                                    ; preds = %1805
  %1813 = load %struct.ShellState** %3, align 8
  %1814 = getelementptr inbounds %struct.ShellState* %1813, i32 0, i32 11
  store i32 9, i32* %1814, align 4
  %1815 = load %struct.ShellState** %3, align 8
  %1816 = getelementptr inbounds %struct.ShellState* %1815, i32 0, i32 16
  %1817 = getelementptr inbounds [20 x i8]* %1816, i32 0, i32 0
  %1818 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %1817, i8* getelementptr inbounds ([2 x i8]* @.str24, i32 0, i32 0))
  %1819 = load %struct.ShellState** %3, align 8
  %1820 = getelementptr inbounds %struct.ShellState* %1819, i32 0, i32 17
  %1821 = getelementptr inbounds [20 x i8]* %1820, i32 0, i32 0
  %1822 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %1821, i8* getelementptr inbounds ([2 x i8]* @.str25, i32 0, i32 0))
  br label %1826

; <label>:1823                                    ; preds = %1805, %1802
  %1824 = load %struct._IO_FILE** @stderr, align 8
  %1825 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1824, i8* getelementptr inbounds ([79 x i8]* @.str251, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %1826

; <label>:1826                                    ; preds = %1823, %1812
  br label %1827

; <label>:1827                                    ; preds = %1826, %1800
  br label %1828

; <label>:1828                                    ; preds = %1827, %1773
  br label %1829

; <label>:1829                                    ; preds = %1828, %1752
  br label %1830

; <label>:1830                                    ; preds = %1829, %1735
  br label %1831

; <label>:1831                                    ; preds = %1830, %1722
  br label %1832

; <label>:1832                                    ; preds = %1831, %1709
  br label %1833

; <label>:1833                                    ; preds = %1832, %1693
  br label %1834

; <label>:1834                                    ; preds = %1833, %1680
  br label %3349

; <label>:1835                                    ; preds = %1645, %1642
  %1836 = load i32* %c, align 4
  %1837 = icmp eq i32 %1836, 110
  br i1 %1837, label %1838, label %1859

; <label>:1838                                    ; preds = %1835
  %1839 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1840 = load i8** %1839, align 8
  %1841 = load i32* %n, align 4
  %1842 = sext i32 %1841 to i64
  %1843 = call i32 @strncmp(i8* %1840, i8* getelementptr inbounds ([10 x i8]* @.str252, i32 0, i32 0), i64 %1842) #7
  %1844 = icmp eq i32 %1843, 0
  br i1 %1844, label %1845, label %1859

; <label>:1845                                    ; preds = %1838
  %1846 = load i32* %nArg, align 4
  %1847 = icmp eq i32 %1846, 2
  br i1 %1847, label %1848, label %1855

; <label>:1848                                    ; preds = %1845
  %1849 = load %struct.ShellState** %3, align 8
  %1850 = getelementptr inbounds %struct.ShellState* %1849, i32 0, i32 20
  %1851 = getelementptr inbounds [20 x i8]* %1850, i32 0, i32 0
  %1852 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1853 = load i8** %1852, align 8
  %1854 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %1851, i8* getelementptr inbounds ([5 x i8]* @.str89, i32 0, i32 0), i32 19, i8* %1853)
  br label %1858

; <label>:1855                                    ; preds = %1845
  %1856 = load %struct._IO_FILE** @stderr, align 8
  %1857 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1856, i8* getelementptr inbounds ([26 x i8]* @.str253, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %1858

; <label>:1858                                    ; preds = %1855, %1848
  br label %3348

; <label>:1859                                    ; preds = %1838, %1835
  %1860 = load i32* %c, align 4
  %1861 = icmp eq i32 %1860, 111
  br i1 %1861, label %1862, label %1913

; <label>:1862                                    ; preds = %1859
  %1863 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1864 = load i8** %1863, align 8
  %1865 = load i32* %n, align 4
  %1866 = sext i32 %1865 to i64
  %1867 = call i32 @strncmp(i8* %1864, i8* getelementptr inbounds ([5 x i8]* @.str254, i32 0, i32 0), i64 %1866) #7
  %1868 = icmp eq i32 %1867, 0
  br i1 %1868, label %1869, label %1913

; <label>:1869                                    ; preds = %1862
  %1870 = load i32* %n, align 4
  %1871 = icmp sge i32 %1870, 2
  br i1 %1871, label %1872, label %1913

; <label>:1872                                    ; preds = %1869
  %1873 = load %struct.ShellState** %3, align 8
  %1874 = getelementptr inbounds %struct.ShellState* %1873, i32 0, i32 0
  %1875 = load %struct.sqlite3** %1874, align 8
  store %struct.sqlite3* %1875, %struct.sqlite3** %savedDb, align 8
  %1876 = load %struct.ShellState** %3, align 8
  %1877 = getelementptr inbounds %struct.ShellState* %1876, i32 0, i32 23
  %1878 = load i8** %1877, align 8
  store i8* %1878, i8** %zSavedFilename, align 8
  store i8* null, i8** %zNewFilename, align 8
  %1879 = load %struct.ShellState** %3, align 8
  %1880 = getelementptr inbounds %struct.ShellState* %1879, i32 0, i32 0
  store %struct.sqlite3* null, %struct.sqlite3** %1880, align 8
  %1881 = load i32* %nArg, align 4
  %1882 = icmp sge i32 %1881, 2
  br i1 %1882, label %1883, label %1889

; <label>:1883                                    ; preds = %1872
  %1884 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1885 = load i8** %1884, align 8
  %1886 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1885)
  store i8* %1886, i8** %zNewFilename, align 8
  %1887 = load %struct.ShellState** %3, align 8
  %1888 = getelementptr inbounds %struct.ShellState* %1887, i32 0, i32 23
  store i8* %1886, i8** %1888, align 8
  br label %1889

; <label>:1889                                    ; preds = %1883, %1872
  %1890 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %1890, i32 1)
  %1891 = load %struct.ShellState** %3, align 8
  %1892 = getelementptr inbounds %struct.ShellState* %1891, i32 0, i32 0
  %1893 = load %struct.sqlite3** %1892, align 8
  %1894 = icmp ne %struct.sqlite3* %1893, null
  br i1 %1894, label %1895, label %1904

; <label>:1895                                    ; preds = %1889
  %1896 = load %struct.sqlite3** %savedDb, align 8
  %1897 = call i32 @sqlite3_close(%struct.sqlite3* %1896)
  %1898 = load %struct.ShellState** %3, align 8
  %1899 = getelementptr inbounds %struct.ShellState* %1898, i32 0, i32 24
  %1900 = load i8** %1899, align 8
  call void @sqlite3_free(i8* %1900)
  %1901 = load i8** %zNewFilename, align 8
  %1902 = load %struct.ShellState** %3, align 8
  %1903 = getelementptr inbounds %struct.ShellState* %1902, i32 0, i32 24
  store i8* %1901, i8** %1903, align 8
  br label %1912

; <label>:1904                                    ; preds = %1889
  %1905 = load i8** %zNewFilename, align 8
  call void @sqlite3_free(i8* %1905)
  %1906 = load %struct.sqlite3** %savedDb, align 8
  %1907 = load %struct.ShellState** %3, align 8
  %1908 = getelementptr inbounds %struct.ShellState* %1907, i32 0, i32 0
  store %struct.sqlite3* %1906, %struct.sqlite3** %1908, align 8
  %1909 = load i8** %zSavedFilename, align 8
  %1910 = load %struct.ShellState** %3, align 8
  %1911 = getelementptr inbounds %struct.ShellState* %1910, i32 0, i32 23
  store i8* %1909, i8** %1911, align 8
  br label %1912

; <label>:1912                                    ; preds = %1904, %1895
  br label %3347

; <label>:1913                                    ; preds = %1869, %1862, %1859
  %1914 = load i32* %c, align 4
  %1915 = icmp eq i32 %1914, 111
  br i1 %1915, label %1916, label %2029

; <label>:1916                                    ; preds = %1913
  %1917 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1918 = load i8** %1917, align 8
  %1919 = load i32* %n, align 4
  %1920 = sext i32 %1919 to i64
  %1921 = call i32 @strncmp(i8* %1918, i8* getelementptr inbounds ([7 x i8]* @.str255, i32 0, i32 0), i64 %1920) #7
  %1922 = icmp eq i32 %1921, 0
  br i1 %1922, label %1930, label %1923

; <label>:1923                                    ; preds = %1916
  %1924 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1925 = load i8** %1924, align 8
  %1926 = load i32* %n, align 4
  %1927 = sext i32 %1926 to i64
  %1928 = call i32 @strncmp(i8* %1925, i8* getelementptr inbounds ([5 x i8]* @.str256, i32 0, i32 0), i64 %1927) #7
  %1929 = icmp eq i32 %1928, 0
  br i1 %1929, label %1930, label %2029

; <label>:1930                                    ; preds = %1923, %1916
  %1931 = load i32* %nArg, align 4
  %1932 = icmp sge i32 %1931, 2
  br i1 %1932, label %1933, label %1936

; <label>:1933                                    ; preds = %1930
  %1934 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %1935 = load i8** %1934, align 8
  br label %1937

; <label>:1936                                    ; preds = %1930
  br label %1937

; <label>:1937                                    ; preds = %1936, %1933
  %1938 = phi i8* [ %1935, %1933 ], [ getelementptr inbounds ([7 x i8]* @.str257, i32 0, i32 0), %1936 ]
  store i8* %1938, i8** %zFile14, align 8
  %1939 = load i32* %nArg, align 4
  %1940 = icmp sgt i32 %1939, 2
  br i1 %1940, label %1941, label %1946

; <label>:1941                                    ; preds = %1937
  %1942 = load %struct._IO_FILE** @stderr, align 8
  %1943 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1944 = load i8** %1943, align 8
  %1945 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1942, i8* getelementptr inbounds ([17 x i8]* @.str258, i32 0, i32 0), i8* %1944)
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:1946                                    ; preds = %1937
  %1947 = load i32* %n, align 4
  %1948 = icmp sgt i32 %1947, 1
  br i1 %1948, label %1949, label %1965

; <label>:1949                                    ; preds = %1946
  %1950 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %1951 = load i8** %1950, align 8
  %1952 = load i32* %n, align 4
  %1953 = sext i32 %1952 to i64
  %1954 = call i32 @strncmp(i8* %1951, i8* getelementptr inbounds ([5 x i8]* @.str256, i32 0, i32 0), i64 %1953) #7
  %1955 = icmp eq i32 %1954, 0
  br i1 %1955, label %1956, label %1965

; <label>:1956                                    ; preds = %1949
  %1957 = load i32* %nArg, align 4
  %1958 = icmp slt i32 %1957, 2
  br i1 %1958, label %1959, label %1962

; <label>:1959                                    ; preds = %1956
  %1960 = load %struct._IO_FILE** @stderr, align 8
  %1961 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1960, i8* getelementptr inbounds ([19 x i8]* @.str259, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:1962                                    ; preds = %1956
  %1963 = load %struct.ShellState** %3, align 8
  %1964 = getelementptr inbounds %struct.ShellState* %1963, i32 0, i32 6
  store i32 2, i32* %1964, align 4
  br label %1968

; <label>:1965                                    ; preds = %1949, %1946
  %1966 = load %struct.ShellState** %3, align 8
  %1967 = getelementptr inbounds %struct.ShellState* %1966, i32 0, i32 6
  store i32 0, i32* %1967, align 4
  br label %1968

; <label>:1968                                    ; preds = %1965, %1962
  %1969 = load %struct.ShellState** %3, align 8
  call void @output_reset(%struct.ShellState* %1969)
  %1970 = load i8** %zFile14, align 8
  %1971 = getelementptr inbounds i8* %1970, i64 0
  %1972 = load i8* %1971, align 1
  %1973 = sext i8 %1972 to i32
  %1974 = icmp eq i32 %1973, 124
  br i1 %1974, label %1975, label %2000

; <label>:1975                                    ; preds = %1968
  %1976 = load i8** %zFile14, align 8
  %1977 = getelementptr inbounds i8* %1976, i64 1
  %1978 = call %struct._IO_FILE* @popen(i8* %1977, i8* getelementptr inbounds ([2 x i8]* @.str260, i32 0, i32 0))
  %1979 = load %struct.ShellState** %3, align 8
  %1980 = getelementptr inbounds %struct.ShellState* %1979, i32 0, i32 8
  store %struct._IO_FILE* %1978, %struct._IO_FILE** %1980, align 8
  %1981 = load %struct.ShellState** %3, align 8
  %1982 = getelementptr inbounds %struct.ShellState* %1981, i32 0, i32 8
  %1983 = load %struct._IO_FILE** %1982, align 8
  %1984 = icmp eq %struct._IO_FILE* %1983, null
  br i1 %1984, label %1985, label %1993

; <label>:1985                                    ; preds = %1975
  %1986 = load %struct._IO_FILE** @stderr, align 8
  %1987 = load i8** %zFile14, align 8
  %1988 = getelementptr inbounds i8* %1987, i64 1
  %1989 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1986, i8* getelementptr inbounds ([30 x i8]* @.str261, i32 0, i32 0), i8* %1988)
  %1990 = load %struct._IO_FILE** @stdout, align 8
  %1991 = load %struct.ShellState** %3, align 8
  %1992 = getelementptr inbounds %struct.ShellState* %1991, i32 0, i32 8
  store %struct._IO_FILE* %1990, %struct._IO_FILE** %1992, align 8
  store i32 1, i32* %rc, align 4
  br label %1999

; <label>:1993                                    ; preds = %1975
  %1994 = load %struct.ShellState** %3, align 8
  %1995 = getelementptr inbounds %struct.ShellState* %1994, i32 0, i32 22
  %1996 = getelementptr inbounds [4096 x i8]* %1995, i32 0, i32 0
  %1997 = load i8** %zFile14, align 8
  %1998 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 4096, i8* %1996, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1997)
  br label %1999

; <label>:1999                                    ; preds = %1993, %1985
  br label %2028

; <label>:2000                                    ; preds = %1968
  %2001 = load i8** %zFile14, align 8
  %2002 = call %struct._IO_FILE* @output_file_open(i8* %2001)
  %2003 = load %struct.ShellState** %3, align 8
  %2004 = getelementptr inbounds %struct.ShellState* %2003, i32 0, i32 8
  store %struct._IO_FILE* %2002, %struct._IO_FILE** %2004, align 8
  %2005 = load %struct.ShellState** %3, align 8
  %2006 = getelementptr inbounds %struct.ShellState* %2005, i32 0, i32 8
  %2007 = load %struct._IO_FILE** %2006, align 8
  %2008 = icmp eq %struct._IO_FILE* %2007, null
  br i1 %2008, label %2009, label %2021

; <label>:2009                                    ; preds = %2000
  %2010 = load i8** %zFile14, align 8
  %2011 = call i32 @strcmp(i8* %2010, i8* getelementptr inbounds ([4 x i8]* @.str262, i32 0, i32 0)) #7
  %2012 = icmp ne i32 %2011, 0
  br i1 %2012, label %2013, label %2017

; <label>:2013                                    ; preds = %2009
  %2014 = load %struct._IO_FILE** @stderr, align 8
  %2015 = load i8** %zFile14, align 8
  %2016 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2014, i8* getelementptr inbounds ([29 x i8]* @.str263, i32 0, i32 0), i8* %2015)
  br label %2017

; <label>:2017                                    ; preds = %2013, %2009
  %2018 = load %struct._IO_FILE** @stdout, align 8
  %2019 = load %struct.ShellState** %3, align 8
  %2020 = getelementptr inbounds %struct.ShellState* %2019, i32 0, i32 8
  store %struct._IO_FILE* %2018, %struct._IO_FILE** %2020, align 8
  store i32 1, i32* %rc, align 4
  br label %2027

; <label>:2021                                    ; preds = %2000
  %2022 = load %struct.ShellState** %3, align 8
  %2023 = getelementptr inbounds %struct.ShellState* %2022, i32 0, i32 22
  %2024 = getelementptr inbounds [4096 x i8]* %2023, i32 0, i32 0
  %2025 = load i8** %zFile14, align 8
  %2026 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 4096, i8* %2024, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %2025)
  br label %2027

; <label>:2027                                    ; preds = %2021, %2017
  br label %2028

; <label>:2028                                    ; preds = %2027, %1999
  br label %3346

; <label>:2029                                    ; preds = %1923, %1913
  %2030 = load i32* %c, align 4
  %2031 = icmp eq i32 %2030, 112
  br i1 %2031, label %2032, label %2072

; <label>:2032                                    ; preds = %2029
  %2033 = load i32* %n, align 4
  %2034 = icmp sge i32 %2033, 3
  br i1 %2034, label %2035, label %2072

; <label>:2035                                    ; preds = %2032
  %2036 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2037 = load i8** %2036, align 8
  %2038 = load i32* %n, align 4
  %2039 = sext i32 %2038 to i64
  %2040 = call i32 @strncmp(i8* %2037, i8* getelementptr inbounds ([6 x i8]* @.str264, i32 0, i32 0), i64 %2039) #7
  %2041 = icmp eq i32 %2040, 0
  br i1 %2041, label %2042, label %2072

; <label>:2042                                    ; preds = %2035
  store i32 1, i32* %i15, align 4
  br label %2043

; <label>:2043                                    ; preds = %2064, %2042
  %2044 = load i32* %i15, align 4
  %2045 = load i32* %nArg, align 4
  %2046 = icmp slt i32 %2044, %2045
  br i1 %2046, label %2047, label %2067

; <label>:2047                                    ; preds = %2043
  %2048 = load i32* %i15, align 4
  %2049 = icmp sgt i32 %2048, 1
  br i1 %2049, label %2050, label %2055

; <label>:2050                                    ; preds = %2047
  %2051 = load %struct.ShellState** %3, align 8
  %2052 = getelementptr inbounds %struct.ShellState* %2051, i32 0, i32 8
  %2053 = load %struct._IO_FILE** %2052, align 8
  %2054 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2053, i8* getelementptr inbounds ([2 x i8]* @.str244, i32 0, i32 0))
  br label %2055

; <label>:2055                                    ; preds = %2050, %2047
  %2056 = load %struct.ShellState** %3, align 8
  %2057 = getelementptr inbounds %struct.ShellState* %2056, i32 0, i32 8
  %2058 = load %struct._IO_FILE** %2057, align 8
  %2059 = load i32* %i15, align 4
  %2060 = sext i32 %2059 to i64
  %2061 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %2060
  %2062 = load i8** %2061, align 8
  %2063 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2058, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %2062)
  br label %2064

; <label>:2064                                    ; preds = %2055
  %2065 = load i32* %i15, align 4
  %2066 = add nsw i32 %2065, 1
  store i32 %2066, i32* %i15, align 4
  br label %2043

; <label>:2067                                    ; preds = %2043
  %2068 = load %struct.ShellState** %3, align 8
  %2069 = getelementptr inbounds %struct.ShellState* %2068, i32 0, i32 8
  %2070 = load %struct._IO_FILE** %2069, align 8
  %2071 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2070, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  br label %3345

; <label>:2072                                    ; preds = %2035, %2032, %2029
  %2073 = load i32* %c, align 4
  %2074 = icmp eq i32 %2073, 112
  br i1 %2074, label %2075, label %2097

; <label>:2075                                    ; preds = %2072
  %2076 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2077 = load i8** %2076, align 8
  %2078 = load i32* %n, align 4
  %2079 = sext i32 %2078 to i64
  %2080 = call i32 @strncmp(i8* %2077, i8* getelementptr inbounds ([7 x i8]* @.str265, i32 0, i32 0), i64 %2079) #7
  %2081 = icmp eq i32 %2080, 0
  br i1 %2081, label %2082, label %2097

; <label>:2082                                    ; preds = %2075
  %2083 = load i32* %nArg, align 4
  %2084 = icmp sge i32 %2083, 2
  br i1 %2084, label %2085, label %2089

; <label>:2085                                    ; preds = %2082
  %2086 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2087 = load i8** %2086, align 8
  %2088 = call i8* @strncpy(i8* getelementptr inbounds ([20 x i8]* @mainPrompt, i32 0, i32 0), i8* %2087, i64 19) #5
  br label %2089

; <label>:2089                                    ; preds = %2085, %2082
  %2090 = load i32* %nArg, align 4
  %2091 = icmp sge i32 %2090, 3
  br i1 %2091, label %2092, label %2096

; <label>:2092                                    ; preds = %2089
  %2093 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %2094 = load i8** %2093, align 8
  %2095 = call i8* @strncpy(i8* getelementptr inbounds ([20 x i8]* @continuePrompt, i32 0, i32 0), i8* %2094, i64 19) #5
  br label %2096

; <label>:2096                                    ; preds = %2092, %2089
  br label %3344

; <label>:2097                                    ; preds = %2075, %2072
  %2098 = load i32* %c, align 4
  %2099 = icmp eq i32 %2098, 113
  br i1 %2099, label %2100, label %2108

; <label>:2100                                    ; preds = %2097
  %2101 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2102 = load i8** %2101, align 8
  %2103 = load i32* %n, align 4
  %2104 = sext i32 %2103 to i64
  %2105 = call i32 @strncmp(i8* %2102, i8* getelementptr inbounds ([5 x i8]* @.str266, i32 0, i32 0), i64 %2104) #7
  %2106 = icmp eq i32 %2105, 0
  br i1 %2106, label %2107, label %2108

; <label>:2107                                    ; preds = %2100
  store i32 2, i32* %rc, align 4
  br label %3343

; <label>:2108                                    ; preds = %2100, %2097
  %2109 = load i32* %c, align 4
  %2110 = icmp eq i32 %2109, 114
  br i1 %2110, label %2111, label %2145

; <label>:2111                                    ; preds = %2108
  %2112 = load i32* %n, align 4
  %2113 = icmp sge i32 %2112, 3
  br i1 %2113, label %2114, label %2145

; <label>:2114                                    ; preds = %2111
  %2115 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2116 = load i8** %2115, align 8
  %2117 = load i32* %n, align 4
  %2118 = sext i32 %2117 to i64
  %2119 = call i32 @strncmp(i8* %2116, i8* getelementptr inbounds ([5 x i8]* @.str267, i32 0, i32 0), i64 %2118) #7
  %2120 = icmp eq i32 %2119, 0
  br i1 %2120, label %2121, label %2145

; <label>:2121                                    ; preds = %2114
  %2122 = load i32* %nArg, align 4
  %2123 = icmp ne i32 %2122, 2
  br i1 %2123, label %2124, label %2127

; <label>:2124                                    ; preds = %2121
  %2125 = load %struct._IO_FILE** @stderr, align 8
  %2126 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2125, i8* getelementptr inbounds ([19 x i8]* @.str268, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:2127                                    ; preds = %2121
  %2128 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2129 = load i8** %2128, align 8
  %2130 = call %struct._IO_FILE* @fopen64(i8* %2129, i8* getelementptr inbounds ([3 x i8]* @.str197, i32 0, i32 0))
  store %struct._IO_FILE* %2130, %struct._IO_FILE** %alt, align 8
  %2131 = load %struct._IO_FILE** %alt, align 8
  %2132 = icmp eq %struct._IO_FILE* %2131, null
  br i1 %2132, label %2133, label %2138

; <label>:2133                                    ; preds = %2127
  %2134 = load %struct._IO_FILE** @stderr, align 8
  %2135 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2136 = load i8** %2135, align 8
  %2137 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2134, i8* getelementptr inbounds ([25 x i8]* @.str141, i32 0, i32 0), i8* %2136)
  store i32 1, i32* %rc, align 4
  br label %2144

; <label>:2138                                    ; preds = %2127
  %2139 = load %struct.ShellState** %3, align 8
  %2140 = load %struct._IO_FILE** %alt, align 8
  %2141 = call i32 @process_input(%struct.ShellState* %2139, %struct._IO_FILE* %2140)
  store i32 %2141, i32* %rc, align 4
  %2142 = load %struct._IO_FILE** %alt, align 8
  %2143 = call i32 @fclose(%struct._IO_FILE* %2142)
  br label %2144

; <label>:2144                                    ; preds = %2138, %2133
  br label %3342

; <label>:2145                                    ; preds = %2114, %2111, %2108
  %2146 = load i32* %c, align 4
  %2147 = icmp eq i32 %2146, 114
  br i1 %2147, label %2148, label %2253

; <label>:2148                                    ; preds = %2145
  %2149 = load i32* %n, align 4
  %2150 = icmp sge i32 %2149, 3
  br i1 %2150, label %2151, label %2253

; <label>:2151                                    ; preds = %2148
  %2152 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2153 = load i8** %2152, align 8
  %2154 = load i32* %n, align 4
  %2155 = sext i32 %2154 to i64
  %2156 = call i32 @strncmp(i8* %2153, i8* getelementptr inbounds ([8 x i8]* @.str269, i32 0, i32 0), i64 %2155) #7
  %2157 = icmp eq i32 %2156, 0
  br i1 %2157, label %2158, label %2253

; <label>:2158                                    ; preds = %2151
  store i32 0, i32* %nTimeout, align 4
  %2159 = load i32* %nArg, align 4
  %2160 = icmp eq i32 %2159, 2
  br i1 %2160, label %2161, label %2164

; <label>:2161                                    ; preds = %2158
  %2162 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2163 = load i8** %2162, align 8
  store i8* %2163, i8** %zSrcFile, align 8
  store i8* getelementptr inbounds ([5 x i8]* @.str140, i32 0, i32 0), i8** %zDb16, align 8
  br label %2176

; <label>:2164                                    ; preds = %2158
  %2165 = load i32* %nArg, align 4
  %2166 = icmp eq i32 %2165, 3
  br i1 %2166, label %2167, label %2172

; <label>:2167                                    ; preds = %2164
  %2168 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %2169 = load i8** %2168, align 8
  store i8* %2169, i8** %zSrcFile, align 8
  %2170 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2171 = load i8** %2170, align 8
  store i8* %2171, i8** %zDb16, align 8
  br label %2175

; <label>:2172                                    ; preds = %2164
  %2173 = load %struct._IO_FILE** @stderr, align 8
  %2174 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2173, i8* getelementptr inbounds ([27 x i8]* @.str270, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:2175                                    ; preds = %2167
  br label %2176

; <label>:2176                                    ; preds = %2175, %2161
  %2177 = load i8** %zSrcFile, align 8
  %2178 = call i32 @sqlite3_open(i8* %2177, %struct.sqlite3** %pSrc)
  store i32 %2178, i32* %rc, align 4
  %2179 = load i32* %rc, align 4
  %2180 = icmp ne i32 %2179, 0
  br i1 %2180, label %2181, label %2187

; <label>:2181                                    ; preds = %2176
  %2182 = load %struct._IO_FILE** @stderr, align 8
  %2183 = load i8** %zSrcFile, align 8
  %2184 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2182, i8* getelementptr inbounds ([25 x i8]* @.str141, i32 0, i32 0), i8* %2183)
  %2185 = load %struct.sqlite3** %pSrc, align 8
  %2186 = call i32 @sqlite3_close(%struct.sqlite3* %2185)
  store i32 1, i32* %1
  br label %3389

; <label>:2187                                    ; preds = %2176
  %2188 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %2188, i32 0)
  %2189 = load %struct.ShellState** %3, align 8
  %2190 = getelementptr inbounds %struct.ShellState* %2189, i32 0, i32 0
  %2191 = load %struct.sqlite3** %2190, align 8
  %2192 = load i8** %zDb16, align 8
  %2193 = load %struct.sqlite3** %pSrc, align 8
  %2194 = call %struct.sqlite3_backup* @sqlite3_backup_init(%struct.sqlite3* %2191, i8* %2192, %struct.sqlite3* %2193, i8* getelementptr inbounds ([5 x i8]* @.str140, i32 0, i32 0))
  store %struct.sqlite3_backup* %2194, %struct.sqlite3_backup** %pBackup17, align 8
  %2195 = load %struct.sqlite3_backup** %pBackup17, align 8
  %2196 = icmp eq %struct.sqlite3_backup* %2195, null
  br i1 %2196, label %2197, label %2206

; <label>:2197                                    ; preds = %2187
  %2198 = load %struct._IO_FILE** @stderr, align 8
  %2199 = load %struct.ShellState** %3, align 8
  %2200 = getelementptr inbounds %struct.ShellState* %2199, i32 0, i32 0
  %2201 = load %struct.sqlite3** %2200, align 8
  %2202 = call i8* @sqlite3_errmsg(%struct.sqlite3* %2201)
  %2203 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2198, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %2202)
  %2204 = load %struct.sqlite3** %pSrc, align 8
  %2205 = call i32 @sqlite3_close(%struct.sqlite3* %2204)
  store i32 1, i32* %1
  br label %3389

; <label>:2206                                    ; preds = %2187
  br label %2207

; <label>:2207                                    ; preds = %2226, %2206
  %2208 = load %struct.sqlite3_backup** %pBackup17, align 8
  %2209 = call i32 @sqlite3_backup_step(%struct.sqlite3_backup* %2208, i32 100)
  store i32 %2209, i32* %rc, align 4
  %2210 = icmp eq i32 %2209, 0
  br i1 %2210, label %2214, label %2211

; <label>:2211                                    ; preds = %2207
  %2212 = load i32* %rc, align 4
  %2213 = icmp eq i32 %2212, 5
  br label %2214

; <label>:2214                                    ; preds = %2211, %2207
  %2215 = phi i1 [ true, %2207 ], [ %2213, %2211 ]
  br i1 %2215, label %2216, label %2227

; <label>:2216                                    ; preds = %2214
  %2217 = load i32* %rc, align 4
  %2218 = icmp eq i32 %2217, 5
  br i1 %2218, label %2219, label %2226

; <label>:2219                                    ; preds = %2216
  %2220 = load i32* %nTimeout, align 4
  %2221 = add nsw i32 %2220, 1
  store i32 %2221, i32* %nTimeout, align 4
  %2222 = icmp sge i32 %2220, 3
  br i1 %2222, label %2223, label %2224

; <label>:2223                                    ; preds = %2219
  br label %2227

; <label>:2224                                    ; preds = %2219
  %2225 = call i32 @sqlite3_sleep(i32 100)
  br label %2226

; <label>:2226                                    ; preds = %2224, %2216
  br label %2207

; <label>:2227                                    ; preds = %2223, %2214
  %2228 = load %struct.sqlite3_backup** %pBackup17, align 8
  %2229 = call i32 @sqlite3_backup_finish(%struct.sqlite3_backup* %2228)
  %2230 = load i32* %rc, align 4
  %2231 = icmp eq i32 %2230, 101
  br i1 %2231, label %2232, label %2233

; <label>:2232                                    ; preds = %2227
  store i32 0, i32* %rc, align 4
  br label %2250

; <label>:2233                                    ; preds = %2227
  %2234 = load i32* %rc, align 4
  %2235 = icmp eq i32 %2234, 5
  br i1 %2235, label %2239, label %2236

; <label>:2236                                    ; preds = %2233
  %2237 = load i32* %rc, align 4
  %2238 = icmp eq i32 %2237, 6
  br i1 %2238, label %2239, label %2242

; <label>:2239                                    ; preds = %2236, %2233
  %2240 = load %struct._IO_FILE** @stderr, align 8
  %2241 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2240, i8* getelementptr inbounds ([32 x i8]* @.str271, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %2249

; <label>:2242                                    ; preds = %2236
  %2243 = load %struct._IO_FILE** @stderr, align 8
  %2244 = load %struct.ShellState** %3, align 8
  %2245 = getelementptr inbounds %struct.ShellState* %2244, i32 0, i32 0
  %2246 = load %struct.sqlite3** %2245, align 8
  %2247 = call i8* @sqlite3_errmsg(%struct.sqlite3* %2246)
  %2248 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2243, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %2247)
  store i32 1, i32* %rc, align 4
  br label %2249

; <label>:2249                                    ; preds = %2242, %2239
  br label %2250

; <label>:2250                                    ; preds = %2249, %2232
  %2251 = load %struct.sqlite3** %pSrc, align 8
  %2252 = call i32 @sqlite3_close(%struct.sqlite3* %2251)
  br label %3341

; <label>:2253                                    ; preds = %2151, %2148, %2145
  %2254 = load i32* %c, align 4
  %2255 = icmp eq i32 %2254, 115
  br i1 %2255, label %2256, label %2278

; <label>:2256                                    ; preds = %2253
  %2257 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2258 = load i8** %2257, align 8
  %2259 = load i32* %n, align 4
  %2260 = sext i32 %2259 to i64
  %2261 = call i32 @strncmp(i8* %2258, i8* getelementptr inbounds ([10 x i8]* @.str272, i32 0, i32 0), i64 %2260) #7
  %2262 = icmp eq i32 %2261, 0
  br i1 %2262, label %2263, label %2278

; <label>:2263                                    ; preds = %2256
  %2264 = load i32* %nArg, align 4
  %2265 = icmp eq i32 %2264, 2
  br i1 %2265, label %2266, label %2274

; <label>:2266                                    ; preds = %2263
  %2267 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2268 = load i8** %2267, align 8
  %2269 = call i32 @booleanValue(i8* %2268)
  %2270 = load %struct.ShellState** %3, align 8
  %2271 = getelementptr inbounds %struct.ShellState* %2270, i32 0, i32 4
  store i32 %2269, i32* %2271, align 4
  %2272 = load %struct._IO_FILE** @stderr, align 8
  %2273 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2272, i8* getelementptr inbounds ([50 x i8]* @.str273, i32 0, i32 0))
  br label %2277

; <label>:2274                                    ; preds = %2263
  %2275 = load %struct._IO_FILE** @stderr, align 8
  %2276 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2275, i8* getelementptr inbounds ([26 x i8]* @.str274, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %2277

; <label>:2277                                    ; preds = %2274, %2266
  br label %3340

; <label>:2278                                    ; preds = %2256, %2253
  %2279 = load i32* %c, align 4
  %2280 = icmp eq i32 %2279, 115
  br i1 %2280, label %2281, label %2392

; <label>:2281                                    ; preds = %2278
  %2282 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2283 = load i8** %2282, align 8
  %2284 = load i32* %n, align 4
  %2285 = sext i32 %2284 to i64
  %2286 = call i32 @strncmp(i8* %2283, i8* getelementptr inbounds ([7 x i8]* @.str275, i32 0, i32 0), i64 %2285) #7
  %2287 = icmp eq i32 %2286, 0
  br i1 %2287, label %2288, label %2392

; <label>:2288                                    ; preds = %2281
  store i8* null, i8** %zErrMsg19, align 8
  %2289 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %2289, i32 0)
  %2290 = bitcast %struct.ShellState* %data18 to i8*
  %2291 = load %struct.ShellState** %3, align 8
  %2292 = bitcast %struct.ShellState* %2291 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %2290, i8* %2292, i64 5512, i32 8, i1 false)
  %2293 = getelementptr inbounds %struct.ShellState* %data18, i32 0, i32 13
  store i32 0, i32* %2293, align 4
  %2294 = getelementptr inbounds %struct.ShellState* %data18, i32 0, i32 11
  store i32 3, i32* %2294, align 4
  %2295 = load i32* %nArg, align 4
  %2296 = icmp eq i32 %2295, 2
  br i1 %2296, label %2297, label %2362

; <label>:2297                                    ; preds = %2288
  store i32 0, i32* %i20, align 4
  br label %2298

; <label>:2298                                    ; preds = %2321, %2297
  %2299 = load i32* %i20, align 4
  %2300 = sext i32 %2299 to i64
  %2301 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2302 = load i8** %2301, align 8
  %2303 = getelementptr inbounds i8* %2302, i64 %2300
  %2304 = load i8* %2303, align 1
  %2305 = icmp ne i8 %2304, 0
  br i1 %2305, label %2306, label %2324

; <label>:2306                                    ; preds = %2298
  %2307 = load i32* %i20, align 4
  %2308 = sext i32 %2307 to i64
  %2309 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2310 = load i8** %2309, align 8
  %2311 = getelementptr inbounds i8* %2310, i64 %2308
  %2312 = load i8* %2311, align 1
  %2313 = zext i8 %2312 to i32
  %2314 = call i32 @tolower(i32 %2313) #5
  %2315 = trunc i32 %2314 to i8
  %2316 = load i32* %i20, align 4
  %2317 = sext i32 %2316 to i64
  %2318 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2319 = load i8** %2318, align 8
  %2320 = getelementptr inbounds i8* %2319, i64 %2317
  store i8 %2315, i8* %2320, align 1
  br label %2321

; <label>:2321                                    ; preds = %2306
  %2322 = load i32* %i20, align 4
  %2323 = add nsw i32 %2322, 1
  store i32 %2323, i32* %i20, align 4
  br label %2298

; <label>:2324                                    ; preds = %2298
  %2325 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2326 = load i8** %2325, align 8
  %2327 = call i32 @strcmp(i8* %2326, i8* getelementptr inbounds ([14 x i8]* @.str276, i32 0, i32 0)) #7
  %2328 = icmp eq i32 %2327, 0
  br i1 %2328, label %2329, label %2338

; <label>:2329                                    ; preds = %2324
  %2330 = getelementptr inbounds [2 x i8*]* %new_argv, i32 0, i64 0
  store i8* getelementptr inbounds ([105 x i8]* @.str277, i32 0, i32 0), i8** %2330, align 8
  %2331 = getelementptr inbounds [2 x i8*]* %new_argv, i32 0, i64 1
  store i8* null, i8** %2331, align 8
  %2332 = getelementptr inbounds [2 x i8*]* %new_colv, i32 0, i64 0
  store i8* getelementptr inbounds ([4 x i8]* @.str278, i32 0, i32 0), i8** %2332, align 8
  %2333 = getelementptr inbounds [2 x i8*]* %new_colv, i32 0, i64 1
  store i8* null, i8** %2333, align 8
  %2334 = bitcast %struct.ShellState* %data18 to i8*
  %2335 = getelementptr inbounds [2 x i8*]* %new_argv, i32 0, i32 0
  %2336 = getelementptr inbounds [2 x i8*]* %new_colv, i32 0, i32 0
  %2337 = call i32 @callback(i8* %2334, i32 1, i8** %2335, i8** %2336)
  store i32 0, i32* %rc, align 4
  br label %2361

; <label>:2338                                    ; preds = %2324
  %2339 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2340 = load i8** %2339, align 8
  %2341 = call i32 @strcmp(i8* %2340, i8* getelementptr inbounds ([19 x i8]* @.str279, i32 0, i32 0)) #7
  %2342 = icmp eq i32 %2341, 0
  br i1 %2342, label %2343, label %2352

; <label>:2343                                    ; preds = %2338
  %2344 = getelementptr inbounds [2 x i8*]* %new_argv21, i32 0, i64 0
  store i8* getelementptr inbounds ([115 x i8]* @.str280, i32 0, i32 0), i8** %2344, align 8
  %2345 = getelementptr inbounds [2 x i8*]* %new_argv21, i32 0, i64 1
  store i8* null, i8** %2345, align 8
  %2346 = getelementptr inbounds [2 x i8*]* %new_colv22, i32 0, i64 0
  store i8* getelementptr inbounds ([4 x i8]* @.str278, i32 0, i32 0), i8** %2346, align 8
  %2347 = getelementptr inbounds [2 x i8*]* %new_colv22, i32 0, i64 1
  store i8* null, i8** %2347, align 8
  %2348 = bitcast %struct.ShellState* %data18 to i8*
  %2349 = getelementptr inbounds [2 x i8*]* %new_argv21, i32 0, i32 0
  %2350 = getelementptr inbounds [2 x i8*]* %new_colv22, i32 0, i32 0
  %2351 = call i32 @callback(i8* %2348, i32 1, i8** %2349, i8** %2350)
  store i32 0, i32* %rc, align 4
  br label %2360

; <label>:2352                                    ; preds = %2338
  %2353 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2354 = load i8** %2353, align 8
  store i8* %2354, i8** @zShellStatic, align 8
  %2355 = load %struct.ShellState** %3, align 8
  %2356 = getelementptr inbounds %struct.ShellState* %2355, i32 0, i32 0
  %2357 = load %struct.sqlite3** %2356, align 8
  %2358 = bitcast %struct.ShellState* %data18 to i8*
  %2359 = call i32 @sqlite3_exec(%struct.sqlite3* %2357, i8* getelementptr inbounds ([274 x i8]* @.str281, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* @callback, i8* %2358, i8** %zErrMsg19)
  store i32 %2359, i32* %rc, align 4
  store i8* null, i8** @zShellStatic, align 8
  br label %2360

; <label>:2360                                    ; preds = %2352, %2343
  br label %2361

; <label>:2361                                    ; preds = %2360, %2329
  br label %2375

; <label>:2362                                    ; preds = %2288
  %2363 = load i32* %nArg, align 4
  %2364 = icmp eq i32 %2363, 1
  br i1 %2364, label %2365, label %2371

; <label>:2365                                    ; preds = %2362
  %2366 = load %struct.ShellState** %3, align 8
  %2367 = getelementptr inbounds %struct.ShellState* %2366, i32 0, i32 0
  %2368 = load %struct.sqlite3** %2367, align 8
  %2369 = bitcast %struct.ShellState* %data18 to i8*
  %2370 = call i32 @sqlite3_exec(%struct.sqlite3* %2368, i8* getelementptr inbounds ([263 x i8]* @.str282, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* @callback, i8* %2369, i8** %zErrMsg19)
  store i32 %2370, i32* %rc, align 4
  br label %2374

; <label>:2371                                    ; preds = %2362
  %2372 = load %struct._IO_FILE** @stderr, align 8
  %2373 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2372, i8* getelementptr inbounds ([31 x i8]* @.str283, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:2374                                    ; preds = %2365
  br label %2375

; <label>:2375                                    ; preds = %2374, %2361
  %2376 = load i8** %zErrMsg19, align 8
  %2377 = icmp ne i8* %2376, null
  br i1 %2377, label %2378, label %2383

; <label>:2378                                    ; preds = %2375
  %2379 = load %struct._IO_FILE** @stderr, align 8
  %2380 = load i8** %zErrMsg19, align 8
  %2381 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2379, i8* getelementptr inbounds ([11 x i8]* @.str39, i32 0, i32 0), i8* %2380)
  %2382 = load i8** %zErrMsg19, align 8
  call void @sqlite3_free(i8* %2382)
  store i32 1, i32* %rc, align 4
  br label %2391

; <label>:2383                                    ; preds = %2375
  %2384 = load i32* %rc, align 4
  %2385 = icmp ne i32 %2384, 0
  br i1 %2385, label %2386, label %2389

; <label>:2386                                    ; preds = %2383
  %2387 = load %struct._IO_FILE** @stderr, align 8
  %2388 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2387, i8* getelementptr inbounds ([36 x i8]* @.str284, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %2390

; <label>:2389                                    ; preds = %2383
  store i32 0, i32* %rc, align 4
  br label %2390

; <label>:2390                                    ; preds = %2389, %2386
  br label %2391

; <label>:2391                                    ; preds = %2390, %2378
  br label %3339

; <label>:2392                                    ; preds = %2281, %2278
  %2393 = load i32* %c, align 4
  %2394 = icmp eq i32 %2393, 115
  br i1 %2394, label %2395, label %2432

; <label>:2395                                    ; preds = %2392
  %2396 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2397 = load i8** %2396, align 8
  %2398 = load i32* %n, align 4
  %2399 = sext i32 %2398 to i64
  %2400 = call i32 @strncmp(i8* %2397, i8* getelementptr inbounds ([10 x i8]* @.str285, i32 0, i32 0), i64 %2399) #7
  %2401 = icmp eq i32 %2400, 0
  br i1 %2401, label %2402, label %2432

; <label>:2402                                    ; preds = %2395
  %2403 = load i32* %nArg, align 4
  %2404 = icmp slt i32 %2403, 2
  br i1 %2404, label %2408, label %2405

; <label>:2405                                    ; preds = %2402
  %2406 = load i32* %nArg, align 4
  %2407 = icmp sgt i32 %2406, 3
  br i1 %2407, label %2408, label %2411

; <label>:2408                                    ; preds = %2405, %2402
  %2409 = load %struct._IO_FILE** @stderr, align 8
  %2410 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2409, i8* getelementptr inbounds ([29 x i8]* @.str286, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %2411

; <label>:2411                                    ; preds = %2408, %2405
  %2412 = load i32* %nArg, align 4
  %2413 = icmp sge i32 %2412, 2
  br i1 %2413, label %2414, label %2421

; <label>:2414                                    ; preds = %2411
  %2415 = load %struct.ShellState** %3, align 8
  %2416 = getelementptr inbounds %struct.ShellState* %2415, i32 0, i32 16
  %2417 = getelementptr inbounds [20 x i8]* %2416, i32 0, i32 0
  %2418 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2419 = load i8** %2418, align 8
  %2420 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %2417, i8* getelementptr inbounds ([5 x i8]* @.str89, i32 0, i32 0), i32 19, i8* %2419)
  br label %2421

; <label>:2421                                    ; preds = %2414, %2411
  %2422 = load i32* %nArg, align 4
  %2423 = icmp sge i32 %2422, 3
  br i1 %2423, label %2424, label %2431

; <label>:2424                                    ; preds = %2421
  %2425 = load %struct.ShellState** %3, align 8
  %2426 = getelementptr inbounds %struct.ShellState* %2425, i32 0, i32 17
  %2427 = getelementptr inbounds [20 x i8]* %2426, i32 0, i32 0
  %2428 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %2429 = load i8** %2428, align 8
  %2430 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 20, i8* %2427, i8* getelementptr inbounds ([5 x i8]* @.str89, i32 0, i32 0), i32 19, i8* %2429)
  br label %2431

; <label>:2431                                    ; preds = %2424, %2421
  br label %3338

; <label>:2432                                    ; preds = %2395, %2392
  %2433 = load i32* %c, align 4
  %2434 = icmp eq i32 %2433, 115
  br i1 %2434, label %2435, label %2496

; <label>:2435                                    ; preds = %2432
  %2436 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2437 = load i8** %2436, align 8
  %2438 = load i32* %n, align 4
  %2439 = sext i32 %2438 to i64
  %2440 = call i32 @strncmp(i8* %2437, i8* getelementptr inbounds ([6 x i8]* @.str287, i32 0, i32 0), i64 %2439) #7
  %2441 = icmp eq i32 %2440, 0
  br i1 %2441, label %2449, label %2442

; <label>:2442                                    ; preds = %2435
  %2443 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2444 = load i8** %2443, align 8
  %2445 = load i32* %n, align 4
  %2446 = sext i32 %2445 to i64
  %2447 = call i32 @strncmp(i8* %2444, i8* getelementptr inbounds ([7 x i8]* @.str288, i32 0, i32 0), i64 %2446) #7
  %2448 = icmp eq i32 %2447, 0
  br i1 %2448, label %2449, label %2496

; <label>:2449                                    ; preds = %2442, %2435
  %2450 = load i32* %nArg, align 4
  %2451 = icmp slt i32 %2450, 2
  br i1 %2451, label %2452, label %2455

; <label>:2452                                    ; preds = %2449
  %2453 = load %struct._IO_FILE** @stderr, align 8
  %2454 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2453, i8* getelementptr inbounds ([24 x i8]* @.str289, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:2455                                    ; preds = %2449
  %2456 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2457 = load i8** %2456, align 8
  %2458 = call i8* @strchr(i8* %2457, i32 32) #7
  %2459 = icmp eq i8* %2458, null
  %2460 = select i1 %2459, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str290, i32 0, i32 0)
  %2461 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2462 = load i8** %2461, align 8
  %2463 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* %2460, i8* %2462)
  store i8* %2463, i8** %zCmd, align 8
  store i32 2, i32* %i23, align 4
  br label %2464

; <label>:2464                                    ; preds = %2482, %2455
  %2465 = load i32* %i23, align 4
  %2466 = load i32* %nArg, align 4
  %2467 = icmp slt i32 %2465, %2466
  br i1 %2467, label %2468, label %2485

; <label>:2468                                    ; preds = %2464
  %2469 = load i32* %i23, align 4
  %2470 = sext i32 %2469 to i64
  %2471 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %2470
  %2472 = load i8** %2471, align 8
  %2473 = call i8* @strchr(i8* %2472, i32 32) #7
  %2474 = icmp eq i8* %2473, null
  %2475 = select i1 %2474, i8* getelementptr inbounds ([6 x i8]* @.str291, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str292, i32 0, i32 0)
  %2476 = load i8** %zCmd, align 8
  %2477 = load i32* %i23, align 4
  %2478 = sext i32 %2477 to i64
  %2479 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %2478
  %2480 = load i8** %2479, align 8
  %2481 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* %2475, i8* %2476, i8* %2480)
  store i8* %2481, i8** %zCmd, align 8
  br label %2482

; <label>:2482                                    ; preds = %2468
  %2483 = load i32* %i23, align 4
  %2484 = add nsw i32 %2483, 1
  store i32 %2484, i32* %i23, align 4
  br label %2464

; <label>:2485                                    ; preds = %2464
  %2486 = load i8** %zCmd, align 8
  %2487 = call i32 @system(i8* %2486)
  store i32 %2487, i32* %x, align 4
  %2488 = load i8** %zCmd, align 8
  call void @sqlite3_free(i8* %2488)
  %2489 = load i32* %x, align 4
  %2490 = icmp ne i32 %2489, 0
  br i1 %2490, label %2491, label %2495

; <label>:2491                                    ; preds = %2485
  %2492 = load %struct._IO_FILE** @stderr, align 8
  %2493 = load i32* %x, align 4
  %2494 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2492, i8* getelementptr inbounds ([27 x i8]* @.str293, i32 0, i32 0), i32 %2493)
  br label %2495

; <label>:2495                                    ; preds = %2491, %2485
  br label %3337

; <label>:2496                                    ; preds = %2442, %2432
  %2497 = load i32* %c, align 4
  %2498 = icmp eq i32 %2497, 115
  br i1 %2498, label %2499, label %2663

; <label>:2499                                    ; preds = %2496
  %2500 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2501 = load i8** %2500, align 8
  %2502 = load i32* %n, align 4
  %2503 = sext i32 %2502 to i64
  %2504 = call i32 @strncmp(i8* %2501, i8* getelementptr inbounds ([5 x i8]* @.str294, i32 0, i32 0), i64 %2503) #7
  %2505 = icmp eq i32 %2504, 0
  br i1 %2505, label %2506, label %2663

; <label>:2506                                    ; preds = %2499
  %2507 = load i32* %nArg, align 4
  %2508 = icmp ne i32 %2507, 1
  br i1 %2508, label %2509, label %2512

; <label>:2509                                    ; preds = %2506
  %2510 = load %struct._IO_FILE** @stderr, align 8
  %2511 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2510, i8* getelementptr inbounds ([14 x i8]* @.str295, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:2512                                    ; preds = %2506
  %2513 = load %struct.ShellState** %3, align 8
  %2514 = getelementptr inbounds %struct.ShellState* %2513, i32 0, i32 8
  %2515 = load %struct._IO_FILE** %2514, align 8
  %2516 = load %struct.ShellState** %3, align 8
  %2517 = getelementptr inbounds %struct.ShellState* %2516, i32 0, i32 1
  %2518 = load i32* %2517, align 4
  %2519 = icmp ne i32 %2518, 0
  %2520 = select i1 %2519, i8* getelementptr inbounds ([3 x i8]* @.str297, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str262, i32 0, i32 0)
  %2521 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2515, i8* getelementptr inbounds ([13 x i8]* @.str296, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str167, i32 0, i32 0), i8* %2520)
  %2522 = load %struct.ShellState** %3, align 8
  %2523 = getelementptr inbounds %struct.ShellState* %2522, i32 0, i32 8
  %2524 = load %struct._IO_FILE** %2523, align 8
  %2525 = load %struct.ShellState** %3, align 8
  %2526 = getelementptr inbounds %struct.ShellState* %2525, i32 0, i32 2
  %2527 = load i32* %2526, align 4
  %2528 = icmp ne i32 %2527, 0
  %2529 = select i1 %2528, i8* getelementptr inbounds ([3 x i8]* @.str297, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str262, i32 0, i32 0)
  %2530 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2524, i8* getelementptr inbounds ([13 x i8]* @.str296, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str169, i32 0, i32 0), i8* %2529)
  %2531 = load %struct.ShellState** %3, align 8
  %2532 = getelementptr inbounds %struct.ShellState* %2531, i32 0, i32 8
  %2533 = load %struct._IO_FILE** %2532, align 8
  %2534 = load %struct.ShellState** %3, align 8
  %2535 = getelementptr inbounds %struct.ShellState* %2534, i32 0, i32 21
  %2536 = getelementptr inbounds %struct.SavedModeInfo* %2535, i32 0, i32 0
  %2537 = load i32* %2536, align 4
  %2538 = icmp ne i32 %2537, 0
  %2539 = select i1 %2538, i8* getelementptr inbounds ([3 x i8]* @.str297, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str262, i32 0, i32 0)
  %2540 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2533, i8* getelementptr inbounds ([11 x i8]* @.str298, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str134, i32 0, i32 0), i8* %2539)
  %2541 = load %struct.ShellState** %3, align 8
  %2542 = getelementptr inbounds %struct.ShellState* %2541, i32 0, i32 8
  %2543 = load %struct._IO_FILE** %2542, align 8
  %2544 = load %struct.ShellState** %3, align 8
  %2545 = getelementptr inbounds %struct.ShellState* %2544, i32 0, i32 13
  %2546 = load i32* %2545, align 4
  %2547 = icmp ne i32 %2546, 0
  %2548 = select i1 %2547, i8* getelementptr inbounds ([3 x i8]* @.str297, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str262, i32 0, i32 0)
  %2549 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2543, i8* getelementptr inbounds ([13 x i8]* @.str296, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str185, i32 0, i32 0), i8* %2548)
  %2550 = load %struct.ShellState** %3, align 8
  %2551 = getelementptr inbounds %struct.ShellState* %2550, i32 0, i32 8
  %2552 = load %struct._IO_FILE** %2551, align 8
  %2553 = load %struct.ShellState** %3, align 8
  %2554 = getelementptr inbounds %struct.ShellState* %2553, i32 0, i32 11
  %2555 = load i32* %2554, align 4
  %2556 = sext i32 %2555 to i64
  %2557 = getelementptr inbounds [10 x i8*]* @modeDescr, i32 0, i64 %2556
  %2558 = load i8** %2557, align 8
  %2559 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2552, i8* getelementptr inbounds ([13 x i8]* @.str296, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str238, i32 0, i32 0), i8* %2558)
  %2560 = load %struct.ShellState** %3, align 8
  %2561 = getelementptr inbounds %struct.ShellState* %2560, i32 0, i32 8
  %2562 = load %struct._IO_FILE** %2561, align 8
  %2563 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2562, i8* getelementptr inbounds ([10 x i8]* @.str299, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8]* @.str252, i32 0, i32 0))
  %2564 = load %struct.ShellState** %3, align 8
  %2565 = getelementptr inbounds %struct.ShellState* %2564, i32 0, i32 8
  %2566 = load %struct._IO_FILE** %2565, align 8
  %2567 = load %struct.ShellState** %3, align 8
  %2568 = getelementptr inbounds %struct.ShellState* %2567, i32 0, i32 20
  %2569 = getelementptr inbounds [20 x i8]* %2568, i32 0, i32 0
  call void @output_c_string(%struct._IO_FILE* %2566, i8* %2569)
  %2570 = load %struct.ShellState** %3, align 8
  %2571 = getelementptr inbounds %struct.ShellState* %2570, i32 0, i32 8
  %2572 = load %struct._IO_FILE** %2571, align 8
  %2573 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2572, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  %2574 = load %struct.ShellState** %3, align 8
  %2575 = getelementptr inbounds %struct.ShellState* %2574, i32 0, i32 8
  %2576 = load %struct._IO_FILE** %2575, align 8
  %2577 = load %struct.ShellState** %3, align 8
  %2578 = getelementptr inbounds %struct.ShellState* %2577, i32 0, i32 22
  %2579 = getelementptr inbounds [4096 x i8]* %2578, i32 0, i32 0
  %2580 = call i32 @strlen30(i8* %2579)
  %2581 = icmp ne i32 %2580, 0
  br i1 %2581, label %2582, label %2586

; <label>:2582                                    ; preds = %2512
  %2583 = load %struct.ShellState** %3, align 8
  %2584 = getelementptr inbounds %struct.ShellState* %2583, i32 0, i32 22
  %2585 = getelementptr inbounds [4096 x i8]* %2584, i32 0, i32 0
  br label %2587

; <label>:2586                                    ; preds = %2512
  br label %2587

; <label>:2587                                    ; preds = %2586, %2582
  %2588 = phi i8* [ %2585, %2582 ], [ getelementptr inbounds ([7 x i8]* @.str257, i32 0, i32 0), %2586 ]
  %2589 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2576, i8* getelementptr inbounds ([13 x i8]* @.str296, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8]* @.str255, i32 0, i32 0), i8* %2588)
  %2590 = load %struct.ShellState** %3, align 8
  %2591 = getelementptr inbounds %struct.ShellState* %2590, i32 0, i32 8
  %2592 = load %struct._IO_FILE** %2591, align 8
  %2593 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2592, i8* getelementptr inbounds ([10 x i8]* @.str299, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8]* @.str300, i32 0, i32 0))
  %2594 = load %struct.ShellState** %3, align 8
  %2595 = getelementptr inbounds %struct.ShellState* %2594, i32 0, i32 8
  %2596 = load %struct._IO_FILE** %2595, align 8
  %2597 = load %struct.ShellState** %3, align 8
  %2598 = getelementptr inbounds %struct.ShellState* %2597, i32 0, i32 16
  %2599 = getelementptr inbounds [20 x i8]* %2598, i32 0, i32 0
  call void @output_c_string(%struct._IO_FILE* %2596, i8* %2599)
  %2600 = load %struct.ShellState** %3, align 8
  %2601 = getelementptr inbounds %struct.ShellState* %2600, i32 0, i32 8
  %2602 = load %struct._IO_FILE** %2601, align 8
  %2603 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2602, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  %2604 = load %struct.ShellState** %3, align 8
  %2605 = getelementptr inbounds %struct.ShellState* %2604, i32 0, i32 8
  %2606 = load %struct._IO_FILE** %2605, align 8
  %2607 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2606, i8* getelementptr inbounds ([10 x i8]* @.str299, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8]* @.str301, i32 0, i32 0))
  %2608 = load %struct.ShellState** %3, align 8
  %2609 = getelementptr inbounds %struct.ShellState* %2608, i32 0, i32 8
  %2610 = load %struct._IO_FILE** %2609, align 8
  %2611 = load %struct.ShellState** %3, align 8
  %2612 = getelementptr inbounds %struct.ShellState* %2611, i32 0, i32 17
  %2613 = getelementptr inbounds [20 x i8]* %2612, i32 0, i32 0
  call void @output_c_string(%struct._IO_FILE* %2610, i8* %2613)
  %2614 = load %struct.ShellState** %3, align 8
  %2615 = getelementptr inbounds %struct.ShellState* %2614, i32 0, i32 8
  %2616 = load %struct._IO_FILE** %2615, align 8
  %2617 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2616, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  %2618 = load %struct.ShellState** %3, align 8
  %2619 = getelementptr inbounds %struct.ShellState* %2618, i32 0, i32 8
  %2620 = load %struct._IO_FILE** %2619, align 8
  %2621 = load %struct.ShellState** %3, align 8
  %2622 = getelementptr inbounds %struct.ShellState* %2621, i32 0, i32 3
  %2623 = load i32* %2622, align 4
  %2624 = icmp ne i32 %2623, 0
  %2625 = select i1 %2624, i8* getelementptr inbounds ([3 x i8]* @.str297, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str262, i32 0, i32 0)
  %2626 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2620, i8* getelementptr inbounds ([13 x i8]* @.str296, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str302, i32 0, i32 0), i8* %2625)
  %2627 = load %struct.ShellState** %3, align 8
  %2628 = getelementptr inbounds %struct.ShellState* %2627, i32 0, i32 8
  %2629 = load %struct._IO_FILE** %2628, align 8
  %2630 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2629, i8* getelementptr inbounds ([10 x i8]* @.str299, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str303, i32 0, i32 0))
  store i32 0, i32* %i24, align 4
  br label %2631

; <label>:2631                                    ; preds = %2655, %2587
  %2632 = load i32* %i24, align 4
  %2633 = icmp slt i32 %2632, 100
  br i1 %2633, label %2634, label %2642

; <label>:2634                                    ; preds = %2631
  %2635 = load i32* %i24, align 4
  %2636 = sext i32 %2635 to i64
  %2637 = load %struct.ShellState** %3, align 8
  %2638 = getelementptr inbounds %struct.ShellState* %2637, i32 0, i32 18
  %2639 = getelementptr inbounds [100 x i32]* %2638, i32 0, i64 %2636
  %2640 = load i32* %2639, align 4
  %2641 = icmp ne i32 %2640, 0
  br label %2642

; <label>:2642                                    ; preds = %2634, %2631
  %2643 = phi i1 [ false, %2631 ], [ %2641, %2634 ]
  br i1 %2643, label %2644, label %2658

; <label>:2644                                    ; preds = %2642
  %2645 = load %struct.ShellState** %3, align 8
  %2646 = getelementptr inbounds %struct.ShellState* %2645, i32 0, i32 8
  %2647 = load %struct._IO_FILE** %2646, align 8
  %2648 = load i32* %i24, align 4
  %2649 = sext i32 %2648 to i64
  %2650 = load %struct.ShellState** %3, align 8
  %2651 = getelementptr inbounds %struct.ShellState* %2650, i32 0, i32 18
  %2652 = getelementptr inbounds [100 x i32]* %2651, i32 0, i64 %2649
  %2653 = load i32* %2652, align 4
  %2654 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2647, i8* getelementptr inbounds ([4 x i8]* @.str304, i32 0, i32 0), i32 %2653)
  br label %2655

; <label>:2655                                    ; preds = %2644
  %2656 = load i32* %i24, align 4
  %2657 = add nsw i32 %2656, 1
  store i32 %2657, i32* %i24, align 4
  br label %2631

; <label>:2658                                    ; preds = %2642
  %2659 = load %struct.ShellState** %3, align 8
  %2660 = getelementptr inbounds %struct.ShellState* %2659, i32 0, i32 8
  %2661 = load %struct._IO_FILE** %2660, align 8
  %2662 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2661, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  br label %3336

; <label>:2663                                    ; preds = %2499, %2496
  %2664 = load i32* %c, align 4
  %2665 = icmp eq i32 %2664, 115
  br i1 %2665, label %2666, label %2686

; <label>:2666                                    ; preds = %2663
  %2667 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2668 = load i8** %2667, align 8
  %2669 = load i32* %n, align 4
  %2670 = sext i32 %2669 to i64
  %2671 = call i32 @strncmp(i8* %2668, i8* getelementptr inbounds ([6 x i8]* @.str302, i32 0, i32 0), i64 %2670) #7
  %2672 = icmp eq i32 %2671, 0
  br i1 %2672, label %2673, label %2686

; <label>:2673                                    ; preds = %2666
  %2674 = load i32* %nArg, align 4
  %2675 = icmp eq i32 %2674, 2
  br i1 %2675, label %2676, label %2682

; <label>:2676                                    ; preds = %2673
  %2677 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2678 = load i8** %2677, align 8
  %2679 = call i32 @booleanValue(i8* %2678)
  %2680 = load %struct.ShellState** %3, align 8
  %2681 = getelementptr inbounds %struct.ShellState* %2680, i32 0, i32 3
  store i32 %2679, i32* %2681, align 4
  br label %2685

; <label>:2682                                    ; preds = %2673
  %2683 = load %struct._IO_FILE** @stderr, align 8
  %2684 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2683, i8* getelementptr inbounds ([22 x i8]* @.str305, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %2685

; <label>:2685                                    ; preds = %2682, %2676
  br label %3335

; <label>:2686                                    ; preds = %2666, %2663
  %2687 = load i32* %c, align 4
  %2688 = icmp eq i32 %2687, 116
  br i1 %2688, label %2689, label %2916

; <label>:2689                                    ; preds = %2686
  %2690 = load i32* %n, align 4
  %2691 = icmp sgt i32 %2690, 1
  br i1 %2691, label %2692, label %2916

; <label>:2692                                    ; preds = %2689
  %2693 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2694 = load i8** %2693, align 8
  %2695 = load i32* %n, align 4
  %2696 = sext i32 %2695 to i64
  %2697 = call i32 @strncmp(i8* %2694, i8* getelementptr inbounds ([7 x i8]* @.str306, i32 0, i32 0), i64 %2696) #7
  %2698 = icmp eq i32 %2697, 0
  br i1 %2698, label %2699, label %2916

; <label>:2699                                    ; preds = %2692
  store i8* null, i8** %zSql26, align 8
  %2700 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %2700, i32 0)
  %2701 = load %struct.ShellState** %3, align 8
  %2702 = getelementptr inbounds %struct.ShellState* %2701, i32 0, i32 0
  %2703 = load %struct.sqlite3** %2702, align 8
  %2704 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %2703, i8* getelementptr inbounds ([21 x i8]* @.str307, i32 0, i32 0), i32 -1, %struct.sqlite3_stmt** %pStmt25, i8** null)
  store i32 %2704, i32* %rc, align 4
  %2705 = load i32* %rc, align 4
  %2706 = icmp ne i32 %2705, 0
  br i1 %2706, label %2707, label %2709

; <label>:2707                                    ; preds = %2699
  %2708 = load i32* %rc, align 4
  store i32 %2708, i32* %1
  br label %3389

; <label>:2709                                    ; preds = %2699
  %2710 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([113 x i8]* @.str308, i32 0, i32 0))
  store i8* %2710, i8** %zSql26, align 8
  br label %2711

; <label>:2711                                    ; preds = %2737, %2724, %2709
  %2712 = load %struct.sqlite3_stmt** %pStmt25, align 8
  %2713 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %2712)
  %2714 = icmp eq i32 %2713, 100
  br i1 %2714, label %2715, label %2738

; <label>:2715                                    ; preds = %2711
  %2716 = load %struct.sqlite3_stmt** %pStmt25, align 8
  %2717 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %2716, i32 1)
  store i8* %2717, i8** %zDbName, align 8
  %2718 = load i8** %zDbName, align 8
  %2719 = icmp eq i8* %2718, null
  br i1 %2719, label %2724, label %2720

; <label>:2720                                    ; preds = %2715
  %2721 = load i8** %zDbName, align 8
  %2722 = call i32 @strcmp(i8* %2721, i8* getelementptr inbounds ([5 x i8]* @.str140, i32 0, i32 0)) #7
  %2723 = icmp eq i32 %2722, 0
  br i1 %2723, label %2724, label %2725

; <label>:2724                                    ; preds = %2720, %2715
  br label %2711

; <label>:2725                                    ; preds = %2720
  %2726 = load i8** %zDbName, align 8
  %2727 = call i32 @strcmp(i8* %2726, i8* getelementptr inbounds ([5 x i8]* @.str309, i32 0, i32 0)) #7
  %2728 = icmp eq i32 %2727, 0
  br i1 %2728, label %2729, label %2732

; <label>:2729                                    ; preds = %2725
  %2730 = load i8** %zSql26, align 8
  %2731 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([142 x i8]* @.str310, i32 0, i32 0), i8* %2730)
  store i8* %2731, i8** %zSql26, align 8
  br label %2737

; <label>:2732                                    ; preds = %2725
  %2733 = load i8** %zSql26, align 8
  %2734 = load i8** %zDbName, align 8
  %2735 = load i8** %zDbName, align 8
  %2736 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([140 x i8]* @.str311, i32 0, i32 0), i8* %2733, i8* %2734, i8* %2735)
  store i8* %2736, i8** %zSql26, align 8
  br label %2737

; <label>:2737                                    ; preds = %2732, %2729
  br label %2711

; <label>:2738                                    ; preds = %2711
  %2739 = load %struct.sqlite3_stmt** %pStmt25, align 8
  %2740 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %2739)
  %2741 = load i8** %zSql26, align 8
  %2742 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([14 x i8]* @.str312, i32 0, i32 0), i8* %2741)
  store i8* %2742, i8** %zSql26, align 8
  %2743 = load %struct.ShellState** %3, align 8
  %2744 = getelementptr inbounds %struct.ShellState* %2743, i32 0, i32 0
  %2745 = load %struct.sqlite3** %2744, align 8
  %2746 = load i8** %zSql26, align 8
  %2747 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %2745, i8* %2746, i32 -1, %struct.sqlite3_stmt** %pStmt25, i8** null)
  store i32 %2747, i32* %rc, align 4
  %2748 = load i8** %zSql26, align 8
  call void @sqlite3_free(i8* %2748)
  %2749 = load i32* %rc, align 4
  %2750 = icmp ne i32 %2749, 0
  br i1 %2750, label %2751, label %2753

; <label>:2751                                    ; preds = %2738
  %2752 = load i32* %rc, align 4
  store i32 %2752, i32* %1
  br label %3389

; <label>:2753                                    ; preds = %2738
  store i32 0, i32* %nAlloc, align 4
  store i32 0, i32* %nRow, align 4
  store i8** null, i8*** %azResult, align 8
  %2754 = load i32* %nArg, align 4
  %2755 = icmp sgt i32 %2754, 1
  br i1 %2755, label %2756, label %2761

; <label>:2756                                    ; preds = %2753
  %2757 = load %struct.sqlite3_stmt** %pStmt25, align 8
  %2758 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2759 = load i8** %2758, align 8
  %2760 = call i32 @sqlite3_bind_text(%struct.sqlite3_stmt* %2757, i32 1, i8* %2759, i32 -1, void (i8*)* inttoptr (i64 -1 to void (i8*)*))
  br label %2764

; <label>:2761                                    ; preds = %2753
  %2762 = load %struct.sqlite3_stmt** %pStmt25, align 8
  %2763 = call i32 @sqlite3_bind_text(%struct.sqlite3_stmt* %2762, i32 1, i8* getelementptr inbounds ([2 x i8]* @.str313, i32 0, i32 0), i32 -1, void (i8*)* null)
  br label %2764

; <label>:2764                                    ; preds = %2761, %2756
  br label %2765

; <label>:2765                                    ; preds = %2809, %2764
  %2766 = load %struct.sqlite3_stmt** %pStmt25, align 8
  %2767 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %2766)
  %2768 = icmp eq i32 %2767, 100
  br i1 %2768, label %2769, label %2810

; <label>:2769                                    ; preds = %2765
  %2770 = load i32* %nRow, align 4
  %2771 = load i32* %nAlloc, align 4
  %2772 = icmp sge i32 %2770, %2771
  br i1 %2772, label %2773, label %2792

; <label>:2773                                    ; preds = %2769
  %2774 = load i32* %nAlloc, align 4
  %2775 = mul nsw i32 %2774, 2
  %2776 = add nsw i32 %2775, 10
  store i32 %2776, i32* %n227, align 4
  %2777 = load i8*** %azResult, align 8
  %2778 = bitcast i8** %2777 to i8*
  %2779 = load i32* %n227, align 4
  %2780 = sext i32 %2779 to i64
  %2781 = mul i64 8, %2780
  %2782 = call i8* @sqlite3_realloc64(i8* %2778, i64 %2781)
  %2783 = bitcast i8* %2782 to i8**
  store i8** %2783, i8*** %azNew, align 8
  %2784 = load i8*** %azNew, align 8
  %2785 = icmp eq i8** %2784, null
  br i1 %2785, label %2786, label %2789

; <label>:2786                                    ; preds = %2773
  %2787 = load %struct._IO_FILE** @stderr, align 8
  %2788 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2787, i8* getelementptr inbounds ([22 x i8]* @.str49, i32 0, i32 0))
  br label %2810

; <label>:2789                                    ; preds = %2773
  %2790 = load i32* %n227, align 4
  store i32 %2790, i32* %nAlloc, align 4
  %2791 = load i8*** %azNew, align 8
  store i8** %2791, i8*** %azResult, align 8
  br label %2792

; <label>:2792                                    ; preds = %2789, %2769
  %2793 = load %struct.sqlite3_stmt** %pStmt25, align 8
  %2794 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %2793, i32 0)
  %2795 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %2794)
  %2796 = load i32* %nRow, align 4
  %2797 = sext i32 %2796 to i64
  %2798 = load i8*** %azResult, align 8
  %2799 = getelementptr inbounds i8** %2798, i64 %2797
  store i8* %2795, i8** %2799, align 8
  %2800 = load i32* %nRow, align 4
  %2801 = sext i32 %2800 to i64
  %2802 = load i8*** %azResult, align 8
  %2803 = getelementptr inbounds i8** %2802, i64 %2801
  %2804 = load i8** %2803, align 8
  %2805 = icmp ne i8* %2804, null
  br i1 %2805, label %2806, label %2809

; <label>:2806                                    ; preds = %2792
  %2807 = load i32* %nRow, align 4
  %2808 = add nsw i32 %2807, 1
  store i32 %2808, i32* %nRow, align 4
  br label %2809

; <label>:2809                                    ; preds = %2806, %2792
  br label %2765

; <label>:2810                                    ; preds = %2786, %2765
  %2811 = load %struct.sqlite3_stmt** %pStmt25, align 8
  %2812 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %2811)
  %2813 = load i32* %nRow, align 4
  %2814 = icmp sgt i32 %2813, 0
  br i1 %2814, label %2815, label %2899

; <label>:2815                                    ; preds = %2810
  store i32 0, i32* %maxlen, align 4
  store i32 0, i32* %i28, align 4
  br label %2816

; <label>:2816                                    ; preds = %2833, %2815
  %2817 = load i32* %i28, align 4
  %2818 = load i32* %nRow, align 4
  %2819 = icmp slt i32 %2817, %2818
  br i1 %2819, label %2820, label %2836

; <label>:2820                                    ; preds = %2816
  %2821 = load i32* %i28, align 4
  %2822 = sext i32 %2821 to i64
  %2823 = load i8*** %azResult, align 8
  %2824 = getelementptr inbounds i8** %2823, i64 %2822
  %2825 = load i8** %2824, align 8
  %2826 = call i32 @strlen30(i8* %2825)
  store i32 %2826, i32* %len, align 4
  %2827 = load i32* %len, align 4
  %2828 = load i32* %maxlen, align 4
  %2829 = icmp sgt i32 %2827, %2828
  br i1 %2829, label %2830, label %2832

; <label>:2830                                    ; preds = %2820
  %2831 = load i32* %len, align 4
  store i32 %2831, i32* %maxlen, align 4
  br label %2832

; <label>:2832                                    ; preds = %2830, %2820
  br label %2833

; <label>:2833                                    ; preds = %2832
  %2834 = load i32* %i28, align 4
  %2835 = add nsw i32 %2834, 1
  store i32 %2835, i32* %i28, align 4
  br label %2816

; <label>:2836                                    ; preds = %2816
  %2837 = load i32* %maxlen, align 4
  %2838 = add nsw i32 %2837, 2
  %2839 = sdiv i32 80, %2838
  store i32 %2839, i32* %nPrintCol, align 4
  %2840 = load i32* %nPrintCol, align 4
  %2841 = icmp slt i32 %2840, 1
  br i1 %2841, label %2842, label %2843

; <label>:2842                                    ; preds = %2836
  store i32 1, i32* %nPrintCol, align 4
  br label %2843

; <label>:2843                                    ; preds = %2842, %2836
  %2844 = load i32* %nRow, align 4
  %2845 = load i32* %nPrintCol, align 4
  %2846 = add nsw i32 %2844, %2845
  %2847 = sub nsw i32 %2846, 1
  %2848 = load i32* %nPrintCol, align 4
  %2849 = sdiv i32 %2847, %2848
  store i32 %2849, i32* %nPrintRow, align 4
  store i32 0, i32* %i28, align 4
  br label %2850

; <label>:2850                                    ; preds = %2895, %2843
  %2851 = load i32* %i28, align 4
  %2852 = load i32* %nPrintRow, align 4
  %2853 = icmp slt i32 %2851, %2852
  br i1 %2853, label %2854, label %2898

; <label>:2854                                    ; preds = %2850
  %2855 = load i32* %i28, align 4
  store i32 %2855, i32* %j29, align 4
  br label %2856

; <label>:2856                                    ; preds = %2886, %2854
  %2857 = load i32* %j29, align 4
  %2858 = load i32* %nRow, align 4
  %2859 = icmp slt i32 %2857, %2858
  br i1 %2859, label %2860, label %2890

; <label>:2860                                    ; preds = %2856
  %2861 = load i32* %j29, align 4
  %2862 = load i32* %nPrintRow, align 4
  %2863 = icmp slt i32 %2861, %2862
  %2864 = select i1 %2863, i8* getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), i8* getelementptr inbounds ([3 x i8]* @.str64, i32 0, i32 0)
  store i8* %2864, i8** %zSp, align 8
  %2865 = load %struct.ShellState** %3, align 8
  %2866 = getelementptr inbounds %struct.ShellState* %2865, i32 0, i32 8
  %2867 = load %struct._IO_FILE** %2866, align 8
  %2868 = load i8** %zSp, align 8
  %2869 = load i32* %maxlen, align 4
  %2870 = load i32* %j29, align 4
  %2871 = sext i32 %2870 to i64
  %2872 = load i8*** %azResult, align 8
  %2873 = getelementptr inbounds i8** %2872, i64 %2871
  %2874 = load i8** %2873, align 8
  %2875 = icmp ne i8* %2874, null
  br i1 %2875, label %2876, label %2882

; <label>:2876                                    ; preds = %2860
  %2877 = load i32* %j29, align 4
  %2878 = sext i32 %2877 to i64
  %2879 = load i8*** %azResult, align 8
  %2880 = getelementptr inbounds i8** %2879, i64 %2878
  %2881 = load i8** %2880, align 8
  br label %2883

; <label>:2882                                    ; preds = %2860
  br label %2883

; <label>:2883                                    ; preds = %2882, %2876
  %2884 = phi i8* [ %2881, %2876 ], [ getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), %2882 ]
  %2885 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2867, i8* getelementptr inbounds ([7 x i8]* @.str314, i32 0, i32 0), i8* %2868, i32 %2869, i8* %2884)
  br label %2886

; <label>:2886                                    ; preds = %2883
  %2887 = load i32* %nPrintRow, align 4
  %2888 = load i32* %j29, align 4
  %2889 = add nsw i32 %2888, %2887
  store i32 %2889, i32* %j29, align 4
  br label %2856

; <label>:2890                                    ; preds = %2856
  %2891 = load %struct.ShellState** %3, align 8
  %2892 = getelementptr inbounds %struct.ShellState* %2891, i32 0, i32 8
  %2893 = load %struct._IO_FILE** %2892, align 8
  %2894 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2893, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  br label %2895

; <label>:2895                                    ; preds = %2890
  %2896 = load i32* %i28, align 4
  %2897 = add nsw i32 %2896, 1
  store i32 %2897, i32* %i28, align 4
  br label %2850

; <label>:2898                                    ; preds = %2850
  br label %2899

; <label>:2899                                    ; preds = %2898, %2810
  store i32 0, i32* %ii, align 4
  br label %2900

; <label>:2900                                    ; preds = %2910, %2899
  %2901 = load i32* %ii, align 4
  %2902 = load i32* %nRow, align 4
  %2903 = icmp slt i32 %2901, %2902
  br i1 %2903, label %2904, label %2913

; <label>:2904                                    ; preds = %2900
  %2905 = load i32* %ii, align 4
  %2906 = sext i32 %2905 to i64
  %2907 = load i8*** %azResult, align 8
  %2908 = getelementptr inbounds i8** %2907, i64 %2906
  %2909 = load i8** %2908, align 8
  call void @sqlite3_free(i8* %2909)
  br label %2910

; <label>:2910                                    ; preds = %2904
  %2911 = load i32* %ii, align 4
  %2912 = add nsw i32 %2911, 1
  store i32 %2912, i32* %ii, align 4
  br label %2900

; <label>:2913                                    ; preds = %2900
  %2914 = load i8*** %azResult, align 8
  %2915 = bitcast i8** %2914 to i8*
  call void @sqlite3_free(i8* %2915)
  br label %3334

; <label>:2916                                    ; preds = %2692, %2689, %2686
  %2917 = load i32* %c, align 4
  %2918 = icmp eq i32 %2917, 116
  br i1 %2918, label %2919, label %3116

; <label>:2919                                    ; preds = %2916
  %2920 = load i32* %n, align 4
  %2921 = icmp sge i32 %2920, 8
  br i1 %2921, label %2922, label %3116

; <label>:2922                                    ; preds = %2919
  %2923 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %2924 = load i8** %2923, align 8
  %2925 = load i32* %n, align 4
  %2926 = sext i32 %2925 to i64
  %2927 = call i32 @strncmp(i8* %2924, i8* getelementptr inbounds ([9 x i8]* @.str315, i32 0, i32 0), i64 %2926) #7
  %2928 = icmp eq i32 %2927, 0
  br i1 %2928, label %2929, label %3116

; <label>:2929                                    ; preds = %2922
  %2930 = load i32* %nArg, align 4
  %2931 = icmp sge i32 %2930, 2
  br i1 %2931, label %2932, label %3116

; <label>:2932                                    ; preds = %2929
  store i32 -1, i32* %testctrl, align 4
  store i32 0, i32* %rc2, align 4
  %2933 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %2933, i32 0)
  %2934 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2935 = load i8** %2934, align 8
  %2936 = call i32 @strlen30(i8* %2935)
  store i32 %2936, i32* %n231, align 4
  store i32 0, i32* %i30, align 4
  br label %2937

; <label>:2937                                    ; preds = %2968, %2932
  %2938 = load i32* %i30, align 4
  %2939 = icmp slt i32 %2938, 16
  br i1 %2939, label %2940, label %2971

; <label>:2940                                    ; preds = %2937
  %2941 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2942 = load i8** %2941, align 8
  %2943 = load i32* %i30, align 4
  %2944 = sext i32 %2943 to i64
  %2945 = getelementptr inbounds [16 x %struct.anon.13]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @do_meta_command.aCtrl to [16 x %struct.anon.13]*), i32 0, i64 %2944
  %2946 = getelementptr inbounds %struct.anon.13* %2945, i32 0, i32 0
  %2947 = load i8** %2946, align 8
  %2948 = load i32* %n231, align 4
  %2949 = sext i32 %2948 to i64
  %2950 = call i32 @strncmp(i8* %2942, i8* %2947, i64 %2949) #7
  %2951 = icmp eq i32 %2950, 0
  br i1 %2951, label %2952, label %2967

; <label>:2952                                    ; preds = %2940
  %2953 = load i32* %testctrl, align 4
  %2954 = icmp slt i32 %2953, 0
  br i1 %2954, label %2955, label %2961

; <label>:2955                                    ; preds = %2952
  %2956 = load i32* %i30, align 4
  %2957 = sext i32 %2956 to i64
  %2958 = getelementptr inbounds [16 x %struct.anon.13]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @do_meta_command.aCtrl to [16 x %struct.anon.13]*), i32 0, i64 %2957
  %2959 = getelementptr inbounds %struct.anon.13* %2958, i32 0, i32 1
  %2960 = load i32* %2959, align 4
  store i32 %2960, i32* %testctrl, align 4
  br label %2966

; <label>:2961                                    ; preds = %2952
  %2962 = load %struct._IO_FILE** @stderr, align 8
  %2963 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2964 = load i8** %2963, align 8
  %2965 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2962, i8* getelementptr inbounds ([29 x i8]* @.str332, i32 0, i32 0), i8* %2964)
  store i32 -1, i32* %testctrl, align 4
  br label %2971

; <label>:2966                                    ; preds = %2955
  br label %2967

; <label>:2967                                    ; preds = %2966, %2940
  br label %2968

; <label>:2968                                    ; preds = %2967
  %2969 = load i32* %i30, align 4
  %2970 = add nsw i32 %2969, 1
  store i32 %2970, i32* %i30, align 4
  br label %2937

; <label>:2971                                    ; preds = %2961, %2937
  %2972 = load i32* %testctrl, align 4
  %2973 = icmp slt i32 %2972, 0
  br i1 %2973, label %2974, label %2979

; <label>:2974                                    ; preds = %2971
  %2975 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2976 = load i8** %2975, align 8
  %2977 = call i64 @integerValue(i8* %2976)
  %2978 = trunc i64 %2977 to i32
  store i32 %2978, i32* %testctrl, align 4
  br label %2979

; <label>:2979                                    ; preds = %2974, %2971
  %2980 = load i32* %testctrl, align 4
  %2981 = icmp slt i32 %2980, 5
  br i1 %2981, label %2985, label %2982

; <label>:2982                                    ; preds = %2979
  %2983 = load i32* %testctrl, align 4
  %2984 = icmp sgt i32 %2983, 25
  br i1 %2984, label %2985, label %2990

; <label>:2985                                    ; preds = %2982, %2979
  %2986 = load %struct._IO_FILE** @stderr, align 8
  %2987 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %2988 = load i8** %2987, align 8
  %2989 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %2986, i8* getelementptr inbounds ([36 x i8]* @.str333, i32 0, i32 0), i8* %2988)
  br label %3115

; <label>:2990                                    ; preds = %2982
  %2991 = load i32* %testctrl, align 4
  switch i32 %2991, label %3109 [
    i32 15, label %2992
    i32 14, label %2992
    i32 5, label %3018
    i32 6, label %3018
    i32 7, label %3018
    i32 22, label %3018
    i32 11, label %3036
    i32 12, label %3059
    i32 13, label %3059
    i32 20, label %3059
    i32 25, label %3081
    i32 8, label %3108
    i32 9, label %3108
    i32 10, label %3108
    i32 17, label %3108
  ]

; <label>:2992                                    ; preds = %2990, %2990
  %2993 = load i32* %nArg, align 4
  %2994 = icmp eq i32 %2993, 3
  br i1 %2994, label %2995, label %3012

; <label>:2995                                    ; preds = %2992
  %2996 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %2997 = load i8** %2996, align 8
  %2998 = call i64 @strtol(i8* %2997, i8** null, i32 0) #5
  %2999 = trunc i64 %2998 to i32
  store i32 %2999, i32* %opt, align 4
  %3000 = load i32* %testctrl, align 4
  %3001 = load %struct.ShellState** %3, align 8
  %3002 = getelementptr inbounds %struct.ShellState* %3001, i32 0, i32 0
  %3003 = load %struct.sqlite3** %3002, align 8
  %3004 = load i32* %opt, align 4
  %3005 = call i32 (i32, ...)* @sqlite3_test_control(i32 %3000, %struct.sqlite3* %3003, i32 %3004)
  store i32 %3005, i32* %rc2, align 4
  %3006 = load %struct.ShellState** %3, align 8
  %3007 = getelementptr inbounds %struct.ShellState* %3006, i32 0, i32 8
  %3008 = load %struct._IO_FILE** %3007, align 8
  %3009 = load i32* %rc2, align 4
  %3010 = load i32* %rc2, align 4
  %3011 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3008, i8* getelementptr inbounds ([13 x i8]* @.str334, i32 0, i32 0), i32 %3009, i32 %3010)
  br label %3017

; <label>:3012                                    ; preds = %2992
  %3013 = load %struct._IO_FILE** @stderr, align 8
  %3014 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3015 = load i8** %3014, align 8
  %3016 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3013, i8* getelementptr inbounds ([46 x i8]* @.str335, i32 0, i32 0), i8* %3015)
  br label %3017

; <label>:3017                                    ; preds = %3012, %2995
  br label %3114

; <label>:3018                                    ; preds = %2990, %2990, %2990, %2990
  %3019 = load i32* %nArg, align 4
  %3020 = icmp eq i32 %3019, 2
  br i1 %3020, label %3021, label %3030

; <label>:3021                                    ; preds = %3018
  %3022 = load i32* %testctrl, align 4
  %3023 = call i32 (i32, ...)* @sqlite3_test_control(i32 %3022)
  store i32 %3023, i32* %rc2, align 4
  %3024 = load %struct.ShellState** %3, align 8
  %3025 = getelementptr inbounds %struct.ShellState* %3024, i32 0, i32 8
  %3026 = load %struct._IO_FILE** %3025, align 8
  %3027 = load i32* %rc2, align 4
  %3028 = load i32* %rc2, align 4
  %3029 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3026, i8* getelementptr inbounds ([13 x i8]* @.str334, i32 0, i32 0), i32 %3027, i32 %3028)
  br label %3035

; <label>:3030                                    ; preds = %3018
  %3031 = load %struct._IO_FILE** @stderr, align 8
  %3032 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3033 = load i8** %3032, align 8
  %3034 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3031, i8* getelementptr inbounds ([37 x i8]* @.str336, i32 0, i32 0), i8* %3033)
  br label %3035

; <label>:3035                                    ; preds = %3030, %3021
  br label %3114

; <label>:3036                                    ; preds = %2990
  %3037 = load i32* %nArg, align 4
  %3038 = icmp eq i32 %3037, 3
  br i1 %3038, label %3039, label %3053

; <label>:3039                                    ; preds = %3036
  %3040 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %3041 = load i8** %3040, align 8
  %3042 = call i64 @integerValue(i8* %3041)
  %3043 = trunc i64 %3042 to i32
  store i32 %3043, i32* %opt32, align 4
  %3044 = load i32* %testctrl, align 4
  %3045 = load i32* %opt32, align 4
  %3046 = call i32 (i32, ...)* @sqlite3_test_control(i32 %3044, i32 %3045)
  store i32 %3046, i32* %rc2, align 4
  %3047 = load %struct.ShellState** %3, align 8
  %3048 = getelementptr inbounds %struct.ShellState* %3047, i32 0, i32 8
  %3049 = load %struct._IO_FILE** %3048, align 8
  %3050 = load i32* %rc2, align 4
  %3051 = load i32* %rc2, align 4
  %3052 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3049, i8* getelementptr inbounds ([13 x i8]* @.str334, i32 0, i32 0), i32 %3050, i32 %3051)
  br label %3058

; <label>:3053                                    ; preds = %3036
  %3054 = load %struct._IO_FILE** @stderr, align 8
  %3055 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3056 = load i8** %3055, align 8
  %3057 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3054, i8* getelementptr inbounds ([55 x i8]* @.str337, i32 0, i32 0), i8* %3056)
  br label %3058

; <label>:3058                                    ; preds = %3053, %3039
  br label %3114

; <label>:3059                                    ; preds = %2990, %2990, %2990
  %3060 = load i32* %nArg, align 4
  %3061 = icmp eq i32 %3060, 3
  br i1 %3061, label %3062, label %3075

; <label>:3062                                    ; preds = %3059
  %3063 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %3064 = load i8** %3063, align 8
  %3065 = call i32 @booleanValue(i8* %3064)
  store i32 %3065, i32* %opt33, align 4
  %3066 = load i32* %testctrl, align 4
  %3067 = load i32* %opt33, align 4
  %3068 = call i32 (i32, ...)* @sqlite3_test_control(i32 %3066, i32 %3067)
  store i32 %3068, i32* %rc2, align 4
  %3069 = load %struct.ShellState** %3, align 8
  %3070 = getelementptr inbounds %struct.ShellState* %3069, i32 0, i32 8
  %3071 = load %struct._IO_FILE** %3070, align 8
  %3072 = load i32* %rc2, align 4
  %3073 = load i32* %rc2, align 4
  %3074 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3071, i8* getelementptr inbounds ([13 x i8]* @.str334, i32 0, i32 0), i32 %3072, i32 %3073)
  br label %3080

; <label>:3075                                    ; preds = %3059
  %3076 = load %struct._IO_FILE** @stderr, align 8
  %3077 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3078 = load i8** %3077, align 8
  %3079 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3076, i8* getelementptr inbounds ([46 x i8]* @.str335, i32 0, i32 0), i8* %3078)
  br label %3080

; <label>:3080                                    ; preds = %3075, %3062
  br label %3114

; <label>:3081                                    ; preds = %2990
  %3082 = load i32* %nArg, align 4
  %3083 = icmp eq i32 %3082, 5
  br i1 %3083, label %3084, label %3104

; <label>:3084                                    ; preds = %3081
  %3085 = load i32* %testctrl, align 4
  %3086 = load %struct.ShellState** %3, align 8
  %3087 = getelementptr inbounds %struct.ShellState* %3086, i32 0, i32 0
  %3088 = load %struct.sqlite3** %3087, align 8
  %3089 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 2
  %3090 = load i8** %3089, align 8
  %3091 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 3
  %3092 = load i8** %3091, align 8
  %3093 = call i64 @integerValue(i8* %3092)
  %3094 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 4
  %3095 = load i8** %3094, align 8
  %3096 = call i64 @integerValue(i8* %3095)
  %3097 = call i32 (i32, ...)* @sqlite3_test_control(i32 %3085, %struct.sqlite3* %3088, i8* %3090, i64 %3093, i64 %3096)
  store i32 %3097, i32* %rc2, align 4
  %3098 = load %struct.ShellState** %3, align 8
  %3099 = getelementptr inbounds %struct.ShellState* %3098, i32 0, i32 8
  %3100 = load %struct._IO_FILE** %3099, align 8
  %3101 = load i32* %rc2, align 4
  %3102 = load i32* %rc2, align 4
  %3103 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3100, i8* getelementptr inbounds ([13 x i8]* @.str334, i32 0, i32 0), i32 %3101, i32 %3102)
  br label %3107

; <label>:3104                                    ; preds = %3081
  %3105 = load %struct._IO_FILE** @stderr, align 8
  %3106 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3105, i8* getelementptr inbounds ([45 x i8]* @.str338, i32 0, i32 0))
  br label %3107

; <label>:3107                                    ; preds = %3104, %3084
  br label %3114

; <label>:3108                                    ; preds = %2990, %2990, %2990, %2990
  br label %3109

; <label>:3109                                    ; preds = %2990, %3108
  %3110 = load %struct._IO_FILE** @stderr, align 8
  %3111 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3112 = load i8** %3111, align 8
  %3113 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3110, i8* getelementptr inbounds ([52 x i8]* @.str339, i32 0, i32 0), i8* %3112)
  br label %3114

; <label>:3114                                    ; preds = %3109, %3107, %3080, %3058, %3035, %3017
  br label %3115

; <label>:3115                                    ; preds = %3114, %2985
  br label %3333

; <label>:3116                                    ; preds = %2929, %2922, %2919, %2916
  %3117 = load i32* %c, align 4
  %3118 = icmp eq i32 %3117, 116
  br i1 %3118, label %3119, label %3145

; <label>:3119                                    ; preds = %3116
  %3120 = load i32* %n, align 4
  %3121 = icmp sgt i32 %3120, 4
  br i1 %3121, label %3122, label %3145

; <label>:3122                                    ; preds = %3119
  %3123 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %3124 = load i8** %3123, align 8
  %3125 = load i32* %n, align 4
  %3126 = sext i32 %3125 to i64
  %3127 = call i32 @strncmp(i8* %3124, i8* getelementptr inbounds ([8 x i8]* @.str340, i32 0, i32 0), i64 %3126) #7
  %3128 = icmp eq i32 %3127, 0
  br i1 %3128, label %3129, label %3145

; <label>:3129                                    ; preds = %3122
  %3130 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %3130, i32 0)
  %3131 = load %struct.ShellState** %3, align 8
  %3132 = getelementptr inbounds %struct.ShellState* %3131, i32 0, i32 0
  %3133 = load %struct.sqlite3** %3132, align 8
  %3134 = load i32* %nArg, align 4
  %3135 = icmp sge i32 %3134, 2
  br i1 %3135, label %3136, label %3141

; <label>:3136                                    ; preds = %3129
  %3137 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3138 = load i8** %3137, align 8
  %3139 = call i64 @integerValue(i8* %3138)
  %3140 = trunc i64 %3139 to i32
  br label %3142

; <label>:3141                                    ; preds = %3129
  br label %3142

; <label>:3142                                    ; preds = %3141, %3136
  %3143 = phi i32 [ %3140, %3136 ], [ 0, %3141 ]
  %3144 = call i32 @sqlite3_busy_timeout(%struct.sqlite3* %3133, i32 %3143)
  br label %3332

; <label>:3145                                    ; preds = %3122, %3119, %3116
  %3146 = load i32* %c, align 4
  %3147 = icmp eq i32 %3146, 116
  br i1 %3147, label %3148, label %3176

; <label>:3148                                    ; preds = %3145
  %3149 = load i32* %n, align 4
  %3150 = icmp sge i32 %3149, 5
  br i1 %3150, label %3151, label %3176

; <label>:3151                                    ; preds = %3148
  %3152 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %3153 = load i8** %3152, align 8
  %3154 = load i32* %n, align 4
  %3155 = sext i32 %3154 to i64
  %3156 = call i32 @strncmp(i8* %3153, i8* getelementptr inbounds ([6 x i8]* @.str341, i32 0, i32 0), i64 %3155) #7
  %3157 = icmp eq i32 %3156, 0
  br i1 %3157, label %3158, label %3176

; <label>:3158                                    ; preds = %3151
  %3159 = load i32* %nArg, align 4
  %3160 = icmp eq i32 %3159, 2
  br i1 %3160, label %3161, label %3172

; <label>:3161                                    ; preds = %3158
  %3162 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3163 = load i8** %3162, align 8
  %3164 = call i32 @booleanValue(i8* %3163)
  store i32 %3164, i32* @enableTimer, align 4
  %3165 = load i32* @enableTimer, align 4
  %3166 = icmp ne i32 %3165, 0
  br i1 %3166, label %3167, label %3171

; <label>:3167                                    ; preds = %3161
  br i1 true, label %3171, label %3168

; <label>:3168                                    ; preds = %3167
  %3169 = load %struct._IO_FILE** @stderr, align 8
  %3170 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3169, i8* getelementptr inbounds ([44 x i8]* @.str342, i32 0, i32 0))
  store i32 0, i32* @enableTimer, align 4
  br label %3171

; <label>:3171                                    ; preds = %3168, %3167, %3161
  br label %3175

; <label>:3172                                    ; preds = %3158
  %3173 = load %struct._IO_FILE** @stderr, align 8
  %3174 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3173, i8* getelementptr inbounds ([22 x i8]* @.str343, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3175

; <label>:3175                                    ; preds = %3172, %3171
  br label %3331

; <label>:3176                                    ; preds = %3151, %3148, %3145
  %3177 = load i32* %c, align 4
  %3178 = icmp eq i32 %3177, 116
  br i1 %3178, label %3179, label %3221

; <label>:3179                                    ; preds = %3176
  %3180 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %3181 = load i8** %3180, align 8
  %3182 = load i32* %n, align 4
  %3183 = sext i32 %3182 to i64
  %3184 = call i32 @strncmp(i8* %3181, i8* getelementptr inbounds ([6 x i8]* @.str344, i32 0, i32 0), i64 %3183) #7
  %3185 = icmp eq i32 %3184, 0
  br i1 %3185, label %3186, label %3221

; <label>:3186                                    ; preds = %3179
  %3187 = load %struct.ShellState** %3, align 8
  call void @open_db(%struct.ShellState* %3187, i32 0)
  %3188 = load i32* %nArg, align 4
  %3189 = icmp ne i32 %3188, 2
  br i1 %3189, label %3190, label %3193

; <label>:3190                                    ; preds = %3186
  %3191 = load %struct._IO_FILE** @stderr, align 8
  %3192 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3191, i8* getelementptr inbounds ([24 x i8]* @.str345, i32 0, i32 0))
  store i32 1, i32* %rc, align 4
  br label %3370

; <label>:3193                                    ; preds = %3186
  %3194 = load %struct.ShellState** %3, align 8
  %3195 = getelementptr inbounds %struct.ShellState* %3194, i32 0, i32 9
  %3196 = load %struct._IO_FILE** %3195, align 8
  call void @output_file_close(%struct._IO_FILE* %3196)
  %3197 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3198 = load i8** %3197, align 8
  %3199 = call %struct._IO_FILE* @output_file_open(i8* %3198)
  %3200 = load %struct.ShellState** %3, align 8
  %3201 = getelementptr inbounds %struct.ShellState* %3200, i32 0, i32 9
  store %struct._IO_FILE* %3199, %struct._IO_FILE** %3201, align 8
  %3202 = load %struct.ShellState** %3, align 8
  %3203 = getelementptr inbounds %struct.ShellState* %3202, i32 0, i32 9
  %3204 = load %struct._IO_FILE** %3203, align 8
  %3205 = icmp eq %struct._IO_FILE* %3204, null
  br i1 %3205, label %3206, label %3211

; <label>:3206                                    ; preds = %3193
  %3207 = load %struct.ShellState** %3, align 8
  %3208 = getelementptr inbounds %struct.ShellState* %3207, i32 0, i32 0
  %3209 = load %struct.sqlite3** %3208, align 8
  %3210 = call i8* @sqlite3_trace(%struct.sqlite3* %3209, void (i8*, i8*)* null, i8* null)
  br label %3220

; <label>:3211                                    ; preds = %3193
  %3212 = load %struct.ShellState** %3, align 8
  %3213 = getelementptr inbounds %struct.ShellState* %3212, i32 0, i32 0
  %3214 = load %struct.sqlite3** %3213, align 8
  %3215 = load %struct.ShellState** %3, align 8
  %3216 = getelementptr inbounds %struct.ShellState* %3215, i32 0, i32 9
  %3217 = load %struct._IO_FILE** %3216, align 8
  %3218 = bitcast %struct._IO_FILE* %3217 to i8*
  %3219 = call i8* @sqlite3_trace(%struct.sqlite3* %3214, void (i8*, i8*)* @sql_trace_callback, i8* %3218)
  br label %3220

; <label>:3220                                    ; preds = %3211, %3206
  br label %3330

; <label>:3221                                    ; preds = %3179, %3176
  %3222 = load i32* %c, align 4
  %3223 = icmp eq i32 %3222, 118
  br i1 %3223, label %3224, label %3238

; <label>:3224                                    ; preds = %3221
  %3225 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %3226 = load i8** %3225, align 8
  %3227 = load i32* %n, align 4
  %3228 = sext i32 %3227 to i64
  %3229 = call i32 @strncmp(i8* %3226, i8* getelementptr inbounds ([8 x i8]* @.str346, i32 0, i32 0), i64 %3228) #7
  %3230 = icmp eq i32 %3229, 0
  br i1 %3230, label %3231, label %3238

; <label>:3231                                    ; preds = %3224
  %3232 = load %struct.ShellState** %3, align 8
  %3233 = getelementptr inbounds %struct.ShellState* %3232, i32 0, i32 8
  %3234 = load %struct._IO_FILE** %3233, align 8
  %3235 = call i8* @sqlite3_libversion()
  %3236 = call i8* @sqlite3_sourceid()
  %3237 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3234, i8* getelementptr inbounds ([14 x i8]* @.str347, i32 0, i32 0), i8* %3235, i8* %3236)
  br label %3329

; <label>:3238                                    ; preds = %3224, %3221
  %3239 = load i32* %c, align 4
  %3240 = icmp eq i32 %3239, 118
  br i1 %3240, label %3241, label %3279

; <label>:3241                                    ; preds = %3238
  %3242 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %3243 = load i8** %3242, align 8
  %3244 = load i32* %n, align 4
  %3245 = sext i32 %3244 to i64
  %3246 = call i32 @strncmp(i8* %3243, i8* getelementptr inbounds ([8 x i8]* @.str348, i32 0, i32 0), i64 %3245) #7
  %3247 = icmp eq i32 %3246, 0
  br i1 %3247, label %3248, label %3279

; <label>:3248                                    ; preds = %3241
  %3249 = load i32* %nArg, align 4
  %3250 = icmp eq i32 %3249, 2
  br i1 %3250, label %3251, label %3254

; <label>:3251                                    ; preds = %3248
  %3252 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 1
  %3253 = load i8** %3252, align 8
  br label %3255

; <label>:3254                                    ; preds = %3248
  br label %3255

; <label>:3255                                    ; preds = %3254, %3251
  %3256 = phi i8* [ %3253, %3251 ], [ getelementptr inbounds ([5 x i8]* @.str140, i32 0, i32 0), %3254 ]
  store i8* %3256, i8** %zDbName34, align 8
  store i8* null, i8** %zVfsName, align 8
  %3257 = load %struct.ShellState** %3, align 8
  %3258 = getelementptr inbounds %struct.ShellState* %3257, i32 0, i32 0
  %3259 = load %struct.sqlite3** %3258, align 8
  %3260 = icmp ne %struct.sqlite3* %3259, null
  br i1 %3260, label %3261, label %3278

; <label>:3261                                    ; preds = %3255
  %3262 = load %struct.ShellState** %3, align 8
  %3263 = getelementptr inbounds %struct.ShellState* %3262, i32 0, i32 0
  %3264 = load %struct.sqlite3** %3263, align 8
  %3265 = load i8** %zDbName34, align 8
  %3266 = bitcast i8** %zVfsName to i8*
  %3267 = call i32 @sqlite3_file_control(%struct.sqlite3* %3264, i8* %3265, i32 12, i8* %3266)
  %3268 = load i8** %zVfsName, align 8
  %3269 = icmp ne i8* %3268, null
  br i1 %3269, label %3270, label %3277

; <label>:3270                                    ; preds = %3261
  %3271 = load %struct.ShellState** %3, align 8
  %3272 = getelementptr inbounds %struct.ShellState* %3271, i32 0, i32 8
  %3273 = load %struct._IO_FILE** %3272, align 8
  %3274 = load i8** %zVfsName, align 8
  %3275 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3273, i8* getelementptr inbounds ([4 x i8]* @.str51, i32 0, i32 0), i8* %3274)
  %3276 = load i8** %zVfsName, align 8
  call void @sqlite3_free(i8* %3276)
  br label %3277

; <label>:3277                                    ; preds = %3270, %3261
  br label %3278

; <label>:3278                                    ; preds = %3277, %3255
  br label %3328

; <label>:3279                                    ; preds = %3241, %3238
  %3280 = load i32* %c, align 4
  %3281 = icmp eq i32 %3280, 119
  br i1 %3281, label %3282, label %3322

; <label>:3282                                    ; preds = %3279
  %3283 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %3284 = load i8** %3283, align 8
  %3285 = load i32* %n, align 4
  %3286 = sext i32 %3285 to i64
  %3287 = call i32 @strncmp(i8* %3284, i8* getelementptr inbounds ([6 x i8]* @.str303, i32 0, i32 0), i64 %3286) #7
  %3288 = icmp eq i32 %3287, 0
  br i1 %3288, label %3289, label %3322

; <label>:3289                                    ; preds = %3282
  %3290 = load i32* %nArg, align 4
  %3291 = icmp sle i32 %3290, 50
  br i1 %3291, label %3292, label %3293

; <label>:3292                                    ; preds = %3289
  br label %3295

; <label>:3293                                    ; preds = %3289
  call void @__assert_fail(i8* getelementptr inbounds ([44 x i8]* @.str349, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str54, i32 0, i32 0), i32 4052, i8* getelementptr inbounds ([42 x i8]* @__PRETTY_FUNCTION__.do_meta_command, i32 0, i32 0)) #8
  unreachable
                                                  ; No predecessors!
  br label %3295

; <label>:3295                                    ; preds = %3294, %3292
  store i32 1, i32* %j35, align 4
  br label %3296

; <label>:3296                                    ; preds = %3318, %3295
  %3297 = load i32* %j35, align 4
  %3298 = load i32* %nArg, align 4
  %3299 = icmp slt i32 %3297, %3298
  br i1 %3299, label %3300, label %3303

; <label>:3300                                    ; preds = %3296
  %3301 = load i32* %j35, align 4
  %3302 = icmp slt i32 %3301, 100
  br label %3303

; <label>:3303                                    ; preds = %3300, %3296
  %3304 = phi i1 [ false, %3296 ], [ %3302, %3300 ]
  br i1 %3304, label %3305, label %3321

; <label>:3305                                    ; preds = %3303
  %3306 = load i32* %j35, align 4
  %3307 = sext i32 %3306 to i64
  %3308 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 %3307
  %3309 = load i8** %3308, align 8
  %3310 = call i64 @integerValue(i8* %3309)
  %3311 = trunc i64 %3310 to i32
  %3312 = load i32* %j35, align 4
  %3313 = sub nsw i32 %3312, 1
  %3314 = sext i32 %3313 to i64
  %3315 = load %struct.ShellState** %3, align 8
  %3316 = getelementptr inbounds %struct.ShellState* %3315, i32 0, i32 18
  %3317 = getelementptr inbounds [100 x i32]* %3316, i32 0, i64 %3314
  store i32 %3311, i32* %3317, align 4
  br label %3318

; <label>:3318                                    ; preds = %3305
  %3319 = load i32* %j35, align 4
  %3320 = add nsw i32 %3319, 1
  store i32 %3320, i32* %j35, align 4
  br label %3296

; <label>:3321                                    ; preds = %3303
  br label %3327

; <label>:3322                                    ; preds = %3282, %3279
  %3323 = load %struct._IO_FILE** @stderr, align 8
  %3324 = getelementptr inbounds [50 x i8*]* %azArg, i32 0, i64 0
  %3325 = load i8** %3324, align 8
  %3326 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3323, i8* getelementptr inbounds ([76 x i8]* @.str350, i32 0, i32 0), i8* %3325)
  store i32 1, i32* %rc, align 4
  br label %3327

; <label>:3327                                    ; preds = %3322, %3321
  br label %3328

; <label>:3328                                    ; preds = %3327, %3278
  br label %3329

; <label>:3329                                    ; preds = %3328, %3231
  br label %3330

; <label>:3330                                    ; preds = %3329, %3220
  br label %3331

; <label>:3331                                    ; preds = %3330, %3175
  br label %3332

; <label>:3332                                    ; preds = %3331, %3142
  br label %3333

; <label>:3333                                    ; preds = %3332, %3115
  br label %3334

; <label>:3334                                    ; preds = %3333, %2913
  br label %3335

; <label>:3335                                    ; preds = %3334, %2685
  br label %3336

; <label>:3336                                    ; preds = %3335, %2658
  br label %3337

; <label>:3337                                    ; preds = %3336, %2495
  br label %3338

; <label>:3338                                    ; preds = %3337, %2431
  br label %3339

; <label>:3339                                    ; preds = %3338, %2391
  br label %3340

; <label>:3340                                    ; preds = %3339, %2277
  br label %3341

; <label>:3341                                    ; preds = %3340, %2250
  br label %3342

; <label>:3342                                    ; preds = %3341, %2144
  br label %3343

; <label>:3343                                    ; preds = %3342, %2107
  br label %3344

; <label>:3344                                    ; preds = %3343, %2096
  br label %3345

; <label>:3345                                    ; preds = %3344, %2067
  br label %3346

; <label>:3346                                    ; preds = %3345, %2028
  br label %3347

; <label>:3347                                    ; preds = %3346, %1912
  br label %3348

; <label>:3348                                    ; preds = %3347, %1858
  br label %3349

; <label>:3349                                    ; preds = %3348, %1834
  br label %3350

; <label>:3350                                    ; preds = %3349, %1641
  br label %3351

; <label>:3351                                    ; preds = %3350, %1614
  br label %3352

; <label>:3352                                    ; preds = %3351, %1572
  br label %3353

; <label>:3353                                    ; preds = %3352, %1447
  br label %3354

; <label>:3354                                    ; preds = %3353, %1384
  br label %3355

; <label>:3355                                    ; preds = %3354, %890
  br label %3356

; <label>:3356                                    ; preds = %3355, %879
  br label %3357

; <label>:3357                                    ; preds = %3356, %856
  br label %3358

; <label>:3358                                    ; preds = %3357, %776
  br label %3359

; <label>:3359                                    ; preds = %3358, %662
  br label %3360

; <label>:3360                                    ; preds = %3359, %640
  br label %3361

; <label>:3361                                    ; preds = %3360, %617
  br label %3362

; <label>:3362                                    ; preds = %3361, %577
  br label %3363

; <label>:3363                                    ; preds = %3362, %496
  br label %3364

; <label>:3364                                    ; preds = %3363, %485
  br label %3365

; <label>:3365                                    ; preds = %3364, %445
  br label %3366

; <label>:3366                                    ; preds = %3365, %424
  br label %3367

; <label>:3367                                    ; preds = %3366, %410
  br label %3368

; <label>:3368                                    ; preds = %3367, %382
  br label %3369

; <label>:3369                                    ; preds = %3368, %356
  br label %3370

; <label>:3370                                    ; preds = %3369, %3190, %2509, %2452, %2371, %2172, %2124, %1959, %1941, %1586, %1533, %1520, %1493, %1428, %908, %790, %518
  %3371 = load %struct.ShellState** %3, align 8
  %3372 = getelementptr inbounds %struct.ShellState* %3371, i32 0, i32 6
  %3373 = load i32* %3372, align 4
  %3374 = icmp ne i32 %3373, 0
  br i1 %3374, label %3375, label %3387

; <label>:3375                                    ; preds = %3370
  %3376 = load %struct.ShellState** %3, align 8
  %3377 = getelementptr inbounds %struct.ShellState* %3376, i32 0, i32 6
  %3378 = load i32* %3377, align 4
  %3379 = add nsw i32 %3378, -1
  store i32 %3379, i32* %3377, align 4
  %3380 = load %struct.ShellState** %3, align 8
  %3381 = getelementptr inbounds %struct.ShellState* %3380, i32 0, i32 6
  %3382 = load i32* %3381, align 4
  %3383 = icmp eq i32 %3382, 0
  br i1 %3383, label %3384, label %3386

; <label>:3384                                    ; preds = %3375
  %3385 = load %struct.ShellState** %3, align 8
  call void @output_reset(%struct.ShellState* %3385)
  br label %3386

; <label>:3386                                    ; preds = %3384, %3375
  br label %3387

; <label>:3387                                    ; preds = %3386, %3370
  %3388 = load i32* %rc, align 4
  store i32 %3388, i32* %1
  br label %3389

; <label>:3389                                    ; preds = %3387, %2751, %2707, %2197, %2181, %1221, %1158, %1146, %1128, %1098, %1075, %1026, %1005, %969, %940, %930, %924, %332, %316, %304, %292, %268, %208
  %3390 = load i32* %1
  ret i32 %3390
}

; Function Attrs: nounwind uwtable
define internal i32 @shell_exec(%struct.sqlite3* %db, i8* %zSql, i32 (i8*, i32, i8**, i8**, i32*)* %xCallback, %struct.ShellState* %pArg, i8** %pzErrMsg) #0 {
  %1 = alloca %struct.sqlite3*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i32 (i8*, i32, i8**, i8**, i32*)*, align 8
  %4 = alloca %struct.ShellState*, align 8
  %5 = alloca i8**, align 8
  %pStmt = alloca %struct.sqlite3_stmt*, align 8
  %rc = alloca i32, align 4
  %rc2 = alloca i32, align 4
  %zLeftover = alloca i8*, align 8
  %zStmtSql = alloca i8*, align 8
  %pExplain = alloca %struct.sqlite3_stmt*, align 8
  %zEQP = alloca i8*, align 8
  %nCol = alloca i32, align 4
  %pData = alloca i8*, align 8
  %azCols = alloca i8**, align 8
  %azVals = alloca i8**, align 8
  %aiTypes = alloca i32*, align 8
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  store %struct.sqlite3* %db, %struct.sqlite3** %1, align 8
  store i8* %zSql, i8** %2, align 8
  store i32 (i8*, i32, i8**, i8**, i32*)* %xCallback, i32 (i8*, i32, i8**, i8**, i32*)** %3, align 8
  store %struct.ShellState* %pArg, %struct.ShellState** %4, align 8
  store i8** %pzErrMsg, i8*** %5, align 8
  store %struct.sqlite3_stmt* null, %struct.sqlite3_stmt** %pStmt, align 8
  store i32 0, i32* %rc, align 4
  %6 = load i8*** %5, align 8
  %7 = icmp ne i8** %6, null
  br i1 %7, label %8, label %10

; <label>:8                                       ; preds = %0
  %9 = load i8*** %5, align 8
  store i8* null, i8** %9, align 8
  br label %10

; <label>:10                                      ; preds = %8, %0
  br label %11

; <label>:11                                      ; preds = %358, %57, %10
  %12 = load i8** %2, align 8
  %13 = getelementptr inbounds i8* %12, i64 0
  %14 = load i8* %13, align 1
  %15 = sext i8 %14 to i32
  %16 = icmp ne i32 %15, 0
  br i1 %16, label %17, label %20

; <label>:17                                      ; preds = %11
  %18 = load i32* %rc, align 4
  %19 = icmp eq i32 0, %18
  br label %20

; <label>:20                                      ; preds = %17, %11
  %21 = phi i1 [ false, %11 ], [ %19, %17 ]
  br i1 %21, label %22, label %359

; <label>:22                                      ; preds = %20
  %23 = load %struct.sqlite3** %1, align 8
  %24 = load i8** %2, align 8
  %25 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %23, i8* %24, i32 -1, %struct.sqlite3_stmt** %pStmt, i8** %zLeftover)
  store i32 %25, i32* %rc, align 4
  %26 = load i32* %rc, align 4
  %27 = icmp ne i32 0, %26
  br i1 %27, label %28, label %36

; <label>:28                                      ; preds = %22
  %29 = load i8*** %5, align 8
  %30 = icmp ne i8** %29, null
  br i1 %30, label %31, label %35

; <label>:31                                      ; preds = %28
  %32 = load %struct.sqlite3** %1, align 8
  %33 = call i8* @save_err_msg(%struct.sqlite3* %32)
  %34 = load i8*** %5, align 8
  store i8* %33, i8** %34, align 8
  br label %35

; <label>:35                                      ; preds = %31, %28
  br label %358

; <label>:36                                      ; preds = %22
  %37 = load %struct.sqlite3_stmt** %pStmt, align 8
  %38 = icmp ne %struct.sqlite3_stmt* %37, null
  br i1 %38, label %58, label %39

; <label>:39                                      ; preds = %36
  %40 = load i8** %zLeftover, align 8
  store i8* %40, i8** %2, align 8
  br label %41

; <label>:41                                      ; preds = %54, %39
  %42 = load i8** %2, align 8
  %43 = getelementptr inbounds i8* %42, i64 0
  %44 = load i8* %43, align 1
  %45 = zext i8 %44 to i32
  %46 = sext i32 %45 to i64
  %47 = call i16** @__ctype_b_loc() #9
  %48 = load i16** %47, align 8
  %49 = getelementptr inbounds i16* %48, i64 %46
  %50 = load i16* %49, align 2
  %51 = zext i16 %50 to i32
  %52 = and i32 %51, 8192
  %53 = icmp ne i32 %52, 0
  br i1 %53, label %54, label %57

; <label>:54                                      ; preds = %41
  %55 = load i8** %2, align 8
  %56 = getelementptr inbounds i8* %55, i32 1
  store i8* %56, i8** %2, align 8
  br label %41

; <label>:57                                      ; preds = %41
  br label %11

; <label>:58                                      ; preds = %36
  %59 = load %struct.ShellState** %4, align 8
  %60 = icmp ne %struct.ShellState* %59, null
  br i1 %60, label %61, label %67

; <label>:61                                      ; preds = %58
  %62 = load %struct.sqlite3_stmt** %pStmt, align 8
  %63 = load %struct.ShellState** %4, align 8
  %64 = getelementptr inbounds %struct.ShellState* %63, i32 0, i32 26
  store %struct.sqlite3_stmt* %62, %struct.sqlite3_stmt** %64, align 8
  %65 = load %struct.ShellState** %4, align 8
  %66 = getelementptr inbounds %struct.ShellState* %65, i32 0, i32 7
  store i32 0, i32* %66, align 4
  br label %67

; <label>:67                                      ; preds = %61, %58
  %68 = load %struct.ShellState** %4, align 8
  %69 = icmp ne %struct.ShellState* %68, null
  br i1 %69, label %70, label %90

; <label>:70                                      ; preds = %67
  %71 = load %struct.ShellState** %4, align 8
  %72 = getelementptr inbounds %struct.ShellState* %71, i32 0, i32 1
  %73 = load i32* %72, align 4
  %74 = icmp ne i32 %73, 0
  br i1 %74, label %75, label %90

; <label>:75                                      ; preds = %70
  %76 = load %struct.sqlite3_stmt** %pStmt, align 8
  %77 = call i8* @sqlite3_sql(%struct.sqlite3_stmt* %76)
  store i8* %77, i8** %zStmtSql, align 8
  %78 = load %struct.ShellState** %4, align 8
  %79 = getelementptr inbounds %struct.ShellState* %78, i32 0, i32 8
  %80 = load %struct._IO_FILE** %79, align 8
  %81 = load i8** %zStmtSql, align 8
  %82 = icmp ne i8* %81, null
  br i1 %82, label %83, label %85

; <label>:83                                      ; preds = %75
  %84 = load i8** %zStmtSql, align 8
  br label %87

; <label>:85                                      ; preds = %75
  %86 = load i8** %2, align 8
  br label %87

; <label>:87                                      ; preds = %85, %83
  %88 = phi i8* [ %84, %83 ], [ %86, %85 ]
  %89 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %80, i8* getelementptr inbounds ([4 x i8]* @.str51, i32 0, i32 0), i8* %88)
  br label %90

; <label>:90                                      ; preds = %87, %70, %67
  %91 = load %struct.ShellState** %4, align 8
  %92 = icmp ne %struct.ShellState* %91, null
  br i1 %92, label %93, label %142

; <label>:93                                      ; preds = %90
  %94 = load %struct.ShellState** %4, align 8
  %95 = getelementptr inbounds %struct.ShellState* %94, i32 0, i32 2
  %96 = load i32* %95, align 4
  %97 = icmp ne i32 %96, 0
  br i1 %97, label %98, label %142

; <label>:98                                      ; preds = %93
  %99 = load %struct.sqlite3_stmt** %pStmt, align 8
  %100 = call i8* @sqlite3_sql(%struct.sqlite3_stmt* %99)
  %101 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([22 x i8]* @.str95, i32 0, i32 0), i8* %100)
  store i8* %101, i8** %zEQP, align 8
  %102 = load %struct.sqlite3** %1, align 8
  %103 = load i8** %zEQP, align 8
  %104 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %102, i8* %103, i32 -1, %struct.sqlite3_stmt** %pExplain, i8** null)
  store i32 %104, i32* %rc, align 4
  %105 = load i32* %rc, align 4
  %106 = icmp eq i32 %105, 0
  br i1 %106, label %107, label %138

; <label>:107                                     ; preds = %98
  br label %108

; <label>:108                                     ; preds = %112, %107
  %109 = load %struct.sqlite3_stmt** %pExplain, align 8
  %110 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %109)
  %111 = icmp eq i32 %110, 100
  br i1 %111, label %112, label %137

; <label>:112                                     ; preds = %108
  %113 = load %struct.ShellState** %4, align 8
  %114 = getelementptr inbounds %struct.ShellState* %113, i32 0, i32 8
  %115 = load %struct._IO_FILE** %114, align 8
  %116 = load %struct.sqlite3_stmt** %pExplain, align 8
  %117 = call i32 @sqlite3_column_int(%struct.sqlite3_stmt* %116, i32 0)
  %118 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %115, i8* getelementptr inbounds ([12 x i8]* @.str96, i32 0, i32 0), i32 %117)
  %119 = load %struct.ShellState** %4, align 8
  %120 = getelementptr inbounds %struct.ShellState* %119, i32 0, i32 8
  %121 = load %struct._IO_FILE** %120, align 8
  %122 = load %struct.sqlite3_stmt** %pExplain, align 8
  %123 = call i32 @sqlite3_column_int(%struct.sqlite3_stmt* %122, i32 1)
  %124 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %121, i8* getelementptr inbounds ([4 x i8]* @.str97, i32 0, i32 0), i32 %123)
  %125 = load %struct.ShellState** %4, align 8
  %126 = getelementptr inbounds %struct.ShellState* %125, i32 0, i32 8
  %127 = load %struct._IO_FILE** %126, align 8
  %128 = load %struct.sqlite3_stmt** %pExplain, align 8
  %129 = call i32 @sqlite3_column_int(%struct.sqlite3_stmt* %128, i32 2)
  %130 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %127, i8* getelementptr inbounds ([4 x i8]* @.str97, i32 0, i32 0), i32 %129)
  %131 = load %struct.ShellState** %4, align 8
  %132 = getelementptr inbounds %struct.ShellState* %131, i32 0, i32 8
  %133 = load %struct._IO_FILE** %132, align 8
  %134 = load %struct.sqlite3_stmt** %pExplain, align 8
  %135 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %134, i32 3)
  %136 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %133, i8* getelementptr inbounds ([4 x i8]* @.str51, i32 0, i32 0), i8* %135)
  br label %108

; <label>:137                                     ; preds = %108
  br label %138

; <label>:138                                     ; preds = %137, %98
  %139 = load %struct.sqlite3_stmt** %pExplain, align 8
  %140 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %139)
  %141 = load i8** %zEQP, align 8
  call void @sqlite3_free(i8* %141)
  br label %142

; <label>:142                                     ; preds = %138, %93, %90
  %143 = load %struct.ShellState** %4, align 8
  %144 = icmp ne %struct.ShellState* %143, null
  br i1 %144, label %145, label %153

; <label>:145                                     ; preds = %142
  %146 = load %struct.ShellState** %4, align 8
  %147 = getelementptr inbounds %struct.ShellState* %146, i32 0, i32 11
  %148 = load i32* %147, align 4
  %149 = icmp eq i32 %148, 8
  br i1 %149, label %150, label %153

; <label>:150                                     ; preds = %145
  %151 = load %struct.ShellState** %4, align 8
  %152 = load %struct.sqlite3_stmt** %pStmt, align 8
  call void @explain_data_prepare(%struct.ShellState* %151, %struct.sqlite3_stmt* %152)
  br label %153

; <label>:153                                     ; preds = %150, %145, %142
  %154 = load %struct.sqlite3_stmt** %pStmt, align 8
  %155 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %154)
  store i32 %155, i32* %rc, align 4
  %156 = load i32* %rc, align 4
  %157 = icmp eq i32 100, %156
  br i1 %157, label %158, label %290

; <label>:158                                     ; preds = %153
  %159 = load i32 (i8*, i32, i8**, i8**, i32*)** %3, align 8
  %160 = icmp ne i32 (i8*, i32, i8**, i8**, i32*)* %159, null
  br i1 %160, label %161, label %281

; <label>:161                                     ; preds = %158
  %162 = load %struct.sqlite3_stmt** %pStmt, align 8
  %163 = call i32 @sqlite3_column_count(%struct.sqlite3_stmt* %162)
  store i32 %163, i32* %nCol, align 4
  %164 = load i32* %nCol, align 4
  %165 = mul nsw i32 3, %164
  %166 = sext i32 %165 to i64
  %167 = mul i64 %166, 8
  %168 = add i64 %167, 1
  %169 = call i8* @sqlite3_malloc64(i64 %168)
  store i8* %169, i8** %pData, align 8
  %170 = load i8** %pData, align 8
  %171 = icmp ne i8* %170, null
  br i1 %171, label %173, label %172

; <label>:172                                     ; preds = %161
  store i32 7, i32* %rc, align 4
  br label %280

; <label>:173                                     ; preds = %161
  %174 = load i8** %pData, align 8
  %175 = bitcast i8* %174 to i8**
  store i8** %175, i8*** %azCols, align 8
  %176 = load i32* %nCol, align 4
  %177 = sext i32 %176 to i64
  %178 = load i8*** %azCols, align 8
  %179 = getelementptr inbounds i8** %178, i64 %177
  store i8** %179, i8*** %azVals, align 8
  %180 = load i32* %nCol, align 4
  %181 = sext i32 %180 to i64
  %182 = load i8*** %azVals, align 8
  %183 = getelementptr inbounds i8** %182, i64 %181
  %184 = bitcast i8** %183 to i32*
  store i32* %184, i32** %aiTypes, align 8
  store i32 0, i32* %i, align 4
  br label %185

; <label>:185                                     ; preds = %197, %173
  %186 = load i32* %i, align 4
  %187 = load i32* %nCol, align 4
  %188 = icmp slt i32 %186, %187
  br i1 %188, label %189, label %200

; <label>:189                                     ; preds = %185
  %190 = load %struct.sqlite3_stmt** %pStmt, align 8
  %191 = load i32* %i, align 4
  %192 = call i8* @sqlite3_column_name(%struct.sqlite3_stmt* %190, i32 %191)
  %193 = load i32* %i, align 4
  %194 = sext i32 %193 to i64
  %195 = load i8*** %azCols, align 8
  %196 = getelementptr inbounds i8** %195, i64 %194
  store i8* %192, i8** %196, align 8
  br label %197

; <label>:197                                     ; preds = %189
  %198 = load i32* %i, align 4
  %199 = add nsw i32 %198, 1
  store i32 %199, i32* %i, align 4
  br label %185

; <label>:200                                     ; preds = %185
  br label %201

; <label>:201                                     ; preds = %275, %200
  store i32 0, i32* %i, align 4
  br label %202

; <label>:202                                     ; preds = %253, %201
  %203 = load i32* %i, align 4
  %204 = load i32* %nCol, align 4
  %205 = icmp slt i32 %203, %204
  br i1 %205, label %206, label %256

; <label>:206                                     ; preds = %202
  %207 = load %struct.sqlite3_stmt** %pStmt, align 8
  %208 = load i32* %i, align 4
  %209 = call i32 @sqlite3_column_type(%struct.sqlite3_stmt* %207, i32 %208)
  store i32 %209, i32* %x, align 4
  %210 = load i32* %i, align 4
  %211 = sext i32 %210 to i64
  %212 = load i32** %aiTypes, align 8
  %213 = getelementptr inbounds i32* %212, i64 %211
  store i32 %209, i32* %213, align 4
  %214 = load i32* %x, align 4
  %215 = icmp eq i32 %214, 4
  br i1 %215, label %216, label %229

; <label>:216                                     ; preds = %206
  %217 = load %struct.ShellState** %4, align 8
  %218 = icmp ne %struct.ShellState* %217, null
  br i1 %218, label %219, label %229

; <label>:219                                     ; preds = %216
  %220 = load %struct.ShellState** %4, align 8
  %221 = getelementptr inbounds %struct.ShellState* %220, i32 0, i32 11
  %222 = load i32* %221, align 4
  %223 = icmp eq i32 %222, 5
  br i1 %223, label %224, label %229

; <label>:224                                     ; preds = %219
  %225 = load i32* %i, align 4
  %226 = sext i32 %225 to i64
  %227 = load i8*** %azVals, align 8
  %228 = getelementptr inbounds i8** %227, i64 %226
  store i8* getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), i8** %228, align 8
  br label %237

; <label>:229                                     ; preds = %219, %216, %206
  %230 = load %struct.sqlite3_stmt** %pStmt, align 8
  %231 = load i32* %i, align 4
  %232 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %230, i32 %231)
  %233 = load i32* %i, align 4
  %234 = sext i32 %233 to i64
  %235 = load i8*** %azVals, align 8
  %236 = getelementptr inbounds i8** %235, i64 %234
  store i8* %232, i8** %236, align 8
  br label %237

; <label>:237                                     ; preds = %229, %224
  %238 = load i32* %i, align 4
  %239 = sext i32 %238 to i64
  %240 = load i8*** %azVals, align 8
  %241 = getelementptr inbounds i8** %240, i64 %239
  %242 = load i8** %241, align 8
  %243 = icmp ne i8* %242, null
  br i1 %243, label %252, label %244

; <label>:244                                     ; preds = %237
  %245 = load i32* %i, align 4
  %246 = sext i32 %245 to i64
  %247 = load i32** %aiTypes, align 8
  %248 = getelementptr inbounds i32* %247, i64 %246
  %249 = load i32* %248, align 4
  %250 = icmp ne i32 %249, 5
  br i1 %250, label %251, label %252

; <label>:251                                     ; preds = %244
  store i32 7, i32* %rc, align 4
  br label %256

; <label>:252                                     ; preds = %244, %237
  br label %253

; <label>:253                                     ; preds = %252
  %254 = load i32* %i, align 4
  %255 = add nsw i32 %254, 1
  store i32 %255, i32* %i, align 4
  br label %202

; <label>:256                                     ; preds = %251, %202
  %257 = load i32* %rc, align 4
  %258 = icmp eq i32 100, %257
  br i1 %258, label %259, label %274

; <label>:259                                     ; preds = %256
  %260 = load i32 (i8*, i32, i8**, i8**, i32*)** %3, align 8
  %261 = load %struct.ShellState** %4, align 8
  %262 = bitcast %struct.ShellState* %261 to i8*
  %263 = load i32* %nCol, align 4
  %264 = load i8*** %azVals, align 8
  %265 = load i8*** %azCols, align 8
  %266 = load i32** %aiTypes, align 8
  %267 = call i32 %260(i8* %262, i32 %263, i8** %264, i8** %265, i32* %266)
  %268 = icmp ne i32 %267, 0
  br i1 %268, label %269, label %270

; <label>:269                                     ; preds = %259
  store i32 4, i32* %rc, align 4
  br label %273

; <label>:270                                     ; preds = %259
  %271 = load %struct.sqlite3_stmt** %pStmt, align 8
  %272 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %271)
  store i32 %272, i32* %rc, align 4
  br label %273

; <label>:273                                     ; preds = %270, %269
  br label %274

; <label>:274                                     ; preds = %273, %256
  br label %275

; <label>:275                                     ; preds = %274
  %276 = load i32* %rc, align 4
  %277 = icmp eq i32 100, %276
  br i1 %277, label %201, label %278

; <label>:278                                     ; preds = %275
  %279 = load i8** %pData, align 8
  call void @sqlite3_free(i8* %279)
  br label %280

; <label>:280                                     ; preds = %278, %172
  br label %289

; <label>:281                                     ; preds = %158
  br label %282

; <label>:282                                     ; preds = %285, %281
  %283 = load %struct.sqlite3_stmt** %pStmt, align 8
  %284 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %283)
  store i32 %284, i32* %rc, align 4
  br label %285

; <label>:285                                     ; preds = %282
  %286 = load i32* %rc, align 4
  %287 = icmp eq i32 %286, 100
  br i1 %287, label %282, label %288

; <label>:288                                     ; preds = %285
  br label %289

; <label>:289                                     ; preds = %288, %280
  br label %290

; <label>:290                                     ; preds = %289, %153
  %291 = load %struct.ShellState** %4, align 8
  call void @explain_data_delete(%struct.ShellState* %291)
  %292 = load %struct.ShellState** %4, align 8
  %293 = icmp ne %struct.ShellState* %292, null
  br i1 %293, label %294, label %303

; <label>:294                                     ; preds = %290
  %295 = load %struct.ShellState** %4, align 8
  %296 = getelementptr inbounds %struct.ShellState* %295, i32 0, i32 3
  %297 = load i32* %296, align 4
  %298 = icmp ne i32 %297, 0
  br i1 %298, label %299, label %303

; <label>:299                                     ; preds = %294
  %300 = load %struct.sqlite3** %1, align 8
  %301 = load %struct.ShellState** %4, align 8
  %302 = call i32 @display_stats(%struct.sqlite3* %300, %struct.ShellState* %301, i32 0)
  br label %303

; <label>:303                                     ; preds = %299, %294, %290
  %304 = load %struct.ShellState** %4, align 8
  %305 = icmp ne %struct.ShellState* %304, null
  br i1 %305, label %306, label %314

; <label>:306                                     ; preds = %303
  %307 = load %struct.ShellState** %4, align 8
  %308 = getelementptr inbounds %struct.ShellState* %307, i32 0, i32 4
  %309 = load i32* %308, align 4
  %310 = icmp ne i32 %309, 0
  br i1 %310, label %311, label %314

; <label>:311                                     ; preds = %306
  %312 = load %struct.sqlite3** %1, align 8
  %313 = load %struct.ShellState** %4, align 8
  call void @display_scanstats(%struct.sqlite3* %312, %struct.ShellState* %313)
  br label %314

; <label>:314                                     ; preds = %311, %306, %303
  %315 = load %struct.sqlite3_stmt** %pStmt, align 8
  %316 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %315)
  store i32 %316, i32* %rc2, align 4
  %317 = load i32* %rc, align 4
  %318 = icmp ne i32 %317, 7
  br i1 %318, label %319, label %321

; <label>:319                                     ; preds = %314
  %320 = load i32* %rc2, align 4
  store i32 %320, i32* %rc, align 4
  br label %321

; <label>:321                                     ; preds = %319, %314
  %322 = load i32* %rc, align 4
  %323 = icmp eq i32 %322, 0
  br i1 %323, label %324, label %343

; <label>:324                                     ; preds = %321
  %325 = load i8** %zLeftover, align 8
  store i8* %325, i8** %2, align 8
  br label %326

; <label>:326                                     ; preds = %339, %324
  %327 = load i8** %2, align 8
  %328 = getelementptr inbounds i8* %327, i64 0
  %329 = load i8* %328, align 1
  %330 = zext i8 %329 to i32
  %331 = sext i32 %330 to i64
  %332 = call i16** @__ctype_b_loc() #9
  %333 = load i16** %332, align 8
  %334 = getelementptr inbounds i16* %333, i64 %331
  %335 = load i16* %334, align 2
  %336 = zext i16 %335 to i32
  %337 = and i32 %336, 8192
  %338 = icmp ne i32 %337, 0
  br i1 %338, label %339, label %342

; <label>:339                                     ; preds = %326
  %340 = load i8** %2, align 8
  %341 = getelementptr inbounds i8* %340, i32 1
  store i8* %341, i8** %2, align 8
  br label %326

; <label>:342                                     ; preds = %326
  br label %351

; <label>:343                                     ; preds = %321
  %344 = load i8*** %5, align 8
  %345 = icmp ne i8** %344, null
  br i1 %345, label %346, label %350

; <label>:346                                     ; preds = %343
  %347 = load %struct.sqlite3** %1, align 8
  %348 = call i8* @save_err_msg(%struct.sqlite3* %347)
  %349 = load i8*** %5, align 8
  store i8* %348, i8** %349, align 8
  br label %350

; <label>:350                                     ; preds = %346, %343
  br label %351

; <label>:351                                     ; preds = %350, %342
  %352 = load %struct.ShellState** %4, align 8
  %353 = icmp ne %struct.ShellState* %352, null
  br i1 %353, label %354, label %357

; <label>:354                                     ; preds = %351
  %355 = load %struct.ShellState** %4, align 8
  %356 = getelementptr inbounds %struct.ShellState* %355, i32 0, i32 26
  store %struct.sqlite3_stmt* null, %struct.sqlite3_stmt** %356, align 8
  br label %357

; <label>:357                                     ; preds = %354, %351
  br label %358

; <label>:358                                     ; preds = %357, %35
  br label %11

; <label>:359                                     ; preds = %20
  %360 = load i32* %rc, align 4
  ret i32 %360
}

; Function Attrs: nounwind uwtable
define internal i32 @shell_callback(i8* %pArg, i32 %nArg, i8** %azArg, i8** %azCol, i32* %aiType) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %4 = alloca i8**, align 8
  %5 = alloca i32*, align 8
  %i = alloca i32, align 4
  %p = alloca %struct.ShellState*, align 8
  %w = alloca i32, align 4
  %len = alloca i32, align 4
  %w1 = alloca i32, align 4
  %n = alloca i32, align 4
  %w2 = alloca i32, align 4
  %w3 = alloca i32, align 4
  %z = alloca i8*, align 8
  %zSep = alloca i8*, align 8
  %zSep4 = alloca i8*, align 8
  %pBlob = alloca i8*, align 8
  %nBlob = alloca i32, align 4
  store i8* %pArg, i8** %1, align 8
  store i32 %nArg, i32* %2, align 4
  store i8** %azArg, i8*** %3, align 8
  store i8** %azCol, i8*** %4, align 8
  store i32* %aiType, i32** %5, align 8
  %6 = load i8** %1, align 8
  %7 = bitcast i8* %6 to %struct.ShellState*
  store %struct.ShellState* %7, %struct.ShellState** %p, align 8
  %8 = load %struct.ShellState** %p, align 8
  %9 = getelementptr inbounds %struct.ShellState* %8, i32 0, i32 11
  %10 = load i32* %9, align 4
  switch i32 %10, label %1217 [
    i32 0, label %11
    i32 8, label %101
    i32 1, label %101
    i32 3, label %466
    i32 2, label %466
    i32 4, label %576
    i32 6, label %672
    i32 7, label %786
    i32 5, label %867
    i32 9, label %1105
  ]

; <label>:11                                      ; preds = %0
  store i32 5, i32* %w, align 4
  %12 = load i8*** %3, align 8
  %13 = icmp eq i8** %12, null
  br i1 %13, label %14, label %15

; <label>:14                                      ; preds = %11
  br label %1217

; <label>:15                                      ; preds = %11
  store i32 0, i32* %i, align 4
  br label %16

; <label>:16                                      ; preds = %43, %15
  %17 = load i32* %i, align 4
  %18 = load i32* %2, align 4
  %19 = icmp slt i32 %17, %18
  br i1 %19, label %20, label %46

; <label>:20                                      ; preds = %16
  %21 = load i32* %i, align 4
  %22 = sext i32 %21 to i64
  %23 = load i8*** %4, align 8
  %24 = getelementptr inbounds i8** %23, i64 %22
  %25 = load i8** %24, align 8
  %26 = icmp ne i8* %25, null
  br i1 %26, label %27, label %33

; <label>:27                                      ; preds = %20
  %28 = load i32* %i, align 4
  %29 = sext i32 %28 to i64
  %30 = load i8*** %4, align 8
  %31 = getelementptr inbounds i8** %30, i64 %29
  %32 = load i8** %31, align 8
  br label %34

; <label>:33                                      ; preds = %20
  br label %34

; <label>:34                                      ; preds = %33, %27
  %35 = phi i8* [ %32, %27 ], [ getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), %33 ]
  %36 = call i32 @strlen30(i8* %35)
  store i32 %36, i32* %len, align 4
  %37 = load i32* %len, align 4
  %38 = load i32* %w, align 4
  %39 = icmp sgt i32 %37, %38
  br i1 %39, label %40, label %42

; <label>:40                                      ; preds = %34
  %41 = load i32* %len, align 4
  store i32 %41, i32* %w, align 4
  br label %42

; <label>:42                                      ; preds = %40, %34
  br label %43

; <label>:43                                      ; preds = %42
  %44 = load i32* %i, align 4
  %45 = add nsw i32 %44, 1
  store i32 %45, i32* %i, align 4
  br label %16

; <label>:46                                      ; preds = %16
  %47 = load %struct.ShellState** %p, align 8
  %48 = getelementptr inbounds %struct.ShellState* %47, i32 0, i32 7
  %49 = load i32* %48, align 4
  %50 = add nsw i32 %49, 1
  store i32 %50, i32* %48, align 4
  %51 = icmp sgt i32 %49, 0
  br i1 %51, label %52, label %60

; <label>:52                                      ; preds = %46
  %53 = load %struct.ShellState** %p, align 8
  %54 = getelementptr inbounds %struct.ShellState* %53, i32 0, i32 8
  %55 = load %struct._IO_FILE** %54, align 8
  %56 = load %struct.ShellState** %p, align 8
  %57 = getelementptr inbounds %struct.ShellState* %56, i32 0, i32 17
  %58 = getelementptr inbounds [20 x i8]* %57, i32 0, i32 0
  %59 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %55, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %58)
  br label %60

; <label>:60                                      ; preds = %52, %46
  store i32 0, i32* %i, align 4
  br label %61

; <label>:61                                      ; preds = %97, %60
  %62 = load i32* %i, align 4
  %63 = load i32* %2, align 4
  %64 = icmp slt i32 %62, %63
  br i1 %64, label %65, label %100

; <label>:65                                      ; preds = %61
  %66 = load %struct.ShellState** %p, align 8
  %67 = getelementptr inbounds %struct.ShellState* %66, i32 0, i32 8
  %68 = load %struct._IO_FILE** %67, align 8
  %69 = load i32* %w, align 4
  %70 = load i32* %i, align 4
  %71 = sext i32 %70 to i64
  %72 = load i8*** %4, align 8
  %73 = getelementptr inbounds i8** %72, i64 %71
  %74 = load i8** %73, align 8
  %75 = load i32* %i, align 4
  %76 = sext i32 %75 to i64
  %77 = load i8*** %3, align 8
  %78 = getelementptr inbounds i8** %77, i64 %76
  %79 = load i8** %78, align 8
  %80 = icmp ne i8* %79, null
  br i1 %80, label %81, label %87

; <label>:81                                      ; preds = %65
  %82 = load i32* %i, align 4
  %83 = sext i32 %82 to i64
  %84 = load i8*** %3, align 8
  %85 = getelementptr inbounds i8** %84, i64 %83
  %86 = load i8** %85, align 8
  br label %91

; <label>:87                                      ; preds = %65
  %88 = load %struct.ShellState** %p, align 8
  %89 = getelementptr inbounds %struct.ShellState* %88, i32 0, i32 20
  %90 = getelementptr inbounds [20 x i8]* %89, i32 0, i32 0
  br label %91

; <label>:91                                      ; preds = %87, %81
  %92 = phi i8* [ %86, %81 ], [ %90, %87 ]
  %93 = load %struct.ShellState** %p, align 8
  %94 = getelementptr inbounds %struct.ShellState* %93, i32 0, i32 17
  %95 = getelementptr inbounds [20 x i8]* %94, i32 0, i32 0
  %96 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %68, i8* getelementptr inbounds ([11 x i8]* @.str62, i32 0, i32 0), i32 %69, i8* %74, i8* %92, i8* %95)
  br label %97

; <label>:97                                      ; preds = %91
  %98 = load i32* %i, align 4
  %99 = add nsw i32 %98, 1
  store i32 %99, i32* %i, align 4
  br label %61

; <label>:100                                     ; preds = %61
  br label %1217

; <label>:101                                     ; preds = %0, %0
  %102 = load %struct.ShellState** %p, align 8
  %103 = getelementptr inbounds %struct.ShellState* %102, i32 0, i32 7
  %104 = load i32* %103, align 4
  %105 = add nsw i32 %104, 1
  store i32 %105, i32* %103, align 4
  %106 = icmp eq i32 %104, 0
  br i1 %106, label %107, label %296

; <label>:107                                     ; preds = %101
  store i32 0, i32* %i, align 4
  br label %108

; <label>:108                                     ; preds = %243, %107
  %109 = load i32* %i, align 4
  %110 = load i32* %2, align 4
  %111 = icmp slt i32 %109, %110
  br i1 %111, label %112, label %246

; <label>:112                                     ; preds = %108
  %113 = load i32* %i, align 4
  %114 = icmp slt i32 %113, 100
  br i1 %114, label %115, label %122

; <label>:115                                     ; preds = %112
  %116 = load i32* %i, align 4
  %117 = sext i32 %116 to i64
  %118 = load %struct.ShellState** %p, align 8
  %119 = getelementptr inbounds %struct.ShellState* %118, i32 0, i32 18
  %120 = getelementptr inbounds [100 x i32]* %119, i32 0, i64 %117
  %121 = load i32* %120, align 4
  store i32 %121, i32* %w1, align 4
  br label %123

; <label>:122                                     ; preds = %112
  store i32 0, i32* %w1, align 4
  br label %123

; <label>:123                                     ; preds = %122, %115
  %124 = load i32* %w1, align 4
  %125 = icmp eq i32 %124, 0
  br i1 %125, label %126, label %175

; <label>:126                                     ; preds = %123
  %127 = load i32* %i, align 4
  %128 = sext i32 %127 to i64
  %129 = load i8*** %4, align 8
  %130 = getelementptr inbounds i8** %129, i64 %128
  %131 = load i8** %130, align 8
  %132 = icmp ne i8* %131, null
  br i1 %132, label %133, label %139

; <label>:133                                     ; preds = %126
  %134 = load i32* %i, align 4
  %135 = sext i32 %134 to i64
  %136 = load i8*** %4, align 8
  %137 = getelementptr inbounds i8** %136, i64 %135
  %138 = load i8** %137, align 8
  br label %140

; <label>:139                                     ; preds = %126
  br label %140

; <label>:140                                     ; preds = %139, %133
  %141 = phi i8* [ %138, %133 ], [ getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), %139 ]
  %142 = call i32 @strlen30(i8* %141)
  store i32 %142, i32* %w1, align 4
  %143 = load i32* %w1, align 4
  %144 = icmp slt i32 %143, 10
  br i1 %144, label %145, label %146

; <label>:145                                     ; preds = %140
  store i32 10, i32* %w1, align 4
  br label %146

; <label>:146                                     ; preds = %145, %140
  %147 = load i8*** %3, align 8
  %148 = icmp ne i8** %147, null
  br i1 %148, label %149, label %162

; <label>:149                                     ; preds = %146
  %150 = load i32* %i, align 4
  %151 = sext i32 %150 to i64
  %152 = load i8*** %3, align 8
  %153 = getelementptr inbounds i8** %152, i64 %151
  %154 = load i8** %153, align 8
  %155 = icmp ne i8* %154, null
  br i1 %155, label %156, label %162

; <label>:156                                     ; preds = %149
  %157 = load i32* %i, align 4
  %158 = sext i32 %157 to i64
  %159 = load i8*** %3, align 8
  %160 = getelementptr inbounds i8** %159, i64 %158
  %161 = load i8** %160, align 8
  br label %166

; <label>:162                                     ; preds = %149, %146
  %163 = load %struct.ShellState** %p, align 8
  %164 = getelementptr inbounds %struct.ShellState* %163, i32 0, i32 20
  %165 = getelementptr inbounds [20 x i8]* %164, i32 0, i32 0
  br label %166

; <label>:166                                     ; preds = %162, %156
  %167 = phi i8* [ %161, %156 ], [ %165, %162 ]
  %168 = call i32 @strlen30(i8* %167)
  store i32 %168, i32* %n, align 4
  %169 = load i32* %w1, align 4
  %170 = load i32* %n, align 4
  %171 = icmp slt i32 %169, %170
  br i1 %171, label %172, label %174

; <label>:172                                     ; preds = %166
  %173 = load i32* %n, align 4
  store i32 %173, i32* %w1, align 4
  br label %174

; <label>:174                                     ; preds = %172, %166
  br label %175

; <label>:175                                     ; preds = %174, %123
  %176 = load i32* %i, align 4
  %177 = icmp slt i32 %176, 100
  br i1 %177, label %178, label %185

; <label>:178                                     ; preds = %175
  %179 = load i32* %w1, align 4
  %180 = load i32* %i, align 4
  %181 = sext i32 %180 to i64
  %182 = load %struct.ShellState** %p, align 8
  %183 = getelementptr inbounds %struct.ShellState* %182, i32 0, i32 19
  %184 = getelementptr inbounds [100 x i32]* %183, i32 0, i64 %181
  store i32 %179, i32* %184, align 4
  br label %185

; <label>:185                                     ; preds = %178, %175
  %186 = load %struct.ShellState** %p, align 8
  %187 = getelementptr inbounds %struct.ShellState* %186, i32 0, i32 13
  %188 = load i32* %187, align 4
  %189 = icmp ne i32 %188, 0
  br i1 %189, label %190, label %242

; <label>:190                                     ; preds = %185
  %191 = load i32* %w1, align 4
  %192 = icmp slt i32 %191, 0
  br i1 %192, label %193, label %218

; <label>:193                                     ; preds = %190
  %194 = load %struct.ShellState** %p, align 8
  %195 = getelementptr inbounds %struct.ShellState* %194, i32 0, i32 8
  %196 = load %struct._IO_FILE** %195, align 8
  %197 = load i32* %w1, align 4
  %198 = sub nsw i32 0, %197
  %199 = load i32* %w1, align 4
  %200 = sub nsw i32 0, %199
  %201 = load i32* %i, align 4
  %202 = sext i32 %201 to i64
  %203 = load i8*** %4, align 8
  %204 = getelementptr inbounds i8** %203, i64 %202
  %205 = load i8** %204, align 8
  %206 = load i32* %i, align 4
  %207 = load i32* %2, align 4
  %208 = sub nsw i32 %207, 1
  %209 = icmp eq i32 %206, %208
  br i1 %209, label %210, label %214

; <label>:210                                     ; preds = %193
  %211 = load %struct.ShellState** %p, align 8
  %212 = getelementptr inbounds %struct.ShellState* %211, i32 0, i32 17
  %213 = getelementptr inbounds [20 x i8]* %212, i32 0, i32 0
  br label %215

; <label>:214                                     ; preds = %193
  br label %215

; <label>:215                                     ; preds = %214, %210
  %216 = phi i8* [ %213, %210 ], [ getelementptr inbounds ([3 x i8]* @.str64, i32 0, i32 0), %214 ]
  %217 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %196, i8* getelementptr inbounds ([8 x i8]* @.str63, i32 0, i32 0), i32 %198, i32 %200, i8* %205, i8* %216)
  br label %241

; <label>:218                                     ; preds = %190
  %219 = load %struct.ShellState** %p, align 8
  %220 = getelementptr inbounds %struct.ShellState* %219, i32 0, i32 8
  %221 = load %struct._IO_FILE** %220, align 8
  %222 = load i32* %w1, align 4
  %223 = load i32* %w1, align 4
  %224 = load i32* %i, align 4
  %225 = sext i32 %224 to i64
  %226 = load i8*** %4, align 8
  %227 = getelementptr inbounds i8** %226, i64 %225
  %228 = load i8** %227, align 8
  %229 = load i32* %i, align 4
  %230 = load i32* %2, align 4
  %231 = sub nsw i32 %230, 1
  %232 = icmp eq i32 %229, %231
  br i1 %232, label %233, label %237

; <label>:233                                     ; preds = %218
  %234 = load %struct.ShellState** %p, align 8
  %235 = getelementptr inbounds %struct.ShellState* %234, i32 0, i32 17
  %236 = getelementptr inbounds [20 x i8]* %235, i32 0, i32 0
  br label %238

; <label>:237                                     ; preds = %218
  br label %238

; <label>:238                                     ; preds = %237, %233
  %239 = phi i8* [ %236, %233 ], [ getelementptr inbounds ([3 x i8]* @.str64, i32 0, i32 0), %237 ]
  %240 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %221, i8* getelementptr inbounds ([9 x i8]* @.str65, i32 0, i32 0), i32 %222, i32 %223, i8* %228, i8* %239)
  br label %241

; <label>:241                                     ; preds = %238, %215
  br label %242

; <label>:242                                     ; preds = %241, %185
  br label %243

; <label>:243                                     ; preds = %242
  %244 = load i32* %i, align 4
  %245 = add nsw i32 %244, 1
  store i32 %245, i32* %i, align 4
  br label %108

; <label>:246                                     ; preds = %108
  %247 = load %struct.ShellState** %p, align 8
  %248 = getelementptr inbounds %struct.ShellState* %247, i32 0, i32 13
  %249 = load i32* %248, align 4
  %250 = icmp ne i32 %249, 0
  br i1 %250, label %251, label %295

; <label>:251                                     ; preds = %246
  store i32 0, i32* %i, align 4
  br label %252

; <label>:252                                     ; preds = %291, %251
  %253 = load i32* %i, align 4
  %254 = load i32* %2, align 4
  %255 = icmp slt i32 %253, %254
  br i1 %255, label %256, label %294

; <label>:256                                     ; preds = %252
  %257 = load i32* %i, align 4
  %258 = icmp slt i32 %257, 100
  br i1 %258, label %259, label %272

; <label>:259                                     ; preds = %256
  %260 = load i32* %i, align 4
  %261 = sext i32 %260 to i64
  %262 = load %struct.ShellState** %p, align 8
  %263 = getelementptr inbounds %struct.ShellState* %262, i32 0, i32 19
  %264 = getelementptr inbounds [100 x i32]* %263, i32 0, i64 %261
  %265 = load i32* %264, align 4
  store i32 %265, i32* %w2, align 4
  %266 = load i32* %w2, align 4
  %267 = icmp slt i32 %266, 0
  br i1 %267, label %268, label %271

; <label>:268                                     ; preds = %259
  %269 = load i32* %w2, align 4
  %270 = sub nsw i32 0, %269
  store i32 %270, i32* %w2, align 4
  br label %271

; <label>:271                                     ; preds = %268, %259
  br label %273

; <label>:272                                     ; preds = %256
  store i32 10, i32* %w2, align 4
  br label %273

; <label>:273                                     ; preds = %272, %271
  %274 = load %struct.ShellState** %p, align 8
  %275 = getelementptr inbounds %struct.ShellState* %274, i32 0, i32 8
  %276 = load %struct._IO_FILE** %275, align 8
  %277 = load i32* %w2, align 4
  %278 = load i32* %w2, align 4
  %279 = load i32* %i, align 4
  %280 = load i32* %2, align 4
  %281 = sub nsw i32 %280, 1
  %282 = icmp eq i32 %279, %281
  br i1 %282, label %283, label %287

; <label>:283                                     ; preds = %273
  %284 = load %struct.ShellState** %p, align 8
  %285 = getelementptr inbounds %struct.ShellState* %284, i32 0, i32 17
  %286 = getelementptr inbounds [20 x i8]* %285, i32 0, i32 0
  br label %288

; <label>:287                                     ; preds = %273
  br label %288

; <label>:288                                     ; preds = %287, %283
  %289 = phi i8* [ %286, %283 ], [ getelementptr inbounds ([3 x i8]* @.str64, i32 0, i32 0), %287 ]
  %290 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %276, i8* getelementptr inbounds ([9 x i8]* @.str65, i32 0, i32 0), i32 %277, i32 %278, i8* getelementptr inbounds ([94 x i8]* @.str66, i32 0, i32 0), i8* %289)
  br label %291

; <label>:291                                     ; preds = %288
  %292 = load i32* %i, align 4
  %293 = add nsw i32 %292, 1
  store i32 %293, i32* %i, align 4
  br label %252

; <label>:294                                     ; preds = %252
  br label %295

; <label>:295                                     ; preds = %294, %246
  br label %296

; <label>:296                                     ; preds = %295, %101
  %297 = load i8*** %3, align 8
  %298 = icmp eq i8** %297, null
  br i1 %298, label %299, label %300

; <label>:299                                     ; preds = %296
  br label %1217

; <label>:300                                     ; preds = %296
  store i32 0, i32* %i, align 4
  br label %301

; <label>:301                                     ; preds = %462, %300
  %302 = load i32* %i, align 4
  %303 = load i32* %2, align 4
  %304 = icmp slt i32 %302, %303
  br i1 %304, label %305, label %465

; <label>:305                                     ; preds = %301
  %306 = load i32* %i, align 4
  %307 = icmp slt i32 %306, 100
  br i1 %307, label %308, label %315

; <label>:308                                     ; preds = %305
  %309 = load i32* %i, align 4
  %310 = sext i32 %309 to i64
  %311 = load %struct.ShellState** %p, align 8
  %312 = getelementptr inbounds %struct.ShellState* %311, i32 0, i32 19
  %313 = getelementptr inbounds [100 x i32]* %312, i32 0, i64 %310
  %314 = load i32* %313, align 4
  store i32 %314, i32* %w3, align 4
  br label %316

; <label>:315                                     ; preds = %305
  store i32 10, i32* %w3, align 4
  br label %316

; <label>:316                                     ; preds = %315, %308
  %317 = load %struct.ShellState** %p, align 8
  %318 = getelementptr inbounds %struct.ShellState* %317, i32 0, i32 11
  %319 = load i32* %318, align 4
  %320 = icmp eq i32 %319, 8
  br i1 %320, label %321, label %344

; <label>:321                                     ; preds = %316
  %322 = load i32* %i, align 4
  %323 = sext i32 %322 to i64
  %324 = load i8*** %3, align 8
  %325 = getelementptr inbounds i8** %324, i64 %323
  %326 = load i8** %325, align 8
  %327 = icmp ne i8* %326, null
  br i1 %327, label %328, label %344

; <label>:328                                     ; preds = %321
  %329 = load i32* %i, align 4
  %330 = sext i32 %329 to i64
  %331 = load i8*** %3, align 8
  %332 = getelementptr inbounds i8** %331, i64 %330
  %333 = load i8** %332, align 8
  %334 = call i32 @strlen30(i8* %333)
  %335 = load i32* %w3, align 4
  %336 = icmp sgt i32 %334, %335
  br i1 %336, label %337, label %344

; <label>:337                                     ; preds = %328
  %338 = load i32* %i, align 4
  %339 = sext i32 %338 to i64
  %340 = load i8*** %3, align 8
  %341 = getelementptr inbounds i8** %340, i64 %339
  %342 = load i8** %341, align 8
  %343 = call i32 @strlen30(i8* %342)
  store i32 %343, i32* %w3, align 4
  br label %344

; <label>:344                                     ; preds = %337, %328, %321, %316
  %345 = load i32* %i, align 4
  %346 = icmp eq i32 %345, 1
  br i1 %346, label %347, label %384

; <label>:347                                     ; preds = %344
  %348 = load %struct.ShellState** %p, align 8
  %349 = getelementptr inbounds %struct.ShellState* %348, i32 0, i32 28
  %350 = load i32** %349, align 8
  %351 = icmp ne i32* %350, null
  br i1 %351, label %352, label %384

; <label>:352                                     ; preds = %347
  %353 = load %struct.ShellState** %p, align 8
  %354 = getelementptr inbounds %struct.ShellState* %353, i32 0, i32 26
  %355 = load %struct.sqlite3_stmt** %354, align 8
  %356 = icmp ne %struct.sqlite3_stmt* %355, null
  br i1 %356, label %357, label %384

; <label>:357                                     ; preds = %352
  %358 = load %struct.ShellState** %p, align 8
  %359 = getelementptr inbounds %struct.ShellState* %358, i32 0, i32 30
  %360 = load i32* %359, align 4
  %361 = load %struct.ShellState** %p, align 8
  %362 = getelementptr inbounds %struct.ShellState* %361, i32 0, i32 29
  %363 = load i32* %362, align 4
  %364 = icmp slt i32 %360, %363
  br i1 %364, label %365, label %379

; <label>:365                                     ; preds = %357
  %366 = load %struct.ShellState** %p, align 8
  %367 = getelementptr inbounds %struct.ShellState* %366, i32 0, i32 8
  %368 = load %struct._IO_FILE** %367, align 8
  %369 = load %struct.ShellState** %p, align 8
  %370 = getelementptr inbounds %struct.ShellState* %369, i32 0, i32 30
  %371 = load i32* %370, align 4
  %372 = sext i32 %371 to i64
  %373 = load %struct.ShellState** %p, align 8
  %374 = getelementptr inbounds %struct.ShellState* %373, i32 0, i32 28
  %375 = load i32** %374, align 8
  %376 = getelementptr inbounds i32* %375, i64 %372
  %377 = load i32* %376, align 4
  %378 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %368, i8* getelementptr inbounds ([5 x i8]* @.str67, i32 0, i32 0), i32 %377, i8* getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0))
  br label %379

; <label>:379                                     ; preds = %365, %357
  %380 = load %struct.ShellState** %p, align 8
  %381 = getelementptr inbounds %struct.ShellState* %380, i32 0, i32 30
  %382 = load i32* %381, align 4
  %383 = add nsw i32 %382, 1
  store i32 %383, i32* %381, align 4
  br label %384

; <label>:384                                     ; preds = %379, %352, %347, %344
  %385 = load i32* %w3, align 4
  %386 = icmp slt i32 %385, 0
  br i1 %386, label %387, label %425

; <label>:387                                     ; preds = %384
  %388 = load %struct.ShellState** %p, align 8
  %389 = getelementptr inbounds %struct.ShellState* %388, i32 0, i32 8
  %390 = load %struct._IO_FILE** %389, align 8
  %391 = load i32* %w3, align 4
  %392 = sub nsw i32 0, %391
  %393 = load i32* %w3, align 4
  %394 = sub nsw i32 0, %393
  %395 = load i32* %i, align 4
  %396 = sext i32 %395 to i64
  %397 = load i8*** %3, align 8
  %398 = getelementptr inbounds i8** %397, i64 %396
  %399 = load i8** %398, align 8
  %400 = icmp ne i8* %399, null
  br i1 %400, label %401, label %407

; <label>:401                                     ; preds = %387
  %402 = load i32* %i, align 4
  %403 = sext i32 %402 to i64
  %404 = load i8*** %3, align 8
  %405 = getelementptr inbounds i8** %404, i64 %403
  %406 = load i8** %405, align 8
  br label %411

; <label>:407                                     ; preds = %387
  %408 = load %struct.ShellState** %p, align 8
  %409 = getelementptr inbounds %struct.ShellState* %408, i32 0, i32 20
  %410 = getelementptr inbounds [20 x i8]* %409, i32 0, i32 0
  br label %411

; <label>:411                                     ; preds = %407, %401
  %412 = phi i8* [ %406, %401 ], [ %410, %407 ]
  %413 = load i32* %i, align 4
  %414 = load i32* %2, align 4
  %415 = sub nsw i32 %414, 1
  %416 = icmp eq i32 %413, %415
  br i1 %416, label %417, label %421

; <label>:417                                     ; preds = %411
  %418 = load %struct.ShellState** %p, align 8
  %419 = getelementptr inbounds %struct.ShellState* %418, i32 0, i32 17
  %420 = getelementptr inbounds [20 x i8]* %419, i32 0, i32 0
  br label %422

; <label>:421                                     ; preds = %411
  br label %422

; <label>:422                                     ; preds = %421, %417
  %423 = phi i8* [ %420, %417 ], [ getelementptr inbounds ([3 x i8]* @.str64, i32 0, i32 0), %421 ]
  %424 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %390, i8* getelementptr inbounds ([8 x i8]* @.str63, i32 0, i32 0), i32 %392, i32 %394, i8* %412, i8* %423)
  br label %461

; <label>:425                                     ; preds = %384
  %426 = load %struct.ShellState** %p, align 8
  %427 = getelementptr inbounds %struct.ShellState* %426, i32 0, i32 8
  %428 = load %struct._IO_FILE** %427, align 8
  %429 = load i32* %w3, align 4
  %430 = load i32* %w3, align 4
  %431 = load i32* %i, align 4
  %432 = sext i32 %431 to i64
  %433 = load i8*** %3, align 8
  %434 = getelementptr inbounds i8** %433, i64 %432
  %435 = load i8** %434, align 8
  %436 = icmp ne i8* %435, null
  br i1 %436, label %437, label %443

; <label>:437                                     ; preds = %425
  %438 = load i32* %i, align 4
  %439 = sext i32 %438 to i64
  %440 = load i8*** %3, align 8
  %441 = getelementptr inbounds i8** %440, i64 %439
  %442 = load i8** %441, align 8
  br label %447

; <label>:443                                     ; preds = %425
  %444 = load %struct.ShellState** %p, align 8
  %445 = getelementptr inbounds %struct.ShellState* %444, i32 0, i32 20
  %446 = getelementptr inbounds [20 x i8]* %445, i32 0, i32 0
  br label %447

; <label>:447                                     ; preds = %443, %437
  %448 = phi i8* [ %442, %437 ], [ %446, %443 ]
  %449 = load i32* %i, align 4
  %450 = load i32* %2, align 4
  %451 = sub nsw i32 %450, 1
  %452 = icmp eq i32 %449, %451
  br i1 %452, label %453, label %457

; <label>:453                                     ; preds = %447
  %454 = load %struct.ShellState** %p, align 8
  %455 = getelementptr inbounds %struct.ShellState* %454, i32 0, i32 17
  %456 = getelementptr inbounds [20 x i8]* %455, i32 0, i32 0
  br label %458

; <label>:457                                     ; preds = %447
  br label %458

; <label>:458                                     ; preds = %457, %453
  %459 = phi i8* [ %456, %453 ], [ getelementptr inbounds ([3 x i8]* @.str64, i32 0, i32 0), %457 ]
  %460 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %428, i8* getelementptr inbounds ([9 x i8]* @.str65, i32 0, i32 0), i32 %429, i32 %430, i8* %448, i8* %459)
  br label %461

; <label>:461                                     ; preds = %458, %422
  br label %462

; <label>:462                                     ; preds = %461
  %463 = load i32* %i, align 4
  %464 = add nsw i32 %463, 1
  store i32 %464, i32* %i, align 4
  br label %301

; <label>:465                                     ; preds = %301
  br label %1217

; <label>:466                                     ; preds = %0, %0
  %467 = load %struct.ShellState** %p, align 8
  %468 = getelementptr inbounds %struct.ShellState* %467, i32 0, i32 7
  %469 = load i32* %468, align 4
  %470 = add nsw i32 %469, 1
  store i32 %470, i32* %468, align 4
  %471 = icmp eq i32 %469, 0
  br i1 %471, label %472, label %510

; <label>:472                                     ; preds = %466
  %473 = load %struct.ShellState** %p, align 8
  %474 = getelementptr inbounds %struct.ShellState* %473, i32 0, i32 13
  %475 = load i32* %474, align 4
  %476 = icmp ne i32 %475, 0
  br i1 %476, label %477, label %510

; <label>:477                                     ; preds = %472
  store i32 0, i32* %i, align 4
  br label %478

; <label>:478                                     ; preds = %506, %477
  %479 = load i32* %i, align 4
  %480 = load i32* %2, align 4
  %481 = icmp slt i32 %479, %480
  br i1 %481, label %482, label %509

; <label>:482                                     ; preds = %478
  %483 = load %struct.ShellState** %p, align 8
  %484 = getelementptr inbounds %struct.ShellState* %483, i32 0, i32 8
  %485 = load %struct._IO_FILE** %484, align 8
  %486 = load i32* %i, align 4
  %487 = sext i32 %486 to i64
  %488 = load i8*** %4, align 8
  %489 = getelementptr inbounds i8** %488, i64 %487
  %490 = load i8** %489, align 8
  %491 = load i32* %i, align 4
  %492 = load i32* %2, align 4
  %493 = sub nsw i32 %492, 1
  %494 = icmp eq i32 %491, %493
  br i1 %494, label %495, label %499

; <label>:495                                     ; preds = %482
  %496 = load %struct.ShellState** %p, align 8
  %497 = getelementptr inbounds %struct.ShellState* %496, i32 0, i32 17
  %498 = getelementptr inbounds [20 x i8]* %497, i32 0, i32 0
  br label %503

; <label>:499                                     ; preds = %482
  %500 = load %struct.ShellState** %p, align 8
  %501 = getelementptr inbounds %struct.ShellState* %500, i32 0, i32 16
  %502 = getelementptr inbounds [20 x i8]* %501, i32 0, i32 0
  br label %503

; <label>:503                                     ; preds = %499, %495
  %504 = phi i8* [ %498, %495 ], [ %502, %499 ]
  %505 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %485, i8* getelementptr inbounds ([5 x i8]* @.str68, i32 0, i32 0), i8* %490, i8* %504)
  br label %506

; <label>:506                                     ; preds = %503
  %507 = load i32* %i, align 4
  %508 = add nsw i32 %507, 1
  store i32 %508, i32* %i, align 4
  br label %478

; <label>:509                                     ; preds = %478
  br label %510

; <label>:510                                     ; preds = %509, %472, %466
  %511 = load i8*** %3, align 8
  %512 = icmp eq i8** %511, null
  br i1 %512, label %513, label %514

; <label>:513                                     ; preds = %510
  br label %1217

; <label>:514                                     ; preds = %510
  store i32 0, i32* %i, align 4
  br label %515

; <label>:515                                     ; preds = %572, %514
  %516 = load i32* %i, align 4
  %517 = load i32* %2, align 4
  %518 = icmp slt i32 %516, %517
  br i1 %518, label %519, label %575

; <label>:519                                     ; preds = %515
  %520 = load i32* %i, align 4
  %521 = sext i32 %520 to i64
  %522 = load i8*** %3, align 8
  %523 = getelementptr inbounds i8** %522, i64 %521
  %524 = load i8** %523, align 8
  store i8* %524, i8** %z, align 8
  %525 = load i8** %z, align 8
  %526 = icmp eq i8* %525, null
  br i1 %526, label %527, label %531

; <label>:527                                     ; preds = %519
  %528 = load %struct.ShellState** %p, align 8
  %529 = getelementptr inbounds %struct.ShellState* %528, i32 0, i32 20
  %530 = getelementptr inbounds [20 x i8]* %529, i32 0, i32 0
  store i8* %530, i8** %z, align 8
  br label %531

; <label>:531                                     ; preds = %527, %519
  %532 = load %struct.ShellState** %p, align 8
  %533 = getelementptr inbounds %struct.ShellState* %532, i32 0, i32 8
  %534 = load %struct._IO_FILE** %533, align 8
  %535 = load i8** %z, align 8
  %536 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %534, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %535)
  %537 = load i32* %i, align 4
  %538 = load i32* %2, align 4
  %539 = sub nsw i32 %538, 1
  %540 = icmp slt i32 %537, %539
  br i1 %540, label %541, label %549

; <label>:541                                     ; preds = %531
  %542 = load %struct.ShellState** %p, align 8
  %543 = getelementptr inbounds %struct.ShellState* %542, i32 0, i32 8
  %544 = load %struct._IO_FILE** %543, align 8
  %545 = load %struct.ShellState** %p, align 8
  %546 = getelementptr inbounds %struct.ShellState* %545, i32 0, i32 16
  %547 = getelementptr inbounds [20 x i8]* %546, i32 0, i32 0
  %548 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %544, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %547)
  br label %571

; <label>:549                                     ; preds = %531
  %550 = load %struct.ShellState** %p, align 8
  %551 = getelementptr inbounds %struct.ShellState* %550, i32 0, i32 11
  %552 = load i32* %551, align 4
  %553 = icmp eq i32 %552, 3
  br i1 %553, label %554, label %562

; <label>:554                                     ; preds = %549
  %555 = load %struct.ShellState** %p, align 8
  %556 = getelementptr inbounds %struct.ShellState* %555, i32 0, i32 8
  %557 = load %struct._IO_FILE** %556, align 8
  %558 = load %struct.ShellState** %p, align 8
  %559 = getelementptr inbounds %struct.ShellState* %558, i32 0, i32 17
  %560 = getelementptr inbounds [20 x i8]* %559, i32 0, i32 0
  %561 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %557, i8* getelementptr inbounds ([4 x i8]* @.str69, i32 0, i32 0), i8* %560)
  br label %570

; <label>:562                                     ; preds = %549
  %563 = load %struct.ShellState** %p, align 8
  %564 = getelementptr inbounds %struct.ShellState* %563, i32 0, i32 8
  %565 = load %struct._IO_FILE** %564, align 8
  %566 = load %struct.ShellState** %p, align 8
  %567 = getelementptr inbounds %struct.ShellState* %566, i32 0, i32 17
  %568 = getelementptr inbounds [20 x i8]* %567, i32 0, i32 0
  %569 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %565, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %568)
  br label %570

; <label>:570                                     ; preds = %562, %554
  br label %571

; <label>:571                                     ; preds = %570, %541
  br label %572

; <label>:572                                     ; preds = %571
  %573 = load i32* %i, align 4
  %574 = add nsw i32 %573, 1
  store i32 %574, i32* %i, align 4
  br label %515

; <label>:575                                     ; preds = %515
  br label %1217

; <label>:576                                     ; preds = %0
  %577 = load %struct.ShellState** %p, align 8
  %578 = getelementptr inbounds %struct.ShellState* %577, i32 0, i32 7
  %579 = load i32* %578, align 4
  %580 = add nsw i32 %579, 1
  store i32 %580, i32* %578, align 4
  %581 = icmp eq i32 %579, 0
  br i1 %581, label %582, label %621

; <label>:582                                     ; preds = %576
  %583 = load %struct.ShellState** %p, align 8
  %584 = getelementptr inbounds %struct.ShellState* %583, i32 0, i32 13
  %585 = load i32* %584, align 4
  %586 = icmp ne i32 %585, 0
  br i1 %586, label %587, label %621

; <label>:587                                     ; preds = %582
  %588 = load %struct.ShellState** %p, align 8
  %589 = getelementptr inbounds %struct.ShellState* %588, i32 0, i32 8
  %590 = load %struct._IO_FILE** %589, align 8
  %591 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %590, i8* getelementptr inbounds ([5 x i8]* @.str70, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %592

; <label>:592                                     ; preds = %613, %587
  %593 = load i32* %i, align 4
  %594 = load i32* %2, align 4
  %595 = icmp slt i32 %593, %594
  br i1 %595, label %596, label %616

; <label>:596                                     ; preds = %592
  %597 = load %struct.ShellState** %p, align 8
  %598 = getelementptr inbounds %struct.ShellState* %597, i32 0, i32 8
  %599 = load %struct._IO_FILE** %598, align 8
  %600 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %599, i8* getelementptr inbounds ([5 x i8]* @.str71, i32 0, i32 0))
  %601 = load %struct.ShellState** %p, align 8
  %602 = getelementptr inbounds %struct.ShellState* %601, i32 0, i32 8
  %603 = load %struct._IO_FILE** %602, align 8
  %604 = load i32* %i, align 4
  %605 = sext i32 %604 to i64
  %606 = load i8*** %4, align 8
  %607 = getelementptr inbounds i8** %606, i64 %605
  %608 = load i8** %607, align 8
  call void @output_html_string(%struct._IO_FILE* %603, i8* %608)
  %609 = load %struct.ShellState** %p, align 8
  %610 = getelementptr inbounds %struct.ShellState* %609, i32 0, i32 8
  %611 = load %struct._IO_FILE** %610, align 8
  %612 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %611, i8* getelementptr inbounds ([7 x i8]* @.str72, i32 0, i32 0))
  br label %613

; <label>:613                                     ; preds = %596
  %614 = load i32* %i, align 4
  %615 = add nsw i32 %614, 1
  store i32 %615, i32* %i, align 4
  br label %592

; <label>:616                                     ; preds = %592
  %617 = load %struct.ShellState** %p, align 8
  %618 = getelementptr inbounds %struct.ShellState* %617, i32 0, i32 8
  %619 = load %struct._IO_FILE** %618, align 8
  %620 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %619, i8* getelementptr inbounds ([7 x i8]* @.str73, i32 0, i32 0))
  br label %621

; <label>:621                                     ; preds = %616, %582, %576
  %622 = load i8*** %3, align 8
  %623 = icmp eq i8** %622, null
  br i1 %623, label %624, label %625

; <label>:624                                     ; preds = %621
  br label %1217

; <label>:625                                     ; preds = %621
  %626 = load %struct.ShellState** %p, align 8
  %627 = getelementptr inbounds %struct.ShellState* %626, i32 0, i32 8
  %628 = load %struct._IO_FILE** %627, align 8
  %629 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %628, i8* getelementptr inbounds ([5 x i8]* @.str70, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %630

; <label>:630                                     ; preds = %664, %625
  %631 = load i32* %i, align 4
  %632 = load i32* %2, align 4
  %633 = icmp slt i32 %631, %632
  br i1 %633, label %634, label %667

; <label>:634                                     ; preds = %630
  %635 = load %struct.ShellState** %p, align 8
  %636 = getelementptr inbounds %struct.ShellState* %635, i32 0, i32 8
  %637 = load %struct._IO_FILE** %636, align 8
  %638 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %637, i8* getelementptr inbounds ([5 x i8]* @.str74, i32 0, i32 0))
  %639 = load %struct.ShellState** %p, align 8
  %640 = getelementptr inbounds %struct.ShellState* %639, i32 0, i32 8
  %641 = load %struct._IO_FILE** %640, align 8
  %642 = load i32* %i, align 4
  %643 = sext i32 %642 to i64
  %644 = load i8*** %3, align 8
  %645 = getelementptr inbounds i8** %644, i64 %643
  %646 = load i8** %645, align 8
  %647 = icmp ne i8* %646, null
  br i1 %647, label %648, label %654

; <label>:648                                     ; preds = %634
  %649 = load i32* %i, align 4
  %650 = sext i32 %649 to i64
  %651 = load i8*** %3, align 8
  %652 = getelementptr inbounds i8** %651, i64 %650
  %653 = load i8** %652, align 8
  br label %658

; <label>:654                                     ; preds = %634
  %655 = load %struct.ShellState** %p, align 8
  %656 = getelementptr inbounds %struct.ShellState* %655, i32 0, i32 20
  %657 = getelementptr inbounds [20 x i8]* %656, i32 0, i32 0
  br label %658

; <label>:658                                     ; preds = %654, %648
  %659 = phi i8* [ %653, %648 ], [ %657, %654 ]
  call void @output_html_string(%struct._IO_FILE* %641, i8* %659)
  %660 = load %struct.ShellState** %p, align 8
  %661 = getelementptr inbounds %struct.ShellState* %660, i32 0, i32 8
  %662 = load %struct._IO_FILE** %661, align 8
  %663 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %662, i8* getelementptr inbounds ([7 x i8]* @.str75, i32 0, i32 0))
  br label %664

; <label>:664                                     ; preds = %658
  %665 = load i32* %i, align 4
  %666 = add nsw i32 %665, 1
  store i32 %666, i32* %i, align 4
  br label %630

; <label>:667                                     ; preds = %630
  %668 = load %struct.ShellState** %p, align 8
  %669 = getelementptr inbounds %struct.ShellState* %668, i32 0, i32 8
  %670 = load %struct._IO_FILE** %669, align 8
  %671 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %670, i8* getelementptr inbounds ([7 x i8]* @.str73, i32 0, i32 0))
  br label %1217

; <label>:672                                     ; preds = %0
  %673 = load %struct.ShellState** %p, align 8
  %674 = getelementptr inbounds %struct.ShellState* %673, i32 0, i32 7
  %675 = load i32* %674, align 4
  %676 = add nsw i32 %675, 1
  store i32 %676, i32* %674, align 4
  %677 = icmp eq i32 %675, 0
  br i1 %677, label %678, label %731

; <label>:678                                     ; preds = %672
  %679 = load %struct.ShellState** %p, align 8
  %680 = getelementptr inbounds %struct.ShellState* %679, i32 0, i32 13
  %681 = load i32* %680, align 4
  %682 = icmp ne i32 %681, 0
  br i1 %682, label %683, label %731

; <label>:683                                     ; preds = %678
  store i32 0, i32* %i, align 4
  br label %684

; <label>:684                                     ; preds = %720, %683
  %685 = load i32* %i, align 4
  %686 = load i32* %2, align 4
  %687 = icmp slt i32 %685, %686
  br i1 %687, label %688, label %723

; <label>:688                                     ; preds = %684
  %689 = load %struct.ShellState** %p, align 8
  %690 = getelementptr inbounds %struct.ShellState* %689, i32 0, i32 8
  %691 = load %struct._IO_FILE** %690, align 8
  %692 = load i32* %i, align 4
  %693 = sext i32 %692 to i64
  %694 = load i8*** %4, align 8
  %695 = getelementptr inbounds i8** %694, i64 %693
  %696 = load i8** %695, align 8
  %697 = icmp ne i8* %696, null
  br i1 %697, label %698, label %704

; <label>:698                                     ; preds = %688
  %699 = load i32* %i, align 4
  %700 = sext i32 %699 to i64
  %701 = load i8*** %4, align 8
  %702 = getelementptr inbounds i8** %701, i64 %700
  %703 = load i8** %702, align 8
  br label %705

; <label>:704                                     ; preds = %688
  br label %705

; <label>:705                                     ; preds = %704, %698
  %706 = phi i8* [ %703, %698 ], [ getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), %704 ]
  call void @output_c_string(%struct._IO_FILE* %691, i8* %706)
  %707 = load i32* %i, align 4
  %708 = load i32* %2, align 4
  %709 = sub nsw i32 %708, 1
  %710 = icmp slt i32 %707, %709
  br i1 %710, label %711, label %719

; <label>:711                                     ; preds = %705
  %712 = load %struct.ShellState** %p, align 8
  %713 = getelementptr inbounds %struct.ShellState* %712, i32 0, i32 8
  %714 = load %struct._IO_FILE** %713, align 8
  %715 = load %struct.ShellState** %p, align 8
  %716 = getelementptr inbounds %struct.ShellState* %715, i32 0, i32 16
  %717 = getelementptr inbounds [20 x i8]* %716, i32 0, i32 0
  %718 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %714, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %717)
  br label %719

; <label>:719                                     ; preds = %711, %705
  br label %720

; <label>:720                                     ; preds = %719
  %721 = load i32* %i, align 4
  %722 = add nsw i32 %721, 1
  store i32 %722, i32* %i, align 4
  br label %684

; <label>:723                                     ; preds = %684
  %724 = load %struct.ShellState** %p, align 8
  %725 = getelementptr inbounds %struct.ShellState* %724, i32 0, i32 8
  %726 = load %struct._IO_FILE** %725, align 8
  %727 = load %struct.ShellState** %p, align 8
  %728 = getelementptr inbounds %struct.ShellState* %727, i32 0, i32 17
  %729 = getelementptr inbounds [20 x i8]* %728, i32 0, i32 0
  %730 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %726, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %729)
  br label %731

; <label>:731                                     ; preds = %723, %678, %672
  %732 = load i8*** %3, align 8
  %733 = icmp eq i8** %732, null
  br i1 %733, label %734, label %735

; <label>:734                                     ; preds = %731
  br label %1217

; <label>:735                                     ; preds = %731
  store i32 0, i32* %i, align 4
  br label %736

; <label>:736                                     ; preds = %775, %735
  %737 = load i32* %i, align 4
  %738 = load i32* %2, align 4
  %739 = icmp slt i32 %737, %738
  br i1 %739, label %740, label %778

; <label>:740                                     ; preds = %736
  %741 = load %struct.ShellState** %p, align 8
  %742 = getelementptr inbounds %struct.ShellState* %741, i32 0, i32 8
  %743 = load %struct._IO_FILE** %742, align 8
  %744 = load i32* %i, align 4
  %745 = sext i32 %744 to i64
  %746 = load i8*** %3, align 8
  %747 = getelementptr inbounds i8** %746, i64 %745
  %748 = load i8** %747, align 8
  %749 = icmp ne i8* %748, null
  br i1 %749, label %750, label %756

; <label>:750                                     ; preds = %740
  %751 = load i32* %i, align 4
  %752 = sext i32 %751 to i64
  %753 = load i8*** %3, align 8
  %754 = getelementptr inbounds i8** %753, i64 %752
  %755 = load i8** %754, align 8
  br label %760

; <label>:756                                     ; preds = %740
  %757 = load %struct.ShellState** %p, align 8
  %758 = getelementptr inbounds %struct.ShellState* %757, i32 0, i32 20
  %759 = getelementptr inbounds [20 x i8]* %758, i32 0, i32 0
  br label %760

; <label>:760                                     ; preds = %756, %750
  %761 = phi i8* [ %755, %750 ], [ %759, %756 ]
  call void @output_c_string(%struct._IO_FILE* %743, i8* %761)
  %762 = load i32* %i, align 4
  %763 = load i32* %2, align 4
  %764 = sub nsw i32 %763, 1
  %765 = icmp slt i32 %762, %764
  br i1 %765, label %766, label %774

; <label>:766                                     ; preds = %760
  %767 = load %struct.ShellState** %p, align 8
  %768 = getelementptr inbounds %struct.ShellState* %767, i32 0, i32 8
  %769 = load %struct._IO_FILE** %768, align 8
  %770 = load %struct.ShellState** %p, align 8
  %771 = getelementptr inbounds %struct.ShellState* %770, i32 0, i32 16
  %772 = getelementptr inbounds [20 x i8]* %771, i32 0, i32 0
  %773 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %769, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %772)
  br label %774

; <label>:774                                     ; preds = %766, %760
  br label %775

; <label>:775                                     ; preds = %774
  %776 = load i32* %i, align 4
  %777 = add nsw i32 %776, 1
  store i32 %777, i32* %i, align 4
  br label %736

; <label>:778                                     ; preds = %736
  %779 = load %struct.ShellState** %p, align 8
  %780 = getelementptr inbounds %struct.ShellState* %779, i32 0, i32 8
  %781 = load %struct._IO_FILE** %780, align 8
  %782 = load %struct.ShellState** %p, align 8
  %783 = getelementptr inbounds %struct.ShellState* %782, i32 0, i32 17
  %784 = getelementptr inbounds [20 x i8]* %783, i32 0, i32 0
  %785 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %781, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %784)
  br label %1217

; <label>:786                                     ; preds = %0
  %787 = load %struct.ShellState** %p, align 8
  %788 = getelementptr inbounds %struct.ShellState* %787, i32 0, i32 7
  %789 = load i32* %788, align 4
  %790 = add nsw i32 %789, 1
  store i32 %790, i32* %788, align 4
  %791 = icmp eq i32 %789, 0
  br i1 %791, label %792, label %835

; <label>:792                                     ; preds = %786
  %793 = load %struct.ShellState** %p, align 8
  %794 = getelementptr inbounds %struct.ShellState* %793, i32 0, i32 13
  %795 = load i32* %794, align 4
  %796 = icmp ne i32 %795, 0
  br i1 %796, label %797, label %835

; <label>:797                                     ; preds = %792
  store i32 0, i32* %i, align 4
  br label %798

; <label>:798                                     ; preds = %824, %797
  %799 = load i32* %i, align 4
  %800 = load i32* %2, align 4
  %801 = icmp slt i32 %799, %800
  br i1 %801, label %802, label %827

; <label>:802                                     ; preds = %798
  %803 = load %struct.ShellState** %p, align 8
  %804 = load i32* %i, align 4
  %805 = sext i32 %804 to i64
  %806 = load i8*** %4, align 8
  %807 = getelementptr inbounds i8** %806, i64 %805
  %808 = load i8** %807, align 8
  %809 = icmp ne i8* %808, null
  br i1 %809, label %810, label %816

; <label>:810                                     ; preds = %802
  %811 = load i32* %i, align 4
  %812 = sext i32 %811 to i64
  %813 = load i8*** %4, align 8
  %814 = getelementptr inbounds i8** %813, i64 %812
  %815 = load i8** %814, align 8
  br label %817

; <label>:816                                     ; preds = %802
  br label %817

; <label>:817                                     ; preds = %816, %810
  %818 = phi i8* [ %815, %810 ], [ getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), %816 ]
  %819 = load i32* %i, align 4
  %820 = load i32* %2, align 4
  %821 = sub nsw i32 %820, 1
  %822 = icmp slt i32 %819, %821
  %823 = zext i1 %822 to i32
  call void @output_csv(%struct.ShellState* %803, i8* %818, i32 %823)
  br label %824

; <label>:824                                     ; preds = %817
  %825 = load i32* %i, align 4
  %826 = add nsw i32 %825, 1
  store i32 %826, i32* %i, align 4
  br label %798

; <label>:827                                     ; preds = %798
  %828 = load %struct.ShellState** %p, align 8
  %829 = getelementptr inbounds %struct.ShellState* %828, i32 0, i32 8
  %830 = load %struct._IO_FILE** %829, align 8
  %831 = load %struct.ShellState** %p, align 8
  %832 = getelementptr inbounds %struct.ShellState* %831, i32 0, i32 17
  %833 = getelementptr inbounds [20 x i8]* %832, i32 0, i32 0
  %834 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %830, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %833)
  br label %835

; <label>:835                                     ; preds = %827, %792, %786
  %836 = load i32* %2, align 4
  %837 = icmp sgt i32 %836, 0
  br i1 %837, label %838, label %866

; <label>:838                                     ; preds = %835
  store i32 0, i32* %i, align 4
  br label %839

; <label>:839                                     ; preds = %855, %838
  %840 = load i32* %i, align 4
  %841 = load i32* %2, align 4
  %842 = icmp slt i32 %840, %841
  br i1 %842, label %843, label %858

; <label>:843                                     ; preds = %839
  %844 = load %struct.ShellState** %p, align 8
  %845 = load i32* %i, align 4
  %846 = sext i32 %845 to i64
  %847 = load i8*** %3, align 8
  %848 = getelementptr inbounds i8** %847, i64 %846
  %849 = load i8** %848, align 8
  %850 = load i32* %i, align 4
  %851 = load i32* %2, align 4
  %852 = sub nsw i32 %851, 1
  %853 = icmp slt i32 %850, %852
  %854 = zext i1 %853 to i32
  call void @output_csv(%struct.ShellState* %844, i8* %849, i32 %854)
  br label %855

; <label>:855                                     ; preds = %843
  %856 = load i32* %i, align 4
  %857 = add nsw i32 %856, 1
  store i32 %857, i32* %i, align 4
  br label %839

; <label>:858                                     ; preds = %839
  %859 = load %struct.ShellState** %p, align 8
  %860 = getelementptr inbounds %struct.ShellState* %859, i32 0, i32 8
  %861 = load %struct._IO_FILE** %860, align 8
  %862 = load %struct.ShellState** %p, align 8
  %863 = getelementptr inbounds %struct.ShellState* %862, i32 0, i32 17
  %864 = getelementptr inbounds [20 x i8]* %863, i32 0, i32 0
  %865 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %861, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %864)
  br label %866

; <label>:866                                     ; preds = %858, %835
  br label %1217

; <label>:867                                     ; preds = %0
  %868 = load %struct.ShellState** %p, align 8
  %869 = getelementptr inbounds %struct.ShellState* %868, i32 0, i32 7
  %870 = load i32* %869, align 4
  %871 = add nsw i32 %870, 1
  store i32 %871, i32* %869, align 4
  %872 = load i8*** %3, align 8
  %873 = icmp eq i8** %872, null
  br i1 %873, label %874, label %875

; <label>:874                                     ; preds = %867
  br label %1217

; <label>:875                                     ; preds = %867
  %876 = load %struct.ShellState** %p, align 8
  %877 = getelementptr inbounds %struct.ShellState* %876, i32 0, i32 8
  %878 = load %struct._IO_FILE** %877, align 8
  %879 = load %struct.ShellState** %p, align 8
  %880 = getelementptr inbounds %struct.ShellState* %879, i32 0, i32 15
  %881 = load i8** %880, align 8
  %882 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %878, i8* getelementptr inbounds ([15 x i8]* @.str76, i32 0, i32 0), i8* %881)
  %883 = load %struct.ShellState** %p, align 8
  %884 = getelementptr inbounds %struct.ShellState* %883, i32 0, i32 13
  %885 = load i32* %884, align 4
  %886 = icmp ne i32 %885, 0
  br i1 %886, label %887, label %918

; <label>:887                                     ; preds = %875
  %888 = load %struct.ShellState** %p, align 8
  %889 = getelementptr inbounds %struct.ShellState* %888, i32 0, i32 8
  %890 = load %struct._IO_FILE** %889, align 8
  %891 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %890, i8* getelementptr inbounds ([2 x i8]* @.str77, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %892

; <label>:892                                     ; preds = %910, %887
  %893 = load i32* %i, align 4
  %894 = load i32* %2, align 4
  %895 = icmp slt i32 %893, %894
  br i1 %895, label %896, label %913

; <label>:896                                     ; preds = %892
  %897 = load i32* %i, align 4
  %898 = icmp sgt i32 %897, 0
  %899 = select i1 %898, i8* getelementptr inbounds ([2 x i8]* @.str22, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0)
  store i8* %899, i8** %zSep, align 8
  %900 = load %struct.ShellState** %p, align 8
  %901 = getelementptr inbounds %struct.ShellState* %900, i32 0, i32 8
  %902 = load %struct._IO_FILE** %901, align 8
  %903 = load i8** %zSep, align 8
  %904 = load i32* %i, align 4
  %905 = sext i32 %904 to i64
  %906 = load i8*** %4, align 8
  %907 = getelementptr inbounds i8** %906, i64 %905
  %908 = load i8** %907, align 8
  %909 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %902, i8* getelementptr inbounds ([5 x i8]* @.str68, i32 0, i32 0), i8* %903, i8* %908)
  br label %910

; <label>:910                                     ; preds = %896
  %911 = load i32* %i, align 4
  %912 = add nsw i32 %911, 1
  store i32 %912, i32* %i, align 4
  br label %892

; <label>:913                                     ; preds = %892
  %914 = load %struct.ShellState** %p, align 8
  %915 = getelementptr inbounds %struct.ShellState* %914, i32 0, i32 8
  %916 = load %struct._IO_FILE** %915, align 8
  %917 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %916, i8* getelementptr inbounds ([2 x i8]* @.str78, i32 0, i32 0))
  br label %918

; <label>:918                                     ; preds = %913, %875
  %919 = load %struct.ShellState** %p, align 8
  %920 = getelementptr inbounds %struct.ShellState* %919, i32 0, i32 8
  %921 = load %struct._IO_FILE** %920, align 8
  %922 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %921, i8* getelementptr inbounds ([9 x i8]* @.str79, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %923

; <label>:923                                     ; preds = %1097, %918
  %924 = load i32* %i, align 4
  %925 = load i32* %2, align 4
  %926 = icmp slt i32 %924, %925
  br i1 %926, label %927, label %1100

; <label>:927                                     ; preds = %923
  %928 = load i32* %i, align 4
  %929 = icmp sgt i32 %928, 0
  %930 = select i1 %929, i8* getelementptr inbounds ([2 x i8]* @.str22, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0)
  store i8* %930, i8** %zSep4, align 8
  %931 = load i32* %i, align 4
  %932 = sext i32 %931 to i64
  %933 = load i8*** %3, align 8
  %934 = getelementptr inbounds i8** %933, i64 %932
  %935 = load i8** %934, align 8
  %936 = icmp eq i8* %935, null
  br i1 %936, label %947, label %937

; <label>:937                                     ; preds = %927
  %938 = load i32** %5, align 8
  %939 = icmp ne i32* %938, null
  br i1 %939, label %940, label %953

; <label>:940                                     ; preds = %937
  %941 = load i32* %i, align 4
  %942 = sext i32 %941 to i64
  %943 = load i32** %5, align 8
  %944 = getelementptr inbounds i32* %943, i64 %942
  %945 = load i32* %944, align 4
  %946 = icmp eq i32 %945, 5
  br i1 %946, label %947, label %953

; <label>:947                                     ; preds = %940, %927
  %948 = load %struct.ShellState** %p, align 8
  %949 = getelementptr inbounds %struct.ShellState* %948, i32 0, i32 8
  %950 = load %struct._IO_FILE** %949, align 8
  %951 = load i8** %zSep4, align 8
  %952 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %950, i8* getelementptr inbounds ([7 x i8]* @.str80, i32 0, i32 0), i8* %951)
  br label %1096

; <label>:953                                     ; preds = %940, %937
  %954 = load i32** %5, align 8
  %955 = icmp ne i32* %954, null
  br i1 %955, label %956, label %983

; <label>:956                                     ; preds = %953
  %957 = load i32* %i, align 4
  %958 = sext i32 %957 to i64
  %959 = load i32** %5, align 8
  %960 = getelementptr inbounds i32* %959, i64 %958
  %961 = load i32* %960, align 4
  %962 = icmp eq i32 %961, 3
  br i1 %962, label %963, label %983

; <label>:963                                     ; preds = %956
  %964 = load i8** %zSep4, align 8
  %965 = getelementptr inbounds i8* %964, i64 0
  %966 = load i8* %965, align 1
  %967 = icmp ne i8 %966, 0
  br i1 %967, label %968, label %974

; <label>:968                                     ; preds = %963
  %969 = load %struct.ShellState** %p, align 8
  %970 = getelementptr inbounds %struct.ShellState* %969, i32 0, i32 8
  %971 = load %struct._IO_FILE** %970, align 8
  %972 = load i8** %zSep4, align 8
  %973 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %971, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %972)
  br label %974

; <label>:974                                     ; preds = %968, %963
  %975 = load %struct.ShellState** %p, align 8
  %976 = getelementptr inbounds %struct.ShellState* %975, i32 0, i32 8
  %977 = load %struct._IO_FILE** %976, align 8
  %978 = load i32* %i, align 4
  %979 = sext i32 %978 to i64
  %980 = load i8*** %3, align 8
  %981 = getelementptr inbounds i8** %980, i64 %979
  %982 = load i8** %981, align 8
  call void @output_quoted_string(%struct._IO_FILE* %977, i8* %982)
  br label %1095

; <label>:983                                     ; preds = %956, %953
  %984 = load i32** %5, align 8
  %985 = icmp ne i32* %984, null
  br i1 %985, label %986, label %1011

; <label>:986                                     ; preds = %983
  %987 = load i32* %i, align 4
  %988 = sext i32 %987 to i64
  %989 = load i32** %5, align 8
  %990 = getelementptr inbounds i32* %989, i64 %988
  %991 = load i32* %990, align 4
  %992 = icmp eq i32 %991, 1
  br i1 %992, label %1000, label %993

; <label>:993                                     ; preds = %986
  %994 = load i32* %i, align 4
  %995 = sext i32 %994 to i64
  %996 = load i32** %5, align 8
  %997 = getelementptr inbounds i32* %996, i64 %995
  %998 = load i32* %997, align 4
  %999 = icmp eq i32 %998, 2
  br i1 %999, label %1000, label %1011

; <label>:1000                                    ; preds = %993, %986
  %1001 = load %struct.ShellState** %p, align 8
  %1002 = getelementptr inbounds %struct.ShellState* %1001, i32 0, i32 8
  %1003 = load %struct._IO_FILE** %1002, align 8
  %1004 = load i8** %zSep4, align 8
  %1005 = load i32* %i, align 4
  %1006 = sext i32 %1005 to i64
  %1007 = load i8*** %3, align 8
  %1008 = getelementptr inbounds i8** %1007, i64 %1006
  %1009 = load i8** %1008, align 8
  %1010 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1003, i8* getelementptr inbounds ([5 x i8]* @.str68, i32 0, i32 0), i8* %1004, i8* %1009)
  br label %1094

; <label>:1011                                    ; preds = %993, %983
  %1012 = load i32** %5, align 8
  %1013 = icmp ne i32* %1012, null
  br i1 %1013, label %1014, label %1053

; <label>:1014                                    ; preds = %1011
  %1015 = load i32* %i, align 4
  %1016 = sext i32 %1015 to i64
  %1017 = load i32** %5, align 8
  %1018 = getelementptr inbounds i32* %1017, i64 %1016
  %1019 = load i32* %1018, align 4
  %1020 = icmp eq i32 %1019, 4
  br i1 %1020, label %1021, label %1053

; <label>:1021                                    ; preds = %1014
  %1022 = load %struct.ShellState** %p, align 8
  %1023 = getelementptr inbounds %struct.ShellState* %1022, i32 0, i32 26
  %1024 = load %struct.sqlite3_stmt** %1023, align 8
  %1025 = icmp ne %struct.sqlite3_stmt* %1024, null
  br i1 %1025, label %1026, label %1053

; <label>:1026                                    ; preds = %1021
  %1027 = load %struct.ShellState** %p, align 8
  %1028 = getelementptr inbounds %struct.ShellState* %1027, i32 0, i32 26
  %1029 = load %struct.sqlite3_stmt** %1028, align 8
  %1030 = load i32* %i, align 4
  %1031 = call i8* @sqlite3_column_blob(%struct.sqlite3_stmt* %1029, i32 %1030)
  store i8* %1031, i8** %pBlob, align 8
  %1032 = load %struct.ShellState** %p, align 8
  %1033 = getelementptr inbounds %struct.ShellState* %1032, i32 0, i32 26
  %1034 = load %struct.sqlite3_stmt** %1033, align 8
  %1035 = load i32* %i, align 4
  %1036 = call i32 @sqlite3_column_bytes(%struct.sqlite3_stmt* %1034, i32 %1035)
  store i32 %1036, i32* %nBlob, align 4
  %1037 = load i8** %zSep4, align 8
  %1038 = getelementptr inbounds i8* %1037, i64 0
  %1039 = load i8* %1038, align 1
  %1040 = icmp ne i8 %1039, 0
  br i1 %1040, label %1041, label %1047

; <label>:1041                                    ; preds = %1026
  %1042 = load %struct.ShellState** %p, align 8
  %1043 = getelementptr inbounds %struct.ShellState* %1042, i32 0, i32 8
  %1044 = load %struct._IO_FILE** %1043, align 8
  %1045 = load i8** %zSep4, align 8
  %1046 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1044, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1045)
  br label %1047

; <label>:1047                                    ; preds = %1041, %1026
  %1048 = load %struct.ShellState** %p, align 8
  %1049 = getelementptr inbounds %struct.ShellState* %1048, i32 0, i32 8
  %1050 = load %struct._IO_FILE** %1049, align 8
  %1051 = load i8** %pBlob, align 8
  %1052 = load i32* %nBlob, align 4
  call void @output_hex_blob(%struct._IO_FILE* %1050, i8* %1051, i32 %1052)
  br label %1093

; <label>:1053                                    ; preds = %1021, %1014, %1011
  %1054 = load i32* %i, align 4
  %1055 = sext i32 %1054 to i64
  %1056 = load i8*** %3, align 8
  %1057 = getelementptr inbounds i8** %1056, i64 %1055
  %1058 = load i8** %1057, align 8
  %1059 = call i32 @isNumber(i8* %1058, i32* null)
  %1060 = icmp ne i32 %1059, 0
  br i1 %1060, label %1061, label %1072

; <label>:1061                                    ; preds = %1053
  %1062 = load %struct.ShellState** %p, align 8
  %1063 = getelementptr inbounds %struct.ShellState* %1062, i32 0, i32 8
  %1064 = load %struct._IO_FILE** %1063, align 8
  %1065 = load i8** %zSep4, align 8
  %1066 = load i32* %i, align 4
  %1067 = sext i32 %1066 to i64
  %1068 = load i8*** %3, align 8
  %1069 = getelementptr inbounds i8** %1068, i64 %1067
  %1070 = load i8** %1069, align 8
  %1071 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1064, i8* getelementptr inbounds ([5 x i8]* @.str68, i32 0, i32 0), i8* %1065, i8* %1070)
  br label %1092

; <label>:1072                                    ; preds = %1053
  %1073 = load i8** %zSep4, align 8
  %1074 = getelementptr inbounds i8* %1073, i64 0
  %1075 = load i8* %1074, align 1
  %1076 = icmp ne i8 %1075, 0
  br i1 %1076, label %1077, label %1083

; <label>:1077                                    ; preds = %1072
  %1078 = load %struct.ShellState** %p, align 8
  %1079 = getelementptr inbounds %struct.ShellState* %1078, i32 0, i32 8
  %1080 = load %struct._IO_FILE** %1079, align 8
  %1081 = load i8** %zSep4, align 8
  %1082 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1080, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1081)
  br label %1083

; <label>:1083                                    ; preds = %1077, %1072
  %1084 = load %struct.ShellState** %p, align 8
  %1085 = getelementptr inbounds %struct.ShellState* %1084, i32 0, i32 8
  %1086 = load %struct._IO_FILE** %1085, align 8
  %1087 = load i32* %i, align 4
  %1088 = sext i32 %1087 to i64
  %1089 = load i8*** %3, align 8
  %1090 = getelementptr inbounds i8** %1089, i64 %1088
  %1091 = load i8** %1090, align 8
  call void @output_quoted_string(%struct._IO_FILE* %1086, i8* %1091)
  br label %1092

; <label>:1092                                    ; preds = %1083, %1061
  br label %1093

; <label>:1093                                    ; preds = %1092, %1047
  br label %1094

; <label>:1094                                    ; preds = %1093, %1000
  br label %1095

; <label>:1095                                    ; preds = %1094, %974
  br label %1096

; <label>:1096                                    ; preds = %1095, %947
  br label %1097

; <label>:1097                                    ; preds = %1096
  %1098 = load i32* %i, align 4
  %1099 = add nsw i32 %1098, 1
  store i32 %1099, i32* %i, align 4
  br label %923

; <label>:1100                                    ; preds = %923
  %1101 = load %struct.ShellState** %p, align 8
  %1102 = getelementptr inbounds %struct.ShellState* %1101, i32 0, i32 8
  %1103 = load %struct._IO_FILE** %1102, align 8
  %1104 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1103, i8* getelementptr inbounds ([4 x i8]* @.str81, i32 0, i32 0))
  br label %1217

; <label>:1105                                    ; preds = %0
  %1106 = load %struct.ShellState** %p, align 8
  %1107 = getelementptr inbounds %struct.ShellState* %1106, i32 0, i32 7
  %1108 = load i32* %1107, align 4
  %1109 = add nsw i32 %1108, 1
  store i32 %1109, i32* %1107, align 4
  %1110 = icmp eq i32 %1108, 0
  br i1 %1110, label %1111, label %1163

; <label>:1111                                    ; preds = %1105
  %1112 = load %struct.ShellState** %p, align 8
  %1113 = getelementptr inbounds %struct.ShellState* %1112, i32 0, i32 13
  %1114 = load i32* %1113, align 4
  %1115 = icmp ne i32 %1114, 0
  br i1 %1115, label %1116, label %1163

; <label>:1116                                    ; preds = %1111
  store i32 0, i32* %i, align 4
  br label %1117

; <label>:1117                                    ; preds = %1152, %1116
  %1118 = load i32* %i, align 4
  %1119 = load i32* %2, align 4
  %1120 = icmp slt i32 %1118, %1119
  br i1 %1120, label %1121, label %1155

; <label>:1121                                    ; preds = %1117
  %1122 = load i32* %i, align 4
  %1123 = icmp sgt i32 %1122, 0
  br i1 %1123, label %1124, label %1132

; <label>:1124                                    ; preds = %1121
  %1125 = load %struct.ShellState** %p, align 8
  %1126 = getelementptr inbounds %struct.ShellState* %1125, i32 0, i32 8
  %1127 = load %struct._IO_FILE** %1126, align 8
  %1128 = load %struct.ShellState** %p, align 8
  %1129 = getelementptr inbounds %struct.ShellState* %1128, i32 0, i32 16
  %1130 = getelementptr inbounds [20 x i8]* %1129, i32 0, i32 0
  %1131 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1127, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1130)
  br label %1132

; <label>:1132                                    ; preds = %1124, %1121
  %1133 = load %struct.ShellState** %p, align 8
  %1134 = getelementptr inbounds %struct.ShellState* %1133, i32 0, i32 8
  %1135 = load %struct._IO_FILE** %1134, align 8
  %1136 = load i32* %i, align 4
  %1137 = sext i32 %1136 to i64
  %1138 = load i8*** %4, align 8
  %1139 = getelementptr inbounds i8** %1138, i64 %1137
  %1140 = load i8** %1139, align 8
  %1141 = icmp ne i8* %1140, null
  br i1 %1141, label %1142, label %1148

; <label>:1142                                    ; preds = %1132
  %1143 = load i32* %i, align 4
  %1144 = sext i32 %1143 to i64
  %1145 = load i8*** %4, align 8
  %1146 = getelementptr inbounds i8** %1145, i64 %1144
  %1147 = load i8** %1146, align 8
  br label %1149

; <label>:1148                                    ; preds = %1132
  br label %1149

; <label>:1149                                    ; preds = %1148, %1142
  %1150 = phi i8* [ %1147, %1142 ], [ getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), %1148 ]
  %1151 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1135, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1150)
  br label %1152

; <label>:1152                                    ; preds = %1149
  %1153 = load i32* %i, align 4
  %1154 = add nsw i32 %1153, 1
  store i32 %1154, i32* %i, align 4
  br label %1117

; <label>:1155                                    ; preds = %1117
  %1156 = load %struct.ShellState** %p, align 8
  %1157 = getelementptr inbounds %struct.ShellState* %1156, i32 0, i32 8
  %1158 = load %struct._IO_FILE** %1157, align 8
  %1159 = load %struct.ShellState** %p, align 8
  %1160 = getelementptr inbounds %struct.ShellState* %1159, i32 0, i32 17
  %1161 = getelementptr inbounds [20 x i8]* %1160, i32 0, i32 0
  %1162 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1158, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1161)
  br label %1163

; <label>:1163                                    ; preds = %1155, %1111, %1105
  %1164 = load i8*** %3, align 8
  %1165 = icmp eq i8** %1164, null
  br i1 %1165, label %1166, label %1167

; <label>:1166                                    ; preds = %1163
  br label %1217

; <label>:1167                                    ; preds = %1163
  store i32 0, i32* %i, align 4
  br label %1168

; <label>:1168                                    ; preds = %1206, %1167
  %1169 = load i32* %i, align 4
  %1170 = load i32* %2, align 4
  %1171 = icmp slt i32 %1169, %1170
  br i1 %1171, label %1172, label %1209

; <label>:1172                                    ; preds = %1168
  %1173 = load i32* %i, align 4
  %1174 = icmp sgt i32 %1173, 0
  br i1 %1174, label %1175, label %1183

; <label>:1175                                    ; preds = %1172
  %1176 = load %struct.ShellState** %p, align 8
  %1177 = getelementptr inbounds %struct.ShellState* %1176, i32 0, i32 8
  %1178 = load %struct._IO_FILE** %1177, align 8
  %1179 = load %struct.ShellState** %p, align 8
  %1180 = getelementptr inbounds %struct.ShellState* %1179, i32 0, i32 16
  %1181 = getelementptr inbounds [20 x i8]* %1180, i32 0, i32 0
  %1182 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1178, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1181)
  br label %1183

; <label>:1183                                    ; preds = %1175, %1172
  %1184 = load %struct.ShellState** %p, align 8
  %1185 = getelementptr inbounds %struct.ShellState* %1184, i32 0, i32 8
  %1186 = load %struct._IO_FILE** %1185, align 8
  %1187 = load i32* %i, align 4
  %1188 = sext i32 %1187 to i64
  %1189 = load i8*** %3, align 8
  %1190 = getelementptr inbounds i8** %1189, i64 %1188
  %1191 = load i8** %1190, align 8
  %1192 = icmp ne i8* %1191, null
  br i1 %1192, label %1193, label %1199

; <label>:1193                                    ; preds = %1183
  %1194 = load i32* %i, align 4
  %1195 = sext i32 %1194 to i64
  %1196 = load i8*** %3, align 8
  %1197 = getelementptr inbounds i8** %1196, i64 %1195
  %1198 = load i8** %1197, align 8
  br label %1203

; <label>:1199                                    ; preds = %1183
  %1200 = load %struct.ShellState** %p, align 8
  %1201 = getelementptr inbounds %struct.ShellState* %1200, i32 0, i32 20
  %1202 = getelementptr inbounds [20 x i8]* %1201, i32 0, i32 0
  br label %1203

; <label>:1203                                    ; preds = %1199, %1193
  %1204 = phi i8* [ %1198, %1193 ], [ %1202, %1199 ]
  %1205 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1186, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1204)
  br label %1206

; <label>:1206                                    ; preds = %1203
  %1207 = load i32* %i, align 4
  %1208 = add nsw i32 %1207, 1
  store i32 %1208, i32* %i, align 4
  br label %1168

; <label>:1209                                    ; preds = %1168
  %1210 = load %struct.ShellState** %p, align 8
  %1211 = getelementptr inbounds %struct.ShellState* %1210, i32 0, i32 8
  %1212 = load %struct._IO_FILE** %1211, align 8
  %1213 = load %struct.ShellState** %p, align 8
  %1214 = getelementptr inbounds %struct.ShellState* %1213, i32 0, i32 17
  %1215 = getelementptr inbounds [20 x i8]* %1214, i32 0, i32 0
  %1216 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %1212, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %1215)
  br label %1217

; <label>:1217                                    ; preds = %0, %1209, %1166, %1100, %874, %866, %778, %734, %667, %624, %575, %513, %465, %299, %100, %14
  ret i32 0
}

; Function Attrs: nounwind
declare void @free(i8*) #4

; Function Attrs: nounwind uwtable
define internal void @printBold(i8* %zText) #0 {
  %1 = alloca i8*, align 8
  store i8* %zText, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([11 x i8]* @.str60, i32 0, i32 0), i8* %2)
  ret void
}

; Function Attrs: nounwind uwtable
define internal i8* @find_home_dir() #0 {
  %1 = alloca i8*, align 8
  %pwent = alloca %struct.passwd*, align 8
  %uid = alloca i32, align 4
  %n = alloca i32, align 4
  %z = alloca i8*, align 8
  %2 = load i8** @find_home_dir.home_dir, align 8
  %3 = icmp ne i8* %2, null
  br i1 %3, label %4, label %6

; <label>:4                                       ; preds = %0
  %5 = load i8** @find_home_dir.home_dir, align 8
  store i8* %5, i8** %1
  br label %41

; <label>:6                                       ; preds = %0
  %7 = call i32 @getuid() #5
  store i32 %7, i32* %uid, align 4
  %8 = load i32* %uid, align 4
  %9 = call %struct.passwd* @getpwuid(i32 %8)
  store %struct.passwd* %9, %struct.passwd** %pwent, align 8
  %10 = icmp ne %struct.passwd* %9, null
  br i1 %10, label %11, label %15

; <label>:11                                      ; preds = %6
  %12 = load %struct.passwd** %pwent, align 8
  %13 = getelementptr inbounds %struct.passwd* %12, i32 0, i32 5
  %14 = load i8** %13, align 8
  store i8* %14, i8** @find_home_dir.home_dir, align 8
  br label %15

; <label>:15                                      ; preds = %11, %6
  %16 = load i8** @find_home_dir.home_dir, align 8
  %17 = icmp ne i8* %16, null
  br i1 %17, label %20, label %18

; <label>:18                                      ; preds = %15
  %19 = call i8* @getenv(i8* getelementptr inbounds ([5 x i8]* @.str59, i32 0, i32 0)) #5
  store i8* %19, i8** @find_home_dir.home_dir, align 8
  br label %20

; <label>:20                                      ; preds = %18, %15
  %21 = load i8** @find_home_dir.home_dir, align 8
  %22 = icmp ne i8* %21, null
  br i1 %22, label %23, label %39

; <label>:23                                      ; preds = %20
  %24 = load i8** @find_home_dir.home_dir, align 8
  %25 = call i32 @strlen30(i8* %24)
  %26 = add nsw i32 %25, 1
  store i32 %26, i32* %n, align 4
  %27 = load i32* %n, align 4
  %28 = sext i32 %27 to i64
  %29 = call noalias i8* @malloc(i64 %28) #5
  store i8* %29, i8** %z, align 8
  %30 = load i8** %z, align 8
  %31 = icmp ne i8* %30, null
  br i1 %31, label %32, label %37

; <label>:32                                      ; preds = %23
  %33 = load i8** %z, align 8
  %34 = load i8** @find_home_dir.home_dir, align 8
  %35 = load i32* %n, align 4
  %36 = sext i32 %35 to i64
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %33, i8* %34, i64 %36, i32 1, i1 false)
  br label %37

; <label>:37                                      ; preds = %32, %23
  %38 = load i8** %z, align 8
  store i8* %38, i8** @find_home_dir.home_dir, align 8
  br label %39

; <label>:39                                      ; preds = %37, %20
  %40 = load i8** @find_home_dir.home_dir, align 8
  store i8* %40, i8** %1
  br label %41

; <label>:41                                      ; preds = %39, %4
  %42 = load i8** %1
  ret i8* %42
}

; Function Attrs: nounwind uwtable
define internal i32 @strlen30(i8* %z) #0 {
  %1 = alloca i8*, align 8
  %z2 = alloca i8*, align 8
  store i8* %z, i8** %1, align 8
  %2 = load i8** %1, align 8
  store i8* %2, i8** %z2, align 8
  br label %3

; <label>:3                                       ; preds = %7, %0
  %4 = load i8** %z2, align 8
  %5 = load i8* %4, align 1
  %6 = icmp ne i8 %5, 0
  br i1 %6, label %7, label %10

; <label>:7                                       ; preds = %3
  %8 = load i8** %z2, align 8
  %9 = getelementptr inbounds i8* %8, i32 1
  store i8* %9, i8** %z2, align 8
  br label %3

; <label>:10                                      ; preds = %3
  %11 = load i8** %z2, align 8
  %12 = load i8** %1, align 8
  %13 = ptrtoint i8* %11 to i64
  %14 = ptrtoint i8* %12 to i64
  %15 = sub i64 %13, %14
  %16 = trunc i64 %15 to i32
  %17 = and i32 1073741823, %16
  ret i32 %17
}

; Function Attrs: nounwind uwtable
define internal i32 @process_input(%struct.ShellState* %p, %struct._IO_FILE* %in) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca %struct._IO_FILE*, align 8
  %zLine = alloca i8*, align 8
  %zSql = alloca i8*, align 8
  %nLine = alloca i32, align 4
  %nSql = alloca i32, align 4
  %nAlloc = alloca i32, align 4
  %nSqlPrior = alloca i32, align 4
  %zErrMsg = alloca i8*, align 8
  %rc = alloca i32, align 4
  %errCnt = alloca i32, align 4
  %lineno = alloca i32, align 4
  %startline = alloca i32, align 4
  %i = alloca i32, align 4
  %zPrefix = alloca [100 x i8], align 16
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store %struct._IO_FILE* %in, %struct._IO_FILE** %2, align 8
  store i8* null, i8** %zLine, align 8
  store i8* null, i8** %zSql, align 8
  store i32 0, i32* %nSql, align 4
  store i32 0, i32* %nAlloc, align 4
  store i32 0, i32* %nSqlPrior, align 4
  store i32 0, i32* %errCnt, align 4
  store i32 0, i32* %lineno, align 4
  store i32 0, i32* %startline, align 4
  br label %3

; <label>:3                                       ; preds = %308, %98, %63, %0
  %4 = load i32* %errCnt, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %17, label %6

; <label>:6                                       ; preds = %3
  %7 = load i32* @bail_on_error, align 4
  %8 = icmp ne i32 %7, 0
  br i1 %8, label %9, label %17

; <label>:9                                       ; preds = %6
  %10 = load %struct._IO_FILE** %2, align 8
  %11 = icmp eq %struct._IO_FILE* %10, null
  br i1 %11, label %12, label %15

; <label>:12                                      ; preds = %9
  %13 = load i32* @stdin_is_interactive, align 4
  %14 = icmp ne i32 %13, 0
  br label %15

; <label>:15                                      ; preds = %12, %9
  %16 = phi i1 [ false, %9 ], [ %14, %12 ]
  br label %17

; <label>:17                                      ; preds = %15, %6, %3
  %18 = phi i1 [ true, %6 ], [ true, %3 ], [ %16, %15 ]
  br i1 %18, label %19, label %309

; <label>:19                                      ; preds = %17
  %20 = load %struct.ShellState** %1, align 8
  %21 = getelementptr inbounds %struct.ShellState* %20, i32 0, i32 8
  %22 = load %struct._IO_FILE** %21, align 8
  %23 = call i32 @fflush(%struct._IO_FILE* %22)
  %24 = load %struct._IO_FILE** %2, align 8
  %25 = load i8** %zLine, align 8
  %26 = load i32* %nSql, align 4
  %27 = icmp sgt i32 %26, 0
  %28 = zext i1 %27 to i32
  %29 = call i8* @one_input_line(%struct._IO_FILE* %24, i8* %25, i32 %28)
  store i8* %29, i8** %zLine, align 8
  %30 = load i8** %zLine, align 8
  %31 = icmp eq i8* %30, null
  br i1 %31, label %32, label %38

; <label>:32                                      ; preds = %19
  %33 = load i32* @stdin_is_interactive, align 4
  %34 = icmp ne i32 %33, 0
  br i1 %34, label %35, label %37

; <label>:35                                      ; preds = %32
  %36 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  br label %37

; <label>:37                                      ; preds = %35, %32
  br label %309

; <label>:38                                      ; preds = %19
  %39 = load volatile i32* @seenInterrupt, align 4
  %40 = icmp ne i32 %39, 0
  br i1 %40, label %41, label %46

; <label>:41                                      ; preds = %38
  %42 = load %struct._IO_FILE** %2, align 8
  %43 = icmp ne %struct._IO_FILE* %42, null
  br i1 %43, label %44, label %45

; <label>:44                                      ; preds = %41
  br label %309

; <label>:45                                      ; preds = %41
  store volatile i32 0, i32* @seenInterrupt, align 4
  br label %46

; <label>:46                                      ; preds = %45, %38
  %47 = load i32* %lineno, align 4
  %48 = add nsw i32 %47, 1
  store i32 %48, i32* %lineno, align 4
  %49 = load i32* %nSql, align 4
  %50 = icmp eq i32 %49, 0
  br i1 %50, label %51, label %64

; <label>:51                                      ; preds = %46
  %52 = load i8** %zLine, align 8
  %53 = call i32 @_all_whitespace(i8* %52)
  %54 = icmp ne i32 %53, 0
  br i1 %54, label %55, label %64

; <label>:55                                      ; preds = %51
  %56 = load %struct.ShellState** %1, align 8
  %57 = getelementptr inbounds %struct.ShellState* %56, i32 0, i32 1
  %58 = load i32* %57, align 4
  %59 = icmp ne i32 %58, 0
  br i1 %59, label %60, label %63

; <label>:60                                      ; preds = %55
  %61 = load i8** %zLine, align 8
  %62 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @.str51, i32 0, i32 0), i8* %61)
  br label %63

; <label>:63                                      ; preds = %60, %55
  br label %3

; <label>:64                                      ; preds = %51, %46
  %65 = load i8** %zLine, align 8
  %66 = icmp ne i8* %65, null
  br i1 %66, label %67, label %99

; <label>:67                                      ; preds = %64
  %68 = load i8** %zLine, align 8
  %69 = getelementptr inbounds i8* %68, i64 0
  %70 = load i8* %69, align 1
  %71 = sext i8 %70 to i32
  %72 = icmp eq i32 %71, 46
  br i1 %72, label %73, label %99

; <label>:73                                      ; preds = %67
  %74 = load i32* %nSql, align 4
  %75 = icmp eq i32 %74, 0
  br i1 %75, label %76, label %99

; <label>:76                                      ; preds = %73
  %77 = load %struct.ShellState** %1, align 8
  %78 = getelementptr inbounds %struct.ShellState* %77, i32 0, i32 1
  %79 = load i32* %78, align 4
  %80 = icmp ne i32 %79, 0
  br i1 %80, label %81, label %84

; <label>:81                                      ; preds = %76
  %82 = load i8** %zLine, align 8
  %83 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @.str51, i32 0, i32 0), i8* %82)
  br label %84

; <label>:84                                      ; preds = %81, %76
  %85 = load i8** %zLine, align 8
  %86 = load %struct.ShellState** %1, align 8
  %87 = call i32 @do_meta_command(i8* %85, %struct.ShellState* %86)
  store i32 %87, i32* %rc, align 4
  %88 = load i32* %rc, align 4
  %89 = icmp eq i32 %88, 2
  br i1 %89, label %90, label %91

; <label>:90                                      ; preds = %84
  br label %309

; <label>:91                                      ; preds = %84
  %92 = load i32* %rc, align 4
  %93 = icmp ne i32 %92, 0
  br i1 %93, label %94, label %97

; <label>:94                                      ; preds = %91
  %95 = load i32* %errCnt, align 4
  %96 = add nsw i32 %95, 1
  store i32 %96, i32* %errCnt, align 4
  br label %97

; <label>:97                                      ; preds = %94, %91
  br label %98

; <label>:98                                      ; preds = %97
  br label %3

; <label>:99                                      ; preds = %73, %67, %64
  %100 = load i8** %zLine, align 8
  %101 = call i32 @line_is_command_terminator(i8* %100)
  %102 = icmp ne i32 %101, 0
  br i1 %102, label %103, label %110

; <label>:103                                     ; preds = %99
  %104 = load i8** %zSql, align 8
  %105 = load i32* %nSql, align 4
  %106 = call i32 @line_is_complete(i8* %104, i32 %105)
  %107 = icmp ne i32 %106, 0
  br i1 %107, label %108, label %110

; <label>:108                                     ; preds = %103
  %109 = load i8** %zLine, align 8
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %109, i8* getelementptr inbounds ([2 x i8]* @.str52, i32 0, i32 0), i64 2, i32 1, i1 false)
  br label %110

; <label>:110                                     ; preds = %108, %103, %99
  %111 = load i8** %zLine, align 8
  %112 = call i32 @strlen30(i8* %111)
  store i32 %112, i32* %nLine, align 4
  %113 = load i32* %nSql, align 4
  %114 = load i32* %nLine, align 4
  %115 = add nsw i32 %113, %114
  %116 = add nsw i32 %115, 2
  %117 = load i32* %nAlloc, align 4
  %118 = icmp sge i32 %116, %117
  br i1 %118, label %119, label %134

; <label>:119                                     ; preds = %110
  %120 = load i32* %nSql, align 4
  %121 = load i32* %nLine, align 4
  %122 = add nsw i32 %120, %121
  %123 = add nsw i32 %122, 100
  store i32 %123, i32* %nAlloc, align 4
  %124 = load i8** %zSql, align 8
  %125 = load i32* %nAlloc, align 4
  %126 = sext i32 %125 to i64
  %127 = call i8* @realloc(i8* %124, i64 %126) #5
  store i8* %127, i8** %zSql, align 8
  %128 = load i8** %zSql, align 8
  %129 = icmp eq i8* %128, null
  br i1 %129, label %130, label %133

; <label>:130                                     ; preds = %119
  %131 = load %struct._IO_FILE** @stderr, align 8
  %132 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %131, i8* getelementptr inbounds ([22 x i8]* @.str49, i32 0, i32 0))
  call void @exit(i32 1) #8
  unreachable

; <label>:133                                     ; preds = %119
  br label %134

; <label>:134                                     ; preds = %133, %110
  %135 = load i32* %nSql, align 4
  store i32 %135, i32* %nSqlPrior, align 4
  %136 = load i32* %nSql, align 4
  %137 = icmp eq i32 %136, 0
  br i1 %137, label %138, label %192

; <label>:138                                     ; preds = %134
  store i32 0, i32* %i, align 4
  br label %139

; <label>:139                                     ; preds = %165, %138
  %140 = load i32* %i, align 4
  %141 = sext i32 %140 to i64
  %142 = load i8** %zLine, align 8
  %143 = getelementptr inbounds i8* %142, i64 %141
  %144 = load i8* %143, align 1
  %145 = sext i8 %144 to i32
  %146 = icmp ne i32 %145, 0
  br i1 %146, label %147, label %162

; <label>:147                                     ; preds = %139
  %148 = load i32* %i, align 4
  %149 = sext i32 %148 to i64
  %150 = load i8** %zLine, align 8
  %151 = getelementptr inbounds i8* %150, i64 %149
  %152 = load i8* %151, align 1
  %153 = zext i8 %152 to i32
  %154 = sext i32 %153 to i64
  %155 = call i16** @__ctype_b_loc() #9
  %156 = load i16** %155, align 8
  %157 = getelementptr inbounds i16* %156, i64 %154
  %158 = load i16* %157, align 2
  %159 = zext i16 %158 to i32
  %160 = and i32 %159, 8192
  %161 = icmp ne i32 %160, 0
  br label %162

; <label>:162                                     ; preds = %147, %139
  %163 = phi i1 [ false, %139 ], [ %161, %147 ]
  br i1 %163, label %164, label %168

; <label>:164                                     ; preds = %162
  br label %165

; <label>:165                                     ; preds = %164
  %166 = load i32* %i, align 4
  %167 = add nsw i32 %166, 1
  store i32 %167, i32* %i, align 4
  br label %139

; <label>:168                                     ; preds = %162
  %169 = load i32* %nAlloc, align 4
  %170 = icmp sgt i32 %169, 0
  br i1 %170, label %171, label %175

; <label>:171                                     ; preds = %168
  %172 = load i8** %zSql, align 8
  %173 = icmp ne i8* %172, null
  br i1 %173, label %174, label %175

; <label>:174                                     ; preds = %171
  br label %177

; <label>:175                                     ; preds = %171, %168
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8]* @.str53, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str54, i32 0, i32 0), i32 4202, i8* getelementptr inbounds ([40 x i8]* @__PRETTY_FUNCTION__.process_input, i32 0, i32 0)) #8
  unreachable
                                                  ; No predecessors!
  br label %177

; <label>:177                                     ; preds = %176, %174
  %178 = load i8** %zSql, align 8
  %179 = load i8** %zLine, align 8
  %180 = load i32* %i, align 4
  %181 = sext i32 %180 to i64
  %182 = getelementptr inbounds i8* %179, i64 %181
  %183 = load i32* %nLine, align 4
  %184 = add nsw i32 %183, 1
  %185 = load i32* %i, align 4
  %186 = sub nsw i32 %184, %185
  %187 = sext i32 %186 to i64
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %178, i8* %182, i64 %187, i32 1, i1 false)
  %188 = load i32* %lineno, align 4
  store i32 %188, i32* %startline, align 4
  %189 = load i32* %nLine, align 4
  %190 = load i32* %i, align 4
  %191 = sub nsw i32 %189, %190
  store i32 %191, i32* %nSql, align 4
  br label %209

; <label>:192                                     ; preds = %134
  %193 = load i32* %nSql, align 4
  %194 = add nsw i32 %193, 1
  store i32 %194, i32* %nSql, align 4
  %195 = sext i32 %193 to i64
  %196 = load i8** %zSql, align 8
  %197 = getelementptr inbounds i8* %196, i64 %195
  store i8 10, i8* %197, align 1
  %198 = load i8** %zSql, align 8
  %199 = load i32* %nSql, align 4
  %200 = sext i32 %199 to i64
  %201 = getelementptr inbounds i8* %198, i64 %200
  %202 = load i8** %zLine, align 8
  %203 = load i32* %nLine, align 4
  %204 = add nsw i32 %203, 1
  %205 = sext i32 %204 to i64
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %201, i8* %202, i64 %205, i32 1, i1 false)
  %206 = load i32* %nLine, align 4
  %207 = load i32* %nSql, align 4
  %208 = add nsw i32 %207, %206
  store i32 %208, i32* %nSql, align 4
  br label %209

; <label>:209                                     ; preds = %192, %177
  %210 = load i32* %nSql, align 4
  %211 = icmp ne i32 %210, 0
  br i1 %211, label %212, label %291

; <label>:212                                     ; preds = %209
  %213 = load i32* %nSqlPrior, align 4
  %214 = sext i32 %213 to i64
  %215 = load i8** %zSql, align 8
  %216 = getelementptr inbounds i8* %215, i64 %214
  %217 = load i32* %nSql, align 4
  %218 = load i32* %nSqlPrior, align 4
  %219 = sub nsw i32 %217, %218
  %220 = call i32 @line_contains_semicolon(i8* %216, i32 %219)
  %221 = icmp ne i32 %220, 0
  br i1 %221, label %222, label %291

; <label>:222                                     ; preds = %212
  %223 = load i8** %zSql, align 8
  %224 = call i32 @sqlite3_complete(i8* %223)
  %225 = icmp ne i32 %224, 0
  br i1 %225, label %226, label %291

; <label>:226                                     ; preds = %222
  %227 = load %struct.ShellState** %1, align 8
  %228 = getelementptr inbounds %struct.ShellState* %227, i32 0, i32 7
  store i32 0, i32* %228, align 4
  %229 = load %struct.ShellState** %1, align 8
  call void @open_db(%struct.ShellState* %229, i32 0)
  %230 = load %struct.ShellState** %1, align 8
  %231 = getelementptr inbounds %struct.ShellState* %230, i32 0, i32 5
  %232 = load i32* %231, align 4
  %233 = icmp ne i32 %232, 0
  br i1 %233, label %234, label %236

; <label>:234                                     ; preds = %226
  %235 = load i8** %zSql, align 8
  call void @resolve_backslashes(i8* %235)
  br label %236

; <label>:236                                     ; preds = %234, %226
  call void @beginTimer()
  %237 = load %struct.ShellState** %1, align 8
  %238 = getelementptr inbounds %struct.ShellState* %237, i32 0, i32 0
  %239 = load %struct.sqlite3** %238, align 8
  %240 = load i8** %zSql, align 8
  %241 = load %struct.ShellState** %1, align 8
  %242 = call i32 @shell_exec(%struct.sqlite3* %239, i8* %240, i32 (i8*, i32, i8**, i8**, i32*)* @shell_callback, %struct.ShellState* %241, i8** %zErrMsg)
  store i32 %242, i32* %rc, align 4
  call void @endTimer()
  %243 = load i32* %rc, align 4
  %244 = icmp ne i32 %243, 0
  br i1 %244, label %248, label %245

; <label>:245                                     ; preds = %236
  %246 = load i8** %zErrMsg, align 8
  %247 = icmp ne i8* %246, null
  br i1 %247, label %248, label %281

; <label>:248                                     ; preds = %245, %236
  %249 = load %struct._IO_FILE** %2, align 8
  %250 = icmp ne %struct._IO_FILE* %249, null
  br i1 %250, label %254, label %251

; <label>:251                                     ; preds = %248
  %252 = load i32* @stdin_is_interactive, align 4
  %253 = icmp ne i32 %252, 0
  br i1 %253, label %258, label %254

; <label>:254                                     ; preds = %251, %248
  %255 = getelementptr inbounds [100 x i8]* %zPrefix, i32 0, i32 0
  %256 = load i32* %startline, align 4
  %257 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 100, i8* %255, i8* getelementptr inbounds ([21 x i8]* @.str55, i32 0, i32 0), i32 %256)
  br label %261

; <label>:258                                     ; preds = %251
  %259 = getelementptr inbounds [100 x i8]* %zPrefix, i32 0, i32 0
  %260 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 100, i8* %259, i8* getelementptr inbounds ([7 x i8]* @.str56, i32 0, i32 0))
  br label %261

; <label>:261                                     ; preds = %258, %254
  %262 = load i8** %zErrMsg, align 8
  %263 = icmp ne i8* %262, null
  br i1 %263, label %264, label %270

; <label>:264                                     ; preds = %261
  %265 = load %struct._IO_FILE** @stderr, align 8
  %266 = getelementptr inbounds [100 x i8]* %zPrefix, i32 0, i32 0
  %267 = load i8** %zErrMsg, align 8
  %268 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %265, i8* getelementptr inbounds ([7 x i8]* @.str36, i32 0, i32 0), i8* %266, i8* %267)
  %269 = load i8** %zErrMsg, align 8
  call void @sqlite3_free(i8* %269)
  store i8* null, i8** %zErrMsg, align 8
  br label %278

; <label>:270                                     ; preds = %261
  %271 = load %struct._IO_FILE** @stderr, align 8
  %272 = getelementptr inbounds [100 x i8]* %zPrefix, i32 0, i32 0
  %273 = load %struct.ShellState** %1, align 8
  %274 = getelementptr inbounds %struct.ShellState* %273, i32 0, i32 0
  %275 = load %struct.sqlite3** %274, align 8
  %276 = call i8* @sqlite3_errmsg(%struct.sqlite3* %275)
  %277 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %271, i8* getelementptr inbounds ([7 x i8]* @.str36, i32 0, i32 0), i8* %272, i8* %276)
  br label %278

; <label>:278                                     ; preds = %270, %264
  %279 = load i32* %errCnt, align 4
  %280 = add nsw i32 %279, 1
  store i32 %280, i32* %errCnt, align 4
  br label %281

; <label>:281                                     ; preds = %278, %245
  store i32 0, i32* %nSql, align 4
  %282 = load %struct.ShellState** %1, align 8
  %283 = getelementptr inbounds %struct.ShellState* %282, i32 0, i32 6
  %284 = load i32* %283, align 4
  %285 = icmp ne i32 %284, 0
  br i1 %285, label %286, label %290

; <label>:286                                     ; preds = %281
  %287 = load %struct.ShellState** %1, align 8
  call void @output_reset(%struct.ShellState* %287)
  %288 = load %struct.ShellState** %1, align 8
  %289 = getelementptr inbounds %struct.ShellState* %288, i32 0, i32 6
  store i32 0, i32* %289, align 4
  br label %290

; <label>:290                                     ; preds = %286, %281
  br label %308

; <label>:291                                     ; preds = %222, %212, %209
  %292 = load i32* %nSql, align 4
  %293 = icmp ne i32 %292, 0
  br i1 %293, label %294, label %307

; <label>:294                                     ; preds = %291
  %295 = load i8** %zSql, align 8
  %296 = call i32 @_all_whitespace(i8* %295)
  %297 = icmp ne i32 %296, 0
  br i1 %297, label %298, label %307

; <label>:298                                     ; preds = %294
  %299 = load %struct.ShellState** %1, align 8
  %300 = getelementptr inbounds %struct.ShellState* %299, i32 0, i32 1
  %301 = load i32* %300, align 4
  %302 = icmp ne i32 %301, 0
  br i1 %302, label %303, label %306

; <label>:303                                     ; preds = %298
  %304 = load i8** %zSql, align 8
  %305 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @.str51, i32 0, i32 0), i8* %304)
  br label %306

; <label>:306                                     ; preds = %303, %298
  store i32 0, i32* %nSql, align 4
  br label %307

; <label>:307                                     ; preds = %306, %294, %291
  br label %308

; <label>:308                                     ; preds = %307, %290
  br label %3

; <label>:309                                     ; preds = %90, %44, %37, %17
  %310 = load i32* %nSql, align 4
  %311 = icmp ne i32 %310, 0
  br i1 %311, label %312, label %324

; <label>:312                                     ; preds = %309
  %313 = load i8** %zSql, align 8
  %314 = call i32 @_all_whitespace(i8* %313)
  %315 = icmp ne i32 %314, 0
  br i1 %315, label %322, label %316

; <label>:316                                     ; preds = %312
  %317 = load %struct._IO_FILE** @stderr, align 8
  %318 = load i8** %zSql, align 8
  %319 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %317, i8* getelementptr inbounds ([27 x i8]* @.str57, i32 0, i32 0), i8* %318)
  %320 = load i32* %errCnt, align 4
  %321 = add nsw i32 %320, 1
  store i32 %321, i32* %errCnt, align 4
  br label %322

; <label>:322                                     ; preds = %316, %312
  %323 = load i8** %zSql, align 8
  call void @free(i8* %323) #5
  br label %324

; <label>:324                                     ; preds = %322, %309
  %325 = load i8** %zLine, align 8
  call void @free(i8* %325) #5
  %326 = load i32* %errCnt, align 4
  %327 = icmp sgt i32 %326, 0
  %328 = zext i1 %327 to i32
  ret i32 %328
}

; Function Attrs: nounwind uwtable
define internal void @set_table_name(%struct.ShellState* %p, i8* %zName) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca i8*, align 8
  %i = alloca i32, align 4
  %n = alloca i32, align 4
  %needQuote = alloca i32, align 4
  %z = alloca i8*, align 8
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store i8* %zName, i8** %2, align 8
  %3 = load %struct.ShellState** %1, align 8
  %4 = getelementptr inbounds %struct.ShellState* %3, i32 0, i32 15
  %5 = load i8** %4, align 8
  %6 = icmp ne i8* %5, null
  br i1 %6, label %7, label %13

; <label>:7                                       ; preds = %0
  %8 = load %struct.ShellState** %1, align 8
  %9 = getelementptr inbounds %struct.ShellState* %8, i32 0, i32 15
  %10 = load i8** %9, align 8
  call void @free(i8* %10) #5
  %11 = load %struct.ShellState** %1, align 8
  %12 = getelementptr inbounds %struct.ShellState* %11, i32 0, i32 15
  store i8* null, i8** %12, align 8
  br label %13

; <label>:13                                      ; preds = %7, %0
  %14 = load i8** %2, align 8
  %15 = icmp eq i8* %14, null
  br i1 %15, label %16, label %17

; <label>:16                                      ; preds = %13
  br label %162

; <label>:17                                      ; preds = %13
  %18 = load i8** %2, align 8
  %19 = load i8* %18, align 1
  %20 = zext i8 %19 to i32
  %21 = sext i32 %20 to i64
  %22 = call i16** @__ctype_b_loc() #9
  %23 = load i16** %22, align 8
  %24 = getelementptr inbounds i16* %23, i64 %21
  %25 = load i16* %24, align 2
  %26 = zext i16 %25 to i32
  %27 = and i32 %26, 1024
  %28 = icmp ne i32 %27, 0
  br i1 %28, label %34, label %29

; <label>:29                                      ; preds = %17
  %30 = load i8** %2, align 8
  %31 = load i8* %30, align 1
  %32 = sext i8 %31 to i32
  %33 = icmp ne i32 %32, 95
  br label %34

; <label>:34                                      ; preds = %29, %17
  %35 = phi i1 [ false, %17 ], [ %33, %29 ]
  %36 = zext i1 %35 to i32
  store i32 %36, i32* %needQuote, align 4
  store i32 0, i32* %n, align 4
  store i32 0, i32* %i, align 4
  br label %37

; <label>:37                                      ; preds = %80, %34
  %38 = load i32* %i, align 4
  %39 = sext i32 %38 to i64
  %40 = load i8** %2, align 8
  %41 = getelementptr inbounds i8* %40, i64 %39
  %42 = load i8* %41, align 1
  %43 = icmp ne i8 %42, 0
  br i1 %43, label %44, label %85

; <label>:44                                      ; preds = %37
  %45 = load i32* %i, align 4
  %46 = sext i32 %45 to i64
  %47 = load i8** %2, align 8
  %48 = getelementptr inbounds i8* %47, i64 %46
  %49 = load i8* %48, align 1
  %50 = zext i8 %49 to i32
  %51 = sext i32 %50 to i64
  %52 = call i16** @__ctype_b_loc() #9
  %53 = load i16** %52, align 8
  %54 = getelementptr inbounds i16* %53, i64 %51
  %55 = load i16* %54, align 2
  %56 = zext i16 %55 to i32
  %57 = and i32 %56, 8
  %58 = icmp ne i32 %57, 0
  br i1 %58, label %79, label %59

; <label>:59                                      ; preds = %44
  %60 = load i32* %i, align 4
  %61 = sext i32 %60 to i64
  %62 = load i8** %2, align 8
  %63 = getelementptr inbounds i8* %62, i64 %61
  %64 = load i8* %63, align 1
  %65 = sext i8 %64 to i32
  %66 = icmp ne i32 %65, 95
  br i1 %66, label %67, label %79

; <label>:67                                      ; preds = %59
  store i32 1, i32* %needQuote, align 4
  %68 = load i32* %i, align 4
  %69 = sext i32 %68 to i64
  %70 = load i8** %2, align 8
  %71 = getelementptr inbounds i8* %70, i64 %69
  %72 = load i8* %71, align 1
  %73 = sext i8 %72 to i32
  %74 = icmp eq i32 %73, 39
  br i1 %74, label %75, label %78

; <label>:75                                      ; preds = %67
  %76 = load i32* %n, align 4
  %77 = add nsw i32 %76, 1
  store i32 %77, i32* %n, align 4
  br label %78

; <label>:78                                      ; preds = %75, %67
  br label %79

; <label>:79                                      ; preds = %78, %59, %44
  br label %80

; <label>:80                                      ; preds = %79
  %81 = load i32* %i, align 4
  %82 = add nsw i32 %81, 1
  store i32 %82, i32* %i, align 4
  %83 = load i32* %n, align 4
  %84 = add nsw i32 %83, 1
  store i32 %84, i32* %n, align 4
  br label %37

; <label>:85                                      ; preds = %37
  %86 = load i32* %needQuote, align 4
  %87 = icmp ne i32 %86, 0
  br i1 %87, label %88, label %91

; <label>:88                                      ; preds = %85
  %89 = load i32* %n, align 4
  %90 = add nsw i32 %89, 2
  store i32 %90, i32* %n, align 4
  br label %91

; <label>:91                                      ; preds = %88, %85
  %92 = load i32* %n, align 4
  %93 = add nsw i32 %92, 1
  %94 = sext i32 %93 to i64
  %95 = call noalias i8* @malloc(i64 %94) #5
  %96 = load %struct.ShellState** %1, align 8
  %97 = getelementptr inbounds %struct.ShellState* %96, i32 0, i32 15
  store i8* %95, i8** %97, align 8
  store i8* %95, i8** %z, align 8
  %98 = load i8** %z, align 8
  %99 = icmp eq i8* %98, null
  br i1 %99, label %100, label %103

; <label>:100                                     ; preds = %91
  %101 = load %struct._IO_FILE** @stderr, align 8
  %102 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %101, i8* getelementptr inbounds ([22 x i8]* @.str49, i32 0, i32 0))
  call void @exit(i32 1) #8
  unreachable

; <label>:103                                     ; preds = %91
  store i32 0, i32* %n, align 4
  %104 = load i32* %needQuote, align 4
  %105 = icmp ne i32 %104, 0
  br i1 %105, label %106, label %112

; <label>:106                                     ; preds = %103
  %107 = load i32* %n, align 4
  %108 = add nsw i32 %107, 1
  store i32 %108, i32* %n, align 4
  %109 = sext i32 %107 to i64
  %110 = load i8** %z, align 8
  %111 = getelementptr inbounds i8* %110, i64 %109
  store i8 39, i8* %111, align 1
  br label %112

; <label>:112                                     ; preds = %106, %103
  store i32 0, i32* %i, align 4
  br label %113

; <label>:113                                     ; preds = %145, %112
  %114 = load i32* %i, align 4
  %115 = sext i32 %114 to i64
  %116 = load i8** %2, align 8
  %117 = getelementptr inbounds i8* %116, i64 %115
  %118 = load i8* %117, align 1
  %119 = icmp ne i8 %118, 0
  br i1 %119, label %120, label %148

; <label>:120                                     ; preds = %113
  %121 = load i32* %i, align 4
  %122 = sext i32 %121 to i64
  %123 = load i8** %2, align 8
  %124 = getelementptr inbounds i8* %123, i64 %122
  %125 = load i8* %124, align 1
  %126 = load i32* %n, align 4
  %127 = add nsw i32 %126, 1
  store i32 %127, i32* %n, align 4
  %128 = sext i32 %126 to i64
  %129 = load i8** %z, align 8
  %130 = getelementptr inbounds i8* %129, i64 %128
  store i8 %125, i8* %130, align 1
  %131 = load i32* %i, align 4
  %132 = sext i32 %131 to i64
  %133 = load i8** %2, align 8
  %134 = getelementptr inbounds i8* %133, i64 %132
  %135 = load i8* %134, align 1
  %136 = sext i8 %135 to i32
  %137 = icmp eq i32 %136, 39
  br i1 %137, label %138, label %144

; <label>:138                                     ; preds = %120
  %139 = load i32* %n, align 4
  %140 = add nsw i32 %139, 1
  store i32 %140, i32* %n, align 4
  %141 = sext i32 %139 to i64
  %142 = load i8** %z, align 8
  %143 = getelementptr inbounds i8* %142, i64 %141
  store i8 39, i8* %143, align 1
  br label %144

; <label>:144                                     ; preds = %138, %120
  br label %145

; <label>:145                                     ; preds = %144
  %146 = load i32* %i, align 4
  %147 = add nsw i32 %146, 1
  store i32 %147, i32* %i, align 4
  br label %113

; <label>:148                                     ; preds = %113
  %149 = load i32* %needQuote, align 4
  %150 = icmp ne i32 %149, 0
  br i1 %150, label %151, label %157

; <label>:151                                     ; preds = %148
  %152 = load i32* %n, align 4
  %153 = add nsw i32 %152, 1
  store i32 %153, i32* %n, align 4
  %154 = sext i32 %152 to i64
  %155 = load i8** %z, align 8
  %156 = getelementptr inbounds i8* %155, i64 %154
  store i8 39, i8* %156, align 1
  br label %157

; <label>:157                                     ; preds = %151, %148
  %158 = load i32* %n, align 4
  %159 = sext i32 %158 to i64
  %160 = load i8** %z, align 8
  %161 = getelementptr inbounds i8* %160, i64 %159
  store i8 0, i8* %161, align 1
  br label %162

; <label>:162                                     ; preds = %157, %16
  ret void
}

declare i32 @sqlite3_close(%struct.sqlite3*) #2

declare void @sqlite3_free(i8*) #2

; Function Attrs: nounwind readnone
declare i16** @__ctype_b_loc() #6

declare i32 @fflush(%struct._IO_FILE*) #2

; Function Attrs: nounwind uwtable
define internal i8* @one_input_line(%struct._IO_FILE* %in, i8* %zPrior, i32 %isContinuation) #0 {
  %1 = alloca %struct._IO_FILE*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i32, align 4
  %zPrompt = alloca i8*, align 8
  %zResult = alloca i8*, align 8
  store %struct._IO_FILE* %in, %struct._IO_FILE** %1, align 8
  store i8* %zPrior, i8** %2, align 8
  store i32 %isContinuation, i32* %3, align 4
  %4 = load %struct._IO_FILE** %1, align 8
  %5 = icmp ne %struct._IO_FILE* %4, null
  br i1 %5, label %6, label %10

; <label>:6                                       ; preds = %0
  %7 = load i8** %2, align 8
  %8 = load %struct._IO_FILE** %1, align 8
  %9 = call i8* @local_getline(i8* %7, %struct._IO_FILE* %8)
  store i8* %9, i8** %zResult, align 8
  br label %21

; <label>:10                                      ; preds = %0
  %11 = load i32* %3, align 4
  %12 = icmp ne i32 %11, 0
  %13 = select i1 %12, i8* getelementptr inbounds ([20 x i8]* @continuePrompt, i32 0, i32 0), i8* getelementptr inbounds ([20 x i8]* @mainPrompt, i32 0, i32 0)
  store i8* %13, i8** %zPrompt, align 8
  %14 = load i8** %zPrompt, align 8
  %15 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %14)
  %16 = load %struct._IO_FILE** @stdout, align 8
  %17 = call i32 @fflush(%struct._IO_FILE* %16)
  %18 = load i8** %2, align 8
  %19 = load %struct._IO_FILE** @stdin, align 8
  %20 = call i8* @local_getline(i8* %18, %struct._IO_FILE* %19)
  store i8* %20, i8** %zResult, align 8
  br label %21

; <label>:21                                      ; preds = %10, %6
  %22 = load i8** %zResult, align 8
  ret i8* %22
}

; Function Attrs: nounwind uwtable
define internal i32 @_all_whitespace(i8* %z) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  store i8* %z, i8** %2, align 8
  br label %3

; <label>:3                                       ; preds = %104, %0
  %4 = load i8** %2, align 8
  %5 = load i8* %4, align 1
  %6 = icmp ne i8 %5, 0
  br i1 %6, label %7, label %107

; <label>:7                                       ; preds = %3
  %8 = load i8** %2, align 8
  %9 = getelementptr inbounds i8* %8, i64 0
  %10 = load i8* %9, align 1
  %11 = zext i8 %10 to i32
  %12 = sext i32 %11 to i64
  %13 = call i16** @__ctype_b_loc() #9
  %14 = load i16** %13, align 8
  %15 = getelementptr inbounds i16* %14, i64 %12
  %16 = load i16* %15, align 2
  %17 = zext i16 %16 to i32
  %18 = and i32 %17, 8192
  %19 = icmp ne i32 %18, 0
  br i1 %19, label %20, label %21

; <label>:20                                      ; preds = %7
  br label %104

; <label>:21                                      ; preds = %7
  %22 = load i8** %2, align 8
  %23 = load i8* %22, align 1
  %24 = sext i8 %23 to i32
  %25 = icmp eq i32 %24, 47
  br i1 %25, label %26, label %67

; <label>:26                                      ; preds = %21
  %27 = load i8** %2, align 8
  %28 = getelementptr inbounds i8* %27, i64 1
  %29 = load i8* %28, align 1
  %30 = sext i8 %29 to i32
  %31 = icmp eq i32 %30, 42
  br i1 %31, label %32, label %67

; <label>:32                                      ; preds = %26
  %33 = load i8** %2, align 8
  %34 = getelementptr inbounds i8* %33, i64 2
  store i8* %34, i8** %2, align 8
  br label %35

; <label>:35                                      ; preds = %55, %32
  %36 = load i8** %2, align 8
  %37 = load i8* %36, align 1
  %38 = sext i8 %37 to i32
  %39 = icmp ne i32 %38, 0
  br i1 %39, label %40, label %53

; <label>:40                                      ; preds = %35
  %41 = load i8** %2, align 8
  %42 = load i8* %41, align 1
  %43 = sext i8 %42 to i32
  %44 = icmp ne i32 %43, 42
  br i1 %44, label %51, label %45

; <label>:45                                      ; preds = %40
  %46 = load i8** %2, align 8
  %47 = getelementptr inbounds i8* %46, i64 1
  %48 = load i8* %47, align 1
  %49 = sext i8 %48 to i32
  %50 = icmp ne i32 %49, 47
  br label %51

; <label>:51                                      ; preds = %45, %40
  %52 = phi i1 [ true, %40 ], [ %50, %45 ]
  br label %53

; <label>:53                                      ; preds = %51, %35
  %54 = phi i1 [ false, %35 ], [ %52, %51 ]
  br i1 %54, label %55, label %58

; <label>:55                                      ; preds = %53
  %56 = load i8** %2, align 8
  %57 = getelementptr inbounds i8* %56, i32 1
  store i8* %57, i8** %2, align 8
  br label %35

; <label>:58                                      ; preds = %53
  %59 = load i8** %2, align 8
  %60 = load i8* %59, align 1
  %61 = sext i8 %60 to i32
  %62 = icmp eq i32 %61, 0
  br i1 %62, label %63, label %64

; <label>:63                                      ; preds = %58
  store i32 0, i32* %1
  br label %108

; <label>:64                                      ; preds = %58
  %65 = load i8** %2, align 8
  %66 = getelementptr inbounds i8* %65, i32 1
  store i8* %66, i8** %2, align 8
  br label %104

; <label>:67                                      ; preds = %26, %21
  %68 = load i8** %2, align 8
  %69 = load i8* %68, align 1
  %70 = sext i8 %69 to i32
  %71 = icmp eq i32 %70, 45
  br i1 %71, label %72, label %103

; <label>:72                                      ; preds = %67
  %73 = load i8** %2, align 8
  %74 = getelementptr inbounds i8* %73, i64 1
  %75 = load i8* %74, align 1
  %76 = sext i8 %75 to i32
  %77 = icmp eq i32 %76, 45
  br i1 %77, label %78, label %103

; <label>:78                                      ; preds = %72
  %79 = load i8** %2, align 8
  %80 = getelementptr inbounds i8* %79, i64 2
  store i8* %80, i8** %2, align 8
  br label %81

; <label>:81                                      ; preds = %93, %78
  %82 = load i8** %2, align 8
  %83 = load i8* %82, align 1
  %84 = sext i8 %83 to i32
  %85 = icmp ne i32 %84, 0
  br i1 %85, label %86, label %91

; <label>:86                                      ; preds = %81
  %87 = load i8** %2, align 8
  %88 = load i8* %87, align 1
  %89 = sext i8 %88 to i32
  %90 = icmp ne i32 %89, 10
  br label %91

; <label>:91                                      ; preds = %86, %81
  %92 = phi i1 [ false, %81 ], [ %90, %86 ]
  br i1 %92, label %93, label %96

; <label>:93                                      ; preds = %91
  %94 = load i8** %2, align 8
  %95 = getelementptr inbounds i8* %94, i32 1
  store i8* %95, i8** %2, align 8
  br label %81

; <label>:96                                      ; preds = %91
  %97 = load i8** %2, align 8
  %98 = load i8* %97, align 1
  %99 = sext i8 %98 to i32
  %100 = icmp eq i32 %99, 0
  br i1 %100, label %101, label %102

; <label>:101                                     ; preds = %96
  store i32 1, i32* %1
  br label %108

; <label>:102                                     ; preds = %96
  br label %104

; <label>:103                                     ; preds = %72, %67
  store i32 0, i32* %1
  br label %108

; <label>:104                                     ; preds = %102, %64, %20
  %105 = load i8** %2, align 8
  %106 = getelementptr inbounds i8* %105, i32 1
  store i8* %106, i8** %2, align 8
  br label %3

; <label>:107                                     ; preds = %3
  store i32 1, i32* %1
  br label %108

; <label>:108                                     ; preds = %107, %103, %101, %63
  %109 = load i32* %1
  ret i32 %109
}

; Function Attrs: nounwind uwtable
define internal i32 @line_is_command_terminator(i8* %zLine) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  store i8* %zLine, i8** %2, align 8
  br label %3

; <label>:3                                       ; preds = %16, %0
  %4 = load i8** %2, align 8
  %5 = getelementptr inbounds i8* %4, i64 0
  %6 = load i8* %5, align 1
  %7 = zext i8 %6 to i32
  %8 = sext i32 %7 to i64
  %9 = call i16** @__ctype_b_loc() #9
  %10 = load i16** %9, align 8
  %11 = getelementptr inbounds i16* %10, i64 %8
  %12 = load i16* %11, align 2
  %13 = zext i16 %12 to i32
  %14 = and i32 %13, 8192
  %15 = icmp ne i32 %14, 0
  br i1 %15, label %16, label %19

; <label>:16                                      ; preds = %3
  %17 = load i8** %2, align 8
  %18 = getelementptr inbounds i8* %17, i32 1
  store i8* %18, i8** %2, align 8
  br label %3

; <label>:19                                      ; preds = %3
  %20 = load i8** %2, align 8
  %21 = getelementptr inbounds i8* %20, i64 0
  %22 = load i8* %21, align 1
  %23 = sext i8 %22 to i32
  %24 = icmp eq i32 %23, 47
  br i1 %24, label %25, label %31

; <label>:25                                      ; preds = %19
  %26 = load i8** %2, align 8
  %27 = getelementptr inbounds i8* %26, i64 1
  %28 = call i32 @_all_whitespace(i8* %27)
  %29 = icmp ne i32 %28, 0
  br i1 %29, label %30, label %31

; <label>:30                                      ; preds = %25
  store i32 1, i32* %1
  br label %56

; <label>:31                                      ; preds = %25, %19
  %32 = load i8** %2, align 8
  %33 = getelementptr inbounds i8* %32, i64 0
  %34 = load i8* %33, align 1
  %35 = zext i8 %34 to i32
  %36 = call i32 @tolower(i32 %35) #5
  %37 = trunc i32 %36 to i8
  %38 = sext i8 %37 to i32
  %39 = icmp eq i32 %38, 103
  br i1 %39, label %40, label %55

; <label>:40                                      ; preds = %31
  %41 = load i8** %2, align 8
  %42 = getelementptr inbounds i8* %41, i64 1
  %43 = load i8* %42, align 1
  %44 = zext i8 %43 to i32
  %45 = call i32 @tolower(i32 %44) #5
  %46 = trunc i32 %45 to i8
  %47 = sext i8 %46 to i32
  %48 = icmp eq i32 %47, 111
  br i1 %48, label %49, label %55

; <label>:49                                      ; preds = %40
  %50 = load i8** %2, align 8
  %51 = getelementptr inbounds i8* %50, i64 2
  %52 = call i32 @_all_whitespace(i8* %51)
  %53 = icmp ne i32 %52, 0
  br i1 %53, label %54, label %55

; <label>:54                                      ; preds = %49
  store i32 1, i32* %1
  br label %56

; <label>:55                                      ; preds = %49, %40, %31
  store i32 0, i32* %1
  br label %56

; <label>:56                                      ; preds = %55, %54, %30
  %57 = load i32* %1
  ret i32 %57
}

; Function Attrs: nounwind uwtable
define internal i32 @line_is_complete(i8* %zSql, i32 %nSql) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %3 = alloca i32, align 4
  %rc = alloca i32, align 4
  store i8* %zSql, i8** %2, align 8
  store i32 %nSql, i32* %3, align 4
  %4 = load i8** %2, align 8
  %5 = icmp eq i8* %4, null
  br i1 %5, label %6, label %7

; <label>:6                                       ; preds = %0
  store i32 1, i32* %1
  br label %24

; <label>:7                                       ; preds = %0
  %8 = load i32* %3, align 4
  %9 = sext i32 %8 to i64
  %10 = load i8** %2, align 8
  %11 = getelementptr inbounds i8* %10, i64 %9
  store i8 59, i8* %11, align 1
  %12 = load i32* %3, align 4
  %13 = add nsw i32 %12, 1
  %14 = sext i32 %13 to i64
  %15 = load i8** %2, align 8
  %16 = getelementptr inbounds i8* %15, i64 %14
  store i8 0, i8* %16, align 1
  %17 = load i8** %2, align 8
  %18 = call i32 @sqlite3_complete(i8* %17)
  store i32 %18, i32* %rc, align 4
  %19 = load i32* %3, align 4
  %20 = sext i32 %19 to i64
  %21 = load i8** %2, align 8
  %22 = getelementptr inbounds i8* %21, i64 %20
  store i8 0, i8* %22, align 1
  %23 = load i32* %rc, align 4
  store i32 %23, i32* %1
  br label %24

; <label>:24                                      ; preds = %7, %6
  %25 = load i32* %1
  ret i32 %25
}

; Function Attrs: noreturn nounwind
declare void @__assert_fail(i8*, i8*, i32, i8*) #3

; Function Attrs: nounwind uwtable
define internal i32 @line_contains_semicolon(i8* %z, i32 %N) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %3 = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %z, i8** %2, align 8
  store i32 %N, i32* %3, align 4
  store i32 0, i32* %i, align 4
  br label %4

; <label>:4                                       ; preds = %18, %0
  %5 = load i32* %i, align 4
  %6 = load i32* %3, align 4
  %7 = icmp slt i32 %5, %6
  br i1 %7, label %8, label %21

; <label>:8                                       ; preds = %4
  %9 = load i32* %i, align 4
  %10 = sext i32 %9 to i64
  %11 = load i8** %2, align 8
  %12 = getelementptr inbounds i8* %11, i64 %10
  %13 = load i8* %12, align 1
  %14 = sext i8 %13 to i32
  %15 = icmp eq i32 %14, 59
  br i1 %15, label %16, label %17

; <label>:16                                      ; preds = %8
  store i32 1, i32* %1
  br label %22

; <label>:17                                      ; preds = %8
  br label %18

; <label>:18                                      ; preds = %17
  %19 = load i32* %i, align 4
  %20 = add nsw i32 %19, 1
  store i32 %20, i32* %i, align 4
  br label %4

; <label>:21                                      ; preds = %4
  store i32 0, i32* %1
  br label %22

; <label>:22                                      ; preds = %21, %16
  %23 = load i32* %1
  ret i32 %23
}

declare i32 @sqlite3_complete(i8*) #2

; Function Attrs: nounwind uwtable
define internal void @resolve_backslashes(i8* %z) #0 {
  %1 = alloca i8*, align 8
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %c = alloca i8, align 1
  store i8* %z, i8** %1, align 8
  br label %2

; <label>:2                                       ; preds = %14, %0
  %3 = load i8** %1, align 8
  %4 = load i8* %3, align 1
  %5 = sext i8 %4 to i32
  %6 = icmp ne i32 %5, 0
  br i1 %6, label %7, label %12

; <label>:7                                       ; preds = %2
  %8 = load i8** %1, align 8
  %9 = load i8* %8, align 1
  %10 = sext i8 %9 to i32
  %11 = icmp ne i32 %10, 92
  br label %12

; <label>:12                                      ; preds = %7, %2
  %13 = phi i1 [ false, %2 ], [ %11, %7 ]
  br i1 %13, label %14, label %17

; <label>:14                                      ; preds = %12
  %15 = load i8** %1, align 8
  %16 = getelementptr inbounds i8* %15, i32 1
  store i8* %16, i8** %1, align 8
  br label %2

; <label>:17                                      ; preds = %12
  store i32 0, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %18

; <label>:18                                      ; preds = %191, %17
  %19 = load i32* %i, align 4
  %20 = sext i32 %19 to i64
  %21 = load i8** %1, align 8
  %22 = getelementptr inbounds i8* %21, i64 %20
  %23 = load i8* %22, align 1
  store i8 %23, i8* %c, align 1
  %24 = sext i8 %23 to i32
  %25 = icmp ne i32 %24, 0
  br i1 %25, label %26, label %196

; <label>:26                                      ; preds = %18
  %27 = load i8* %c, align 1
  %28 = sext i8 %27 to i32
  %29 = icmp eq i32 %28, 92
  br i1 %29, label %30, label %185

; <label>:30                                      ; preds = %26
  %31 = load i32* %i, align 4
  %32 = add nsw i32 %31, 1
  %33 = sext i32 %32 to i64
  %34 = load i8** %1, align 8
  %35 = getelementptr inbounds i8* %34, i64 %33
  %36 = load i8* %35, align 1
  %37 = sext i8 %36 to i32
  %38 = icmp ne i32 %37, 0
  br i1 %38, label %39, label %185

; <label>:39                                      ; preds = %30
  %40 = load i32* %i, align 4
  %41 = add nsw i32 %40, 1
  store i32 %41, i32* %i, align 4
  %42 = sext i32 %41 to i64
  %43 = load i8** %1, align 8
  %44 = getelementptr inbounds i8* %43, i64 %42
  %45 = load i8* %44, align 1
  store i8 %45, i8* %c, align 1
  %46 = load i8* %c, align 1
  %47 = sext i8 %46 to i32
  %48 = icmp eq i32 %47, 97
  br i1 %48, label %49, label %50

; <label>:49                                      ; preds = %39
  store i8 7, i8* %c, align 1
  br label %184

; <label>:50                                      ; preds = %39
  %51 = load i8* %c, align 1
  %52 = sext i8 %51 to i32
  %53 = icmp eq i32 %52, 98
  br i1 %53, label %54, label %55

; <label>:54                                      ; preds = %50
  store i8 8, i8* %c, align 1
  br label %183

; <label>:55                                      ; preds = %50
  %56 = load i8* %c, align 1
  %57 = sext i8 %56 to i32
  %58 = icmp eq i32 %57, 116
  br i1 %58, label %59, label %60

; <label>:59                                      ; preds = %55
  store i8 9, i8* %c, align 1
  br label %182

; <label>:60                                      ; preds = %55
  %61 = load i8* %c, align 1
  %62 = sext i8 %61 to i32
  %63 = icmp eq i32 %62, 110
  br i1 %63, label %64, label %65

; <label>:64                                      ; preds = %60
  store i8 10, i8* %c, align 1
  br label %181

; <label>:65                                      ; preds = %60
  %66 = load i8* %c, align 1
  %67 = sext i8 %66 to i32
  %68 = icmp eq i32 %67, 118
  br i1 %68, label %69, label %70

; <label>:69                                      ; preds = %65
  store i8 11, i8* %c, align 1
  br label %180

; <label>:70                                      ; preds = %65
  %71 = load i8* %c, align 1
  %72 = sext i8 %71 to i32
  %73 = icmp eq i32 %72, 102
  br i1 %73, label %74, label %75

; <label>:74                                      ; preds = %70
  store i8 12, i8* %c, align 1
  br label %179

; <label>:75                                      ; preds = %70
  %76 = load i8* %c, align 1
  %77 = sext i8 %76 to i32
  %78 = icmp eq i32 %77, 114
  br i1 %78, label %79, label %80

; <label>:79                                      ; preds = %75
  store i8 13, i8* %c, align 1
  br label %178

; <label>:80                                      ; preds = %75
  %81 = load i8* %c, align 1
  %82 = sext i8 %81 to i32
  %83 = icmp eq i32 %82, 34
  br i1 %83, label %84, label %85

; <label>:84                                      ; preds = %80
  store i8 34, i8* %c, align 1
  br label %177

; <label>:85                                      ; preds = %80
  %86 = load i8* %c, align 1
  %87 = sext i8 %86 to i32
  %88 = icmp eq i32 %87, 39
  br i1 %88, label %89, label %90

; <label>:89                                      ; preds = %85
  store i8 39, i8* %c, align 1
  br label %176

; <label>:90                                      ; preds = %85
  %91 = load i8* %c, align 1
  %92 = sext i8 %91 to i32
  %93 = icmp eq i32 %92, 92
  br i1 %93, label %94, label %95

; <label>:94                                      ; preds = %90
  store i8 92, i8* %c, align 1
  br label %175

; <label>:95                                      ; preds = %90
  %96 = load i8* %c, align 1
  %97 = sext i8 %96 to i32
  %98 = icmp sge i32 %97, 48
  br i1 %98, label %99, label %174

; <label>:99                                      ; preds = %95
  %100 = load i8* %c, align 1
  %101 = sext i8 %100 to i32
  %102 = icmp sle i32 %101, 55
  br i1 %102, label %103, label %174

; <label>:103                                     ; preds = %99
  %104 = load i8* %c, align 1
  %105 = sext i8 %104 to i32
  %106 = sub nsw i32 %105, 48
  %107 = trunc i32 %106 to i8
  store i8 %107, i8* %c, align 1
  %108 = load i32* %i, align 4
  %109 = add nsw i32 %108, 1
  %110 = sext i32 %109 to i64
  %111 = load i8** %1, align 8
  %112 = getelementptr inbounds i8* %111, i64 %110
  %113 = load i8* %112, align 1
  %114 = sext i8 %113 to i32
  %115 = icmp sge i32 %114, 48
  br i1 %115, label %116, label %173

; <label>:116                                     ; preds = %103
  %117 = load i32* %i, align 4
  %118 = add nsw i32 %117, 1
  %119 = sext i32 %118 to i64
  %120 = load i8** %1, align 8
  %121 = getelementptr inbounds i8* %120, i64 %119
  %122 = load i8* %121, align 1
  %123 = sext i8 %122 to i32
  %124 = icmp sle i32 %123, 55
  br i1 %124, label %125, label %173

; <label>:125                                     ; preds = %116
  %126 = load i32* %i, align 4
  %127 = add nsw i32 %126, 1
  store i32 %127, i32* %i, align 4
  %128 = load i8* %c, align 1
  %129 = sext i8 %128 to i32
  %130 = shl i32 %129, 3
  %131 = load i32* %i, align 4
  %132 = sext i32 %131 to i64
  %133 = load i8** %1, align 8
  %134 = getelementptr inbounds i8* %133, i64 %132
  %135 = load i8* %134, align 1
  %136 = sext i8 %135 to i32
  %137 = add nsw i32 %130, %136
  %138 = sub nsw i32 %137, 48
  %139 = trunc i32 %138 to i8
  store i8 %139, i8* %c, align 1
  %140 = load i32* %i, align 4
  %141 = add nsw i32 %140, 1
  %142 = sext i32 %141 to i64
  %143 = load i8** %1, align 8
  %144 = getelementptr inbounds i8* %143, i64 %142
  %145 = load i8* %144, align 1
  %146 = sext i8 %145 to i32
  %147 = icmp sge i32 %146, 48
  br i1 %147, label %148, label %172

; <label>:148                                     ; preds = %125
  %149 = load i32* %i, align 4
  %150 = add nsw i32 %149, 1
  %151 = sext i32 %150 to i64
  %152 = load i8** %1, align 8
  %153 = getelementptr inbounds i8* %152, i64 %151
  %154 = load i8* %153, align 1
  %155 = sext i8 %154 to i32
  %156 = icmp sle i32 %155, 55
  br i1 %156, label %157, label %172

; <label>:157                                     ; preds = %148
  %158 = load i32* %i, align 4
  %159 = add nsw i32 %158, 1
  store i32 %159, i32* %i, align 4
  %160 = load i8* %c, align 1
  %161 = sext i8 %160 to i32
  %162 = shl i32 %161, 3
  %163 = load i32* %i, align 4
  %164 = sext i32 %163 to i64
  %165 = load i8** %1, align 8
  %166 = getelementptr inbounds i8* %165, i64 %164
  %167 = load i8* %166, align 1
  %168 = sext i8 %167 to i32
  %169 = add nsw i32 %162, %168
  %170 = sub nsw i32 %169, 48
  %171 = trunc i32 %170 to i8
  store i8 %171, i8* %c, align 1
  br label %172

; <label>:172                                     ; preds = %157, %148, %125
  br label %173

; <label>:173                                     ; preds = %172, %116, %103
  br label %174

; <label>:174                                     ; preds = %173, %99, %95
  br label %175

; <label>:175                                     ; preds = %174, %94
  br label %176

; <label>:176                                     ; preds = %175, %89
  br label %177

; <label>:177                                     ; preds = %176, %84
  br label %178

; <label>:178                                     ; preds = %177, %79
  br label %179

; <label>:179                                     ; preds = %178, %74
  br label %180

; <label>:180                                     ; preds = %179, %69
  br label %181

; <label>:181                                     ; preds = %180, %64
  br label %182

; <label>:182                                     ; preds = %181, %59
  br label %183

; <label>:183                                     ; preds = %182, %54
  br label %184

; <label>:184                                     ; preds = %183, %49
  br label %185

; <label>:185                                     ; preds = %184, %30, %26
  %186 = load i8* %c, align 1
  %187 = load i32* %j, align 4
  %188 = sext i32 %187 to i64
  %189 = load i8** %1, align 8
  %190 = getelementptr inbounds i8* %189, i64 %188
  store i8 %186, i8* %190, align 1
  br label %191

; <label>:191                                     ; preds = %185
  %192 = load i32* %i, align 4
  %193 = add nsw i32 %192, 1
  store i32 %193, i32* %i, align 4
  %194 = load i32* %j, align 4
  %195 = add nsw i32 %194, 1
  store i32 %195, i32* %j, align 4
  br label %18

; <label>:196                                     ; preds = %18
  %197 = load i32* %j, align 4
  %198 = load i32* %i, align 4
  %199 = icmp slt i32 %197, %198
  br i1 %199, label %200, label %205

; <label>:200                                     ; preds = %196
  %201 = load i32* %j, align 4
  %202 = sext i32 %201 to i64
  %203 = load i8** %1, align 8
  %204 = getelementptr inbounds i8* %203, i64 %202
  store i8 0, i8* %204, align 1
  br label %205

; <label>:205                                     ; preds = %200, %196
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @beginTimer() #0 {
  %1 = load i32* @enableTimer, align 4
  %2 = icmp ne i32 %1, 0
  br i1 %2, label %3, label %6

; <label>:3                                       ; preds = %0
  %4 = call i32 @getrusage(i32 0, %struct.rusage* @sBegin) #5
  %5 = call i64 @timeOfDay()
  store i64 %5, i64* @iBegin, align 8
  br label %6

; <label>:6                                       ; preds = %3, %0
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @endTimer() #0 {
  %iEnd = alloca i64, align 8
  %sEnd = alloca %struct.rusage, align 8
  %1 = load i32* @enableTimer, align 4
  %2 = icmp ne i32 %1, 0
  br i1 %2, label %3, label %16

; <label>:3                                       ; preds = %0
  %4 = call i64 @timeOfDay()
  store i64 %4, i64* %iEnd, align 8
  %5 = call i32 @getrusage(i32 0, %struct.rusage* %sEnd) #5
  %6 = load i64* %iEnd, align 8
  %7 = load i64* @iBegin, align 8
  %8 = sub nsw i64 %6, %7
  %9 = sitofp i64 %8 to double
  %10 = fmul double %9, 1.000000e-03
  %11 = getelementptr inbounds %struct.rusage* %sEnd, i32 0, i32 0
  %12 = call double @timeDiff(%struct.timeval* getelementptr inbounds (%struct.rusage* @sBegin, i32 0, i32 0), %struct.timeval* %11)
  %13 = getelementptr inbounds %struct.rusage* %sEnd, i32 0, i32 1
  %14 = call double @timeDiff(%struct.timeval* getelementptr inbounds (%struct.rusage* @sBegin, i32 0, i32 1), %struct.timeval* %13)
  %15 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([36 x i8]* @.str58, i32 0, i32 0), double %10, double %12, double %14)
  br label %16

; <label>:16                                      ; preds = %3, %0
  ret void
}

declare i8* @sqlite3_errmsg(%struct.sqlite3*) #2

; Function Attrs: nounwind uwtable
define internal void @output_reset(%struct.ShellState* %p) #0 {
  %1 = alloca %struct.ShellState*, align 8
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  %2 = load %struct.ShellState** %1, align 8
  %3 = getelementptr inbounds %struct.ShellState* %2, i32 0, i32 22
  %4 = getelementptr inbounds [4096 x i8]* %3, i32 0, i64 0
  %5 = load i8* %4, align 1
  %6 = sext i8 %5 to i32
  %7 = icmp eq i32 %6, 124
  br i1 %7, label %8, label %13

; <label>:8                                       ; preds = %0
  %9 = load %struct.ShellState** %1, align 8
  %10 = getelementptr inbounds %struct.ShellState* %9, i32 0, i32 8
  %11 = load %struct._IO_FILE** %10, align 8
  %12 = call i32 @pclose(%struct._IO_FILE* %11)
  br label %17

; <label>:13                                      ; preds = %0
  %14 = load %struct.ShellState** %1, align 8
  %15 = getelementptr inbounds %struct.ShellState* %14, i32 0, i32 8
  %16 = load %struct._IO_FILE** %15, align 8
  call void @output_file_close(%struct._IO_FILE* %16)
  br label %17

; <label>:17                                      ; preds = %13, %8
  %18 = load %struct.ShellState** %1, align 8
  %19 = getelementptr inbounds %struct.ShellState* %18, i32 0, i32 22
  %20 = getelementptr inbounds [4096 x i8]* %19, i32 0, i64 0
  store i8 0, i8* %20, align 1
  %21 = load %struct._IO_FILE** @stdout, align 8
  %22 = load %struct.ShellState** %1, align 8
  %23 = getelementptr inbounds %struct.ShellState* %22, i32 0, i32 8
  store %struct._IO_FILE* %21, %struct._IO_FILE** %23, align 8
  ret void
}

declare i32 @pclose(%struct._IO_FILE*) #2

; Function Attrs: nounwind uwtable
define internal void @output_file_close(%struct._IO_FILE* %f) #0 {
  %1 = alloca %struct._IO_FILE*, align 8
  store %struct._IO_FILE* %f, %struct._IO_FILE** %1, align 8
  %2 = load %struct._IO_FILE** %1, align 8
  %3 = icmp ne %struct._IO_FILE* %2, null
  br i1 %3, label %4, label %15

; <label>:4                                       ; preds = %0
  %5 = load %struct._IO_FILE** %1, align 8
  %6 = load %struct._IO_FILE** @stdout, align 8
  %7 = icmp ne %struct._IO_FILE* %5, %6
  br i1 %7, label %8, label %15

; <label>:8                                       ; preds = %4
  %9 = load %struct._IO_FILE** %1, align 8
  %10 = load %struct._IO_FILE** @stderr, align 8
  %11 = icmp ne %struct._IO_FILE* %9, %10
  br i1 %11, label %12, label %15

; <label>:12                                      ; preds = %8
  %13 = load %struct._IO_FILE** %1, align 8
  %14 = call i32 @fclose(%struct._IO_FILE* %13)
  br label %15

; <label>:15                                      ; preds = %12, %8, %4, %0
  ret void
}

declare i32 @fclose(%struct._IO_FILE*) #2

; Function Attrs: nounwind uwtable
define internal i64 @timeOfDay() #0 {
  %t = alloca i64, align 8
  %r = alloca double, align 8
  %1 = load %struct.sqlite3_vfs** @timeOfDay.clockVfs, align 8
  %2 = icmp eq %struct.sqlite3_vfs* %1, null
  br i1 %2, label %3, label %5

; <label>:3                                       ; preds = %0
  %4 = call %struct.sqlite3_vfs* @sqlite3_vfs_find(i8* null)
  store %struct.sqlite3_vfs* %4, %struct.sqlite3_vfs** @timeOfDay.clockVfs, align 8
  br label %5

; <label>:5                                       ; preds = %3, %0
  %6 = load %struct.sqlite3_vfs** @timeOfDay.clockVfs, align 8
  %7 = getelementptr inbounds %struct.sqlite3_vfs* %6, i32 0, i32 0
  %8 = load i32* %7, align 4
  %9 = icmp sge i32 %8, 1
  br i1 %9, label %10, label %21

; <label>:10                                      ; preds = %5
  %11 = load %struct.sqlite3_vfs** @timeOfDay.clockVfs, align 8
  %12 = getelementptr inbounds %struct.sqlite3_vfs* %11, i32 0, i32 18
  %13 = load i32 (%struct.sqlite3_vfs*, i64*)** %12, align 8
  %14 = icmp ne i32 (%struct.sqlite3_vfs*, i64*)* %13, null
  br i1 %14, label %15, label %21

; <label>:15                                      ; preds = %10
  %16 = load %struct.sqlite3_vfs** @timeOfDay.clockVfs, align 8
  %17 = getelementptr inbounds %struct.sqlite3_vfs* %16, i32 0, i32 18
  %18 = load i32 (%struct.sqlite3_vfs*, i64*)** %17, align 8
  %19 = load %struct.sqlite3_vfs** @timeOfDay.clockVfs, align 8
  %20 = call i32 %18(%struct.sqlite3_vfs* %19, i64* %t)
  br label %30

; <label>:21                                      ; preds = %10, %5
  %22 = load %struct.sqlite3_vfs** @timeOfDay.clockVfs, align 8
  %23 = getelementptr inbounds %struct.sqlite3_vfs* %22, i32 0, i32 16
  %24 = load i32 (%struct.sqlite3_vfs*, double*)** %23, align 8
  %25 = load %struct.sqlite3_vfs** @timeOfDay.clockVfs, align 8
  %26 = call i32 %24(%struct.sqlite3_vfs* %25, double* %r)
  %27 = load double* %r, align 8
  %28 = fmul double %27, 8.640000e+07
  %29 = fptosi double %28 to i64
  store i64 %29, i64* %t, align 8
  br label %30

; <label>:30                                      ; preds = %21, %15
  %31 = load i64* %t, align 8
  ret i64 %31
}

; Function Attrs: nounwind
declare i32 @getrusage(i32, %struct.rusage*) #4

; Function Attrs: nounwind uwtable
define internal double @timeDiff(%struct.timeval* %pStart, %struct.timeval* %pEnd) #0 {
  %1 = alloca %struct.timeval*, align 8
  %2 = alloca %struct.timeval*, align 8
  store %struct.timeval* %pStart, %struct.timeval** %1, align 8
  store %struct.timeval* %pEnd, %struct.timeval** %2, align 8
  %3 = load %struct.timeval** %2, align 8
  %4 = getelementptr inbounds %struct.timeval* %3, i32 0, i32 1
  %5 = load i64* %4, align 8
  %6 = load %struct.timeval** %1, align 8
  %7 = getelementptr inbounds %struct.timeval* %6, i32 0, i32 1
  %8 = load i64* %7, align 8
  %9 = sub nsw i64 %5, %8
  %10 = sitofp i64 %9 to double
  %11 = fmul double %10, 1.000000e-06
  %12 = load %struct.timeval** %2, align 8
  %13 = getelementptr inbounds %struct.timeval* %12, i32 0, i32 0
  %14 = load i64* %13, align 8
  %15 = load %struct.timeval** %1, align 8
  %16 = getelementptr inbounds %struct.timeval* %15, i32 0, i32 0
  %17 = load i64* %16, align 8
  %18 = sub nsw i64 %14, %17
  %19 = sitofp i64 %18 to double
  %20 = fadd double %11, %19
  ret double %20
}

; Function Attrs: nounwind
declare i32 @tolower(i32) #4

; Function Attrs: nounwind uwtable
define internal i8* @local_getline(i8* %zLine, %struct._IO_FILE* %in) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca %struct._IO_FILE*, align 8
  %nLine = alloca i32, align 4
  %n = alloca i32, align 4
  store i8* %zLine, i8** %2, align 8
  store %struct._IO_FILE* %in, %struct._IO_FILE** %3, align 8
  %4 = load i8** %2, align 8
  %5 = icmp eq i8* %4, null
  %6 = select i1 %5, i32 0, i32 100
  store i32 %6, i32* %nLine, align 4
  store i32 0, i32* %n, align 4
  br label %7

; <label>:7                                       ; preds = %0, %90
  %8 = load i32* %n, align 4
  %9 = add nsw i32 %8, 100
  %10 = load i32* %nLine, align 4
  %11 = icmp sgt i32 %9, %10
  br i1 %11, label %12, label %24

; <label>:12                                      ; preds = %7
  %13 = load i32* %nLine, align 4
  %14 = mul nsw i32 %13, 2
  %15 = add nsw i32 %14, 100
  store i32 %15, i32* %nLine, align 4
  %16 = load i8** %2, align 8
  %17 = load i32* %nLine, align 4
  %18 = sext i32 %17 to i64
  %19 = call i8* @realloc(i8* %16, i64 %18) #5
  store i8* %19, i8** %2, align 8
  %20 = load i8** %2, align 8
  %21 = icmp eq i8* %20, null
  br i1 %21, label %22, label %23

; <label>:22                                      ; preds = %12
  store i8* null, i8** %1
  br label %93

; <label>:23                                      ; preds = %12
  br label %24

; <label>:24                                      ; preds = %23, %7
  %25 = load i32* %n, align 4
  %26 = sext i32 %25 to i64
  %27 = load i8** %2, align 8
  %28 = getelementptr inbounds i8* %27, i64 %26
  %29 = load i32* %nLine, align 4
  %30 = load i32* %n, align 4
  %31 = sub nsw i32 %29, %30
  %32 = load %struct._IO_FILE** %3, align 8
  %33 = call i8* @fgets(i8* %28, i32 %31, %struct._IO_FILE* %32)
  %34 = icmp eq i8* %33, null
  br i1 %34, label %35, label %45

; <label>:35                                      ; preds = %24
  %36 = load i32* %n, align 4
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %38, label %40

; <label>:38                                      ; preds = %35
  %39 = load i8** %2, align 8
  call void @free(i8* %39) #5
  store i8* null, i8** %1
  br label %93

; <label>:40                                      ; preds = %35
  %41 = load i32* %n, align 4
  %42 = sext i32 %41 to i64
  %43 = load i8** %2, align 8
  %44 = getelementptr inbounds i8* %43, i64 %42
  store i8 0, i8* %44, align 1
  br label %91

; <label>:45                                      ; preds = %24
  br label %46

; <label>:46                                      ; preds = %53, %45
  %47 = load i32* %n, align 4
  %48 = sext i32 %47 to i64
  %49 = load i8** %2, align 8
  %50 = getelementptr inbounds i8* %49, i64 %48
  %51 = load i8* %50, align 1
  %52 = icmp ne i8 %51, 0
  br i1 %52, label %53, label %56

; <label>:53                                      ; preds = %46
  %54 = load i32* %n, align 4
  %55 = add nsw i32 %54, 1
  store i32 %55, i32* %n, align 4
  br label %46

; <label>:56                                      ; preds = %46
  %57 = load i32* %n, align 4
  %58 = icmp sgt i32 %57, 0
  br i1 %58, label %59, label %90

; <label>:59                                      ; preds = %56
  %60 = load i32* %n, align 4
  %61 = sub nsw i32 %60, 1
  %62 = sext i32 %61 to i64
  %63 = load i8** %2, align 8
  %64 = getelementptr inbounds i8* %63, i64 %62
  %65 = load i8* %64, align 1
  %66 = sext i8 %65 to i32
  %67 = icmp eq i32 %66, 10
  br i1 %67, label %68, label %90

; <label>:68                                      ; preds = %59
  %69 = load i32* %n, align 4
  %70 = add nsw i32 %69, -1
  store i32 %70, i32* %n, align 4
  %71 = load i32* %n, align 4
  %72 = icmp sgt i32 %71, 0
  br i1 %72, label %73, label %85

; <label>:73                                      ; preds = %68
  %74 = load i32* %n, align 4
  %75 = sub nsw i32 %74, 1
  %76 = sext i32 %75 to i64
  %77 = load i8** %2, align 8
  %78 = getelementptr inbounds i8* %77, i64 %76
  %79 = load i8* %78, align 1
  %80 = sext i8 %79 to i32
  %81 = icmp eq i32 %80, 13
  br i1 %81, label %82, label %85

; <label>:82                                      ; preds = %73
  %83 = load i32* %n, align 4
  %84 = add nsw i32 %83, -1
  store i32 %84, i32* %n, align 4
  br label %85

; <label>:85                                      ; preds = %82, %73, %68
  %86 = load i32* %n, align 4
  %87 = sext i32 %86 to i64
  %88 = load i8** %2, align 8
  %89 = getelementptr inbounds i8* %88, i64 %87
  store i8 0, i8* %89, align 1
  br label %91

; <label>:90                                      ; preds = %59, %56
  br label %7

; <label>:91                                      ; preds = %85, %40
  %92 = load i8** %2, align 8
  store i8* %92, i8** %1
  br label %93

; <label>:93                                      ; preds = %91, %38, %22
  %94 = load i8** %1
  ret i8* %94
}

declare i8* @fgets(i8*, i32, %struct._IO_FILE*) #2

; Function Attrs: nounwind
declare i32 @getuid() #4

declare %struct.passwd* @getpwuid(i32) #2

; Function Attrs: nounwind
declare i8* @getenv(i8*) #4

; Function Attrs: nounwind uwtable
define internal void @output_html_string(%struct._IO_FILE* %out, i8* %z) #0 {
  %1 = alloca %struct._IO_FILE*, align 8
  %2 = alloca i8*, align 8
  %i = alloca i32, align 4
  store %struct._IO_FILE* %out, %struct._IO_FILE** %1, align 8
  store i8* %z, i8** %2, align 8
  %3 = load i8** %2, align 8
  %4 = icmp eq i8* %3, null
  br i1 %4, label %5, label %6

; <label>:5                                       ; preds = %0
  store i8* getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), i8** %2, align 8
  br label %6

; <label>:6                                       ; preds = %5, %0
  br label %7

; <label>:7                                       ; preds = %134, %6
  %8 = load i8** %2, align 8
  %9 = load i8* %8, align 1
  %10 = icmp ne i8 %9, 0
  br i1 %10, label %11, label %140

; <label>:11                                      ; preds = %7
  store i32 0, i32* %i, align 4
  br label %12

; <label>:12                                      ; preds = %63, %11
  %13 = load i32* %i, align 4
  %14 = sext i32 %13 to i64
  %15 = load i8** %2, align 8
  %16 = getelementptr inbounds i8* %15, i64 %14
  %17 = load i8* %16, align 1
  %18 = sext i8 %17 to i32
  %19 = icmp ne i32 %18, 0
  br i1 %19, label %20, label %60

; <label>:20                                      ; preds = %12
  %21 = load i32* %i, align 4
  %22 = sext i32 %21 to i64
  %23 = load i8** %2, align 8
  %24 = getelementptr inbounds i8* %23, i64 %22
  %25 = load i8* %24, align 1
  %26 = sext i8 %25 to i32
  %27 = icmp ne i32 %26, 60
  br i1 %27, label %28, label %60

; <label>:28                                      ; preds = %20
  %29 = load i32* %i, align 4
  %30 = sext i32 %29 to i64
  %31 = load i8** %2, align 8
  %32 = getelementptr inbounds i8* %31, i64 %30
  %33 = load i8* %32, align 1
  %34 = sext i8 %33 to i32
  %35 = icmp ne i32 %34, 38
  br i1 %35, label %36, label %60

; <label>:36                                      ; preds = %28
  %37 = load i32* %i, align 4
  %38 = sext i32 %37 to i64
  %39 = load i8** %2, align 8
  %40 = getelementptr inbounds i8* %39, i64 %38
  %41 = load i8* %40, align 1
  %42 = sext i8 %41 to i32
  %43 = icmp ne i32 %42, 62
  br i1 %43, label %44, label %60

; <label>:44                                      ; preds = %36
  %45 = load i32* %i, align 4
  %46 = sext i32 %45 to i64
  %47 = load i8** %2, align 8
  %48 = getelementptr inbounds i8* %47, i64 %46
  %49 = load i8* %48, align 1
  %50 = sext i8 %49 to i32
  %51 = icmp ne i32 %50, 34
  br i1 %51, label %52, label %60

; <label>:52                                      ; preds = %44
  %53 = load i32* %i, align 4
  %54 = sext i32 %53 to i64
  %55 = load i8** %2, align 8
  %56 = getelementptr inbounds i8* %55, i64 %54
  %57 = load i8* %56, align 1
  %58 = sext i8 %57 to i32
  %59 = icmp ne i32 %58, 39
  br label %60

; <label>:60                                      ; preds = %52, %44, %36, %28, %20, %12
  %61 = phi i1 [ false, %44 ], [ false, %36 ], [ false, %28 ], [ false, %20 ], [ false, %12 ], [ %59, %52 ]
  br i1 %61, label %62, label %66

; <label>:62                                      ; preds = %60
  br label %63

; <label>:63                                      ; preds = %62
  %64 = load i32* %i, align 4
  %65 = add nsw i32 %64, 1
  store i32 %65, i32* %i, align 4
  br label %12

; <label>:66                                      ; preds = %60
  %67 = load i32* %i, align 4
  %68 = icmp sgt i32 %67, 0
  br i1 %68, label %69, label %74

; <label>:69                                      ; preds = %66
  %70 = load %struct._IO_FILE** %1, align 8
  %71 = load i32* %i, align 4
  %72 = load i8** %2, align 8
  %73 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %70, i8* getelementptr inbounds ([5 x i8]* @.str89, i32 0, i32 0), i32 %71, i8* %72)
  br label %74

; <label>:74                                      ; preds = %69, %66
  %75 = load i32* %i, align 4
  %76 = sext i32 %75 to i64
  %77 = load i8** %2, align 8
  %78 = getelementptr inbounds i8* %77, i64 %76
  %79 = load i8* %78, align 1
  %80 = sext i8 %79 to i32
  %81 = icmp eq i32 %80, 60
  br i1 %81, label %82, label %85

; <label>:82                                      ; preds = %74
  %83 = load %struct._IO_FILE** %1, align 8
  %84 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %83, i8* getelementptr inbounds ([5 x i8]* @.str90, i32 0, i32 0))
  br label %134

; <label>:85                                      ; preds = %74
  %86 = load i32* %i, align 4
  %87 = sext i32 %86 to i64
  %88 = load i8** %2, align 8
  %89 = getelementptr inbounds i8* %88, i64 %87
  %90 = load i8* %89, align 1
  %91 = sext i8 %90 to i32
  %92 = icmp eq i32 %91, 38
  br i1 %92, label %93, label %96

; <label>:93                                      ; preds = %85
  %94 = load %struct._IO_FILE** %1, align 8
  %95 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %94, i8* getelementptr inbounds ([6 x i8]* @.str91, i32 0, i32 0))
  br label %133

; <label>:96                                      ; preds = %85
  %97 = load i32* %i, align 4
  %98 = sext i32 %97 to i64
  %99 = load i8** %2, align 8
  %100 = getelementptr inbounds i8* %99, i64 %98
  %101 = load i8* %100, align 1
  %102 = sext i8 %101 to i32
  %103 = icmp eq i32 %102, 62
  br i1 %103, label %104, label %107

; <label>:104                                     ; preds = %96
  %105 = load %struct._IO_FILE** %1, align 8
  %106 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %105, i8* getelementptr inbounds ([5 x i8]* @.str92, i32 0, i32 0))
  br label %132

; <label>:107                                     ; preds = %96
  %108 = load i32* %i, align 4
  %109 = sext i32 %108 to i64
  %110 = load i8** %2, align 8
  %111 = getelementptr inbounds i8* %110, i64 %109
  %112 = load i8* %111, align 1
  %113 = sext i8 %112 to i32
  %114 = icmp eq i32 %113, 34
  br i1 %114, label %115, label %118

; <label>:115                                     ; preds = %107
  %116 = load %struct._IO_FILE** %1, align 8
  %117 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %116, i8* getelementptr inbounds ([7 x i8]* @.str93, i32 0, i32 0))
  br label %131

; <label>:118                                     ; preds = %107
  %119 = load i32* %i, align 4
  %120 = sext i32 %119 to i64
  %121 = load i8** %2, align 8
  %122 = getelementptr inbounds i8* %121, i64 %120
  %123 = load i8* %122, align 1
  %124 = sext i8 %123 to i32
  %125 = icmp eq i32 %124, 39
  br i1 %125, label %126, label %129

; <label>:126                                     ; preds = %118
  %127 = load %struct._IO_FILE** %1, align 8
  %128 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %127, i8* getelementptr inbounds ([6 x i8]* @.str94, i32 0, i32 0))
  br label %130

; <label>:129                                     ; preds = %118
  br label %140

; <label>:130                                     ; preds = %126
  br label %131

; <label>:131                                     ; preds = %130, %115
  br label %132

; <label>:132                                     ; preds = %131, %104
  br label %133

; <label>:133                                     ; preds = %132, %93
  br label %134

; <label>:134                                     ; preds = %133, %82
  %135 = load i32* %i, align 4
  %136 = add nsw i32 %135, 1
  %137 = load i8** %2, align 8
  %138 = sext i32 %136 to i64
  %139 = getelementptr inbounds i8* %137, i64 %138
  store i8* %139, i8** %2, align 8
  br label %7

; <label>:140                                     ; preds = %129, %7
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @output_c_string(%struct._IO_FILE* %out, i8* %z) #0 {
  %1 = alloca %struct._IO_FILE*, align 8
  %2 = alloca i8*, align 8
  %c = alloca i32, align 4
  store %struct._IO_FILE* %out, %struct._IO_FILE** %1, align 8
  store i8* %z, i8** %2, align 8
  %3 = load %struct._IO_FILE** %1, align 8
  %4 = call i32 @fputc(i32 34, %struct._IO_FILE* %3)
  br label %5

; <label>:5                                       ; preds = %78, %0
  %6 = load i8** %2, align 8
  %7 = getelementptr inbounds i8* %6, i32 1
  store i8* %7, i8** %2, align 8
  %8 = load i8* %6, align 1
  %9 = sext i8 %8 to i32
  store i32 %9, i32* %c, align 4
  %10 = icmp ne i32 %9, 0
  br i1 %10, label %11, label %79

; <label>:11                                      ; preds = %5
  %12 = load i32* %c, align 4
  %13 = icmp eq i32 %12, 92
  br i1 %13, label %14, label %21

; <label>:14                                      ; preds = %11
  %15 = load i32* %c, align 4
  %16 = load %struct._IO_FILE** %1, align 8
  %17 = call i32 @fputc(i32 %15, %struct._IO_FILE* %16)
  %18 = load i32* %c, align 4
  %19 = load %struct._IO_FILE** %1, align 8
  %20 = call i32 @fputc(i32 %18, %struct._IO_FILE* %19)
  br label %78

; <label>:21                                      ; preds = %11
  %22 = load i32* %c, align 4
  %23 = icmp eq i32 %22, 34
  br i1 %23, label %24, label %29

; <label>:24                                      ; preds = %21
  %25 = load %struct._IO_FILE** %1, align 8
  %26 = call i32 @fputc(i32 92, %struct._IO_FILE* %25)
  %27 = load %struct._IO_FILE** %1, align 8
  %28 = call i32 @fputc(i32 34, %struct._IO_FILE* %27)
  br label %77

; <label>:29                                      ; preds = %21
  %30 = load i32* %c, align 4
  %31 = icmp eq i32 %30, 9
  br i1 %31, label %32, label %37

; <label>:32                                      ; preds = %29
  %33 = load %struct._IO_FILE** %1, align 8
  %34 = call i32 @fputc(i32 92, %struct._IO_FILE* %33)
  %35 = load %struct._IO_FILE** %1, align 8
  %36 = call i32 @fputc(i32 116, %struct._IO_FILE* %35)
  br label %76

; <label>:37                                      ; preds = %29
  %38 = load i32* %c, align 4
  %39 = icmp eq i32 %38, 10
  br i1 %39, label %40, label %45

; <label>:40                                      ; preds = %37
  %41 = load %struct._IO_FILE** %1, align 8
  %42 = call i32 @fputc(i32 92, %struct._IO_FILE* %41)
  %43 = load %struct._IO_FILE** %1, align 8
  %44 = call i32 @fputc(i32 110, %struct._IO_FILE* %43)
  br label %75

; <label>:45                                      ; preds = %37
  %46 = load i32* %c, align 4
  %47 = icmp eq i32 %46, 13
  br i1 %47, label %48, label %53

; <label>:48                                      ; preds = %45
  %49 = load %struct._IO_FILE** %1, align 8
  %50 = call i32 @fputc(i32 92, %struct._IO_FILE* %49)
  %51 = load %struct._IO_FILE** %1, align 8
  %52 = call i32 @fputc(i32 114, %struct._IO_FILE* %51)
  br label %74

; <label>:53                                      ; preds = %45
  %54 = load i32* %c, align 4
  %55 = and i32 %54, 255
  %56 = sext i32 %55 to i64
  %57 = call i16** @__ctype_b_loc() #9
  %58 = load i16** %57, align 8
  %59 = getelementptr inbounds i16* %58, i64 %56
  %60 = load i16* %59, align 2
  %61 = zext i16 %60 to i32
  %62 = and i32 %61, 16384
  %63 = icmp ne i32 %62, 0
  br i1 %63, label %69, label %64

; <label>:64                                      ; preds = %53
  %65 = load %struct._IO_FILE** %1, align 8
  %66 = load i32* %c, align 4
  %67 = and i32 %66, 255
  %68 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %65, i8* getelementptr inbounds ([6 x i8]* @.str88, i32 0, i32 0), i32 %67)
  br label %73

; <label>:69                                      ; preds = %53
  %70 = load i32* %c, align 4
  %71 = load %struct._IO_FILE** %1, align 8
  %72 = call i32 @fputc(i32 %70, %struct._IO_FILE* %71)
  br label %73

; <label>:73                                      ; preds = %69, %64
  br label %74

; <label>:74                                      ; preds = %73, %48
  br label %75

; <label>:75                                      ; preds = %74, %40
  br label %76

; <label>:76                                      ; preds = %75, %32
  br label %77

; <label>:77                                      ; preds = %76, %24
  br label %78

; <label>:78                                      ; preds = %77, %14
  br label %5

; <label>:79                                      ; preds = %5
  %80 = load %struct._IO_FILE** %1, align 8
  %81 = call i32 @fputc(i32 34, %struct._IO_FILE* %80)
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @output_csv(%struct.ShellState* %p, i8* %z, i32 %bSep) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i32, align 4
  %out = alloca %struct._IO_FILE*, align 8
  %i = alloca i32, align 4
  %nSep = alloca i32, align 4
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store i8* %z, i8** %2, align 8
  store i32 %bSep, i32* %3, align 4
  %4 = load %struct.ShellState** %1, align 8
  %5 = getelementptr inbounds %struct.ShellState* %4, i32 0, i32 8
  %6 = load %struct._IO_FILE** %5, align 8
  store %struct._IO_FILE* %6, %struct._IO_FILE** %out, align 8
  %7 = load i8** %2, align 8
  %8 = icmp eq i8* %7, null
  br i1 %8, label %9, label %15

; <label>:9                                       ; preds = %0
  %10 = load %struct._IO_FILE** %out, align 8
  %11 = load %struct.ShellState** %1, align 8
  %12 = getelementptr inbounds %struct.ShellState* %11, i32 0, i32 20
  %13 = getelementptr inbounds [20 x i8]* %12, i32 0, i32 0
  %14 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %13)
  br label %112

; <label>:15                                      ; preds = %0
  %16 = load %struct.ShellState** %1, align 8
  %17 = getelementptr inbounds %struct.ShellState* %16, i32 0, i32 16
  %18 = getelementptr inbounds [20 x i8]* %17, i32 0, i32 0
  %19 = call i32 @strlen30(i8* %18)
  store i32 %19, i32* %nSep, align 4
  store i32 0, i32* %i, align 4
  br label %20

; <label>:20                                      ; preds = %65, %15
  %21 = load i32* %i, align 4
  %22 = sext i32 %21 to i64
  %23 = load i8** %2, align 8
  %24 = getelementptr inbounds i8* %23, i64 %22
  %25 = load i8* %24, align 1
  %26 = icmp ne i8 %25, 0
  br i1 %26, label %27, label %68

; <label>:27                                      ; preds = %20
  %28 = load i32* %i, align 4
  %29 = sext i32 %28 to i64
  %30 = load i8** %2, align 8
  %31 = getelementptr inbounds i8* %30, i64 %29
  %32 = load i8* %31, align 1
  %33 = zext i8 %32 to i64
  %34 = getelementptr inbounds [256 x i8]* @needCsvQuote, i32 0, i64 %33
  %35 = load i8* %34, align 1
  %36 = sext i8 %35 to i32
  %37 = icmp ne i32 %36, 0
  br i1 %37, label %63, label %38

; <label>:38                                      ; preds = %27
  %39 = load i32* %i, align 4
  %40 = sext i32 %39 to i64
  %41 = load i8** %2, align 8
  %42 = getelementptr inbounds i8* %41, i64 %40
  %43 = load i8* %42, align 1
  %44 = sext i8 %43 to i32
  %45 = load %struct.ShellState** %1, align 8
  %46 = getelementptr inbounds %struct.ShellState* %45, i32 0, i32 16
  %47 = getelementptr inbounds [20 x i8]* %46, i32 0, i64 0
  %48 = load i8* %47, align 1
  %49 = sext i8 %48 to i32
  %50 = icmp eq i32 %44, %49
  br i1 %50, label %51, label %64

; <label>:51                                      ; preds = %38
  %52 = load i32* %nSep, align 4
  %53 = icmp eq i32 %52, 1
  br i1 %53, label %63, label %54

; <label>:54                                      ; preds = %51
  %55 = load i8** %2, align 8
  %56 = load %struct.ShellState** %1, align 8
  %57 = getelementptr inbounds %struct.ShellState* %56, i32 0, i32 16
  %58 = getelementptr inbounds [20 x i8]* %57, i32 0, i32 0
  %59 = load i32* %nSep, align 4
  %60 = sext i32 %59 to i64
  %61 = call i32 @memcmp(i8* %55, i8* %58, i64 %60) #7
  %62 = icmp eq i32 %61, 0
  br i1 %62, label %63, label %64

; <label>:63                                      ; preds = %54, %51, %27
  store i32 0, i32* %i, align 4
  br label %68

; <label>:64                                      ; preds = %54, %38
  br label %65

; <label>:65                                      ; preds = %64
  %66 = load i32* %i, align 4
  %67 = add nsw i32 %66, 1
  store i32 %67, i32* %i, align 4
  br label %20

; <label>:68                                      ; preds = %63, %20
  %69 = load i32* %i, align 4
  %70 = icmp eq i32 %69, 0
  br i1 %70, label %71, label %107

; <label>:71                                      ; preds = %68
  %72 = load %struct._IO_FILE** %out, align 8
  %73 = call i32 @_IO_putc(i32 34, %struct._IO_FILE* %72)
  store i32 0, i32* %i, align 4
  br label %74

; <label>:74                                      ; preds = %101, %71
  %75 = load i32* %i, align 4
  %76 = sext i32 %75 to i64
  %77 = load i8** %2, align 8
  %78 = getelementptr inbounds i8* %77, i64 %76
  %79 = load i8* %78, align 1
  %80 = icmp ne i8 %79, 0
  br i1 %80, label %81, label %104

; <label>:81                                      ; preds = %74
  %82 = load i32* %i, align 4
  %83 = sext i32 %82 to i64
  %84 = load i8** %2, align 8
  %85 = getelementptr inbounds i8* %84, i64 %83
  %86 = load i8* %85, align 1
  %87 = sext i8 %86 to i32
  %88 = icmp eq i32 %87, 34
  br i1 %88, label %89, label %92

; <label>:89                                      ; preds = %81
  %90 = load %struct._IO_FILE** %out, align 8
  %91 = call i32 @_IO_putc(i32 34, %struct._IO_FILE* %90)
  br label %92

; <label>:92                                      ; preds = %89, %81
  %93 = load i32* %i, align 4
  %94 = sext i32 %93 to i64
  %95 = load i8** %2, align 8
  %96 = getelementptr inbounds i8* %95, i64 %94
  %97 = load i8* %96, align 1
  %98 = sext i8 %97 to i32
  %99 = load %struct._IO_FILE** %out, align 8
  %100 = call i32 @_IO_putc(i32 %98, %struct._IO_FILE* %99)
  br label %101

; <label>:101                                     ; preds = %92
  %102 = load i32* %i, align 4
  %103 = add nsw i32 %102, 1
  store i32 %103, i32* %i, align 4
  br label %74

; <label>:104                                     ; preds = %74
  %105 = load %struct._IO_FILE** %out, align 8
  %106 = call i32 @_IO_putc(i32 34, %struct._IO_FILE* %105)
  br label %111

; <label>:107                                     ; preds = %68
  %108 = load %struct._IO_FILE** %out, align 8
  %109 = load i8** %2, align 8
  %110 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %108, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %109)
  br label %111

; <label>:111                                     ; preds = %107, %104
  br label %112

; <label>:112                                     ; preds = %111, %9
  %113 = load i32* %3, align 4
  %114 = icmp ne i32 %113, 0
  br i1 %114, label %115, label %123

; <label>:115                                     ; preds = %112
  %116 = load %struct.ShellState** %1, align 8
  %117 = getelementptr inbounds %struct.ShellState* %116, i32 0, i32 8
  %118 = load %struct._IO_FILE** %117, align 8
  %119 = load %struct.ShellState** %1, align 8
  %120 = getelementptr inbounds %struct.ShellState* %119, i32 0, i32 16
  %121 = getelementptr inbounds [20 x i8]* %120, i32 0, i32 0
  %122 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %118, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %121)
  br label %123

; <label>:123                                     ; preds = %115, %112
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @output_quoted_string(%struct._IO_FILE* %out, i8* %z) #0 {
  %1 = alloca %struct._IO_FILE*, align 8
  %2 = alloca i8*, align 8
  %i = alloca i32, align 4
  %nSingle = alloca i32, align 4
  store %struct._IO_FILE* %out, %struct._IO_FILE** %1, align 8
  store i8* %z, i8** %2, align 8
  store i32 0, i32* %nSingle, align 4
  store i32 0, i32* %i, align 4
  br label %3

; <label>:3                                       ; preds = %22, %0
  %4 = load i32* %i, align 4
  %5 = sext i32 %4 to i64
  %6 = load i8** %2, align 8
  %7 = getelementptr inbounds i8* %6, i64 %5
  %8 = load i8* %7, align 1
  %9 = icmp ne i8 %8, 0
  br i1 %9, label %10, label %25

; <label>:10                                      ; preds = %3
  %11 = load i32* %i, align 4
  %12 = sext i32 %11 to i64
  %13 = load i8** %2, align 8
  %14 = getelementptr inbounds i8* %13, i64 %12
  %15 = load i8* %14, align 1
  %16 = sext i8 %15 to i32
  %17 = icmp eq i32 %16, 39
  br i1 %17, label %18, label %21

; <label>:18                                      ; preds = %10
  %19 = load i32* %nSingle, align 4
  %20 = add nsw i32 %19, 1
  store i32 %20, i32* %nSingle, align 4
  br label %21

; <label>:21                                      ; preds = %18, %10
  br label %22

; <label>:22                                      ; preds = %21
  %23 = load i32* %i, align 4
  %24 = add nsw i32 %23, 1
  store i32 %24, i32* %i, align 4
  br label %3

; <label>:25                                      ; preds = %3
  %26 = load i32* %nSingle, align 4
  %27 = icmp eq i32 %26, 0
  br i1 %27, label %28, label %32

; <label>:28                                      ; preds = %25
  %29 = load %struct._IO_FILE** %1, align 8
  %30 = load i8** %2, align 8
  %31 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([5 x i8]* @.str85, i32 0, i32 0), i8* %30)
  br label %97

; <label>:32                                      ; preds = %25
  %33 = load %struct._IO_FILE** %1, align 8
  %34 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([2 x i8]* @.str84, i32 0, i32 0))
  br label %35

; <label>:35                                      ; preds = %93, %32
  %36 = load i8** %2, align 8
  %37 = load i8* %36, align 1
  %38 = icmp ne i8 %37, 0
  br i1 %38, label %39, label %94

; <label>:39                                      ; preds = %35
  store i32 0, i32* %i, align 4
  br label %40

; <label>:40                                      ; preds = %59, %39
  %41 = load i32* %i, align 4
  %42 = sext i32 %41 to i64
  %43 = load i8** %2, align 8
  %44 = getelementptr inbounds i8* %43, i64 %42
  %45 = load i8* %44, align 1
  %46 = sext i8 %45 to i32
  %47 = icmp ne i32 %46, 0
  br i1 %47, label %48, label %56

; <label>:48                                      ; preds = %40
  %49 = load i32* %i, align 4
  %50 = sext i32 %49 to i64
  %51 = load i8** %2, align 8
  %52 = getelementptr inbounds i8* %51, i64 %50
  %53 = load i8* %52, align 1
  %54 = sext i8 %53 to i32
  %55 = icmp ne i32 %54, 39
  br label %56

; <label>:56                                      ; preds = %48, %40
  %57 = phi i1 [ false, %40 ], [ %55, %48 ]
  br i1 %57, label %58, label %62

; <label>:58                                      ; preds = %56
  br label %59

; <label>:59                                      ; preds = %58
  %60 = load i32* %i, align 4
  %61 = add nsw i32 %60, 1
  store i32 %61, i32* %i, align 4
  br label %40

; <label>:62                                      ; preds = %56
  %63 = load i32* %i, align 4
  %64 = icmp eq i32 %63, 0
  br i1 %64, label %65, label %70

; <label>:65                                      ; preds = %62
  %66 = load %struct._IO_FILE** %1, align 8
  %67 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %66, i8* getelementptr inbounds ([3 x i8]* @.str86, i32 0, i32 0))
  %68 = load i8** %2, align 8
  %69 = getelementptr inbounds i8* %68, i32 1
  store i8* %69, i8** %2, align 8
  br label %93

; <label>:70                                      ; preds = %62
  %71 = load i32* %i, align 4
  %72 = sext i32 %71 to i64
  %73 = load i8** %2, align 8
  %74 = getelementptr inbounds i8* %73, i64 %72
  %75 = load i8* %74, align 1
  %76 = sext i8 %75 to i32
  %77 = icmp eq i32 %76, 39
  br i1 %77, label %78, label %88

; <label>:78                                      ; preds = %70
  %79 = load %struct._IO_FILE** %1, align 8
  %80 = load i32* %i, align 4
  %81 = load i8** %2, align 8
  %82 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %79, i8* getelementptr inbounds ([7 x i8]* @.str87, i32 0, i32 0), i32 %80, i8* %81)
  %83 = load i32* %i, align 4
  %84 = add nsw i32 %83, 1
  %85 = load i8** %2, align 8
  %86 = sext i32 %84 to i64
  %87 = getelementptr inbounds i8* %85, i64 %86
  store i8* %87, i8** %2, align 8
  br label %92

; <label>:88                                      ; preds = %70
  %89 = load %struct._IO_FILE** %1, align 8
  %90 = load i8** %2, align 8
  %91 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %89, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %90)
  br label %94

; <label>:92                                      ; preds = %78
  br label %93

; <label>:93                                      ; preds = %92, %65
  br label %35

; <label>:94                                      ; preds = %88, %35
  %95 = load %struct._IO_FILE** %1, align 8
  %96 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %95, i8* getelementptr inbounds ([2 x i8]* @.str84, i32 0, i32 0))
  br label %97

; <label>:97                                      ; preds = %94, %28
  ret void
}

declare i8* @sqlite3_column_blob(%struct.sqlite3_stmt*, i32) #2

declare i32 @sqlite3_column_bytes(%struct.sqlite3_stmt*, i32) #2

; Function Attrs: nounwind uwtable
define internal void @output_hex_blob(%struct._IO_FILE* %out, i8* %pBlob, i32 %nBlob) #0 {
  %1 = alloca %struct._IO_FILE*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i32, align 4
  %i = alloca i32, align 4
  %zBlob = alloca i8*, align 8
  store %struct._IO_FILE* %out, %struct._IO_FILE** %1, align 8
  store i8* %pBlob, i8** %2, align 8
  store i32 %nBlob, i32* %3, align 4
  %4 = load i8** %2, align 8
  store i8* %4, i8** %zBlob, align 8
  %5 = load %struct._IO_FILE** %1, align 8
  %6 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %5, i8* getelementptr inbounds ([3 x i8]* @.str82, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %7

; <label>:7                                       ; preds = %21, %0
  %8 = load i32* %i, align 4
  %9 = load i32* %3, align 4
  %10 = icmp slt i32 %8, %9
  br i1 %10, label %11, label %24

; <label>:11                                      ; preds = %7
  %12 = load %struct._IO_FILE** %1, align 8
  %13 = load i32* %i, align 4
  %14 = sext i32 %13 to i64
  %15 = load i8** %zBlob, align 8
  %16 = getelementptr inbounds i8* %15, i64 %14
  %17 = load i8* %16, align 1
  %18 = sext i8 %17 to i32
  %19 = and i32 %18, 255
  %20 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([5 x i8]* @.str83, i32 0, i32 0), i32 %19)
  br label %21

; <label>:21                                      ; preds = %11
  %22 = load i32* %i, align 4
  %23 = add nsw i32 %22, 1
  store i32 %23, i32* %i, align 4
  br label %7

; <label>:24                                      ; preds = %7
  %25 = load %struct._IO_FILE** %1, align 8
  %26 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([2 x i8]* @.str84, i32 0, i32 0))
  ret void
}

; Function Attrs: nounwind uwtable
define internal i32 @isNumber(i8* %z, i32* %realnum) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %3 = alloca i32*, align 8
  store i8* %z, i8** %2, align 8
  store i32* %realnum, i32** %3, align 8
  %4 = load i8** %2, align 8
  %5 = load i8* %4, align 1
  %6 = sext i8 %5 to i32
  %7 = icmp eq i32 %6, 45
  br i1 %7, label %13, label %8

; <label>:8                                       ; preds = %0
  %9 = load i8** %2, align 8
  %10 = load i8* %9, align 1
  %11 = sext i8 %10 to i32
  %12 = icmp eq i32 %11, 43
  br i1 %12, label %13, label %16

; <label>:13                                      ; preds = %8, %0
  %14 = load i8** %2, align 8
  %15 = getelementptr inbounds i8* %14, i32 1
  store i8* %15, i8** %2, align 8
  br label %16

; <label>:16                                      ; preds = %13, %8
  %17 = load i8** %2, align 8
  %18 = load i8* %17, align 1
  %19 = zext i8 %18 to i32
  %20 = sext i32 %19 to i64
  %21 = call i16** @__ctype_b_loc() #9
  %22 = load i16** %21, align 8
  %23 = getelementptr inbounds i16* %22, i64 %20
  %24 = load i16* %23, align 2
  %25 = zext i16 %24 to i32
  %26 = and i32 %25, 2048
  %27 = icmp ne i32 %26, 0
  br i1 %27, label %29, label %28

; <label>:28                                      ; preds = %16
  store i32 0, i32* %1
  br label %160

; <label>:29                                      ; preds = %16
  %30 = load i8** %2, align 8
  %31 = getelementptr inbounds i8* %30, i32 1
  store i8* %31, i8** %2, align 8
  %32 = load i32** %3, align 8
  %33 = icmp ne i32* %32, null
  br i1 %33, label %34, label %36

; <label>:34                                      ; preds = %29
  %35 = load i32** %3, align 8
  store i32 0, i32* %35, align 4
  br label %36

; <label>:36                                      ; preds = %34, %29
  br label %37

; <label>:37                                      ; preds = %49, %36
  %38 = load i8** %2, align 8
  %39 = load i8* %38, align 1
  %40 = zext i8 %39 to i32
  %41 = sext i32 %40 to i64
  %42 = call i16** @__ctype_b_loc() #9
  %43 = load i16** %42, align 8
  %44 = getelementptr inbounds i16* %43, i64 %41
  %45 = load i16* %44, align 2
  %46 = zext i16 %45 to i32
  %47 = and i32 %46, 2048
  %48 = icmp ne i32 %47, 0
  br i1 %48, label %49, label %52

; <label>:49                                      ; preds = %37
  %50 = load i8** %2, align 8
  %51 = getelementptr inbounds i8* %50, i32 1
  store i8* %51, i8** %2, align 8
  br label %37

; <label>:52                                      ; preds = %37
  %53 = load i8** %2, align 8
  %54 = load i8* %53, align 1
  %55 = sext i8 %54 to i32
  %56 = icmp eq i32 %55, 46
  br i1 %56, label %57, label %94

; <label>:57                                      ; preds = %52
  %58 = load i8** %2, align 8
  %59 = getelementptr inbounds i8* %58, i32 1
  store i8* %59, i8** %2, align 8
  %60 = load i8** %2, align 8
  %61 = load i8* %60, align 1
  %62 = zext i8 %61 to i32
  %63 = sext i32 %62 to i64
  %64 = call i16** @__ctype_b_loc() #9
  %65 = load i16** %64, align 8
  %66 = getelementptr inbounds i16* %65, i64 %63
  %67 = load i16* %66, align 2
  %68 = zext i16 %67 to i32
  %69 = and i32 %68, 2048
  %70 = icmp ne i32 %69, 0
  br i1 %70, label %72, label %71

; <label>:71                                      ; preds = %57
  store i32 0, i32* %1
  br label %160

; <label>:72                                      ; preds = %57
  br label %73

; <label>:73                                      ; preds = %85, %72
  %74 = load i8** %2, align 8
  %75 = load i8* %74, align 1
  %76 = zext i8 %75 to i32
  %77 = sext i32 %76 to i64
  %78 = call i16** @__ctype_b_loc() #9
  %79 = load i16** %78, align 8
  %80 = getelementptr inbounds i16* %79, i64 %77
  %81 = load i16* %80, align 2
  %82 = zext i16 %81 to i32
  %83 = and i32 %82, 2048
  %84 = icmp ne i32 %83, 0
  br i1 %84, label %85, label %88

; <label>:85                                      ; preds = %73
  %86 = load i8** %2, align 8
  %87 = getelementptr inbounds i8* %86, i32 1
  store i8* %87, i8** %2, align 8
  br label %73

; <label>:88                                      ; preds = %73
  %89 = load i32** %3, align 8
  %90 = icmp ne i32* %89, null
  br i1 %90, label %91, label %93

; <label>:91                                      ; preds = %88
  %92 = load i32** %3, align 8
  store i32 1, i32* %92, align 4
  br label %93

; <label>:93                                      ; preds = %91, %88
  br label %94

; <label>:94                                      ; preds = %93, %52
  %95 = load i8** %2, align 8
  %96 = load i8* %95, align 1
  %97 = sext i8 %96 to i32
  %98 = icmp eq i32 %97, 101
  br i1 %98, label %104, label %99

; <label>:99                                      ; preds = %94
  %100 = load i8** %2, align 8
  %101 = load i8* %100, align 1
  %102 = sext i8 %101 to i32
  %103 = icmp eq i32 %102, 69
  br i1 %103, label %104, label %154

; <label>:104                                     ; preds = %99, %94
  %105 = load i8** %2, align 8
  %106 = getelementptr inbounds i8* %105, i32 1
  store i8* %106, i8** %2, align 8
  %107 = load i8** %2, align 8
  %108 = load i8* %107, align 1
  %109 = sext i8 %108 to i32
  %110 = icmp eq i32 %109, 43
  br i1 %110, label %116, label %111

; <label>:111                                     ; preds = %104
  %112 = load i8** %2, align 8
  %113 = load i8* %112, align 1
  %114 = sext i8 %113 to i32
  %115 = icmp eq i32 %114, 45
  br i1 %115, label %116, label %119

; <label>:116                                     ; preds = %111, %104
  %117 = load i8** %2, align 8
  %118 = getelementptr inbounds i8* %117, i32 1
  store i8* %118, i8** %2, align 8
  br label %119

; <label>:119                                     ; preds = %116, %111
  %120 = load i8** %2, align 8
  %121 = load i8* %120, align 1
  %122 = zext i8 %121 to i32
  %123 = sext i32 %122 to i64
  %124 = call i16** @__ctype_b_loc() #9
  %125 = load i16** %124, align 8
  %126 = getelementptr inbounds i16* %125, i64 %123
  %127 = load i16* %126, align 2
  %128 = zext i16 %127 to i32
  %129 = and i32 %128, 2048
  %130 = icmp ne i32 %129, 0
  br i1 %130, label %132, label %131

; <label>:131                                     ; preds = %119
  store i32 0, i32* %1
  br label %160

; <label>:132                                     ; preds = %119
  br label %133

; <label>:133                                     ; preds = %145, %132
  %134 = load i8** %2, align 8
  %135 = load i8* %134, align 1
  %136 = zext i8 %135 to i32
  %137 = sext i32 %136 to i64
  %138 = call i16** @__ctype_b_loc() #9
  %139 = load i16** %138, align 8
  %140 = getelementptr inbounds i16* %139, i64 %137
  %141 = load i16* %140, align 2
  %142 = zext i16 %141 to i32
  %143 = and i32 %142, 2048
  %144 = icmp ne i32 %143, 0
  br i1 %144, label %145, label %148

; <label>:145                                     ; preds = %133
  %146 = load i8** %2, align 8
  %147 = getelementptr inbounds i8* %146, i32 1
  store i8* %147, i8** %2, align 8
  br label %133

; <label>:148                                     ; preds = %133
  %149 = load i32** %3, align 8
  %150 = icmp ne i32* %149, null
  br i1 %150, label %151, label %153

; <label>:151                                     ; preds = %148
  %152 = load i32** %3, align 8
  store i32 1, i32* %152, align 4
  br label %153

; <label>:153                                     ; preds = %151, %148
  br label %154

; <label>:154                                     ; preds = %153, %99
  %155 = load i8** %2, align 8
  %156 = load i8* %155, align 1
  %157 = sext i8 %156 to i32
  %158 = icmp eq i32 %157, 0
  %159 = zext i1 %158 to i32
  store i32 %159, i32* %1
  br label %160

; <label>:160                                     ; preds = %154, %131, %71, %28
  %161 = load i32* %1
  ret i32 %161
}

; Function Attrs: nounwind readonly
declare i32 @memcmp(i8*, i8*, i64) #1

declare i32 @_IO_putc(i32, %struct._IO_FILE*) #2

declare i32 @fputc(i32, %struct._IO_FILE*) #2

declare i32 @sqlite3_prepare_v2(%struct.sqlite3*, i8*, i32, %struct.sqlite3_stmt**, i8**) #2

; Function Attrs: nounwind uwtable
define internal i8* @save_err_msg(%struct.sqlite3* %db) #0 {
  %1 = alloca %struct.sqlite3*, align 8
  %nErrMsg = alloca i32, align 4
  %zErrMsg = alloca i8*, align 8
  store %struct.sqlite3* %db, %struct.sqlite3** %1, align 8
  %2 = load %struct.sqlite3** %1, align 8
  %3 = call i8* @sqlite3_errmsg(%struct.sqlite3* %2)
  %4 = call i32 @strlen30(i8* %3)
  %5 = add nsw i32 1, %4
  store i32 %5, i32* %nErrMsg, align 4
  %6 = load i32* %nErrMsg, align 4
  %7 = sext i32 %6 to i64
  %8 = call i8* @sqlite3_malloc64(i64 %7)
  store i8* %8, i8** %zErrMsg, align 8
  %9 = load i8** %zErrMsg, align 8
  %10 = icmp ne i8* %9, null
  br i1 %10, label %11, label %17

; <label>:11                                      ; preds = %0
  %12 = load i8** %zErrMsg, align 8
  %13 = load %struct.sqlite3** %1, align 8
  %14 = call i8* @sqlite3_errmsg(%struct.sqlite3* %13)
  %15 = load i32* %nErrMsg, align 4
  %16 = sext i32 %15 to i64
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %12, i8* %14, i64 %16, i32 1, i1 false)
  br label %17

; <label>:17                                      ; preds = %11, %0
  %18 = load i8** %zErrMsg, align 8
  ret i8* %18
}

declare i8* @sqlite3_sql(%struct.sqlite3_stmt*) #2

declare i8* @sqlite3_mprintf(i8*, ...) #2

declare i32 @sqlite3_step(%struct.sqlite3_stmt*) #2

declare i32 @sqlite3_column_int(%struct.sqlite3_stmt*, i32) #2

declare i8* @sqlite3_column_text(%struct.sqlite3_stmt*, i32) #2

declare i32 @sqlite3_finalize(%struct.sqlite3_stmt*) #2

; Function Attrs: nounwind uwtable
define internal void @explain_data_prepare(%struct.ShellState* %p, %struct.sqlite3_stmt* %pSql) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca %struct.sqlite3_stmt*, align 8
  %zSql = alloca i8*, align 8
  %z = alloca i8*, align 8
  %abYield = alloca i32*, align 8
  %nAlloc = alloca i32, align 4
  %iOp = alloca i32, align 4
  %azNext = alloca [8 x i8*], align 16
  %azYield = alloca [6 x i8*], align 16
  %azGoto = alloca [2 x i8*], align 16
  %i = alloca i32, align 4
  %iAddr = alloca i32, align 4
  %zOp = alloca i8*, align 8
  %p2 = alloca i32, align 4
  %p2op = alloca i32, align 4
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store %struct.sqlite3_stmt* %pSql, %struct.sqlite3_stmt** %2, align 8
  store i32* null, i32** %abYield, align 8
  store i32 0, i32* %nAlloc, align 4
  %3 = bitcast [8 x i8*]* %azNext to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %3, i8* bitcast ([8 x i8*]* @explain_data_prepare.azNext to i8*), i64 64, i32 16, i1 false)
  %4 = bitcast [6 x i8*]* %azYield to i8*
  call void @llvm.memset.p0i8.i64(i8* %4, i8 0, i64 48, i32 16, i1 false)
  %5 = bitcast i8* %4 to [6 x i8*]*
  %6 = getelementptr [6 x i8*]* %5, i32 0, i32 0
  store i8* getelementptr inbounds ([6 x i8]* @.str128, i32 0, i32 0), i8** %6
  %7 = getelementptr [6 x i8*]* %5, i32 0, i32 1
  store i8* getelementptr inbounds ([7 x i8]* @.str129, i32 0, i32 0), i8** %7
  %8 = getelementptr [6 x i8*]* %5, i32 0, i32 2
  store i8* getelementptr inbounds ([7 x i8]* @.str130, i32 0, i32 0), i8** %8
  %9 = getelementptr [6 x i8*]* %5, i32 0, i32 3
  store i8* getelementptr inbounds ([11 x i8]* @.str131, i32 0, i32 0), i8** %9
  %10 = getelementptr [6 x i8*]* %5, i32 0, i32 4
  store i8* getelementptr inbounds ([7 x i8]* @.str132, i32 0, i32 0), i8** %10
  %11 = bitcast [2 x i8*]* %azGoto to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %11, i8* bitcast ([2 x i8*]* @explain_data_prepare.azGoto to i8*), i64 16, i32 16, i1 false)
  %12 = load %struct.sqlite3_stmt** %2, align 8
  %13 = call i8* @sqlite3_sql(%struct.sqlite3_stmt* %12)
  store i8* %13, i8** %zSql, align 8
  %14 = load i8** %zSql, align 8
  %15 = icmp eq i8* %14, null
  br i1 %15, label %16, label %17

; <label>:16                                      ; preds = %0
  br label %190

; <label>:17                                      ; preds = %0
  %18 = load i8** %zSql, align 8
  store i8* %18, i8** %z, align 8
  br label %19

; <label>:19                                      ; preds = %47, %17
  %20 = load i8** %z, align 8
  %21 = load i8* %20, align 1
  %22 = sext i8 %21 to i32
  %23 = icmp eq i32 %22, 32
  br i1 %23, label %44, label %24

; <label>:24                                      ; preds = %19
  %25 = load i8** %z, align 8
  %26 = load i8* %25, align 1
  %27 = sext i8 %26 to i32
  %28 = icmp eq i32 %27, 9
  br i1 %28, label %44, label %29

; <label>:29                                      ; preds = %24
  %30 = load i8** %z, align 8
  %31 = load i8* %30, align 1
  %32 = sext i8 %31 to i32
  %33 = icmp eq i32 %32, 10
  br i1 %33, label %44, label %34

; <label>:34                                      ; preds = %29
  %35 = load i8** %z, align 8
  %36 = load i8* %35, align 1
  %37 = sext i8 %36 to i32
  %38 = icmp eq i32 %37, 12
  br i1 %38, label %44, label %39

; <label>:39                                      ; preds = %34
  %40 = load i8** %z, align 8
  %41 = load i8* %40, align 1
  %42 = sext i8 %41 to i32
  %43 = icmp eq i32 %42, 13
  br label %44

; <label>:44                                      ; preds = %39, %34, %29, %24, %19
  %45 = phi i1 [ true, %34 ], [ true, %29 ], [ true, %24 ], [ true, %19 ], [ %43, %39 ]
  br i1 %45, label %46, label %50

; <label>:46                                      ; preds = %44
  br label %47

; <label>:47                                      ; preds = %46
  %48 = load i8** %z, align 8
  %49 = getelementptr inbounds i8* %48, i32 1
  store i8* %49, i8** %z, align 8
  br label %19

; <label>:50                                      ; preds = %44
  %51 = load i8** %z, align 8
  %52 = call i32 @sqlite3_strnicmp(i8* %51, i8* getelementptr inbounds ([8 x i8]* @.str134, i32 0, i32 0), i32 7)
  %53 = icmp ne i32 %52, 0
  br i1 %53, label %54, label %55

; <label>:54                                      ; preds = %50
  br label %190

; <label>:55                                      ; preds = %50
  store i32 0, i32* %iOp, align 4
  br label %56

; <label>:56                                      ; preds = %180, %55
  %57 = load %struct.sqlite3_stmt** %2, align 8
  %58 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %57)
  %59 = icmp eq i32 100, %58
  br i1 %59, label %60, label %183

; <label>:60                                      ; preds = %56
  %61 = load %struct.sqlite3_stmt** %2, align 8
  %62 = call i32 @sqlite3_column_int(%struct.sqlite3_stmt* %61, i32 0)
  store i32 %62, i32* %iAddr, align 4
  %63 = load %struct.sqlite3_stmt** %2, align 8
  %64 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %63, i32 1)
  store i8* %64, i8** %zOp, align 8
  %65 = load %struct.sqlite3_stmt** %2, align 8
  %66 = call i32 @sqlite3_column_int(%struct.sqlite3_stmt* %65, i32 3)
  store i32 %66, i32* %p2, align 4
  %67 = load i32* %p2, align 4
  %68 = load i32* %iOp, align 4
  %69 = load i32* %iAddr, align 4
  %70 = sub nsw i32 %68, %69
  %71 = add nsw i32 %67, %70
  store i32 %71, i32* %p2op, align 4
  %72 = load i32* %iOp, align 4
  %73 = load i32* %nAlloc, align 4
  %74 = icmp sge i32 %72, %73
  br i1 %74, label %75, label %96

; <label>:75                                      ; preds = %60
  %76 = load i32* %nAlloc, align 4
  %77 = add nsw i32 %76, 100
  store i32 %77, i32* %nAlloc, align 4
  %78 = load %struct.ShellState** %1, align 8
  %79 = getelementptr inbounds %struct.ShellState* %78, i32 0, i32 28
  %80 = load i32** %79, align 8
  %81 = bitcast i32* %80 to i8*
  %82 = load i32* %nAlloc, align 4
  %83 = sext i32 %82 to i64
  %84 = mul i64 %83, 4
  %85 = call i8* @sqlite3_realloc64(i8* %81, i64 %84)
  %86 = bitcast i8* %85 to i32*
  %87 = load %struct.ShellState** %1, align 8
  %88 = getelementptr inbounds %struct.ShellState* %87, i32 0, i32 28
  store i32* %86, i32** %88, align 8
  %89 = load i32** %abYield, align 8
  %90 = bitcast i32* %89 to i8*
  %91 = load i32* %nAlloc, align 4
  %92 = sext i32 %91 to i64
  %93 = mul i64 %92, 4
  %94 = call i8* @sqlite3_realloc64(i8* %90, i64 %93)
  %95 = bitcast i8* %94 to i32*
  store i32* %95, i32** %abYield, align 8
  br label %96

; <label>:96                                      ; preds = %75, %60
  %97 = load i8** %zOp, align 8
  %98 = getelementptr inbounds [6 x i8*]* %azYield, i32 0, i32 0
  %99 = call i32 @str_in_array(i8* %97, i8** %98)
  %100 = load i32* %iOp, align 4
  %101 = sext i32 %100 to i64
  %102 = load i32** %abYield, align 8
  %103 = getelementptr inbounds i32* %102, i64 %101
  store i32 %99, i32* %103, align 4
  %104 = load i32* %iOp, align 4
  %105 = sext i32 %104 to i64
  %106 = load %struct.ShellState** %1, align 8
  %107 = getelementptr inbounds %struct.ShellState* %106, i32 0, i32 28
  %108 = load i32** %107, align 8
  %109 = getelementptr inbounds i32* %108, i64 %105
  store i32 0, i32* %109, align 4
  %110 = load i32* %iOp, align 4
  %111 = add nsw i32 %110, 1
  %112 = load %struct.ShellState** %1, align 8
  %113 = getelementptr inbounds %struct.ShellState* %112, i32 0, i32 29
  store i32 %111, i32* %113, align 4
  %114 = load i8** %zOp, align 8
  %115 = getelementptr inbounds [8 x i8*]* %azNext, i32 0, i32 0
  %116 = call i32 @str_in_array(i8* %114, i8** %115)
  %117 = icmp ne i32 %116, 0
  br i1 %117, label %118, label %137

; <label>:118                                     ; preds = %96
  %119 = load i32* %p2op, align 4
  store i32 %119, i32* %i, align 4
  br label %120

; <label>:120                                     ; preds = %133, %118
  %121 = load i32* %i, align 4
  %122 = load i32* %iOp, align 4
  %123 = icmp slt i32 %121, %122
  br i1 %123, label %124, label %136

; <label>:124                                     ; preds = %120
  %125 = load i32* %i, align 4
  %126 = sext i32 %125 to i64
  %127 = load %struct.ShellState** %1, align 8
  %128 = getelementptr inbounds %struct.ShellState* %127, i32 0, i32 28
  %129 = load i32** %128, align 8
  %130 = getelementptr inbounds i32* %129, i64 %126
  %131 = load i32* %130, align 4
  %132 = add nsw i32 %131, 2
  store i32 %132, i32* %130, align 4
  br label %133

; <label>:133                                     ; preds = %124
  %134 = load i32* %i, align 4
  %135 = add nsw i32 %134, 1
  store i32 %135, i32* %i, align 4
  br label %120

; <label>:136                                     ; preds = %120
  br label %137

; <label>:137                                     ; preds = %136, %96
  %138 = load i8** %zOp, align 8
  %139 = getelementptr inbounds [2 x i8*]* %azGoto, i32 0, i32 0
  %140 = call i32 @str_in_array(i8* %138, i8** %139)
  %141 = icmp ne i32 %140, 0
  br i1 %141, label %142, label %179

; <label>:142                                     ; preds = %137
  %143 = load i32* %p2op, align 4
  %144 = load %struct.ShellState** %1, align 8
  %145 = getelementptr inbounds %struct.ShellState* %144, i32 0, i32 29
  %146 = load i32* %145, align 4
  %147 = icmp slt i32 %143, %146
  br i1 %147, label %148, label %179

; <label>:148                                     ; preds = %142
  %149 = load i32* %p2op, align 4
  %150 = sext i32 %149 to i64
  %151 = load i32** %abYield, align 8
  %152 = getelementptr inbounds i32* %151, i64 %150
  %153 = load i32* %152, align 4
  %154 = icmp ne i32 %153, 0
  br i1 %154, label %159, label %155

; <label>:155                                     ; preds = %148
  %156 = load %struct.sqlite3_stmt** %2, align 8
  %157 = call i32 @sqlite3_column_int(%struct.sqlite3_stmt* %156, i32 2)
  %158 = icmp ne i32 %157, 0
  br i1 %158, label %159, label %179

; <label>:159                                     ; preds = %155, %148
  %160 = load i32* %p2op, align 4
  %161 = add nsw i32 %160, 1
  store i32 %161, i32* %i, align 4
  br label %162

; <label>:162                                     ; preds = %175, %159
  %163 = load i32* %i, align 4
  %164 = load i32* %iOp, align 4
  %165 = icmp slt i32 %163, %164
  br i1 %165, label %166, label %178

; <label>:166                                     ; preds = %162
  %167 = load i32* %i, align 4
  %168 = sext i32 %167 to i64
  %169 = load %struct.ShellState** %1, align 8
  %170 = getelementptr inbounds %struct.ShellState* %169, i32 0, i32 28
  %171 = load i32** %170, align 8
  %172 = getelementptr inbounds i32* %171, i64 %168
  %173 = load i32* %172, align 4
  %174 = add nsw i32 %173, 2
  store i32 %174, i32* %172, align 4
  br label %175

; <label>:175                                     ; preds = %166
  %176 = load i32* %i, align 4
  %177 = add nsw i32 %176, 1
  store i32 %177, i32* %i, align 4
  br label %162

; <label>:178                                     ; preds = %162
  br label %179

; <label>:179                                     ; preds = %178, %155, %142, %137
  br label %180

; <label>:180                                     ; preds = %179
  %181 = load i32* %iOp, align 4
  %182 = add nsw i32 %181, 1
  store i32 %182, i32* %iOp, align 4
  br label %56

; <label>:183                                     ; preds = %56
  %184 = load %struct.ShellState** %1, align 8
  %185 = getelementptr inbounds %struct.ShellState* %184, i32 0, i32 30
  store i32 0, i32* %185, align 4
  %186 = load i32** %abYield, align 8
  %187 = bitcast i32* %186 to i8*
  call void @sqlite3_free(i8* %187)
  %188 = load %struct.sqlite3_stmt** %2, align 8
  %189 = call i32 @sqlite3_reset(%struct.sqlite3_stmt* %188)
  br label %190

; <label>:190                                     ; preds = %183, %54, %16
  ret void
}

declare i32 @sqlite3_column_count(%struct.sqlite3_stmt*) #2

declare i8* @sqlite3_malloc64(i64) #2

declare i8* @sqlite3_column_name(%struct.sqlite3_stmt*, i32) #2

declare i32 @sqlite3_column_type(%struct.sqlite3_stmt*, i32) #2

; Function Attrs: nounwind uwtable
define internal void @explain_data_delete(%struct.ShellState* %p) #0 {
  %1 = alloca %struct.ShellState*, align 8
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  %2 = load %struct.ShellState** %1, align 8
  %3 = getelementptr inbounds %struct.ShellState* %2, i32 0, i32 28
  %4 = load i32** %3, align 8
  %5 = bitcast i32* %4 to i8*
  call void @sqlite3_free(i8* %5)
  %6 = load %struct.ShellState** %1, align 8
  %7 = getelementptr inbounds %struct.ShellState* %6, i32 0, i32 28
  store i32* null, i32** %7, align 8
  %8 = load %struct.ShellState** %1, align 8
  %9 = getelementptr inbounds %struct.ShellState* %8, i32 0, i32 29
  store i32 0, i32* %9, align 4
  %10 = load %struct.ShellState** %1, align 8
  %11 = getelementptr inbounds %struct.ShellState* %10, i32 0, i32 30
  store i32 0, i32* %11, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define internal i32 @display_stats(%struct.sqlite3* %db, %struct.ShellState* %pArg, i32 %bReset) #0 {
  %1 = alloca %struct.sqlite3*, align 8
  %2 = alloca %struct.ShellState*, align 8
  %3 = alloca i32, align 4
  %iCur = alloca i32, align 4
  %iHiwtr = alloca i32, align 4
  store %struct.sqlite3* %db, %struct.sqlite3** %1, align 8
  store %struct.ShellState* %pArg, %struct.ShellState** %2, align 8
  store i32 %bReset, i32* %3, align 4
  %4 = load %struct.ShellState** %2, align 8
  %5 = icmp ne %struct.ShellState* %4, null
  br i1 %5, label %6, label %95

; <label>:6                                       ; preds = %0
  %7 = load %struct.ShellState** %2, align 8
  %8 = getelementptr inbounds %struct.ShellState* %7, i32 0, i32 8
  %9 = load %struct._IO_FILE** %8, align 8
  %10 = icmp ne %struct._IO_FILE* %9, null
  br i1 %10, label %11, label %95

; <label>:11                                      ; preds = %6
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %12 = load i32* %3, align 4
  %13 = call i32 @sqlite3_status(i32 0, i32* %iCur, i32* %iHiwtr, i32 %12)
  %14 = load %struct.ShellState** %2, align 8
  %15 = getelementptr inbounds %struct.ShellState* %14, i32 0, i32 8
  %16 = load %struct._IO_FILE** %15, align 8
  %17 = load i32* %iCur, align 4
  %18 = load i32* %iHiwtr, align 4
  %19 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([56 x i8]* @.str98, i32 0, i32 0), i32 %17, i32 %18)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %20 = load i32* %3, align 4
  %21 = call i32 @sqlite3_status(i32 9, i32* %iCur, i32* %iHiwtr, i32 %20)
  %22 = load %struct.ShellState** %2, align 8
  %23 = getelementptr inbounds %struct.ShellState* %22, i32 0, i32 8
  %24 = load %struct._IO_FILE** %23, align 8
  %25 = load i32* %iCur, align 4
  %26 = load i32* %iHiwtr, align 4
  %27 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([50 x i8]* @.str99, i32 0, i32 0), i32 %25, i32 %26)
  %28 = load %struct.ShellState** %2, align 8
  %29 = getelementptr inbounds %struct.ShellState* %28, i32 0, i32 14
  %30 = load i32* %29, align 4
  %31 = and i32 %30, 2
  %32 = icmp ne i32 %31, 0
  br i1 %32, label %33, label %42

; <label>:33                                      ; preds = %11
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %34 = load i32* %3, align 4
  %35 = call i32 @sqlite3_status(i32 1, i32* %iCur, i32* %iHiwtr, i32 %34)
  %36 = load %struct.ShellState** %2, align 8
  %37 = getelementptr inbounds %struct.ShellState* %36, i32 0, i32 8
  %38 = load %struct._IO_FILE** %37, align 8
  %39 = load i32* %iCur, align 4
  %40 = load i32* %iHiwtr, align 4
  %41 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %38, i8* getelementptr inbounds ([56 x i8]* @.str100, i32 0, i32 0), i32 %39, i32 %40)
  br label %42

; <label>:42                                      ; preds = %33, %11
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %43 = load i32* %3, align 4
  %44 = call i32 @sqlite3_status(i32 2, i32* %iCur, i32* %iHiwtr, i32 %43)
  %45 = load %struct.ShellState** %2, align 8
  %46 = getelementptr inbounds %struct.ShellState* %45, i32 0, i32 8
  %47 = load %struct._IO_FILE** %46, align 8
  %48 = load i32* %iCur, align 4
  %49 = load i32* %iHiwtr, align 4
  %50 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %47, i8* getelementptr inbounds ([56 x i8]* @.str101, i32 0, i32 0), i32 %48, i32 %49)
  %51 = load %struct.ShellState** %2, align 8
  %52 = getelementptr inbounds %struct.ShellState* %51, i32 0, i32 14
  %53 = load i32* %52, align 4
  %54 = and i32 %53, 1
  %55 = icmp ne i32 %54, 0
  br i1 %55, label %56, label %65

; <label>:56                                      ; preds = %42
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %57 = load i32* %3, align 4
  %58 = call i32 @sqlite3_status(i32 3, i32* %iCur, i32* %iHiwtr, i32 %57)
  %59 = load %struct.ShellState** %2, align 8
  %60 = getelementptr inbounds %struct.ShellState* %59, i32 0, i32 8
  %61 = load %struct._IO_FILE** %60, align 8
  %62 = load i32* %iCur, align 4
  %63 = load i32* %iHiwtr, align 4
  %64 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %61, i8* getelementptr inbounds ([50 x i8]* @.str102, i32 0, i32 0), i32 %62, i32 %63)
  br label %65

; <label>:65                                      ; preds = %56, %42
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %66 = load i32* %3, align 4
  %67 = call i32 @sqlite3_status(i32 4, i32* %iCur, i32* %iHiwtr, i32 %66)
  %68 = load %struct.ShellState** %2, align 8
  %69 = getelementptr inbounds %struct.ShellState* %68, i32 0, i32 8
  %70 = load %struct._IO_FILE** %69, align 8
  %71 = load i32* %iCur, align 4
  %72 = load i32* %iHiwtr, align 4
  %73 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %70, i8* getelementptr inbounds ([56 x i8]* @.str103, i32 0, i32 0), i32 %71, i32 %72)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %74 = load i32* %3, align 4
  %75 = call i32 @sqlite3_status(i32 5, i32* %iCur, i32* %iHiwtr, i32 %74)
  %76 = load %struct.ShellState** %2, align 8
  %77 = getelementptr inbounds %struct.ShellState* %76, i32 0, i32 8
  %78 = load %struct._IO_FILE** %77, align 8
  %79 = load i32* %iHiwtr, align 4
  %80 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %78, i8* getelementptr inbounds ([47 x i8]* @.str104, i32 0, i32 0), i32 %79)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %81 = load i32* %3, align 4
  %82 = call i32 @sqlite3_status(i32 7, i32* %iCur, i32* %iHiwtr, i32 %81)
  %83 = load %struct.ShellState** %2, align 8
  %84 = getelementptr inbounds %struct.ShellState* %83, i32 0, i32 8
  %85 = load %struct._IO_FILE** %84, align 8
  %86 = load i32* %iHiwtr, align 4
  %87 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %85, i8* getelementptr inbounds ([47 x i8]* @.str105, i32 0, i32 0), i32 %86)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %88 = load i32* %3, align 4
  %89 = call i32 @sqlite3_status(i32 8, i32* %iCur, i32* %iHiwtr, i32 %88)
  %90 = load %struct.ShellState** %2, align 8
  %91 = getelementptr inbounds %struct.ShellState* %90, i32 0, i32 8
  %92 = load %struct._IO_FILE** %91, align 8
  %93 = load i32* %iHiwtr, align 4
  %94 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %92, i8* getelementptr inbounds ([47 x i8]* @.str106, i32 0, i32 0), i32 %93)
  br label %95

; <label>:95                                      ; preds = %65, %6, %0
  %96 = load %struct.ShellState** %2, align 8
  %97 = icmp ne %struct.ShellState* %96, null
  br i1 %97, label %98, label %192

; <label>:98                                      ; preds = %95
  %99 = load %struct.ShellState** %2, align 8
  %100 = getelementptr inbounds %struct.ShellState* %99, i32 0, i32 8
  %101 = load %struct._IO_FILE** %100, align 8
  %102 = icmp ne %struct._IO_FILE* %101, null
  br i1 %102, label %103, label %192

; <label>:103                                     ; preds = %98
  %104 = load %struct.sqlite3** %1, align 8
  %105 = icmp ne %struct.sqlite3* %104, null
  br i1 %105, label %106, label %192

; <label>:106                                     ; preds = %103
  %107 = load %struct.ShellState** %2, align 8
  %108 = getelementptr inbounds %struct.ShellState* %107, i32 0, i32 14
  %109 = load i32* %108, align 4
  %110 = and i32 %109, 4
  %111 = icmp ne i32 %110, 0
  br i1 %111, label %112, label %146

; <label>:112                                     ; preds = %106
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %113 = load %struct.sqlite3** %1, align 8
  %114 = load i32* %3, align 4
  %115 = call i32 @sqlite3_db_status(%struct.sqlite3* %113, i32 0, i32* %iCur, i32* %iHiwtr, i32 %114)
  %116 = load %struct.ShellState** %2, align 8
  %117 = getelementptr inbounds %struct.ShellState* %116, i32 0, i32 8
  %118 = load %struct._IO_FILE** %117, align 8
  %119 = load i32* %iCur, align 4
  %120 = load i32* %iHiwtr, align 4
  %121 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %118, i8* getelementptr inbounds ([50 x i8]* @.str107, i32 0, i32 0), i32 %119, i32 %120)
  %122 = load %struct.sqlite3** %1, align 8
  %123 = load i32* %3, align 4
  %124 = call i32 @sqlite3_db_status(%struct.sqlite3* %122, i32 4, i32* %iCur, i32* %iHiwtr, i32 %123)
  %125 = load %struct.ShellState** %2, align 8
  %126 = getelementptr inbounds %struct.ShellState* %125, i32 0, i32 8
  %127 = load %struct._IO_FILE** %126, align 8
  %128 = load i32* %iHiwtr, align 4
  %129 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %127, i8* getelementptr inbounds ([41 x i8]* @.str108, i32 0, i32 0), i32 %128)
  %130 = load %struct.sqlite3** %1, align 8
  %131 = load i32* %3, align 4
  %132 = call i32 @sqlite3_db_status(%struct.sqlite3* %130, i32 5, i32* %iCur, i32* %iHiwtr, i32 %131)
  %133 = load %struct.ShellState** %2, align 8
  %134 = getelementptr inbounds %struct.ShellState* %133, i32 0, i32 8
  %135 = load %struct._IO_FILE** %134, align 8
  %136 = load i32* %iHiwtr, align 4
  %137 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %135, i8* getelementptr inbounds ([41 x i8]* @.str109, i32 0, i32 0), i32 %136)
  %138 = load %struct.sqlite3** %1, align 8
  %139 = load i32* %3, align 4
  %140 = call i32 @sqlite3_db_status(%struct.sqlite3* %138, i32 6, i32* %iCur, i32* %iHiwtr, i32 %139)
  %141 = load %struct.ShellState** %2, align 8
  %142 = getelementptr inbounds %struct.ShellState* %141, i32 0, i32 8
  %143 = load %struct._IO_FILE** %142, align 8
  %144 = load i32* %iHiwtr, align 4
  %145 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %143, i8* getelementptr inbounds ([41 x i8]* @.str110, i32 0, i32 0), i32 %144)
  br label %146

; <label>:146                                     ; preds = %112, %106
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %147 = load %struct.sqlite3** %1, align 8
  %148 = load i32* %3, align 4
  %149 = call i32 @sqlite3_db_status(%struct.sqlite3* %147, i32 1, i32* %iCur, i32* %iHiwtr, i32 %148)
  %150 = load %struct.ShellState** %2, align 8
  %151 = getelementptr inbounds %struct.ShellState* %150, i32 0, i32 8
  %152 = load %struct._IO_FILE** %151, align 8
  %153 = load i32* %iCur, align 4
  %154 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %152, i8* getelementptr inbounds ([47 x i8]* @.str111, i32 0, i32 0), i32 %153)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %155 = load %struct.sqlite3** %1, align 8
  %156 = call i32 @sqlite3_db_status(%struct.sqlite3* %155, i32 7, i32* %iCur, i32* %iHiwtr, i32 1)
  %157 = load %struct.ShellState** %2, align 8
  %158 = getelementptr inbounds %struct.ShellState* %157, i32 0, i32 8
  %159 = load %struct._IO_FILE** %158, align 8
  %160 = load i32* %iCur, align 4
  %161 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %159, i8* getelementptr inbounds ([41 x i8]* @.str112, i32 0, i32 0), i32 %160)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %162 = load %struct.sqlite3** %1, align 8
  %163 = call i32 @sqlite3_db_status(%struct.sqlite3* %162, i32 8, i32* %iCur, i32* %iHiwtr, i32 1)
  %164 = load %struct.ShellState** %2, align 8
  %165 = getelementptr inbounds %struct.ShellState* %164, i32 0, i32 8
  %166 = load %struct._IO_FILE** %165, align 8
  %167 = load i32* %iCur, align 4
  %168 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %166, i8* getelementptr inbounds ([41 x i8]* @.str113, i32 0, i32 0), i32 %167)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %169 = load %struct.sqlite3** %1, align 8
  %170 = call i32 @sqlite3_db_status(%struct.sqlite3* %169, i32 9, i32* %iCur, i32* %iHiwtr, i32 1)
  %171 = load %struct.ShellState** %2, align 8
  %172 = getelementptr inbounds %struct.ShellState* %171, i32 0, i32 8
  %173 = load %struct._IO_FILE** %172, align 8
  %174 = load i32* %iCur, align 4
  %175 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %173, i8* getelementptr inbounds ([41 x i8]* @.str114, i32 0, i32 0), i32 %174)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %176 = load %struct.sqlite3** %1, align 8
  %177 = load i32* %3, align 4
  %178 = call i32 @sqlite3_db_status(%struct.sqlite3* %176, i32 2, i32* %iCur, i32* %iHiwtr, i32 %177)
  %179 = load %struct.ShellState** %2, align 8
  %180 = getelementptr inbounds %struct.ShellState* %179, i32 0, i32 8
  %181 = load %struct._IO_FILE** %180, align 8
  %182 = load i32* %iCur, align 4
  %183 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %181, i8* getelementptr inbounds ([47 x i8]* @.str115, i32 0, i32 0), i32 %182)
  store i32 -1, i32* %iCur, align 4
  store i32 -1, i32* %iHiwtr, align 4
  %184 = load %struct.sqlite3** %1, align 8
  %185 = load i32* %3, align 4
  %186 = call i32 @sqlite3_db_status(%struct.sqlite3* %184, i32 3, i32* %iCur, i32* %iHiwtr, i32 %185)
  %187 = load %struct.ShellState** %2, align 8
  %188 = getelementptr inbounds %struct.ShellState* %187, i32 0, i32 8
  %189 = load %struct._IO_FILE** %188, align 8
  %190 = load i32* %iCur, align 4
  %191 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %189, i8* getelementptr inbounds ([47 x i8]* @.str116, i32 0, i32 0), i32 %190)
  br label %192

; <label>:192                                     ; preds = %146, %103, %98, %95
  %193 = load %struct.ShellState** %2, align 8
  %194 = icmp ne %struct.ShellState* %193, null
  br i1 %194, label %195, label %249

; <label>:195                                     ; preds = %192
  %196 = load %struct.ShellState** %2, align 8
  %197 = getelementptr inbounds %struct.ShellState* %196, i32 0, i32 8
  %198 = load %struct._IO_FILE** %197, align 8
  %199 = icmp ne %struct._IO_FILE* %198, null
  br i1 %199, label %200, label %249

; <label>:200                                     ; preds = %195
  %201 = load %struct.sqlite3** %1, align 8
  %202 = icmp ne %struct.sqlite3* %201, null
  br i1 %202, label %203, label %249

; <label>:203                                     ; preds = %200
  %204 = load %struct.ShellState** %2, align 8
  %205 = getelementptr inbounds %struct.ShellState* %204, i32 0, i32 26
  %206 = load %struct.sqlite3_stmt** %205, align 8
  %207 = icmp ne %struct.sqlite3_stmt* %206, null
  br i1 %207, label %208, label %249

; <label>:208                                     ; preds = %203
  %209 = load %struct.ShellState** %2, align 8
  %210 = getelementptr inbounds %struct.ShellState* %209, i32 0, i32 26
  %211 = load %struct.sqlite3_stmt** %210, align 8
  %212 = load i32* %3, align 4
  %213 = call i32 @sqlite3_stmt_status(%struct.sqlite3_stmt* %211, i32 1, i32 %212)
  store i32 %213, i32* %iCur, align 4
  %214 = load %struct.ShellState** %2, align 8
  %215 = getelementptr inbounds %struct.ShellState* %214, i32 0, i32 8
  %216 = load %struct._IO_FILE** %215, align 8
  %217 = load i32* %iCur, align 4
  %218 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %216, i8* getelementptr inbounds ([41 x i8]* @.str117, i32 0, i32 0), i32 %217)
  %219 = load %struct.ShellState** %2, align 8
  %220 = getelementptr inbounds %struct.ShellState* %219, i32 0, i32 26
  %221 = load %struct.sqlite3_stmt** %220, align 8
  %222 = load i32* %3, align 4
  %223 = call i32 @sqlite3_stmt_status(%struct.sqlite3_stmt* %221, i32 2, i32 %222)
  store i32 %223, i32* %iCur, align 4
  %224 = load %struct.ShellState** %2, align 8
  %225 = getelementptr inbounds %struct.ShellState* %224, i32 0, i32 8
  %226 = load %struct._IO_FILE** %225, align 8
  %227 = load i32* %iCur, align 4
  %228 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %226, i8* getelementptr inbounds ([41 x i8]* @.str118, i32 0, i32 0), i32 %227)
  %229 = load %struct.ShellState** %2, align 8
  %230 = getelementptr inbounds %struct.ShellState* %229, i32 0, i32 26
  %231 = load %struct.sqlite3_stmt** %230, align 8
  %232 = load i32* %3, align 4
  %233 = call i32 @sqlite3_stmt_status(%struct.sqlite3_stmt* %231, i32 3, i32 %232)
  store i32 %233, i32* %iCur, align 4
  %234 = load %struct.ShellState** %2, align 8
  %235 = getelementptr inbounds %struct.ShellState* %234, i32 0, i32 8
  %236 = load %struct._IO_FILE** %235, align 8
  %237 = load i32* %iCur, align 4
  %238 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %236, i8* getelementptr inbounds ([41 x i8]* @.str119, i32 0, i32 0), i32 %237)
  %239 = load %struct.ShellState** %2, align 8
  %240 = getelementptr inbounds %struct.ShellState* %239, i32 0, i32 26
  %241 = load %struct.sqlite3_stmt** %240, align 8
  %242 = load i32* %3, align 4
  %243 = call i32 @sqlite3_stmt_status(%struct.sqlite3_stmt* %241, i32 4, i32 %242)
  store i32 %243, i32* %iCur, align 4
  %244 = load %struct.ShellState** %2, align 8
  %245 = getelementptr inbounds %struct.ShellState* %244, i32 0, i32 8
  %246 = load %struct._IO_FILE** %245, align 8
  %247 = load i32* %iCur, align 4
  %248 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %246, i8* getelementptr inbounds ([41 x i8]* @.str120, i32 0, i32 0), i32 %247)
  br label %249

; <label>:249                                     ; preds = %208, %203, %200, %195, %192
  ret i32 0
}

; Function Attrs: nounwind uwtable
define internal void @display_scanstats(%struct.sqlite3* %db, %struct.ShellState* %pArg) #0 {
  %1 = alloca %struct.sqlite3*, align 8
  %2 = alloca %struct.ShellState*, align 8
  store %struct.sqlite3* %db, %struct.sqlite3** %1, align 8
  store %struct.ShellState* %pArg, %struct.ShellState** %2, align 8
  ret void
}

declare i32 @sqlite3_status(i32, i32*, i32*, i32) #2

declare i32 @sqlite3_db_status(%struct.sqlite3*, i32, i32*, i32*, i32) #2

declare i32 @sqlite3_stmt_status(%struct.sqlite3_stmt*, i32, i32) #2

; Function Attrs: nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #5

declare i32 @sqlite3_strnicmp(i8*, i8*, i32) #2

declare i8* @sqlite3_realloc64(i8*, i64) #2

; Function Attrs: nounwind uwtable
define internal i32 @str_in_array(i8* %zStr, i8** %azArray) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %3 = alloca i8**, align 8
  %i = alloca i32, align 4
  store i8* %zStr, i8** %2, align 8
  store i8** %azArray, i8*** %3, align 8
  store i32 0, i32* %i, align 4
  br label %4

; <label>:4                                       ; preds = %22, %0
  %5 = load i32* %i, align 4
  %6 = sext i32 %5 to i64
  %7 = load i8*** %3, align 8
  %8 = getelementptr inbounds i8** %7, i64 %6
  %9 = load i8** %8, align 8
  %10 = icmp ne i8* %9, null
  br i1 %10, label %11, label %25

; <label>:11                                      ; preds = %4
  %12 = load i8** %2, align 8
  %13 = load i32* %i, align 4
  %14 = sext i32 %13 to i64
  %15 = load i8*** %3, align 8
  %16 = getelementptr inbounds i8** %15, i64 %14
  %17 = load i8** %16, align 8
  %18 = call i32 @strcmp(i8* %12, i8* %17) #7
  %19 = icmp eq i32 0, %18
  br i1 %19, label %20, label %21

; <label>:20                                      ; preds = %11
  store i32 1, i32* %1
  br label %26

; <label>:21                                      ; preds = %11
  br label %22

; <label>:22                                      ; preds = %21
  %23 = load i32* %i, align 4
  %24 = add nsw i32 %23, 1
  store i32 %24, i32* %i, align 4
  br label %4

; <label>:25                                      ; preds = %4
  store i32 0, i32* %1
  br label %26

; <label>:26                                      ; preds = %25, %20
  %27 = load i32* %1
  ret i32 %27
}

declare i32 @sqlite3_reset(%struct.sqlite3_stmt*) #2

; Function Attrs: nounwind readonly
declare i32 @strncmp(i8*, i8*, i64) #1

declare i32 @sqlite3_open(i8*, %struct.sqlite3**) #2

declare %struct.sqlite3_backup* @sqlite3_backup_init(%struct.sqlite3*, i8*, %struct.sqlite3*, i8*) #2

declare i32 @sqlite3_backup_step(%struct.sqlite3_backup*, i32) #2

declare i32 @sqlite3_backup_finish(%struct.sqlite3_backup*) #2

; Function Attrs: nounwind uwtable
define internal i32 @booleanValue(i8* %zArg) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %i = alloca i32, align 4
  store i8* %zArg, i8** %2, align 8
  %3 = load i8** %2, align 8
  %4 = getelementptr inbounds i8* %3, i64 0
  %5 = load i8* %4, align 1
  %6 = sext i8 %5 to i32
  %7 = icmp eq i32 %6, 48
  br i1 %7, label %8, label %28

; <label>:8                                       ; preds = %0
  %9 = load i8** %2, align 8
  %10 = getelementptr inbounds i8* %9, i64 1
  %11 = load i8* %10, align 1
  %12 = sext i8 %11 to i32
  %13 = icmp eq i32 %12, 120
  br i1 %13, label %14, label %28

; <label>:14                                      ; preds = %8
  store i32 2, i32* %i, align 4
  br label %15

; <label>:15                                      ; preds = %24, %14
  %16 = load i32* %i, align 4
  %17 = sext i32 %16 to i64
  %18 = load i8** %2, align 8
  %19 = getelementptr inbounds i8* %18, i64 %17
  %20 = load i8* %19, align 1
  %21 = call i32 @hexDigitValue(i8 signext %20)
  %22 = icmp sge i32 %21, 0
  br i1 %22, label %23, label %27

; <label>:23                                      ; preds = %15
  br label %24

; <label>:24                                      ; preds = %23
  %25 = load i32* %i, align 4
  %26 = add nsw i32 %25, 1
  store i32 %26, i32* %i, align 4
  br label %15

; <label>:27                                      ; preds = %15
  br label %52

; <label>:28                                      ; preds = %8, %0
  store i32 0, i32* %i, align 4
  br label %29

; <label>:29                                      ; preds = %48, %28
  %30 = load i32* %i, align 4
  %31 = sext i32 %30 to i64
  %32 = load i8** %2, align 8
  %33 = getelementptr inbounds i8* %32, i64 %31
  %34 = load i8* %33, align 1
  %35 = sext i8 %34 to i32
  %36 = icmp sge i32 %35, 48
  br i1 %36, label %37, label %45

; <label>:37                                      ; preds = %29
  %38 = load i32* %i, align 4
  %39 = sext i32 %38 to i64
  %40 = load i8** %2, align 8
  %41 = getelementptr inbounds i8* %40, i64 %39
  %42 = load i8* %41, align 1
  %43 = sext i8 %42 to i32
  %44 = icmp sle i32 %43, 57
  br label %45

; <label>:45                                      ; preds = %37, %29
  %46 = phi i1 [ false, %29 ], [ %44, %37 ]
  br i1 %46, label %47, label %51

; <label>:47                                      ; preds = %45
  br label %48

; <label>:48                                      ; preds = %47
  %49 = load i32* %i, align 4
  %50 = add nsw i32 %49, 1
  store i32 %50, i32* %i, align 4
  br label %29

; <label>:51                                      ; preds = %45
  br label %52

; <label>:52                                      ; preds = %51, %27
  %53 = load i32* %i, align 4
  %54 = icmp sgt i32 %53, 0
  br i1 %54, label %55, label %68

; <label>:55                                      ; preds = %52
  %56 = load i32* %i, align 4
  %57 = sext i32 %56 to i64
  %58 = load i8** %2, align 8
  %59 = getelementptr inbounds i8* %58, i64 %57
  %60 = load i8* %59, align 1
  %61 = sext i8 %60 to i32
  %62 = icmp eq i32 %61, 0
  br i1 %62, label %63, label %68

; <label>:63                                      ; preds = %55
  %64 = load i8** %2, align 8
  %65 = call i64 @integerValue(i8* %64)
  %66 = and i64 %65, 4294967295
  %67 = trunc i64 %66 to i32
  store i32 %67, i32* %1
  br label %90

; <label>:68                                      ; preds = %55, %52
  %69 = load i8** %2, align 8
  %70 = call i32 @sqlite3_stricmp(i8* %69, i8* getelementptr inbounds ([3 x i8]* @.str297, i32 0, i32 0))
  %71 = icmp eq i32 %70, 0
  br i1 %71, label %76, label %72

; <label>:72                                      ; preds = %68
  %73 = load i8** %2, align 8
  %74 = call i32 @sqlite3_stricmp(i8* %73, i8* getelementptr inbounds ([4 x i8]* @.str440, i32 0, i32 0))
  %75 = icmp eq i32 %74, 0
  br i1 %75, label %76, label %77

; <label>:76                                      ; preds = %72, %68
  store i32 1, i32* %1
  br label %90

; <label>:77                                      ; preds = %72
  %78 = load i8** %2, align 8
  %79 = call i32 @sqlite3_stricmp(i8* %78, i8* getelementptr inbounds ([4 x i8]* @.str262, i32 0, i32 0))
  %80 = icmp eq i32 %79, 0
  br i1 %80, label %85, label %81

; <label>:81                                      ; preds = %77
  %82 = load i8** %2, align 8
  %83 = call i32 @sqlite3_stricmp(i8* %82, i8* getelementptr inbounds ([3 x i8]* @.str441, i32 0, i32 0))
  %84 = icmp eq i32 %83, 0
  br i1 %84, label %85, label %86

; <label>:85                                      ; preds = %81, %77
  store i32 0, i32* %1
  br label %90

; <label>:86                                      ; preds = %81
  %87 = load %struct._IO_FILE** @stderr, align 8
  %88 = load i8** %2, align 8
  %89 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %87, i8* getelementptr inbounds ([50 x i8]* @.str442, i32 0, i32 0), i8* %88)
  store i32 0, i32* %1
  br label %90

; <label>:90                                      ; preds = %86, %85, %76, %63
  %91 = load i32* %1
  ret i32 %91
}

; Function Attrs: nounwind uwtable
define internal void @test_breakpoint() #0 {
  %1 = load i32* @test_breakpoint.nCall, align 4
  %2 = add nsw i32 %1, 1
  store i32 %2, i32* @test_breakpoint.nCall, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @tryToClone(%struct.ShellState* %p, i8* %zNewDb) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca i8*, align 8
  %rc = alloca i32, align 4
  %newDb = alloca %struct.sqlite3*, align 8
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store i8* %zNewDb, i8** %2, align 8
  store %struct.sqlite3* null, %struct.sqlite3** %newDb, align 8
  %3 = load i8** %2, align 8
  %4 = call i32 @access(i8* %3, i32 0) #5
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %6, label %10

; <label>:6                                       ; preds = %0
  %7 = load %struct._IO_FILE** @stderr, align 8
  %8 = load i8** %2, align 8
  %9 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([27 x i8]* @.str418, i32 0, i32 0), i8* %8)
  br label %40

; <label>:10                                      ; preds = %0
  %11 = load i8** %2, align 8
  %12 = call i32 @sqlite3_open(i8* %11, %struct.sqlite3** %newDb)
  store i32 %12, i32* %rc, align 4
  %13 = load i32* %rc, align 4
  %14 = icmp ne i32 %13, 0
  br i1 %14, label %15, label %20

; <label>:15                                      ; preds = %10
  %16 = load %struct._IO_FILE** @stderr, align 8
  %17 = load %struct.sqlite3** %newDb, align 8
  %18 = call i8* @sqlite3_errmsg(%struct.sqlite3* %17)
  %19 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([35 x i8]* @.str419, i32 0, i32 0), i8* %18)
  br label %37

; <label>:20                                      ; preds = %10
  %21 = load %struct.ShellState** %1, align 8
  %22 = getelementptr inbounds %struct.ShellState* %21, i32 0, i32 0
  %23 = load %struct.sqlite3** %22, align 8
  %24 = call i32 @sqlite3_exec(%struct.sqlite3* %23, i8* getelementptr inbounds ([27 x i8]* @.str420, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  %25 = load %struct.sqlite3** %newDb, align 8
  %26 = call i32 @sqlite3_exec(%struct.sqlite3* %25, i8* getelementptr inbounds ([17 x i8]* @.str421, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  %27 = load %struct.ShellState** %1, align 8
  %28 = load %struct.sqlite3** %newDb, align 8
  call void @tryToCloneSchema(%struct.ShellState* %27, %struct.sqlite3* %28, i8* getelementptr inbounds ([13 x i8]* @.str422, i32 0, i32 0), void (%struct.ShellState*, %struct.sqlite3*, i8*)* @tryToCloneData)
  %29 = load %struct.ShellState** %1, align 8
  %30 = load %struct.sqlite3** %newDb, align 8
  call void @tryToCloneSchema(%struct.ShellState* %29, %struct.sqlite3* %30, i8* getelementptr inbounds ([14 x i8]* @.str423, i32 0, i32 0), void (%struct.ShellState*, %struct.sqlite3*, i8*)* null)
  %31 = load %struct.sqlite3** %newDb, align 8
  %32 = call i32 @sqlite3_exec(%struct.sqlite3* %31, i8* getelementptr inbounds ([8 x i8]* @.str424, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  %33 = load %struct.ShellState** %1, align 8
  %34 = getelementptr inbounds %struct.ShellState* %33, i32 0, i32 0
  %35 = load %struct.sqlite3** %34, align 8
  %36 = call i32 @sqlite3_exec(%struct.sqlite3* %35, i8* getelementptr inbounds ([28 x i8]* @.str163, i32 0, i32 0), i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** null)
  br label %37

; <label>:37                                      ; preds = %20, %15
  %38 = load %struct.sqlite3** %newDb, align 8
  %39 = call i32 @sqlite3_close(%struct.sqlite3* %38)
  br label %40

; <label>:40                                      ; preds = %37, %6
  ret void
}

declare i32 @sqlite3_exec(%struct.sqlite3*, i8*, i32 (i8*, i32, i8**, i8**)*, i8*, i8**) #2

; Function Attrs: nounwind uwtable
define internal i32 @callback(i8* %pArg, i32 %nArg, i8** %azArg, i8** %azCol) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %4 = alloca i8**, align 8
  store i8* %pArg, i8** %1, align 8
  store i32 %nArg, i32* %2, align 4
  store i8** %azArg, i8*** %3, align 8
  store i8** %azCol, i8*** %4, align 8
  %5 = load i8** %1, align 8
  %6 = load i32* %2, align 4
  %7 = load i8*** %3, align 8
  %8 = load i8*** %4, align 8
  %9 = call i32 @shell_callback(i8* %5, i32 %6, i8** %7, i8** %8, i32* null)
  ret i32 %9
}

; Function Attrs: nounwind uwtable
define internal i32 @shell_dbinfo_command(%struct.ShellState* %p, i32 %nArg, i8** %azArg) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.ShellState*, align 8
  %3 = alloca i32, align 4
  %4 = alloca i8**, align 8
  %pFile = alloca %struct.sqlite3_file*, align 8
  %i = alloca i32, align 4
  %zSchemaTab = alloca i8*, align 8
  %zDb = alloca i8*, align 8
  %aHdr = alloca [100 x i8], align 16
  %ofst = alloca i32, align 4
  %val = alloca i32, align 4
  %zSql = alloca i8*, align 8
  %val1 = alloca i32, align 4
  store %struct.ShellState* %p, %struct.ShellState** %2, align 8
  store i32 %nArg, i32* %3, align 4
  store i8** %azArg, i8*** %4, align 8
  %5 = load i32* %3, align 4
  %6 = icmp sge i32 %5, 2
  br i1 %6, label %7, label %11

; <label>:7                                       ; preds = %0
  %8 = load i8*** %4, align 8
  %9 = getelementptr inbounds i8** %8, i64 1
  %10 = load i8** %9, align 8
  br label %12

; <label>:11                                      ; preds = %0
  br label %12

; <label>:12                                      ; preds = %11, %7
  %13 = phi i8* [ %10, %7 ], [ getelementptr inbounds ([5 x i8]* @.str140, i32 0, i32 0), %11 ]
  store i8* %13, i8** %zDb, align 8
  %14 = load %struct.ShellState** %2, align 8
  call void @open_db(%struct.ShellState* %14, i32 0)
  %15 = load %struct.ShellState** %2, align 8
  %16 = getelementptr inbounds %struct.ShellState* %15, i32 0, i32 0
  %17 = load %struct.sqlite3** %16, align 8
  %18 = icmp eq %struct.sqlite3* %17, null
  br i1 %18, label %19, label %20

; <label>:19                                      ; preds = %12
  store i32 1, i32* %1
  br label %196

; <label>:20                                      ; preds = %12
  %21 = load %struct.ShellState** %2, align 8
  %22 = getelementptr inbounds %struct.ShellState* %21, i32 0, i32 0
  %23 = load %struct.sqlite3** %22, align 8
  %24 = load i8** %zDb, align 8
  %25 = bitcast %struct.sqlite3_file** %pFile to i8*
  %26 = call i32 @sqlite3_file_control(%struct.sqlite3* %23, i8* %24, i32 7, i8* %25)
  %27 = load %struct.sqlite3_file** %pFile, align 8
  %28 = icmp eq %struct.sqlite3_file* %27, null
  br i1 %28, label %41, label %29

; <label>:29                                      ; preds = %20
  %30 = load %struct.sqlite3_file** %pFile, align 8
  %31 = getelementptr inbounds %struct.sqlite3_file* %30, i32 0, i32 0
  %32 = load %struct.sqlite3_io_methods** %31, align 8
  %33 = icmp eq %struct.sqlite3_io_methods* %32, null
  br i1 %33, label %41, label %34

; <label>:34                                      ; preds = %29
  %35 = load %struct.sqlite3_file** %pFile, align 8
  %36 = getelementptr inbounds %struct.sqlite3_file* %35, i32 0, i32 0
  %37 = load %struct.sqlite3_io_methods** %36, align 8
  %38 = getelementptr inbounds %struct.sqlite3_io_methods* %37, i32 0, i32 2
  %39 = load i32 (%struct.sqlite3_file*, i8*, i32, i64)** %38, align 8
  %40 = icmp eq i32 (%struct.sqlite3_file*, i8*, i32, i64)* %39, null
  br i1 %40, label %41, label %42

; <label>:41                                      ; preds = %34, %29, %20
  store i32 1, i32* %1
  br label %196

; <label>:42                                      ; preds = %34
  %43 = load %struct.sqlite3_file** %pFile, align 8
  %44 = getelementptr inbounds %struct.sqlite3_file* %43, i32 0, i32 0
  %45 = load %struct.sqlite3_io_methods** %44, align 8
  %46 = getelementptr inbounds %struct.sqlite3_io_methods* %45, i32 0, i32 2
  %47 = load i32 (%struct.sqlite3_file*, i8*, i32, i64)** %46, align 8
  %48 = load %struct.sqlite3_file** %pFile, align 8
  %49 = getelementptr inbounds [100 x i8]* %aHdr, i32 0, i32 0
  %50 = call i32 %47(%struct.sqlite3_file* %48, i8* %49, i32 100, i64 0)
  store i32 %50, i32* %i, align 4
  %51 = load i32* %i, align 4
  %52 = icmp ne i32 %51, 0
  br i1 %52, label %53, label %56

; <label>:53                                      ; preds = %42
  %54 = load %struct._IO_FILE** @stderr, align 8
  %55 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %54, i8* getelementptr inbounds ([32 x i8]* @.str406, i32 0, i32 0))
  store i32 1, i32* %1
  br label %196

; <label>:56                                      ; preds = %42
  %57 = getelementptr inbounds [100 x i8]* %aHdr, i32 0, i32 0
  %58 = getelementptr inbounds i8* %57, i64 16
  %59 = call i32 @get2byteInt(i8* %58)
  store i32 %59, i32* %i, align 4
  %60 = load i32* %i, align 4
  %61 = icmp eq i32 %60, 1
  br i1 %61, label %62, label %63

; <label>:62                                      ; preds = %56
  store i32 65536, i32* %i, align 4
  br label %63

; <label>:63                                      ; preds = %62, %56
  %64 = load %struct.ShellState** %2, align 8
  %65 = getelementptr inbounds %struct.ShellState* %64, i32 0, i32 8
  %66 = load %struct._IO_FILE** %65, align 8
  %67 = load i32* %i, align 4
  %68 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %66, i8* getelementptr inbounds ([10 x i8]* @.str407, i32 0, i32 0), i8* getelementptr inbounds ([20 x i8]* @.str408, i32 0, i32 0), i32 %67)
  %69 = load %struct.ShellState** %2, align 8
  %70 = getelementptr inbounds %struct.ShellState* %69, i32 0, i32 8
  %71 = load %struct._IO_FILE** %70, align 8
  %72 = getelementptr inbounds [100 x i8]* %aHdr, i32 0, i64 18
  %73 = load i8* %72, align 1
  %74 = zext i8 %73 to i32
  %75 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %71, i8* getelementptr inbounds ([10 x i8]* @.str407, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8]* @.str409, i32 0, i32 0), i32 %74)
  %76 = load %struct.ShellState** %2, align 8
  %77 = getelementptr inbounds %struct.ShellState* %76, i32 0, i32 8
  %78 = load %struct._IO_FILE** %77, align 8
  %79 = getelementptr inbounds [100 x i8]* %aHdr, i32 0, i64 19
  %80 = load i8* %79, align 1
  %81 = zext i8 %80 to i32
  %82 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %78, i8* getelementptr inbounds ([10 x i8]* @.str407, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8]* @.str410, i32 0, i32 0), i32 %81)
  %83 = load %struct.ShellState** %2, align 8
  %84 = getelementptr inbounds %struct.ShellState* %83, i32 0, i32 8
  %85 = load %struct._IO_FILE** %84, align 8
  %86 = getelementptr inbounds [100 x i8]* %aHdr, i32 0, i64 20
  %87 = load i8* %86, align 1
  %88 = zext i8 %87 to i32
  %89 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %85, i8* getelementptr inbounds ([10 x i8]* @.str407, i32 0, i32 0), i8* getelementptr inbounds ([16 x i8]* @.str411, i32 0, i32 0), i32 %88)
  store i32 0, i32* %i, align 4
  br label %90

; <label>:90                                      ; preds = %146, %63
  %91 = load i32* %i, align 4
  %92 = sext i32 %91 to i64
  %93 = icmp ult i64 %92, 12
  br i1 %93, label %94, label %149

; <label>:94                                      ; preds = %90
  %95 = load i32* %i, align 4
  %96 = sext i32 %95 to i64
  %97 = getelementptr inbounds [12 x %struct.anon.14]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @shell_dbinfo_command.aField to [12 x %struct.anon.14]*), i32 0, i64 %96
  %98 = getelementptr inbounds %struct.anon.14* %97, i32 0, i32 1
  %99 = load i32* %98, align 4
  store i32 %99, i32* %ofst, align 4
  %100 = getelementptr inbounds [100 x i8]* %aHdr, i32 0, i32 0
  %101 = load i32* %ofst, align 4
  %102 = sext i32 %101 to i64
  %103 = getelementptr inbounds i8* %100, i64 %102
  %104 = call i32 @get4byteInt(i8* %103)
  store i32 %104, i32* %val, align 4
  %105 = load %struct.ShellState** %2, align 8
  %106 = getelementptr inbounds %struct.ShellState* %105, i32 0, i32 8
  %107 = load %struct._IO_FILE** %106, align 8
  %108 = load i32* %i, align 4
  %109 = sext i32 %108 to i64
  %110 = getelementptr inbounds [12 x %struct.anon.14]* bitcast (<{ { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] }, { i8*, i32, [4 x i8] } }>* @shell_dbinfo_command.aField to [12 x %struct.anon.14]*), i32 0, i64 %109
  %111 = getelementptr inbounds %struct.anon.14* %110, i32 0, i32 0
  %112 = load i8** %111, align 8
  %113 = load i32* %val, align 4
  %114 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %107, i8* getelementptr inbounds ([9 x i8]* @.str412, i32 0, i32 0), i8* %112, i32 %113)
  %115 = load i32* %ofst, align 4
  switch i32 %115, label %141 [
    i32 56, label %116
  ]

; <label>:116                                     ; preds = %94
  %117 = load i32* %val, align 4
  %118 = icmp eq i32 %117, 1
  br i1 %118, label %119, label %124

; <label>:119                                     ; preds = %116
  %120 = load %struct.ShellState** %2, align 8
  %121 = getelementptr inbounds %struct.ShellState* %120, i32 0, i32 8
  %122 = load %struct._IO_FILE** %121, align 8
  %123 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %122, i8* getelementptr inbounds ([8 x i8]* @.str413, i32 0, i32 0))
  br label %124

; <label>:124                                     ; preds = %119, %116
  %125 = load i32* %val, align 4
  %126 = icmp eq i32 %125, 2
  br i1 %126, label %127, label %132

; <label>:127                                     ; preds = %124
  %128 = load %struct.ShellState** %2, align 8
  %129 = getelementptr inbounds %struct.ShellState* %128, i32 0, i32 8
  %130 = load %struct._IO_FILE** %129, align 8
  %131 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %130, i8* getelementptr inbounds ([11 x i8]* @.str414, i32 0, i32 0))
  br label %132

; <label>:132                                     ; preds = %127, %124
  %133 = load i32* %val, align 4
  %134 = icmp eq i32 %133, 3
  br i1 %134, label %135, label %140

; <label>:135                                     ; preds = %132
  %136 = load %struct.ShellState** %2, align 8
  %137 = getelementptr inbounds %struct.ShellState* %136, i32 0, i32 8
  %138 = load %struct._IO_FILE** %137, align 8
  %139 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %138, i8* getelementptr inbounds ([11 x i8]* @.str415, i32 0, i32 0))
  br label %140

; <label>:140                                     ; preds = %135, %132
  br label %141

; <label>:141                                     ; preds = %140, %94
  %142 = load %struct.ShellState** %2, align 8
  %143 = getelementptr inbounds %struct.ShellState* %142, i32 0, i32 8
  %144 = load %struct._IO_FILE** %143, align 8
  %145 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %144, i8* getelementptr inbounds ([2 x i8]* @.str50, i32 0, i32 0))
  br label %146

; <label>:146                                     ; preds = %141
  %147 = load i32* %i, align 4
  %148 = add nsw i32 %147, 1
  store i32 %148, i32* %i, align 4
  br label %90

; <label>:149                                     ; preds = %90
  %150 = load i8** %zDb, align 8
  %151 = icmp eq i8* %150, null
  br i1 %151, label %152, label %154

; <label>:152                                     ; preds = %149
  %153 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([19 x i8]* @.str416, i32 0, i32 0))
  store i8* %153, i8** %zSchemaTab, align 8
  br label %164

; <label>:154                                     ; preds = %149
  %155 = load i8** %zDb, align 8
  %156 = call i32 @strcmp(i8* %155, i8* getelementptr inbounds ([5 x i8]* @.str309, i32 0, i32 0)) #7
  %157 = icmp eq i32 %156, 0
  br i1 %157, label %158, label %160

; <label>:158                                     ; preds = %154
  %159 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* getelementptr inbounds ([19 x i8]* @.str279, i32 0, i32 0))
  store i8* %159, i8** %zSchemaTab, align 8
  br label %163

; <label>:160                                     ; preds = %154
  %161 = load i8** %zDb, align 8
  %162 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([19 x i8]* @.str417, i32 0, i32 0), i8* %161)
  store i8* %162, i8** %zSchemaTab, align 8
  br label %163

; <label>:163                                     ; preds = %160, %158
  br label %164

; <label>:164                                     ; preds = %163, %152
  store i32 0, i32* %i, align 4
  br label %165

; <label>:165                                     ; preds = %191, %164
  %166 = load i32* %i, align 4
  %167 = sext i32 %166 to i64
  %168 = icmp ult i64 %167, 5
  br i1 %168, label %169, label %194

; <label>:169                                     ; preds = %165
  %170 = load i32* %i, align 4
  %171 = sext i32 %170 to i64
  %172 = getelementptr inbounds [5 x %struct.anon.15]* @shell_dbinfo_command.aQuery, i32 0, i64 %171
  %173 = getelementptr inbounds %struct.anon.15* %172, i32 0, i32 1
  %174 = load i8** %173, align 8
  %175 = load i8** %zSchemaTab, align 8
  %176 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* %174, i8* %175)
  store i8* %176, i8** %zSql, align 8
  %177 = load %struct.ShellState** %2, align 8
  %178 = load i8** %zSql, align 8
  %179 = call i32 @db_int(%struct.ShellState* %177, i8* %178)
  store i32 %179, i32* %val1, align 4
  %180 = load i8** %zSql, align 8
  call void @sqlite3_free(i8* %180)
  %181 = load %struct.ShellState** %2, align 8
  %182 = getelementptr inbounds %struct.ShellState* %181, i32 0, i32 8
  %183 = load %struct._IO_FILE** %182, align 8
  %184 = load i32* %i, align 4
  %185 = sext i32 %184 to i64
  %186 = getelementptr inbounds [5 x %struct.anon.15]* @shell_dbinfo_command.aQuery, i32 0, i64 %185
  %187 = getelementptr inbounds %struct.anon.15* %186, i32 0, i32 0
  %188 = load i8** %187, align 8
  %189 = load i32* %val1, align 4
  %190 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %183, i8* getelementptr inbounds ([10 x i8]* @.str407, i32 0, i32 0), i8* %188, i32 %189)
  br label %191

; <label>:191                                     ; preds = %169
  %192 = load i32* %i, align 4
  %193 = add nsw i32 %192, 1
  store i32 %193, i32* %i, align 4
  br label %165

; <label>:194                                     ; preds = %165
  %195 = load i8** %zSchemaTab, align 8
  call void @sqlite3_free(i8* %195)
  store i32 0, i32* %1
  br label %196

; <label>:196                                     ; preds = %194, %53, %41, %19
  %197 = load i32* %1
  ret i32 %197
}

; Function Attrs: nounwind uwtable
define internal i32 @run_schema_dump_query(%struct.ShellState* %p, i8* %zQuery) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.ShellState*, align 8
  %3 = alloca i8*, align 8
  %rc = alloca i32, align 4
  %zErr = alloca i8*, align 8
  %zQ2 = alloca i8*, align 8
  %len = alloca i32, align 4
  store %struct.ShellState* %p, %struct.ShellState** %2, align 8
  store i8* %zQuery, i8** %3, align 8
  store i8* null, i8** %zErr, align 8
  %4 = load %struct.ShellState** %2, align 8
  %5 = getelementptr inbounds %struct.ShellState* %4, i32 0, i32 0
  %6 = load %struct.sqlite3** %5, align 8
  %7 = load i8** %3, align 8
  %8 = load %struct.ShellState** %2, align 8
  %9 = bitcast %struct.ShellState* %8 to i8*
  %10 = call i32 @sqlite3_exec(%struct.sqlite3* %6, i8* %7, i32 (i8*, i32, i8**, i8**)* @dump_callback, i8* %9, i8** %zErr)
  store i32 %10, i32* %rc, align 4
  %11 = load i32* %rc, align 4
  %12 = icmp eq i32 %11, 11
  br i1 %12, label %13, label %63

; <label>:13                                      ; preds = %0
  %14 = load i8** %3, align 8
  %15 = call i32 @strlen30(i8* %14)
  store i32 %15, i32* %len, align 4
  %16 = load %struct.ShellState** %2, align 8
  %17 = getelementptr inbounds %struct.ShellState* %16, i32 0, i32 8
  %18 = load %struct._IO_FILE** %17, align 8
  %19 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([35 x i8]* @.str362, i32 0, i32 0))
  %20 = load i8** %zErr, align 8
  %21 = icmp ne i8* %20, null
  br i1 %21, label %22, label %29

; <label>:22                                      ; preds = %13
  %23 = load %struct.ShellState** %2, align 8
  %24 = getelementptr inbounds %struct.ShellState* %23, i32 0, i32 8
  %25 = load %struct._IO_FILE** %24, align 8
  %26 = load i8** %zErr, align 8
  %27 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([20 x i8]* @.str363, i32 0, i32 0), i8* %26)
  %28 = load i8** %zErr, align 8
  call void @sqlite3_free(i8* %28)
  store i8* null, i8** %zErr, align 8
  br label %29

; <label>:29                                      ; preds = %22, %13
  %30 = load i32* %len, align 4
  %31 = add nsw i32 %30, 100
  %32 = sext i32 %31 to i64
  %33 = call noalias i8* @malloc(i64 %32) #5
  store i8* %33, i8** %zQ2, align 8
  %34 = load i8** %zQ2, align 8
  %35 = icmp eq i8* %34, null
  br i1 %35, label %36, label %38

; <label>:36                                      ; preds = %29
  %37 = load i32* %rc, align 4
  store i32 %37, i32* %1
  br label %65

; <label>:38                                      ; preds = %29
  %39 = load i32* %len, align 4
  %40 = add nsw i32 %39, 100
  %41 = load i8** %zQ2, align 8
  %42 = load i8** %3, align 8
  %43 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 %40, i8* %41, i8* getelementptr inbounds ([23 x i8]* @.str364, i32 0, i32 0), i8* %42)
  %44 = load %struct.ShellState** %2, align 8
  %45 = getelementptr inbounds %struct.ShellState* %44, i32 0, i32 0
  %46 = load %struct.sqlite3** %45, align 8
  %47 = load i8** %zQ2, align 8
  %48 = load %struct.ShellState** %2, align 8
  %49 = bitcast %struct.ShellState* %48 to i8*
  %50 = call i32 @sqlite3_exec(%struct.sqlite3* %46, i8* %47, i32 (i8*, i32, i8**, i8**)* @dump_callback, i8* %49, i8** %zErr)
  store i32 %50, i32* %rc, align 4
  %51 = load i32* %rc, align 4
  %52 = icmp ne i32 %51, 0
  br i1 %52, label %53, label %59

; <label>:53                                      ; preds = %38
  %54 = load %struct.ShellState** %2, align 8
  %55 = getelementptr inbounds %struct.ShellState* %54, i32 0, i32 8
  %56 = load %struct._IO_FILE** %55, align 8
  %57 = load i8** %zErr, align 8
  %58 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %56, i8* getelementptr inbounds ([27 x i8]* @.str365, i32 0, i32 0), i8* %57)
  br label %60

; <label>:59                                      ; preds = %38
  store i32 11, i32* %rc, align 4
  br label %60

; <label>:60                                      ; preds = %59, %53
  %61 = load i8** %zErr, align 8
  call void @sqlite3_free(i8* %61)
  %62 = load i8** %zQ2, align 8
  call void @free(i8* %62) #5
  br label %63

; <label>:63                                      ; preds = %60, %0
  %64 = load i32* %rc, align 4
  store i32 %64, i32* %1
  br label %65

; <label>:65                                      ; preds = %63, %36
  %66 = load i32* %1
  ret i32 %66
}

; Function Attrs: nounwind uwtable
define internal i32 @run_table_dump_query(%struct.ShellState* %p, i8* %zSelect, i8* %zFirstRow) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.ShellState*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca i8*, align 8
  %pSelect = alloca %struct.sqlite3_stmt*, align 8
  %rc = alloca i32, align 4
  %nResult = alloca i32, align 4
  %i = alloca i32, align 4
  %z = alloca i8*, align 8
  store %struct.ShellState* %p, %struct.ShellState** %2, align 8
  store i8* %zSelect, i8** %3, align 8
  store i8* %zFirstRow, i8** %4, align 8
  %5 = load %struct.ShellState** %2, align 8
  %6 = getelementptr inbounds %struct.ShellState* %5, i32 0, i32 0
  %7 = load %struct.sqlite3** %6, align 8
  %8 = load i8** %3, align 8
  %9 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %7, i8* %8, i32 -1, %struct.sqlite3_stmt** %pSelect, i8** null)
  store i32 %9, i32* %rc, align 4
  %10 = load i32* %rc, align 4
  %11 = icmp ne i32 %10, 0
  br i1 %11, label %15, label %12

; <label>:12                                      ; preds = %0
  %13 = load %struct.sqlite3_stmt** %pSelect, align 8
  %14 = icmp ne %struct.sqlite3_stmt* %13, null
  br i1 %14, label %35, label %15

; <label>:15                                      ; preds = %12, %0
  %16 = load %struct.ShellState** %2, align 8
  %17 = getelementptr inbounds %struct.ShellState* %16, i32 0, i32 8
  %18 = load %struct._IO_FILE** %17, align 8
  %19 = load i32* %rc, align 4
  %20 = load %struct.ShellState** %2, align 8
  %21 = getelementptr inbounds %struct.ShellState* %20, i32 0, i32 0
  %22 = load %struct.sqlite3** %21, align 8
  %23 = call i8* @sqlite3_errmsg(%struct.sqlite3* %22)
  %24 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([29 x i8]* @.str358, i32 0, i32 0), i32 %19, i8* %23)
  %25 = load i32* %rc, align 4
  %26 = and i32 %25, 255
  %27 = icmp ne i32 %26, 11
  br i1 %27, label %28, label %33

; <label>:28                                      ; preds = %15
  %29 = load %struct.ShellState** %2, align 8
  %30 = getelementptr inbounds %struct.ShellState* %29, i32 0, i32 10
  %31 = load i32* %30, align 4
  %32 = add nsw i32 %31, 1
  store i32 %32, i32* %30, align 4
  br label %33

; <label>:33                                      ; preds = %28, %15
  %34 = load i32* %rc, align 4
  store i32 %34, i32* %1
  br label %149

; <label>:35                                      ; preds = %12
  %36 = load %struct.sqlite3_stmt** %pSelect, align 8
  %37 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %36)
  store i32 %37, i32* %rc, align 4
  %38 = load %struct.sqlite3_stmt** %pSelect, align 8
  %39 = call i32 @sqlite3_column_count(%struct.sqlite3_stmt* %38)
  store i32 %39, i32* %nResult, align 4
  br label %40

; <label>:40                                      ; preds = %120, %35
  %41 = load i32* %rc, align 4
  %42 = icmp eq i32 %41, 100
  br i1 %42, label %43, label %123

; <label>:43                                      ; preds = %40
  %44 = load i8** %4, align 8
  %45 = icmp ne i8* %44, null
  br i1 %45, label %46, label %52

; <label>:46                                      ; preds = %43
  %47 = load %struct.ShellState** %2, align 8
  %48 = getelementptr inbounds %struct.ShellState* %47, i32 0, i32 8
  %49 = load %struct._IO_FILE** %48, align 8
  %50 = load i8** %4, align 8
  %51 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %49, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %50)
  store i8* null, i8** %4, align 8
  br label %52

; <label>:52                                      ; preds = %46, %43
  %53 = load %struct.sqlite3_stmt** %pSelect, align 8
  %54 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %53, i32 0)
  store i8* %54, i8** %z, align 8
  %55 = load %struct.ShellState** %2, align 8
  %56 = getelementptr inbounds %struct.ShellState* %55, i32 0, i32 8
  %57 = load %struct._IO_FILE** %56, align 8
  %58 = load i8** %z, align 8
  %59 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %57, i8* getelementptr inbounds ([3 x i8]* @.str26, i32 0, i32 0), i8* %58)
  store i32 1, i32* %i, align 4
  br label %60

; <label>:60                                      ; preds = %72, %52
  %61 = load i32* %i, align 4
  %62 = load i32* %nResult, align 4
  %63 = icmp slt i32 %61, %62
  br i1 %63, label %64, label %75

; <label>:64                                      ; preds = %60
  %65 = load %struct.ShellState** %2, align 8
  %66 = getelementptr inbounds %struct.ShellState* %65, i32 0, i32 8
  %67 = load %struct._IO_FILE** %66, align 8
  %68 = load %struct.sqlite3_stmt** %pSelect, align 8
  %69 = load i32* %i, align 4
  %70 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %68, i32 %69)
  %71 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %67, i8* getelementptr inbounds ([4 x i8]* @.str359, i32 0, i32 0), i8* %70)
  br label %72

; <label>:72                                      ; preds = %64
  %73 = load i32* %i, align 4
  %74 = add nsw i32 %73, 1
  store i32 %74, i32* %i, align 4
  br label %60

; <label>:75                                      ; preds = %60
  %76 = load i8** %z, align 8
  %77 = icmp eq i8* %76, null
  br i1 %77, label %78, label %79

; <label>:78                                      ; preds = %75
  store i8* getelementptr inbounds ([1 x i8]* @.str61, i32 0, i32 0), i8** %z, align 8
  br label %79

; <label>:79                                      ; preds = %78, %75
  br label %80

; <label>:80                                      ; preds = %102, %79
  %81 = load i8** %z, align 8
  %82 = getelementptr inbounds i8* %81, i64 0
  %83 = load i8* %82, align 1
  %84 = sext i8 %83 to i32
  %85 = icmp ne i32 %84, 0
  br i1 %85, label %86, label %100

; <label>:86                                      ; preds = %80
  %87 = load i8** %z, align 8
  %88 = getelementptr inbounds i8* %87, i64 0
  %89 = load i8* %88, align 1
  %90 = sext i8 %89 to i32
  %91 = icmp ne i32 %90, 45
  br i1 %91, label %98, label %92

; <label>:92                                      ; preds = %86
  %93 = load i8** %z, align 8
  %94 = getelementptr inbounds i8* %93, i64 1
  %95 = load i8* %94, align 1
  %96 = sext i8 %95 to i32
  %97 = icmp ne i32 %96, 45
  br label %98

; <label>:98                                      ; preds = %92, %86
  %99 = phi i1 [ true, %86 ], [ %97, %92 ]
  br label %100

; <label>:100                                     ; preds = %98, %80
  %101 = phi i1 [ false, %80 ], [ %99, %98 ]
  br i1 %101, label %102, label %105

; <label>:102                                     ; preds = %100
  %103 = load i8** %z, align 8
  %104 = getelementptr inbounds i8* %103, i32 1
  store i8* %104, i8** %z, align 8
  br label %80

; <label>:105                                     ; preds = %100
  %106 = load i8** %z, align 8
  %107 = getelementptr inbounds i8* %106, i64 0
  %108 = load i8* %107, align 1
  %109 = icmp ne i8 %108, 0
  br i1 %109, label %110, label %115

; <label>:110                                     ; preds = %105
  %111 = load %struct.ShellState** %2, align 8
  %112 = getelementptr inbounds %struct.ShellState* %111, i32 0, i32 8
  %113 = load %struct._IO_FILE** %112, align 8
  %114 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %113, i8* getelementptr inbounds ([4 x i8]* @.str360, i32 0, i32 0))
  br label %120

; <label>:115                                     ; preds = %105
  %116 = load %struct.ShellState** %2, align 8
  %117 = getelementptr inbounds %struct.ShellState* %116, i32 0, i32 8
  %118 = load %struct._IO_FILE** %117, align 8
  %119 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %118, i8* getelementptr inbounds ([3 x i8]* @.str361, i32 0, i32 0))
  br label %120

; <label>:120                                     ; preds = %115, %110
  %121 = load %struct.sqlite3_stmt** %pSelect, align 8
  %122 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %121)
  store i32 %122, i32* %rc, align 4
  br label %40

; <label>:123                                     ; preds = %40
  %124 = load %struct.sqlite3_stmt** %pSelect, align 8
  %125 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %124)
  store i32 %125, i32* %rc, align 4
  %126 = load i32* %rc, align 4
  %127 = icmp ne i32 %126, 0
  br i1 %127, label %128, label %147

; <label>:128                                     ; preds = %123
  %129 = load %struct.ShellState** %2, align 8
  %130 = getelementptr inbounds %struct.ShellState* %129, i32 0, i32 8
  %131 = load %struct._IO_FILE** %130, align 8
  %132 = load i32* %rc, align 4
  %133 = load %struct.ShellState** %2, align 8
  %134 = getelementptr inbounds %struct.ShellState* %133, i32 0, i32 0
  %135 = load %struct.sqlite3** %134, align 8
  %136 = call i8* @sqlite3_errmsg(%struct.sqlite3* %135)
  %137 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %131, i8* getelementptr inbounds ([29 x i8]* @.str358, i32 0, i32 0), i32 %132, i8* %136)
  %138 = load i32* %rc, align 4
  %139 = and i32 %138, 255
  %140 = icmp ne i32 %139, 11
  br i1 %140, label %141, label %146

; <label>:141                                     ; preds = %128
  %142 = load %struct.ShellState** %2, align 8
  %143 = getelementptr inbounds %struct.ShellState* %142, i32 0, i32 10
  %144 = load i32* %143, align 4
  %145 = add nsw i32 %144, 1
  store i32 %145, i32* %143, align 4
  br label %146

; <label>:146                                     ; preds = %141, %128
  br label %147

; <label>:147                                     ; preds = %146, %123
  %148 = load i32* %rc, align 4
  store i32 %148, i32* %1
  br label %149

; <label>:149                                     ; preds = %147, %33
  %150 = load i32* %1
  ret i32 %150
}

declare %struct._IO_FILE* @popen(i8*, i8*) #2

declare %struct._IO_FILE* @fopen64(i8*, i8*) #2

; Function Attrs: nounwind uwtable
define internal i8* @ascii_read_one_field(%struct.ImportCtx* %p) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca %struct.ImportCtx*, align 8
  %c = alloca i32, align 4
  %cSep = alloca i32, align 4
  %rSep = alloca i32, align 4
  store %struct.ImportCtx* %p, %struct.ImportCtx** %2, align 8
  %3 = load %struct.ImportCtx** %2, align 8
  %4 = getelementptr inbounds %struct.ImportCtx* %3, i32 0, i32 7
  %5 = load i32* %4, align 4
  store i32 %5, i32* %cSep, align 4
  %6 = load %struct.ImportCtx** %2, align 8
  %7 = getelementptr inbounds %struct.ImportCtx* %6, i32 0, i32 8
  %8 = load i32* %7, align 4
  store i32 %8, i32* %rSep, align 4
  %9 = load %struct.ImportCtx** %2, align 8
  %10 = getelementptr inbounds %struct.ImportCtx* %9, i32 0, i32 3
  store i32 0, i32* %10, align 4
  %11 = load %struct.ImportCtx** %2, align 8
  %12 = getelementptr inbounds %struct.ImportCtx* %11, i32 0, i32 1
  %13 = load %struct._IO_FILE** %12, align 8
  %14 = call i32 @fgetc(%struct._IO_FILE* %13)
  store i32 %14, i32* %c, align 4
  %15 = load i32* %c, align 4
  %16 = icmp eq i32 %15, -1
  br i1 %16, label %20, label %17

; <label>:17                                      ; preds = %0
  %18 = load volatile i32* @seenInterrupt, align 4
  %19 = icmp ne i32 %18, 0
  br i1 %19, label %20, label %23

; <label>:20                                      ; preds = %17, %0
  %21 = load %struct.ImportCtx** %2, align 8
  %22 = getelementptr inbounds %struct.ImportCtx* %21, i32 0, i32 6
  store i32 -1, i32* %22, align 4
  store i8* null, i8** %1
  br label %74

; <label>:23                                      ; preds = %17
  br label %24

; <label>:24                                      ; preds = %37, %23
  %25 = load i32* %c, align 4
  %26 = icmp ne i32 %25, -1
  br i1 %26, label %27, label %35

; <label>:27                                      ; preds = %24
  %28 = load i32* %c, align 4
  %29 = load i32* %cSep, align 4
  %30 = icmp ne i32 %28, %29
  br i1 %30, label %31, label %35

; <label>:31                                      ; preds = %27
  %32 = load i32* %c, align 4
  %33 = load i32* %rSep, align 4
  %34 = icmp ne i32 %32, %33
  br label %35

; <label>:35                                      ; preds = %31, %27, %24
  %36 = phi i1 [ false, %27 ], [ false, %24 ], [ %34, %31 ]
  br i1 %36, label %37, label %44

; <label>:37                                      ; preds = %35
  %38 = load %struct.ImportCtx** %2, align 8
  %39 = load i32* %c, align 4
  call void @import_append_char(%struct.ImportCtx* %38, i32 %39)
  %40 = load %struct.ImportCtx** %2, align 8
  %41 = getelementptr inbounds %struct.ImportCtx* %40, i32 0, i32 1
  %42 = load %struct._IO_FILE** %41, align 8
  %43 = call i32 @fgetc(%struct._IO_FILE* %42)
  store i32 %43, i32* %c, align 4
  br label %24

; <label>:44                                      ; preds = %35
  %45 = load i32* %c, align 4
  %46 = load i32* %rSep, align 4
  %47 = icmp eq i32 %45, %46
  br i1 %47, label %48, label %53

; <label>:48                                      ; preds = %44
  %49 = load %struct.ImportCtx** %2, align 8
  %50 = getelementptr inbounds %struct.ImportCtx* %49, i32 0, i32 5
  %51 = load i32* %50, align 4
  %52 = add nsw i32 %51, 1
  store i32 %52, i32* %50, align 4
  br label %53

; <label>:53                                      ; preds = %48, %44
  %54 = load i32* %c, align 4
  %55 = load %struct.ImportCtx** %2, align 8
  %56 = getelementptr inbounds %struct.ImportCtx* %55, i32 0, i32 6
  store i32 %54, i32* %56, align 4
  %57 = load %struct.ImportCtx** %2, align 8
  %58 = getelementptr inbounds %struct.ImportCtx* %57, i32 0, i32 2
  %59 = load i8** %58, align 8
  %60 = icmp ne i8* %59, null
  br i1 %60, label %61, label %70

; <label>:61                                      ; preds = %53
  %62 = load %struct.ImportCtx** %2, align 8
  %63 = getelementptr inbounds %struct.ImportCtx* %62, i32 0, i32 3
  %64 = load i32* %63, align 4
  %65 = sext i32 %64 to i64
  %66 = load %struct.ImportCtx** %2, align 8
  %67 = getelementptr inbounds %struct.ImportCtx* %66, i32 0, i32 2
  %68 = load i8** %67, align 8
  %69 = getelementptr inbounds i8* %68, i64 %65
  store i8 0, i8* %69, align 1
  br label %70

; <label>:70                                      ; preds = %61, %53
  %71 = load %struct.ImportCtx** %2, align 8
  %72 = getelementptr inbounds %struct.ImportCtx* %71, i32 0, i32 2
  %73 = load i8** %72, align 8
  store i8* %73, i8** %1
  br label %74

; <label>:74                                      ; preds = %70, %20
  %75 = load i8** %1
  ret i8* %75
}

; Function Attrs: nounwind uwtable
define internal i8* @csv_read_one_field(%struct.ImportCtx* %p) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca %struct.ImportCtx*, align 8
  %c = alloca i32, align 4
  %cSep = alloca i32, align 4
  %rSep = alloca i32, align 4
  %pc = alloca i32, align 4
  %ppc = alloca i32, align 4
  %startLine = alloca i32, align 4
  %cQuote = alloca i32, align 4
  store %struct.ImportCtx* %p, %struct.ImportCtx** %2, align 8
  %3 = load %struct.ImportCtx** %2, align 8
  %4 = getelementptr inbounds %struct.ImportCtx* %3, i32 0, i32 7
  %5 = load i32* %4, align 4
  store i32 %5, i32* %cSep, align 4
  %6 = load %struct.ImportCtx** %2, align 8
  %7 = getelementptr inbounds %struct.ImportCtx* %6, i32 0, i32 8
  %8 = load i32* %7, align 4
  store i32 %8, i32* %rSep, align 4
  %9 = load %struct.ImportCtx** %2, align 8
  %10 = getelementptr inbounds %struct.ImportCtx* %9, i32 0, i32 3
  store i32 0, i32* %10, align 4
  %11 = load %struct.ImportCtx** %2, align 8
  %12 = getelementptr inbounds %struct.ImportCtx* %11, i32 0, i32 1
  %13 = load %struct._IO_FILE** %12, align 8
  %14 = call i32 @fgetc(%struct._IO_FILE* %13)
  store i32 %14, i32* %c, align 4
  %15 = load i32* %c, align 4
  %16 = icmp eq i32 %15, -1
  br i1 %16, label %20, label %17

; <label>:17                                      ; preds = %0
  %18 = load volatile i32* @seenInterrupt, align 4
  %19 = icmp ne i32 %18, 0
  br i1 %19, label %20, label %23

; <label>:20                                      ; preds = %17, %0
  %21 = load %struct.ImportCtx** %2, align 8
  %22 = getelementptr inbounds %struct.ImportCtx* %21, i32 0, i32 6
  store i32 -1, i32* %22, align 4
  store i8* null, i8** %1
  br label %223

; <label>:23                                      ; preds = %17
  %24 = load i32* %c, align 4
  %25 = icmp eq i32 %24, 34
  br i1 %25, label %26, label %148

; <label>:26                                      ; preds = %23
  %27 = load %struct.ImportCtx** %2, align 8
  %28 = getelementptr inbounds %struct.ImportCtx* %27, i32 0, i32 5
  %29 = load i32* %28, align 4
  store i32 %29, i32* %startLine, align 4
  %30 = load i32* %c, align 4
  store i32 %30, i32* %cQuote, align 4
  store i32 0, i32* %ppc, align 4
  store i32 0, i32* %pc, align 4
  br label %31

; <label>:31                                      ; preds = %26, %52, %142
  %32 = load %struct.ImportCtx** %2, align 8
  %33 = getelementptr inbounds %struct.ImportCtx* %32, i32 0, i32 1
  %34 = load %struct._IO_FILE** %33, align 8
  %35 = call i32 @fgetc(%struct._IO_FILE* %34)
  store i32 %35, i32* %c, align 4
  %36 = load i32* %c, align 4
  %37 = load i32* %rSep, align 4
  %38 = icmp eq i32 %36, %37
  br i1 %38, label %39, label %44

; <label>:39                                      ; preds = %31
  %40 = load %struct.ImportCtx** %2, align 8
  %41 = getelementptr inbounds %struct.ImportCtx* %40, i32 0, i32 5
  %42 = load i32* %41, align 4
  %43 = add nsw i32 %42, 1
  store i32 %43, i32* %41, align 4
  br label %44

; <label>:44                                      ; preds = %39, %31
  %45 = load i32* %c, align 4
  %46 = load i32* %cQuote, align 4
  %47 = icmp eq i32 %45, %46
  br i1 %47, label %48, label %54

; <label>:48                                      ; preds = %44
  %49 = load i32* %pc, align 4
  %50 = load i32* %cQuote, align 4
  %51 = icmp eq i32 %49, %50
  br i1 %51, label %52, label %53

; <label>:52                                      ; preds = %48
  store i32 0, i32* %pc, align 4
  br label %31

; <label>:53                                      ; preds = %48
  br label %54

; <label>:54                                      ; preds = %53, %44
  %55 = load i32* %c, align 4
  %56 = load i32* %cSep, align 4
  %57 = icmp eq i32 %55, %56
  br i1 %57, label %58, label %62

; <label>:58                                      ; preds = %54
  %59 = load i32* %pc, align 4
  %60 = load i32* %cQuote, align 4
  %61 = icmp eq i32 %59, %60
  br i1 %61, label %88, label %62

; <label>:62                                      ; preds = %58, %54
  %63 = load i32* %c, align 4
  %64 = load i32* %rSep, align 4
  %65 = icmp eq i32 %63, %64
  br i1 %65, label %66, label %70

; <label>:66                                      ; preds = %62
  %67 = load i32* %pc, align 4
  %68 = load i32* %cQuote, align 4
  %69 = icmp eq i32 %67, %68
  br i1 %69, label %88, label %70

; <label>:70                                      ; preds = %66, %62
  %71 = load i32* %c, align 4
  %72 = load i32* %rSep, align 4
  %73 = icmp eq i32 %71, %72
  br i1 %73, label %74, label %81

; <label>:74                                      ; preds = %70
  %75 = load i32* %pc, align 4
  %76 = icmp eq i32 %75, 13
  br i1 %76, label %77, label %81

; <label>:77                                      ; preds = %74
  %78 = load i32* %ppc, align 4
  %79 = load i32* %cQuote, align 4
  %80 = icmp eq i32 %78, %79
  br i1 %80, label %88, label %81

; <label>:81                                      ; preds = %77, %74, %70
  %82 = load i32* %c, align 4
  %83 = icmp eq i32 %82, -1
  br i1 %83, label %84, label %111

; <label>:84                                      ; preds = %81
  %85 = load i32* %pc, align 4
  %86 = load i32* %cQuote, align 4
  %87 = icmp eq i32 %85, %86
  br i1 %87, label %88, label %111

; <label>:88                                      ; preds = %84, %77, %66, %58
  br label %89

; <label>:89                                      ; preds = %94, %88
  %90 = load %struct.ImportCtx** %2, align 8
  %91 = getelementptr inbounds %struct.ImportCtx* %90, i32 0, i32 3
  %92 = load i32* %91, align 4
  %93 = add nsw i32 %92, -1
  store i32 %93, i32* %91, align 4
  br label %94

; <label>:94                                      ; preds = %89
  %95 = load %struct.ImportCtx** %2, align 8
  %96 = getelementptr inbounds %struct.ImportCtx* %95, i32 0, i32 3
  %97 = load i32* %96, align 4
  %98 = sext i32 %97 to i64
  %99 = load %struct.ImportCtx** %2, align 8
  %100 = getelementptr inbounds %struct.ImportCtx* %99, i32 0, i32 2
  %101 = load i8** %100, align 8
  %102 = getelementptr inbounds i8* %101, i64 %98
  %103 = load i8* %102, align 1
  %104 = sext i8 %103 to i32
  %105 = load i32* %cQuote, align 4
  %106 = icmp ne i32 %104, %105
  br i1 %106, label %89, label %107

; <label>:107                                     ; preds = %94
  %108 = load i32* %c, align 4
  %109 = load %struct.ImportCtx** %2, align 8
  %110 = getelementptr inbounds %struct.ImportCtx* %109, i32 0, i32 6
  store i32 %108, i32* %110, align 4
  br label %147

; <label>:111                                     ; preds = %84, %81
  %112 = load i32* %pc, align 4
  %113 = load i32* %cQuote, align 4
  %114 = icmp eq i32 %112, %113
  br i1 %114, label %115, label %128

; <label>:115                                     ; preds = %111
  %116 = load i32* %c, align 4
  %117 = icmp ne i32 %116, 13
  br i1 %117, label %118, label %128

; <label>:118                                     ; preds = %115
  %119 = load %struct._IO_FILE** @stderr, align 8
  %120 = load %struct.ImportCtx** %2, align 8
  %121 = getelementptr inbounds %struct.ImportCtx* %120, i32 0, i32 0
  %122 = load i8** %121, align 8
  %123 = load %struct.ImportCtx** %2, align 8
  %124 = getelementptr inbounds %struct.ImportCtx* %123, i32 0, i32 5
  %125 = load i32* %124, align 4
  %126 = load i32* %cQuote, align 4
  %127 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %119, i8* getelementptr inbounds ([31 x i8]* @.str356, i32 0, i32 0), i8* %122, i32 %125, i32 %126)
  br label %128

; <label>:128                                     ; preds = %118, %115, %111
  %129 = load i32* %c, align 4
  %130 = icmp eq i32 %129, -1
  br i1 %130, label %131, label %142

; <label>:131                                     ; preds = %128
  %132 = load %struct._IO_FILE** @stderr, align 8
  %133 = load %struct.ImportCtx** %2, align 8
  %134 = getelementptr inbounds %struct.ImportCtx* %133, i32 0, i32 0
  %135 = load i8** %134, align 8
  %136 = load i32* %startLine, align 4
  %137 = load i32* %cQuote, align 4
  %138 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %132, i8* getelementptr inbounds ([37 x i8]* @.str357, i32 0, i32 0), i8* %135, i32 %136, i32 %137)
  %139 = load i32* %c, align 4
  %140 = load %struct.ImportCtx** %2, align 8
  %141 = getelementptr inbounds %struct.ImportCtx* %140, i32 0, i32 6
  store i32 %139, i32* %141, align 4
  br label %147

; <label>:142                                     ; preds = %128
  %143 = load %struct.ImportCtx** %2, align 8
  %144 = load i32* %c, align 4
  call void @import_append_char(%struct.ImportCtx* %143, i32 %144)
  %145 = load i32* %pc, align 4
  store i32 %145, i32* %ppc, align 4
  %146 = load i32* %c, align 4
  store i32 %146, i32* %pc, align 4
  br label %31

; <label>:147                                     ; preds = %131, %107
  br label %205

; <label>:148                                     ; preds = %23
  br label %149

; <label>:149                                     ; preds = %162, %148
  %150 = load i32* %c, align 4
  %151 = icmp ne i32 %150, -1
  br i1 %151, label %152, label %160

; <label>:152                                     ; preds = %149
  %153 = load i32* %c, align 4
  %154 = load i32* %cSep, align 4
  %155 = icmp ne i32 %153, %154
  br i1 %155, label %156, label %160

; <label>:156                                     ; preds = %152
  %157 = load i32* %c, align 4
  %158 = load i32* %rSep, align 4
  %159 = icmp ne i32 %157, %158
  br label %160

; <label>:160                                     ; preds = %156, %152, %149
  %161 = phi i1 [ false, %152 ], [ false, %149 ], [ %159, %156 ]
  br i1 %161, label %162, label %169

; <label>:162                                     ; preds = %160
  %163 = load %struct.ImportCtx** %2, align 8
  %164 = load i32* %c, align 4
  call void @import_append_char(%struct.ImportCtx* %163, i32 %164)
  %165 = load %struct.ImportCtx** %2, align 8
  %166 = getelementptr inbounds %struct.ImportCtx* %165, i32 0, i32 1
  %167 = load %struct._IO_FILE** %166, align 8
  %168 = call i32 @fgetc(%struct._IO_FILE* %167)
  store i32 %168, i32* %c, align 4
  br label %149

; <label>:169                                     ; preds = %160
  %170 = load i32* %c, align 4
  %171 = load i32* %rSep, align 4
  %172 = icmp eq i32 %170, %171
  br i1 %172, label %173, label %201

; <label>:173                                     ; preds = %169
  %174 = load %struct.ImportCtx** %2, align 8
  %175 = getelementptr inbounds %struct.ImportCtx* %174, i32 0, i32 5
  %176 = load i32* %175, align 4
  %177 = add nsw i32 %176, 1
  store i32 %177, i32* %175, align 4
  %178 = load %struct.ImportCtx** %2, align 8
  %179 = getelementptr inbounds %struct.ImportCtx* %178, i32 0, i32 3
  %180 = load i32* %179, align 4
  %181 = icmp sgt i32 %180, 0
  br i1 %181, label %182, label %200

; <label>:182                                     ; preds = %173
  %183 = load %struct.ImportCtx** %2, align 8
  %184 = getelementptr inbounds %struct.ImportCtx* %183, i32 0, i32 3
  %185 = load i32* %184, align 4
  %186 = sub nsw i32 %185, 1
  %187 = sext i32 %186 to i64
  %188 = load %struct.ImportCtx** %2, align 8
  %189 = getelementptr inbounds %struct.ImportCtx* %188, i32 0, i32 2
  %190 = load i8** %189, align 8
  %191 = getelementptr inbounds i8* %190, i64 %187
  %192 = load i8* %191, align 1
  %193 = sext i8 %192 to i32
  %194 = icmp eq i32 %193, 13
  br i1 %194, label %195, label %200

; <label>:195                                     ; preds = %182
  %196 = load %struct.ImportCtx** %2, align 8
  %197 = getelementptr inbounds %struct.ImportCtx* %196, i32 0, i32 3
  %198 = load i32* %197, align 4
  %199 = add nsw i32 %198, -1
  store i32 %199, i32* %197, align 4
  br label %200

; <label>:200                                     ; preds = %195, %182, %173
  br label %201

; <label>:201                                     ; preds = %200, %169
  %202 = load i32* %c, align 4
  %203 = load %struct.ImportCtx** %2, align 8
  %204 = getelementptr inbounds %struct.ImportCtx* %203, i32 0, i32 6
  store i32 %202, i32* %204, align 4
  br label %205

; <label>:205                                     ; preds = %201, %147
  %206 = load %struct.ImportCtx** %2, align 8
  %207 = getelementptr inbounds %struct.ImportCtx* %206, i32 0, i32 2
  %208 = load i8** %207, align 8
  %209 = icmp ne i8* %208, null
  br i1 %209, label %210, label %219

; <label>:210                                     ; preds = %205
  %211 = load %struct.ImportCtx** %2, align 8
  %212 = getelementptr inbounds %struct.ImportCtx* %211, i32 0, i32 3
  %213 = load i32* %212, align 4
  %214 = sext i32 %213 to i64
  %215 = load %struct.ImportCtx** %2, align 8
  %216 = getelementptr inbounds %struct.ImportCtx* %215, i32 0, i32 2
  %217 = load i8** %216, align 8
  %218 = getelementptr inbounds i8* %217, i64 %214
  store i8 0, i8* %218, align 1
  br label %219

; <label>:219                                     ; preds = %210, %205
  %220 = load %struct.ImportCtx** %2, align 8
  %221 = getelementptr inbounds %struct.ImportCtx* %220, i32 0, i32 2
  %222 = load i8** %221, align 8
  store i8* %222, i8** %1
  br label %223

; <label>:223                                     ; preds = %219, %20
  %224 = load i8** %1
  ret i8* %224
}

; Function Attrs: nounwind uwtable
define internal void @import_append_char(%struct.ImportCtx* %p, i32 %c) #0 {
  %1 = alloca %struct.ImportCtx*, align 8
  %2 = alloca i32, align 4
  store %struct.ImportCtx* %p, %struct.ImportCtx** %1, align 8
  store i32 %c, i32* %2, align 4
  %3 = load %struct.ImportCtx** %1, align 8
  %4 = getelementptr inbounds %struct.ImportCtx* %3, i32 0, i32 3
  %5 = load i32* %4, align 4
  %6 = add nsw i32 %5, 1
  %7 = load %struct.ImportCtx** %1, align 8
  %8 = getelementptr inbounds %struct.ImportCtx* %7, i32 0, i32 4
  %9 = load i32* %8, align 4
  %10 = icmp sge i32 %6, %9
  br i1 %10, label %11, label %38

; <label>:11                                      ; preds = %0
  %12 = load %struct.ImportCtx** %1, align 8
  %13 = getelementptr inbounds %struct.ImportCtx* %12, i32 0, i32 4
  %14 = load i32* %13, align 4
  %15 = add nsw i32 %14, 100
  %16 = load %struct.ImportCtx** %1, align 8
  %17 = getelementptr inbounds %struct.ImportCtx* %16, i32 0, i32 4
  %18 = load i32* %17, align 4
  %19 = add nsw i32 %18, %15
  store i32 %19, i32* %17, align 4
  %20 = load %struct.ImportCtx** %1, align 8
  %21 = getelementptr inbounds %struct.ImportCtx* %20, i32 0, i32 2
  %22 = load i8** %21, align 8
  %23 = load %struct.ImportCtx** %1, align 8
  %24 = getelementptr inbounds %struct.ImportCtx* %23, i32 0, i32 4
  %25 = load i32* %24, align 4
  %26 = sext i32 %25 to i64
  %27 = call i8* @sqlite3_realloc64(i8* %22, i64 %26)
  %28 = load %struct.ImportCtx** %1, align 8
  %29 = getelementptr inbounds %struct.ImportCtx* %28, i32 0, i32 2
  store i8* %27, i8** %29, align 8
  %30 = load %struct.ImportCtx** %1, align 8
  %31 = getelementptr inbounds %struct.ImportCtx* %30, i32 0, i32 2
  %32 = load i8** %31, align 8
  %33 = icmp eq i8* %32, null
  br i1 %33, label %34, label %37

; <label>:34                                      ; preds = %11
  %35 = load %struct._IO_FILE** @stderr, align 8
  %36 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %35, i8* getelementptr inbounds ([15 x i8]* @.str2, i32 0, i32 0))
  call void @exit(i32 1) #8
  unreachable

; <label>:37                                      ; preds = %11
  br label %38

; <label>:38                                      ; preds = %37, %0
  %39 = load i32* %2, align 4
  %40 = trunc i32 %39 to i8
  %41 = load %struct.ImportCtx** %1, align 8
  %42 = getelementptr inbounds %struct.ImportCtx* %41, i32 0, i32 3
  %43 = load i32* %42, align 4
  %44 = add nsw i32 %43, 1
  store i32 %44, i32* %42, align 4
  %45 = sext i32 %43 to i64
  %46 = load %struct.ImportCtx** %1, align 8
  %47 = getelementptr inbounds %struct.ImportCtx* %46, i32 0, i32 2
  %48 = load i8** %47, align 8
  %49 = getelementptr inbounds i8* %48, i64 %45
  store i8 %40, i8* %49, align 1
  ret void
}

declare i32 @sqlite3_strglob(i8*, i8*) #2

declare i32 @sqlite3_get_autocommit(%struct.sqlite3*) #2

declare i32 @sqlite3_bind_text(%struct.sqlite3_stmt*, i32, i8*, i32, void (i8*)*) #2

declare i32 @sqlite3_bind_null(%struct.sqlite3_stmt*, i32) #2

declare i32 @sqlite3_limit(%struct.sqlite3*, i32, i32) #2

declare i32 @sqlite3_load_extension(%struct.sqlite3*, i8*, i8*, i8**) #2

; Function Attrs: nounwind uwtable
define internal %struct._IO_FILE* @output_file_open(i8* %zFile) #0 {
  %1 = alloca i8*, align 8
  %f = alloca %struct._IO_FILE*, align 8
  store i8* %zFile, i8** %1, align 8
  %2 = load i8** %1, align 8
  %3 = call i32 @strcmp(i8* %2, i8* getelementptr inbounds ([7 x i8]* @.str257, i32 0, i32 0)) #7
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %5, label %7

; <label>:5                                       ; preds = %0
  %6 = load %struct._IO_FILE** @stdout, align 8
  store %struct._IO_FILE* %6, %struct._IO_FILE** %f, align 8
  br label %30

; <label>:7                                       ; preds = %0
  %8 = load i8** %1, align 8
  %9 = call i32 @strcmp(i8* %8, i8* getelementptr inbounds ([7 x i8]* @.str354, i32 0, i32 0)) #7
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %11, label %13

; <label>:11                                      ; preds = %7
  %12 = load %struct._IO_FILE** @stderr, align 8
  store %struct._IO_FILE* %12, %struct._IO_FILE** %f, align 8
  br label %29

; <label>:13                                      ; preds = %7
  %14 = load i8** %1, align 8
  %15 = call i32 @strcmp(i8* %14, i8* getelementptr inbounds ([4 x i8]* @.str262, i32 0, i32 0)) #7
  %16 = icmp eq i32 %15, 0
  br i1 %16, label %17, label %18

; <label>:17                                      ; preds = %13
  store %struct._IO_FILE* null, %struct._IO_FILE** %f, align 8
  br label %28

; <label>:18                                      ; preds = %13
  %19 = load i8** %1, align 8
  %20 = call %struct._IO_FILE* @fopen64(i8* %19, i8* getelementptr inbounds ([3 x i8]* @.str355, i32 0, i32 0))
  store %struct._IO_FILE* %20, %struct._IO_FILE** %f, align 8
  %21 = load %struct._IO_FILE** %f, align 8
  %22 = icmp eq %struct._IO_FILE* %21, null
  br i1 %22, label %23, label %27

; <label>:23                                      ; preds = %18
  %24 = load %struct._IO_FILE** @stderr, align 8
  %25 = load i8** %1, align 8
  %26 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([25 x i8]* @.str141, i32 0, i32 0), i8* %25)
  br label %27

; <label>:27                                      ; preds = %23, %18
  br label %28

; <label>:28                                      ; preds = %27, %17
  br label %29

; <label>:29                                      ; preds = %28, %11
  br label %30

; <label>:30                                      ; preds = %29, %5
  %31 = load %struct._IO_FILE** %f, align 8
  ret %struct._IO_FILE* %31
}

; Function Attrs: nounwind readonly
declare i64 @strlen(i8*) #1

; Function Attrs: nounwind
declare i8* @strncpy(i8*, i8*, i64) #4

declare i32 @sqlite3_sleep(i32) #2

; Function Attrs: nounwind readonly
declare i8* @strchr(i8*, i32) #1

declare i32 @system(i8*) #2

; Function Attrs: nounwind
declare i64 @strtol(i8*, i8**, i32) #4

declare i32 @sqlite3_test_control(i32, ...) #2

declare i32 @sqlite3_busy_timeout(%struct.sqlite3*, i32) #2

declare i8* @sqlite3_trace(%struct.sqlite3*, void (i8*, i8*)*, i8*) #2

; Function Attrs: nounwind uwtable
define internal void @sql_trace_callback(i8* %pArg, i8* %z) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %f = alloca %struct._IO_FILE*, align 8
  %i = alloca i32, align 4
  store i8* %pArg, i8** %1, align 8
  store i8* %z, i8** %2, align 8
  %3 = load i8** %1, align 8
  %4 = bitcast i8* %3 to %struct._IO_FILE*
  store %struct._IO_FILE* %4, %struct._IO_FILE** %f, align 8
  %5 = load %struct._IO_FILE** %f, align 8
  %6 = icmp ne %struct._IO_FILE* %5, null
  br i1 %6, label %7, label %33

; <label>:7                                       ; preds = %0
  %8 = load i8** %2, align 8
  %9 = call i64 @strlen(i8* %8) #7
  %10 = trunc i64 %9 to i32
  store i32 %10, i32* %i, align 4
  br label %11

; <label>:11                                      ; preds = %25, %7
  %12 = load i32* %i, align 4
  %13 = icmp sgt i32 %12, 0
  br i1 %13, label %14, label %23

; <label>:14                                      ; preds = %11
  %15 = load i32* %i, align 4
  %16 = sub nsw i32 %15, 1
  %17 = sext i32 %16 to i64
  %18 = load i8** %2, align 8
  %19 = getelementptr inbounds i8* %18, i64 %17
  %20 = load i8* %19, align 1
  %21 = sext i8 %20 to i32
  %22 = icmp eq i32 %21, 59
  br label %23

; <label>:23                                      ; preds = %14, %11
  %24 = phi i1 [ false, %11 ], [ %22, %14 ]
  br i1 %24, label %25, label %28

; <label>:25                                      ; preds = %23
  %26 = load i32* %i, align 4
  %27 = add nsw i32 %26, -1
  store i32 %27, i32* %i, align 4
  br label %11

; <label>:28                                      ; preds = %23
  %29 = load %struct._IO_FILE** %f, align 8
  %30 = load i32* %i, align 4
  %31 = load i8** %2, align 8
  %32 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([7 x i8]* @.str351, i32 0, i32 0), i32 %30, i8* %31)
  br label %33

; <label>:33                                      ; preds = %28, %0
  ret void
}

declare i32 @sqlite3_file_control(%struct.sqlite3*, i8*, i32, i8*) #2

declare i32 @fgetc(%struct._IO_FILE*) #2

; Function Attrs: nounwind uwtable
define internal i32 @dump_callback(i8* %pArg, i32 %nArg, i8** %azArg, i8** %azCol) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8*, align 8
  %3 = alloca i32, align 4
  %4 = alloca i8**, align 8
  %5 = alloca i8**, align 8
  %rc = alloca i32, align 4
  %zTable = alloca i8*, align 8
  %zType = alloca i8*, align 8
  %zSql = alloca i8*, align 8
  %zPrepStmt = alloca i8*, align 8
  %p = alloca %struct.ShellState*, align 8
  %zIns = alloca i8*, align 8
  %pTableInfo = alloca %struct.sqlite3_stmt*, align 8
  %zSelect = alloca i8*, align 8
  %zTableInfo = alloca i8*, align 8
  %zTmp = alloca i8*, align 8
  %nRow = alloca i32, align 4
  %zText = alloca i8*, align 8
  store i8* %pArg, i8** %2, align 8
  store i32 %nArg, i32* %3, align 4
  store i8** %azArg, i8*** %4, align 8
  store i8** %azCol, i8*** %5, align 8
  store i8* null, i8** %zPrepStmt, align 8
  %6 = load i8** %2, align 8
  %7 = bitcast i8* %6 to %struct.ShellState*
  store %struct.ShellState* %7, %struct.ShellState** %p, align 8
  %8 = load i8*** %5, align 8
  %9 = load i32* %3, align 4
  %10 = icmp ne i32 %9, 3
  br i1 %10, label %11, label %12

; <label>:11                                      ; preds = %0
  store i32 1, i32* %1
  br label %173

; <label>:12                                      ; preds = %0
  %13 = load i8*** %4, align 8
  %14 = getelementptr inbounds i8** %13, i64 0
  %15 = load i8** %14, align 8
  store i8* %15, i8** %zTable, align 8
  %16 = load i8*** %4, align 8
  %17 = getelementptr inbounds i8** %16, i64 1
  %18 = load i8** %17, align 8
  store i8* %18, i8** %zType, align 8
  %19 = load i8*** %4, align 8
  %20 = getelementptr inbounds i8** %19, i64 2
  %21 = load i8** %20, align 8
  store i8* %21, i8** %zSql, align 8
  %22 = load i8** %zTable, align 8
  %23 = call i32 @strcmp(i8* %22, i8* getelementptr inbounds ([16 x i8]* @.str366, i32 0, i32 0)) #7
  %24 = icmp eq i32 %23, 0
  br i1 %24, label %25, label %26

; <label>:25                                      ; preds = %12
  store i8* getelementptr inbounds ([30 x i8]* @.str367, i32 0, i32 0), i8** %zPrepStmt, align 8
  br label %76

; <label>:26                                      ; preds = %12
  %27 = load i8** %zTable, align 8
  %28 = call i32 @sqlite3_strglob(i8* getelementptr inbounds ([13 x i8]* @.str368, i32 0, i32 0), i8* %27)
  %29 = icmp eq i32 %28, 0
  br i1 %29, label %30, label %35

; <label>:30                                      ; preds = %26
  %31 = load %struct.ShellState** %p, align 8
  %32 = getelementptr inbounds %struct.ShellState* %31, i32 0, i32 8
  %33 = load %struct._IO_FILE** %32, align 8
  %34 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([24 x i8]* @.str177, i32 0, i32 0))
  br label %75

; <label>:35                                      ; preds = %26
  %36 = load i8** %zTable, align 8
  %37 = call i32 @strncmp(i8* %36, i8* getelementptr inbounds ([8 x i8]* @.str369, i32 0, i32 0), i64 7) #7
  %38 = icmp eq i32 %37, 0
  br i1 %38, label %39, label %40

; <label>:39                                      ; preds = %35
  store i32 0, i32* %1
  br label %173

; <label>:40                                      ; preds = %35
  %41 = load i8** %zSql, align 8
  %42 = call i32 @strncmp(i8* %41, i8* getelementptr inbounds ([21 x i8]* @.str370, i32 0, i32 0), i64 20) #7
  %43 = icmp eq i32 %42, 0
  br i1 %43, label %44, label %67

; <label>:44                                      ; preds = %40
  %45 = load %struct.ShellState** %p, align 8
  %46 = getelementptr inbounds %struct.ShellState* %45, i32 0, i32 12
  %47 = load i32* %46, align 4
  %48 = icmp ne i32 %47, 0
  br i1 %48, label %56, label %49

; <label>:49                                      ; preds = %44
  %50 = load %struct.ShellState** %p, align 8
  %51 = getelementptr inbounds %struct.ShellState* %50, i32 0, i32 8
  %52 = load %struct._IO_FILE** %51, align 8
  %53 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %52, i8* getelementptr inbounds ([28 x i8]* @.str371, i32 0, i32 0))
  %54 = load %struct.ShellState** %p, align 8
  %55 = getelementptr inbounds %struct.ShellState* %54, i32 0, i32 12
  store i32 1, i32* %55, align 4
  br label %56

; <label>:56                                      ; preds = %49, %44
  %57 = load i8** %zTable, align 8
  %58 = load i8** %zTable, align 8
  %59 = load i8** %zSql, align 8
  %60 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([92 x i8]* @.str372, i32 0, i32 0), i8* %57, i8* %58, i8* %59)
  store i8* %60, i8** %zIns, align 8
  %61 = load %struct.ShellState** %p, align 8
  %62 = getelementptr inbounds %struct.ShellState* %61, i32 0, i32 8
  %63 = load %struct._IO_FILE** %62, align 8
  %64 = load i8** %zIns, align 8
  %65 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %63, i8* getelementptr inbounds ([4 x i8]* @.str51, i32 0, i32 0), i8* %64)
  %66 = load i8** %zIns, align 8
  call void @sqlite3_free(i8* %66)
  store i32 0, i32* %1
  br label %173

; <label>:67                                      ; preds = %40
  %68 = load %struct.ShellState** %p, align 8
  %69 = getelementptr inbounds %struct.ShellState* %68, i32 0, i32 8
  %70 = load %struct._IO_FILE** %69, align 8
  %71 = load i8** %zSql, align 8
  %72 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %70, i8* getelementptr inbounds ([5 x i8]* @.str373, i32 0, i32 0), i8* %71)
  br label %73

; <label>:73                                      ; preds = %67
  br label %74

; <label>:74                                      ; preds = %73
  br label %75

; <label>:75                                      ; preds = %74, %30
  br label %76

; <label>:76                                      ; preds = %75, %25
  %77 = load i8** %zType, align 8
  %78 = call i32 @strcmp(i8* %77, i8* getelementptr inbounds ([6 x i8]* @.str249, i32 0, i32 0)) #7
  %79 = icmp eq i32 %78, 0
  br i1 %79, label %80, label %172

; <label>:80                                      ; preds = %76
  store %struct.sqlite3_stmt* null, %struct.sqlite3_stmt** %pTableInfo, align 8
  store i8* null, i8** %zSelect, align 8
  store i8* null, i8** %zTableInfo, align 8
  store i8* null, i8** %zTmp, align 8
  store i32 0, i32* %nRow, align 4
  %81 = load i8** %zTableInfo, align 8
  %82 = call i8* @appendText(i8* %81, i8* getelementptr inbounds ([19 x i8]* @.str374, i32 0, i32 0), i8 signext 0)
  store i8* %82, i8** %zTableInfo, align 8
  %83 = load i8** %zTableInfo, align 8
  %84 = load i8** %zTable, align 8
  %85 = call i8* @appendText(i8* %83, i8* %84, i8 signext 34)
  store i8* %85, i8** %zTableInfo, align 8
  %86 = load i8** %zTableInfo, align 8
  %87 = call i8* @appendText(i8* %86, i8* getelementptr inbounds ([3 x i8]* @.str375, i32 0, i32 0), i8 signext 0)
  store i8* %87, i8** %zTableInfo, align 8
  %88 = load %struct.ShellState** %p, align 8
  %89 = getelementptr inbounds %struct.ShellState* %88, i32 0, i32 0
  %90 = load %struct.sqlite3** %89, align 8
  %91 = load i8** %zTableInfo, align 8
  %92 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %90, i8* %91, i32 -1, %struct.sqlite3_stmt** %pTableInfo, i8** null)
  store i32 %92, i32* %rc, align 4
  %93 = load i8** %zTableInfo, align 8
  call void @free(i8* %93) #5
  %94 = load i32* %rc, align 4
  %95 = icmp ne i32 %94, 0
  br i1 %95, label %99, label %96

; <label>:96                                      ; preds = %80
  %97 = load %struct.sqlite3_stmt** %pTableInfo, align 8
  %98 = icmp ne %struct.sqlite3_stmt* %97, null
  br i1 %98, label %100, label %99

; <label>:99                                      ; preds = %96, %80
  store i32 1, i32* %1
  br label %173

; <label>:100                                     ; preds = %96
  %101 = load i8** %zSelect, align 8
  %102 = call i8* @appendText(i8* %101, i8* getelementptr inbounds ([26 x i8]* @.str376, i32 0, i32 0), i8 signext 0)
  store i8* %102, i8** %zSelect, align 8
  %103 = load i8** %zTmp, align 8
  %104 = load i8** %zTable, align 8
  %105 = call i8* @appendText(i8* %103, i8* %104, i8 signext 34)
  store i8* %105, i8** %zTmp, align 8
  %106 = load i8** %zTmp, align 8
  %107 = icmp ne i8* %106, null
  br i1 %107, label %108, label %113

; <label>:108                                     ; preds = %100
  %109 = load i8** %zSelect, align 8
  %110 = load i8** %zTmp, align 8
  %111 = call i8* @appendText(i8* %109, i8* %110, i8 signext 39)
  store i8* %111, i8** %zSelect, align 8
  %112 = load i8** %zTmp, align 8
  call void @free(i8* %112) #5
  br label %113

; <label>:113                                     ; preds = %108, %100
  %114 = load i8** %zSelect, align 8
  %115 = call i8* @appendText(i8* %114, i8* getelementptr inbounds ([19 x i8]* @.str377, i32 0, i32 0), i8 signext 0)
  store i8* %115, i8** %zSelect, align 8
  %116 = load %struct.sqlite3_stmt** %pTableInfo, align 8
  %117 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %116)
  store i32 %117, i32* %rc, align 4
  br label %118

; <label>:118                                     ; preds = %139, %113
  %119 = load i32* %rc, align 4
  %120 = icmp eq i32 %119, 100
  br i1 %120, label %121, label %142

; <label>:121                                     ; preds = %118
  %122 = load %struct.sqlite3_stmt** %pTableInfo, align 8
  %123 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %122, i32 1)
  store i8* %123, i8** %zText, align 8
  %124 = load i8** %zSelect, align 8
  %125 = call i8* @appendText(i8* %124, i8* getelementptr inbounds ([7 x i8]* @.str378, i32 0, i32 0), i8 signext 0)
  store i8* %125, i8** %zSelect, align 8
  %126 = load i8** %zSelect, align 8
  %127 = load i8** %zText, align 8
  %128 = call i8* @appendText(i8* %126, i8* %127, i8 signext 34)
  store i8* %128, i8** %zSelect, align 8
  %129 = load %struct.sqlite3_stmt** %pTableInfo, align 8
  %130 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %129)
  store i32 %130, i32* %rc, align 4
  %131 = load i32* %rc, align 4
  %132 = icmp eq i32 %131, 100
  br i1 %132, label %133, label %136

; <label>:133                                     ; preds = %121
  %134 = load i8** %zSelect, align 8
  %135 = call i8* @appendText(i8* %134, i8* getelementptr inbounds ([4 x i8]* @.str379, i32 0, i32 0), i8 signext 0)
  store i8* %135, i8** %zSelect, align 8
  br label %139

; <label>:136                                     ; preds = %121
  %137 = load i8** %zSelect, align 8
  %138 = call i8* @appendText(i8* %137, i8* getelementptr inbounds ([3 x i8]* @.str380, i32 0, i32 0), i8 signext 0)
  store i8* %138, i8** %zSelect, align 8
  br label %139

; <label>:139                                     ; preds = %136, %133
  %140 = load i32* %nRow, align 4
  %141 = add nsw i32 %140, 1
  store i32 %141, i32* %nRow, align 4
  br label %118

; <label>:142                                     ; preds = %118
  %143 = load %struct.sqlite3_stmt** %pTableInfo, align 8
  %144 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %143)
  store i32 %144, i32* %rc, align 4
  %145 = load i32* %rc, align 4
  %146 = icmp ne i32 %145, 0
  br i1 %146, label %150, label %147

; <label>:147                                     ; preds = %142
  %148 = load i32* %nRow, align 4
  %149 = icmp eq i32 %148, 0
  br i1 %149, label %150, label %152

; <label>:150                                     ; preds = %147, %142
  %151 = load i8** %zSelect, align 8
  call void @free(i8* %151) #5
  store i32 1, i32* %1
  br label %173

; <label>:152                                     ; preds = %147
  %153 = load i8** %zSelect, align 8
  %154 = call i8* @appendText(i8* %153, i8* getelementptr inbounds ([14 x i8]* @.str381, i32 0, i32 0), i8 signext 0)
  store i8* %154, i8** %zSelect, align 8
  %155 = load i8** %zSelect, align 8
  %156 = load i8** %zTable, align 8
  %157 = call i8* @appendText(i8* %155, i8* %156, i8 signext 34)
  store i8* %157, i8** %zSelect, align 8
  %158 = load %struct.ShellState** %p, align 8
  %159 = load i8** %zSelect, align 8
  %160 = load i8** %zPrepStmt, align 8
  %161 = call i32 @run_table_dump_query(%struct.ShellState* %158, i8* %159, i8* %160)
  store i32 %161, i32* %rc, align 4
  %162 = load i32* %rc, align 4
  %163 = icmp eq i32 %162, 11
  br i1 %163, label %164, label %170

; <label>:164                                     ; preds = %152
  %165 = load i8** %zSelect, align 8
  %166 = call i8* @appendText(i8* %165, i8* getelementptr inbounds ([21 x i8]* @.str382, i32 0, i32 0), i8 signext 0)
  store i8* %166, i8** %zSelect, align 8
  %167 = load %struct.ShellState** %p, align 8
  %168 = load i8** %zSelect, align 8
  %169 = call i32 @run_table_dump_query(%struct.ShellState* %167, i8* %168, i8* null)
  br label %170

; <label>:170                                     ; preds = %164, %152
  %171 = load i8** %zSelect, align 8
  call void @free(i8* %171) #5
  br label %172

; <label>:172                                     ; preds = %170, %76
  store i32 0, i32* %1
  br label %173

; <label>:173                                     ; preds = %172, %150, %99, %56, %39, %11
  %174 = load i32* %1
  ret i32 %174
}

; Function Attrs: nounwind uwtable
define internal i8* @appendText(i8* %zIn, i8* %zAppend, i8 signext %quote) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca i8, align 1
  %len = alloca i32, align 4
  %i = alloca i32, align 4
  %nAppend = alloca i32, align 4
  %nIn = alloca i32, align 4
  %zCsr = alloca i8*, align 8
  store i8* %zIn, i8** %2, align 8
  store i8* %zAppend, i8** %3, align 8
  store i8 %quote, i8* %4, align 1
  %5 = load i8** %3, align 8
  %6 = call i32 @strlen30(i8* %5)
  store i32 %6, i32* %nAppend, align 4
  %7 = load i8** %2, align 8
  %8 = icmp ne i8* %7, null
  br i1 %8, label %9, label %12

; <label>:9                                       ; preds = %0
  %10 = load i8** %2, align 8
  %11 = call i32 @strlen30(i8* %10)
  br label %13

; <label>:12                                      ; preds = %0
  br label %13

; <label>:13                                      ; preds = %12, %9
  %14 = phi i32 [ %11, %9 ], [ 0, %12 ]
  store i32 %14, i32* %nIn, align 4
  %15 = load i32* %nAppend, align 4
  %16 = load i32* %nIn, align 4
  %17 = add nsw i32 %15, %16
  %18 = add nsw i32 %17, 1
  store i32 %18, i32* %len, align 4
  %19 = load i8* %4, align 1
  %20 = icmp ne i8 %19, 0
  br i1 %20, label %21, label %46

; <label>:21                                      ; preds = %13
  %22 = load i32* %len, align 4
  %23 = add nsw i32 %22, 2
  store i32 %23, i32* %len, align 4
  store i32 0, i32* %i, align 4
  br label %24

; <label>:24                                      ; preds = %42, %21
  %25 = load i32* %i, align 4
  %26 = load i32* %nAppend, align 4
  %27 = icmp slt i32 %25, %26
  br i1 %27, label %28, label %45

; <label>:28                                      ; preds = %24
  %29 = load i32* %i, align 4
  %30 = sext i32 %29 to i64
  %31 = load i8** %3, align 8
  %32 = getelementptr inbounds i8* %31, i64 %30
  %33 = load i8* %32, align 1
  %34 = sext i8 %33 to i32
  %35 = load i8* %4, align 1
  %36 = sext i8 %35 to i32
  %37 = icmp eq i32 %34, %36
  br i1 %37, label %38, label %41

; <label>:38                                      ; preds = %28
  %39 = load i32* %len, align 4
  %40 = add nsw i32 %39, 1
  store i32 %40, i32* %len, align 4
  br label %41

; <label>:41                                      ; preds = %38, %28
  br label %42

; <label>:42                                      ; preds = %41
  %43 = load i32* %i, align 4
  %44 = add nsw i32 %43, 1
  store i32 %44, i32* %i, align 4
  br label %24

; <label>:45                                      ; preds = %24
  br label %46

; <label>:46                                      ; preds = %45, %13
  %47 = load i8** %2, align 8
  %48 = load i32* %len, align 4
  %49 = sext i32 %48 to i64
  %50 = call i8* @realloc(i8* %47, i64 %49) #5
  store i8* %50, i8** %2, align 8
  %51 = load i8** %2, align 8
  %52 = icmp ne i8* %51, null
  br i1 %52, label %54, label %53

; <label>:53                                      ; preds = %46
  store i8* null, i8** %1
  br label %127

; <label>:54                                      ; preds = %46
  %55 = load i8* %4, align 1
  %56 = icmp ne i8 %55, 0
  br i1 %56, label %57, label %112

; <label>:57                                      ; preds = %54
  %58 = load i32* %nIn, align 4
  %59 = sext i32 %58 to i64
  %60 = load i8** %2, align 8
  %61 = getelementptr inbounds i8* %60, i64 %59
  store i8* %61, i8** %zCsr, align 8
  %62 = load i8* %4, align 1
  %63 = load i8** %zCsr, align 8
  %64 = getelementptr inbounds i8* %63, i32 1
  store i8* %64, i8** %zCsr, align 8
  store i8 %62, i8* %63, align 1
  store i32 0, i32* %i, align 4
  br label %65

; <label>:65                                      ; preds = %91, %57
  %66 = load i32* %i, align 4
  %67 = load i32* %nAppend, align 4
  %68 = icmp slt i32 %66, %67
  br i1 %68, label %69, label %94

; <label>:69                                      ; preds = %65
  %70 = load i32* %i, align 4
  %71 = sext i32 %70 to i64
  %72 = load i8** %3, align 8
  %73 = getelementptr inbounds i8* %72, i64 %71
  %74 = load i8* %73, align 1
  %75 = load i8** %zCsr, align 8
  %76 = getelementptr inbounds i8* %75, i32 1
  store i8* %76, i8** %zCsr, align 8
  store i8 %74, i8* %75, align 1
  %77 = load i32* %i, align 4
  %78 = sext i32 %77 to i64
  %79 = load i8** %3, align 8
  %80 = getelementptr inbounds i8* %79, i64 %78
  %81 = load i8* %80, align 1
  %82 = sext i8 %81 to i32
  %83 = load i8* %4, align 1
  %84 = sext i8 %83 to i32
  %85 = icmp eq i32 %82, %84
  br i1 %85, label %86, label %90

; <label>:86                                      ; preds = %69
  %87 = load i8* %4, align 1
  %88 = load i8** %zCsr, align 8
  %89 = getelementptr inbounds i8* %88, i32 1
  store i8* %89, i8** %zCsr, align 8
  store i8 %87, i8* %88, align 1
  br label %90

; <label>:90                                      ; preds = %86, %69
  br label %91

; <label>:91                                      ; preds = %90
  %92 = load i32* %i, align 4
  %93 = add nsw i32 %92, 1
  store i32 %93, i32* %i, align 4
  br label %65

; <label>:94                                      ; preds = %65
  %95 = load i8* %4, align 1
  %96 = load i8** %zCsr, align 8
  %97 = getelementptr inbounds i8* %96, i32 1
  store i8* %97, i8** %zCsr, align 8
  store i8 %95, i8* %96, align 1
  %98 = load i8** %zCsr, align 8
  %99 = getelementptr inbounds i8* %98, i32 1
  store i8* %99, i8** %zCsr, align 8
  store i8 0, i8* %98, align 1
  %100 = load i8** %zCsr, align 8
  %101 = load i8** %2, align 8
  %102 = ptrtoint i8* %100 to i64
  %103 = ptrtoint i8* %101 to i64
  %104 = sub i64 %102, %103
  %105 = load i32* %len, align 4
  %106 = sext i32 %105 to i64
  %107 = icmp eq i64 %104, %106
  br i1 %107, label %108, label %109

; <label>:108                                     ; preds = %94
  br label %111

; <label>:109                                     ; preds = %94
  call void @__assert_fail(i8* getelementptr inbounds ([16 x i8]* @.str383, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str54, i32 0, i32 0), i32 1131, i8* getelementptr inbounds ([45 x i8]* @__PRETTY_FUNCTION__.appendText, i32 0, i32 0)) #8
  unreachable
                                                  ; No predecessors!
  br label %111

; <label>:111                                     ; preds = %110, %108
  br label %125

; <label>:112                                     ; preds = %54
  %113 = load i32* %nIn, align 4
  %114 = sext i32 %113 to i64
  %115 = load i8** %2, align 8
  %116 = getelementptr inbounds i8* %115, i64 %114
  %117 = load i8** %3, align 8
  %118 = load i32* %nAppend, align 4
  %119 = sext i32 %118 to i64
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %116, i8* %117, i64 %119, i32 1, i1 false)
  %120 = load i32* %len, align 4
  %121 = sub nsw i32 %120, 1
  %122 = sext i32 %121 to i64
  %123 = load i8** %2, align 8
  %124 = getelementptr inbounds i8* %123, i64 %122
  store i8 0, i8* %124, align 1
  br label %125

; <label>:125                                     ; preds = %112, %111
  %126 = load i8** %2, align 8
  store i8* %126, i8** %1
  br label %127

; <label>:127                                     ; preds = %125, %53
  %128 = load i8** %1
  ret i8* %128
}

; Function Attrs: nounwind uwtable
define internal i32 @db_int(%struct.ShellState* %p, i8* %zSql) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca i8*, align 8
  %pStmt = alloca %struct.sqlite3_stmt*, align 8
  %res = alloca i32, align 4
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store i8* %zSql, i8** %2, align 8
  store i32 0, i32* %res, align 4
  %3 = load %struct.ShellState** %1, align 8
  %4 = getelementptr inbounds %struct.ShellState* %3, i32 0, i32 0
  %5 = load %struct.sqlite3** %4, align 8
  %6 = load i8** %2, align 8
  %7 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %5, i8* %6, i32 -1, %struct.sqlite3_stmt** %pStmt, i8** null)
  %8 = load %struct.sqlite3_stmt** %pStmt, align 8
  %9 = icmp ne %struct.sqlite3_stmt* %8, null
  br i1 %9, label %10, label %17

; <label>:10                                      ; preds = %0
  %11 = load %struct.sqlite3_stmt** %pStmt, align 8
  %12 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %11)
  %13 = icmp eq i32 %12, 100
  br i1 %13, label %14, label %17

; <label>:14                                      ; preds = %10
  %15 = load %struct.sqlite3_stmt** %pStmt, align 8
  %16 = call i32 @sqlite3_column_int(%struct.sqlite3_stmt* %15, i32 0)
  store i32 %16, i32* %res, align 4
  br label %17

; <label>:17                                      ; preds = %14, %10, %0
  %18 = load %struct.sqlite3_stmt** %pStmt, align 8
  %19 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %18)
  %20 = load i32* %res, align 4
  ret i32 %20
}

; Function Attrs: nounwind uwtable
define internal void @tryToCloneSchema(%struct.ShellState* %p, %struct.sqlite3* %newDb, i8* %zWhere, void (%struct.ShellState*, %struct.sqlite3*, i8*)* %xForEach) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca %struct.sqlite3*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca void (%struct.ShellState*, %struct.sqlite3*, i8*)*, align 8
  %pQuery = alloca %struct.sqlite3_stmt*, align 8
  %zQuery = alloca i8*, align 8
  %rc = alloca i32, align 4
  %zName = alloca i8*, align 8
  %zSql = alloca i8*, align 8
  %zErrMsg = alloca i8*, align 8
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store %struct.sqlite3* %newDb, %struct.sqlite3** %2, align 8
  store i8* %zWhere, i8** %3, align 8
  store void (%struct.ShellState*, %struct.sqlite3*, i8*)* %xForEach, void (%struct.ShellState*, %struct.sqlite3*, i8*)** %4, align 8
  store %struct.sqlite3_stmt* null, %struct.sqlite3_stmt** %pQuery, align 8
  store i8* null, i8** %zQuery, align 8
  store i8* null, i8** %zErrMsg, align 8
  %5 = load i8** %3, align 8
  %6 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([45 x i8]* @.str434, i32 0, i32 0), i8* %5)
  store i8* %6, i8** %zQuery, align 8
  %7 = load %struct.ShellState** %1, align 8
  %8 = getelementptr inbounds %struct.ShellState* %7, i32 0, i32 0
  %9 = load %struct.sqlite3** %8, align 8
  %10 = load i8** %zQuery, align 8
  %11 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %9, i8* %10, i32 -1, %struct.sqlite3_stmt** %pQuery, i8** null)
  store i32 %11, i32* %rc, align 4
  %12 = load i32* %rc, align 4
  %13 = icmp ne i32 %12, 0
  br i1 %13, label %14, label %26

; <label>:14                                      ; preds = %0
  %15 = load %struct._IO_FILE** @stderr, align 8
  %16 = load %struct.ShellState** %1, align 8
  %17 = getelementptr inbounds %struct.ShellState* %16, i32 0, i32 0
  %18 = load %struct.sqlite3** %17, align 8
  %19 = call i32 @sqlite3_extended_errcode(%struct.sqlite3* %18)
  %20 = load %struct.ShellState** %1, align 8
  %21 = getelementptr inbounds %struct.ShellState* %20, i32 0, i32 0
  %22 = load %struct.sqlite3** %21, align 8
  %23 = call i8* @sqlite3_errmsg(%struct.sqlite3* %22)
  %24 = load i8** %zQuery, align 8
  %25 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([24 x i8]* @.str435, i32 0, i32 0), i32 %19, i8* %23, i8* %24)
  br label %126

; <label>:26                                      ; preds = %0
  br label %27

; <label>:27                                      ; preds = %59, %26
  %28 = load %struct.sqlite3_stmt** %pQuery, align 8
  %29 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %28)
  store i32 %29, i32* %rc, align 4
  %30 = icmp eq i32 %29, 100
  br i1 %30, label %31, label %61

; <label>:31                                      ; preds = %27
  %32 = load %struct.sqlite3_stmt** %pQuery, align 8
  %33 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %32, i32 0)
  store i8* %33, i8** %zName, align 8
  %34 = load %struct.sqlite3_stmt** %pQuery, align 8
  %35 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %34, i32 1)
  store i8* %35, i8** %zSql, align 8
  %36 = load i8** %zName, align 8
  %37 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([7 x i8]* @.str436, i32 0, i32 0), i8* %36)
  %38 = load %struct._IO_FILE** @stdout, align 8
  %39 = call i32 @fflush(%struct._IO_FILE* %38)
  %40 = load %struct.sqlite3** %2, align 8
  %41 = load i8** %zSql, align 8
  %42 = call i32 @sqlite3_exec(%struct.sqlite3* %40, i8* %41, i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** %zErrMsg)
  %43 = load i8** %zErrMsg, align 8
  %44 = icmp ne i8* %43, null
  br i1 %44, label %45, label %51

; <label>:45                                      ; preds = %31
  %46 = load %struct._IO_FILE** @stderr, align 8
  %47 = load i8** %zErrMsg, align 8
  %48 = load i8** %zSql, align 8
  %49 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %46, i8* getelementptr inbounds ([21 x i8]* @.str437, i32 0, i32 0), i8* %47, i8* %48)
  %50 = load i8** %zErrMsg, align 8
  call void @sqlite3_free(i8* %50)
  store i8* null, i8** %zErrMsg, align 8
  br label %51

; <label>:51                                      ; preds = %45, %31
  %52 = load void (%struct.ShellState*, %struct.sqlite3*, i8*)** %4, align 8
  %53 = icmp ne void (%struct.ShellState*, %struct.sqlite3*, i8*)* %52, null
  br i1 %53, label %54, label %59

; <label>:54                                      ; preds = %51
  %55 = load void (%struct.ShellState*, %struct.sqlite3*, i8*)** %4, align 8
  %56 = load %struct.ShellState** %1, align 8
  %57 = load %struct.sqlite3** %2, align 8
  %58 = load i8** %zName, align 8
  call void %55(%struct.ShellState* %56, %struct.sqlite3* %57, i8* %58)
  br label %59

; <label>:59                                      ; preds = %54, %51
  %60 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([6 x i8]* @.str438, i32 0, i32 0))
  br label %27

; <label>:61                                      ; preds = %27
  %62 = load i32* %rc, align 4
  %63 = icmp ne i32 %62, 101
  br i1 %63, label %64, label %125

; <label>:64                                      ; preds = %61
  %65 = load %struct.sqlite3_stmt** %pQuery, align 8
  %66 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %65)
  %67 = load i8** %zQuery, align 8
  call void @sqlite3_free(i8* %67)
  %68 = load i8** %3, align 8
  %69 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([65 x i8]* @.str439, i32 0, i32 0), i8* %68)
  store i8* %69, i8** %zQuery, align 8
  %70 = load %struct.ShellState** %1, align 8
  %71 = getelementptr inbounds %struct.ShellState* %70, i32 0, i32 0
  %72 = load %struct.sqlite3** %71, align 8
  %73 = load i8** %zQuery, align 8
  %74 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %72, i8* %73, i32 -1, %struct.sqlite3_stmt** %pQuery, i8** null)
  store i32 %74, i32* %rc, align 4
  %75 = load i32* %rc, align 4
  %76 = icmp ne i32 %75, 0
  br i1 %76, label %77, label %89

; <label>:77                                      ; preds = %64
  %78 = load %struct._IO_FILE** @stderr, align 8
  %79 = load %struct.ShellState** %1, align 8
  %80 = getelementptr inbounds %struct.ShellState* %79, i32 0, i32 0
  %81 = load %struct.sqlite3** %80, align 8
  %82 = call i32 @sqlite3_extended_errcode(%struct.sqlite3* %81)
  %83 = load %struct.ShellState** %1, align 8
  %84 = getelementptr inbounds %struct.ShellState* %83, i32 0, i32 0
  %85 = load %struct.sqlite3** %84, align 8
  %86 = call i8* @sqlite3_errmsg(%struct.sqlite3* %85)
  %87 = load i8** %zQuery, align 8
  %88 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %78, i8* getelementptr inbounds ([24 x i8]* @.str435, i32 0, i32 0), i32 %82, i8* %86, i8* %87)
  br label %126

; <label>:89                                      ; preds = %64
  br label %90

; <label>:90                                      ; preds = %122, %89
  %91 = load %struct.sqlite3_stmt** %pQuery, align 8
  %92 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %91)
  store i32 %92, i32* %rc, align 4
  %93 = icmp eq i32 %92, 100
  br i1 %93, label %94, label %124

; <label>:94                                      ; preds = %90
  %95 = load %struct.sqlite3_stmt** %pQuery, align 8
  %96 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %95, i32 0)
  store i8* %96, i8** %zName, align 8
  %97 = load %struct.sqlite3_stmt** %pQuery, align 8
  %98 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %97, i32 1)
  store i8* %98, i8** %zSql, align 8
  %99 = load i8** %zName, align 8
  %100 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([7 x i8]* @.str436, i32 0, i32 0), i8* %99)
  %101 = load %struct._IO_FILE** @stdout, align 8
  %102 = call i32 @fflush(%struct._IO_FILE* %101)
  %103 = load %struct.sqlite3** %2, align 8
  %104 = load i8** %zSql, align 8
  %105 = call i32 @sqlite3_exec(%struct.sqlite3* %103, i8* %104, i32 (i8*, i32, i8**, i8**)* null, i8* null, i8** %zErrMsg)
  %106 = load i8** %zErrMsg, align 8
  %107 = icmp ne i8* %106, null
  br i1 %107, label %108, label %114

; <label>:108                                     ; preds = %94
  %109 = load %struct._IO_FILE** @stderr, align 8
  %110 = load i8** %zErrMsg, align 8
  %111 = load i8** %zSql, align 8
  %112 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %109, i8* getelementptr inbounds ([21 x i8]* @.str437, i32 0, i32 0), i8* %110, i8* %111)
  %113 = load i8** %zErrMsg, align 8
  call void @sqlite3_free(i8* %113)
  store i8* null, i8** %zErrMsg, align 8
  br label %114

; <label>:114                                     ; preds = %108, %94
  %115 = load void (%struct.ShellState*, %struct.sqlite3*, i8*)** %4, align 8
  %116 = icmp ne void (%struct.ShellState*, %struct.sqlite3*, i8*)* %115, null
  br i1 %116, label %117, label %122

; <label>:117                                     ; preds = %114
  %118 = load void (%struct.ShellState*, %struct.sqlite3*, i8*)** %4, align 8
  %119 = load %struct.ShellState** %1, align 8
  %120 = load %struct.sqlite3** %2, align 8
  %121 = load i8** %zName, align 8
  call void %118(%struct.ShellState* %119, %struct.sqlite3* %120, i8* %121)
  br label %122

; <label>:122                                     ; preds = %117, %114
  %123 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([6 x i8]* @.str438, i32 0, i32 0))
  br label %90

; <label>:124                                     ; preds = %90
  br label %125

; <label>:125                                     ; preds = %124, %61
  br label %126

; <label>:126                                     ; preds = %125, %77, %14
  %127 = load %struct.sqlite3_stmt** %pQuery, align 8
  %128 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %127)
  %129 = load i8** %zQuery, align 8
  call void @sqlite3_free(i8* %129)
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @tryToCloneData(%struct.ShellState* %p, %struct.sqlite3* %newDb, i8* %zTable) #0 {
  %1 = alloca %struct.ShellState*, align 8
  %2 = alloca %struct.sqlite3*, align 8
  %3 = alloca i8*, align 8
  %pQuery = alloca %struct.sqlite3_stmt*, align 8
  %pInsert = alloca %struct.sqlite3_stmt*, align 8
  %zQuery = alloca i8*, align 8
  %zInsert = alloca i8*, align 8
  %rc = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %n = alloca i32, align 4
  %nTable = alloca i32, align 4
  %k = alloca i32, align 4
  %cnt = alloca i32, align 4
  %spinRate = alloca i32, align 4
  store %struct.ShellState* %p, %struct.ShellState** %1, align 8
  store %struct.sqlite3* %newDb, %struct.sqlite3** %2, align 8
  store i8* %zTable, i8** %3, align 8
  store %struct.sqlite3_stmt* null, %struct.sqlite3_stmt** %pQuery, align 8
  store %struct.sqlite3_stmt* null, %struct.sqlite3_stmt** %pInsert, align 8
  store i8* null, i8** %zQuery, align 8
  store i8* null, i8** %zInsert, align 8
  %4 = load i8** %3, align 8
  %5 = call i64 @strlen(i8* %4) #7
  %6 = trunc i64 %5 to i32
  store i32 %6, i32* %nTable, align 4
  store i32 0, i32* %k, align 4
  store i32 0, i32* %cnt, align 4
  store i32 10000, i32* %spinRate, align 4
  %7 = load i8** %3, align 8
  %8 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([19 x i8]* @.str425, i32 0, i32 0), i8* %7)
  store i8* %8, i8** %zQuery, align 8
  %9 = load %struct.ShellState** %1, align 8
  %10 = getelementptr inbounds %struct.ShellState* %9, i32 0, i32 0
  %11 = load %struct.sqlite3** %10, align 8
  %12 = load i8** %zQuery, align 8
  %13 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %11, i8* %12, i32 -1, %struct.sqlite3_stmt** %pQuery, i8** null)
  store i32 %13, i32* %rc, align 4
  %14 = load i32* %rc, align 4
  %15 = icmp ne i32 %14, 0
  br i1 %15, label %16, label %28

; <label>:16                                      ; preds = %0
  %17 = load %struct._IO_FILE** @stderr, align 8
  %18 = load %struct.ShellState** %1, align 8
  %19 = getelementptr inbounds %struct.ShellState* %18, i32 0, i32 0
  %20 = load %struct.sqlite3** %19, align 8
  %21 = call i32 @sqlite3_extended_errcode(%struct.sqlite3* %20)
  %22 = load %struct.ShellState** %1, align 8
  %23 = getelementptr inbounds %struct.ShellState* %22, i32 0, i32 0
  %24 = load %struct.sqlite3** %23, align 8
  %25 = call i8* @sqlite3_errmsg(%struct.sqlite3* %24)
  %26 = load i8** %zQuery, align 8
  %27 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([22 x i8]* @.str426, i32 0, i32 0), i32 %21, i8* %25, i8* %26)
  br label %210

; <label>:28                                      ; preds = %0
  %29 = load %struct.sqlite3_stmt** %pQuery, align 8
  %30 = call i32 @sqlite3_column_count(%struct.sqlite3_stmt* %29)
  store i32 %30, i32* %n, align 4
  %31 = load i32* %nTable, align 4
  %32 = add nsw i32 200, %31
  %33 = load i32* %n, align 4
  %34 = mul nsw i32 %33, 3
  %35 = add nsw i32 %32, %34
  %36 = sext i32 %35 to i64
  %37 = call i8* @sqlite3_malloc64(i64 %36)
  store i8* %37, i8** %zInsert, align 8
  %38 = load i8** %zInsert, align 8
  %39 = icmp eq i8* %38, null
  br i1 %39, label %40, label %43

; <label>:40                                      ; preds = %28
  %41 = load %struct._IO_FILE** @stderr, align 8
  %42 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %41, i8* getelementptr inbounds ([15 x i8]* @.str2, i32 0, i32 0))
  br label %210

; <label>:43                                      ; preds = %28
  %44 = load i32* %nTable, align 4
  %45 = add nsw i32 200, %44
  %46 = load i8** %zInsert, align 8
  %47 = load i8** %3, align 8
  %48 = call i8* (i32, i8*, i8*, ...)* @sqlite3_snprintf(i32 %45, i8* %46, i8* getelementptr inbounds ([36 x i8]* @.str427, i32 0, i32 0), i8* %47)
  %49 = load i8** %zInsert, align 8
  %50 = call i64 @strlen(i8* %49) #7
  %51 = trunc i64 %50 to i32
  store i32 %51, i32* %i, align 4
  store i32 1, i32* %j, align 4
  br label %52

; <label>:52                                      ; preds = %63, %43
  %53 = load i32* %j, align 4
  %54 = load i32* %n, align 4
  %55 = icmp slt i32 %53, %54
  br i1 %55, label %56, label %66

; <label>:56                                      ; preds = %52
  %57 = load i8** %zInsert, align 8
  %58 = load i32* %i, align 4
  %59 = sext i32 %58 to i64
  %60 = getelementptr inbounds i8* %57, i64 %59
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %60, i8* getelementptr inbounds ([3 x i8]* @.str428, i32 0, i32 0), i64 2, i32 1, i1 false)
  %61 = load i32* %i, align 4
  %62 = add nsw i32 %61, 2
  store i32 %62, i32* %i, align 4
  br label %63

; <label>:63                                      ; preds = %56
  %64 = load i32* %j, align 4
  %65 = add nsw i32 %64, 1
  store i32 %65, i32* %j, align 4
  br label %52

; <label>:66                                      ; preds = %52
  %67 = load i8** %zInsert, align 8
  %68 = load i32* %i, align 4
  %69 = sext i32 %68 to i64
  %70 = getelementptr inbounds i8* %67, i64 %69
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %70, i8* getelementptr inbounds ([3 x i8]* @.str375, i32 0, i32 0), i64 3, i32 1, i1 false)
  %71 = load %struct.sqlite3** %2, align 8
  %72 = load i8** %zInsert, align 8
  %73 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %71, i8* %72, i32 -1, %struct.sqlite3_stmt** %pInsert, i8** null)
  store i32 %73, i32* %rc, align 4
  %74 = load i32* %rc, align 4
  %75 = icmp ne i32 %74, 0
  br i1 %75, label %76, label %84

; <label>:76                                      ; preds = %66
  %77 = load %struct._IO_FILE** @stderr, align 8
  %78 = load %struct.sqlite3** %2, align 8
  %79 = call i32 @sqlite3_extended_errcode(%struct.sqlite3* %78)
  %80 = load %struct.sqlite3** %2, align 8
  %81 = call i8* @sqlite3_errmsg(%struct.sqlite3* %80)
  %82 = load i8** %zQuery, align 8
  %83 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %77, i8* getelementptr inbounds ([22 x i8]* @.str426, i32 0, i32 0), i32 %79, i8* %81, i8* %82)
  br label %210

; <label>:84                                      ; preds = %66
  store i32 0, i32* %k, align 4
  br label %85

; <label>:85                                      ; preds = %206, %84
  %86 = load i32* %k, align 4
  %87 = icmp slt i32 %86, 2
  br i1 %87, label %88, label %209

; <label>:88                                      ; preds = %85
  br label %89

; <label>:89                                      ; preds = %183, %88
  %90 = load %struct.sqlite3_stmt** %pQuery, align 8
  %91 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %90)
  store i32 %91, i32* %rc, align 4
  %92 = icmp eq i32 %91, 100
  br i1 %92, label %93, label %184

; <label>:93                                      ; preds = %89
  store i32 0, i32* %i, align 4
  br label %94

; <label>:94                                      ; preds = %143, %93
  %95 = load i32* %i, align 4
  %96 = load i32* %n, align 4
  %97 = icmp slt i32 %95, %96
  br i1 %97, label %98, label %146

; <label>:98                                      ; preds = %94
  %99 = load %struct.sqlite3_stmt** %pQuery, align 8
  %100 = load i32* %i, align 4
  %101 = call i32 @sqlite3_column_type(%struct.sqlite3_stmt* %99, i32 %100)
  switch i32 %101, label %142 [
    i32 5, label %102
    i32 1, label %107
    i32 2, label %115
    i32 3, label %123
    i32 4, label %131
  ]

; <label>:102                                     ; preds = %98
  %103 = load %struct.sqlite3_stmt** %pInsert, align 8
  %104 = load i32* %i, align 4
  %105 = add nsw i32 %104, 1
  %106 = call i32 @sqlite3_bind_null(%struct.sqlite3_stmt* %103, i32 %105)
  br label %142

; <label>:107                                     ; preds = %98
  %108 = load %struct.sqlite3_stmt** %pInsert, align 8
  %109 = load i32* %i, align 4
  %110 = add nsw i32 %109, 1
  %111 = load %struct.sqlite3_stmt** %pQuery, align 8
  %112 = load i32* %i, align 4
  %113 = call i64 @sqlite3_column_int64(%struct.sqlite3_stmt* %111, i32 %112)
  %114 = call i32 @sqlite3_bind_int64(%struct.sqlite3_stmt* %108, i32 %110, i64 %113)
  br label %142

; <label>:115                                     ; preds = %98
  %116 = load %struct.sqlite3_stmt** %pInsert, align 8
  %117 = load i32* %i, align 4
  %118 = add nsw i32 %117, 1
  %119 = load %struct.sqlite3_stmt** %pQuery, align 8
  %120 = load i32* %i, align 4
  %121 = call double @sqlite3_column_double(%struct.sqlite3_stmt* %119, i32 %120)
  %122 = call i32 @sqlite3_bind_double(%struct.sqlite3_stmt* %116, i32 %118, double %121)
  br label %142

; <label>:123                                     ; preds = %98
  %124 = load %struct.sqlite3_stmt** %pInsert, align 8
  %125 = load i32* %i, align 4
  %126 = add nsw i32 %125, 1
  %127 = load %struct.sqlite3_stmt** %pQuery, align 8
  %128 = load i32* %i, align 4
  %129 = call i8* @sqlite3_column_text(%struct.sqlite3_stmt* %127, i32 %128)
  %130 = call i32 @sqlite3_bind_text(%struct.sqlite3_stmt* %124, i32 %126, i8* %129, i32 -1, void (i8*)* null)
  br label %142

; <label>:131                                     ; preds = %98
  %132 = load %struct.sqlite3_stmt** %pInsert, align 8
  %133 = load i32* %i, align 4
  %134 = add nsw i32 %133, 1
  %135 = load %struct.sqlite3_stmt** %pQuery, align 8
  %136 = load i32* %i, align 4
  %137 = call i8* @sqlite3_column_blob(%struct.sqlite3_stmt* %135, i32 %136)
  %138 = load %struct.sqlite3_stmt** %pQuery, align 8
  %139 = load i32* %i, align 4
  %140 = call i32 @sqlite3_column_bytes(%struct.sqlite3_stmt* %138, i32 %139)
  %141 = call i32 @sqlite3_bind_blob(%struct.sqlite3_stmt* %132, i32 %134, i8* %137, i32 %140, void (i8*)* null)
  br label %142

; <label>:142                                     ; preds = %98, %131, %123, %115, %107, %102
  br label %143

; <label>:143                                     ; preds = %142
  %144 = load i32* %i, align 4
  %145 = add nsw i32 %144, 1
  store i32 %145, i32* %i, align 4
  br label %94

; <label>:146                                     ; preds = %94
  %147 = load %struct.sqlite3_stmt** %pInsert, align 8
  %148 = call i32 @sqlite3_step(%struct.sqlite3_stmt* %147)
  store i32 %148, i32* %rc, align 4
  %149 = load i32* %rc, align 4
  %150 = icmp ne i32 %149, 0
  br i1 %150, label %151, label %164

; <label>:151                                     ; preds = %146
  %152 = load i32* %rc, align 4
  %153 = icmp ne i32 %152, 100
  br i1 %153, label %154, label %164

; <label>:154                                     ; preds = %151
  %155 = load i32* %rc, align 4
  %156 = icmp ne i32 %155, 101
  br i1 %156, label %157, label %164

; <label>:157                                     ; preds = %154
  %158 = load %struct._IO_FILE** @stderr, align 8
  %159 = load %struct.sqlite3** %2, align 8
  %160 = call i32 @sqlite3_extended_errcode(%struct.sqlite3* %159)
  %161 = load %struct.sqlite3** %2, align 8
  %162 = call i8* @sqlite3_errmsg(%struct.sqlite3* %161)
  %163 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %158, i8* getelementptr inbounds ([14 x i8]* @.str429, i32 0, i32 0), i32 %160, i8* %162)
  br label %164

; <label>:164                                     ; preds = %157, %154, %151, %146
  %165 = load %struct.sqlite3_stmt** %pInsert, align 8
  %166 = call i32 @sqlite3_reset(%struct.sqlite3_stmt* %165)
  %167 = load i32* %cnt, align 4
  %168 = add nsw i32 %167, 1
  store i32 %168, i32* %cnt, align 4
  %169 = load i32* %cnt, align 4
  %170 = srem i32 %169, 10000
  %171 = icmp eq i32 %170, 0
  br i1 %171, label %172, label %183

; <label>:172                                     ; preds = %164
  %173 = load i32* %cnt, align 4
  %174 = sdiv i32 %173, 10000
  %175 = srem i32 %174, 4
  %176 = sext i32 %175 to i64
  %177 = getelementptr inbounds [5 x i8]* @.str431, i32 0, i64 %176
  %178 = load i8* %177, align 1
  %179 = sext i8 %178 to i32
  %180 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @.str430, i32 0, i32 0), i32 %179)
  %181 = load %struct._IO_FILE** @stdout, align 8
  %182 = call i32 @fflush(%struct._IO_FILE* %181)
  br label %183

; <label>:183                                     ; preds = %172, %164
  br label %89

; <label>:184                                     ; preds = %89
  %185 = load i32* %rc, align 4
  %186 = icmp eq i32 %185, 101
  br i1 %186, label %187, label %188

; <label>:187                                     ; preds = %184
  br label %209

; <label>:188                                     ; preds = %184
  %189 = load %struct.sqlite3_stmt** %pQuery, align 8
  %190 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %189)
  %191 = load i8** %zQuery, align 8
  call void @sqlite3_free(i8* %191)
  %192 = load i8** %3, align 8
  %193 = call i8* (i8*, ...)* @sqlite3_mprintf(i8* getelementptr inbounds ([40 x i8]* @.str432, i32 0, i32 0), i8* %192)
  store i8* %193, i8** %zQuery, align 8
  %194 = load %struct.ShellState** %1, align 8
  %195 = getelementptr inbounds %struct.ShellState* %194, i32 0, i32 0
  %196 = load %struct.sqlite3** %195, align 8
  %197 = load i8** %zQuery, align 8
  %198 = call i32 @sqlite3_prepare_v2(%struct.sqlite3* %196, i8* %197, i32 -1, %struct.sqlite3_stmt** %pQuery, i8** null)
  store i32 %198, i32* %rc, align 4
  %199 = load i32* %rc, align 4
  %200 = icmp ne i32 %199, 0
  br i1 %200, label %201, label %205

; <label>:201                                     ; preds = %188
  %202 = load %struct._IO_FILE** @stderr, align 8
  %203 = load i8** %3, align 8
  %204 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %202, i8* getelementptr inbounds ([36 x i8]* @.str433, i32 0, i32 0), i8* %203)
  br label %209

; <label>:205                                     ; preds = %188
  br label %206

; <label>:206                                     ; preds = %205
  %207 = load i32* %k, align 4
  %208 = add nsw i32 %207, 1
  store i32 %208, i32* %k, align 4
  br label %85

; <label>:209                                     ; preds = %201, %187, %85
  br label %210

; <label>:210                                     ; preds = %209, %76, %40, %16
  %211 = load %struct.sqlite3_stmt** %pQuery, align 8
  %212 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %211)
  %213 = load %struct.sqlite3_stmt** %pInsert, align 8
  %214 = call i32 @sqlite3_finalize(%struct.sqlite3_stmt* %213)
  %215 = load i8** %zQuery, align 8
  call void @sqlite3_free(i8* %215)
  %216 = load i8** %zInsert, align 8
  call void @sqlite3_free(i8* %216)
  ret void
}

declare i32 @sqlite3_extended_errcode(%struct.sqlite3*) #2

declare i32 @sqlite3_bind_int64(%struct.sqlite3_stmt*, i32, i64) #2

declare i64 @sqlite3_column_int64(%struct.sqlite3_stmt*, i32) #2

declare i32 @sqlite3_bind_double(%struct.sqlite3_stmt*, i32, double) #2

declare double @sqlite3_column_double(%struct.sqlite3_stmt*, i32) #2

declare i32 @sqlite3_bind_blob(%struct.sqlite3_stmt*, i32, i8*, i32, void (i8*)*) #2

; Function Attrs: nounwind uwtable
define internal i32 @hexDigitValue(i8 signext %c) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i8, align 1
  store i8 %c, i8* %2, align 1
  %3 = load i8* %2, align 1
  %4 = sext i8 %3 to i32
  %5 = icmp sge i32 %4, 48
  br i1 %5, label %6, label %14

; <label>:6                                       ; preds = %0
  %7 = load i8* %2, align 1
  %8 = sext i8 %7 to i32
  %9 = icmp sle i32 %8, 57
  br i1 %9, label %10, label %14

; <label>:10                                      ; preds = %6
  %11 = load i8* %2, align 1
  %12 = sext i8 %11 to i32
  %13 = sub nsw i32 %12, 48
  store i32 %13, i32* %1
  br label %41

; <label>:14                                      ; preds = %6, %0
  %15 = load i8* %2, align 1
  %16 = sext i8 %15 to i32
  %17 = icmp sge i32 %16, 97
  br i1 %17, label %18, label %27

; <label>:18                                      ; preds = %14
  %19 = load i8* %2, align 1
  %20 = sext i8 %19 to i32
  %21 = icmp sle i32 %20, 102
  br i1 %21, label %22, label %27

; <label>:22                                      ; preds = %18
  %23 = load i8* %2, align 1
  %24 = sext i8 %23 to i32
  %25 = sub nsw i32 %24, 97
  %26 = add nsw i32 %25, 10
  store i32 %26, i32* %1
  br label %41

; <label>:27                                      ; preds = %18, %14
  %28 = load i8* %2, align 1
  %29 = sext i8 %28 to i32
  %30 = icmp sge i32 %29, 65
  br i1 %30, label %31, label %40

; <label>:31                                      ; preds = %27
  %32 = load i8* %2, align 1
  %33 = sext i8 %32 to i32
  %34 = icmp sle i32 %33, 70
  br i1 %34, label %35, label %40

; <label>:35                                      ; preds = %31
  %36 = load i8* %2, align 1
  %37 = sext i8 %36 to i32
  %38 = sub nsw i32 %37, 65
  %39 = add nsw i32 %38, 10
  store i32 %39, i32* %1
  br label %41

; <label>:40                                      ; preds = %31, %27
  store i32 -1, i32* %1
  br label %41

; <label>:41                                      ; preds = %40, %35, %22, %10
  %42 = load i32* %1
  ret i32 %42
}

declare i32 @sqlite3_stricmp(i8*, i8*) #2

declare i32 @sqlite3_initialize() #2

declare i32 @sqlite3_errcode(%struct.sqlite3*) #2

declare i32 @sqlite3_create_function(%struct.sqlite3*, i8*, i32, i32, i8*, void (%struct.sqlite3_context*, i32, %struct.Mem**)*, void (%struct.sqlite3_context*, i32, %struct.Mem**)*, void (%struct.sqlite3_context*)*) #2

; Function Attrs: nounwind uwtable
define internal void @shellstaticFunc(%struct.sqlite3_context* %context, i32 %argc, %struct.Mem** %argv) #0 {
  %1 = alloca %struct.sqlite3_context*, align 8
  %2 = alloca i32, align 4
  %3 = alloca %struct.Mem**, align 8
  store %struct.sqlite3_context* %context, %struct.sqlite3_context** %1, align 8
  store i32 %argc, i32* %2, align 4
  store %struct.Mem** %argv, %struct.Mem*** %3, align 8
  %4 = load i32* %2, align 4
  %5 = icmp eq i32 0, %4
  br i1 %5, label %6, label %7

; <label>:6                                       ; preds = %0
  br label %9

; <label>:7                                       ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([8 x i8]* @.str453, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str54, i32 0, i32 0), i32 427, i8* getelementptr inbounds ([63 x i8]* @__PRETTY_FUNCTION__.shellstaticFunc, i32 0, i32 0)) #8
  unreachable
                                                  ; No predecessors!
  br label %9

; <label>:9                                       ; preds = %8, %6
  %10 = load i8** @zShellStatic, align 8
  %11 = icmp ne i8* %10, null
  br i1 %11, label %12, label %13

; <label>:12                                      ; preds = %9
  br label %15

; <label>:13                                      ; preds = %9
  call void @__assert_fail(i8* getelementptr inbounds ([13 x i8]* @.str454, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8]* @.str54, i32 0, i32 0), i32 428, i8* getelementptr inbounds ([63 x i8]* @__PRETTY_FUNCTION__.shellstaticFunc, i32 0, i32 0)) #8
  unreachable
                                                  ; No predecessors!
  br label %15

; <label>:15                                      ; preds = %14, %12
  %16 = load i32* %2, align 4
  %17 = load %struct.Mem*** %3, align 8
  %18 = load %struct.sqlite3_context** %1, align 8
  %19 = load i8** @zShellStatic, align 8
  call void @sqlite3_result_text(%struct.sqlite3_context* %18, i8* %19, i32 -1, void (i8*)* null)
  ret void
}

declare i32 @sqlite3_enable_load_extension(%struct.sqlite3*, i32) #2

; Function Attrs: nounwind uwtable
define internal void @readfileFunc(%struct.sqlite3_context* %context, i32 %argc, %struct.Mem** %argv) #0 {
  %1 = alloca %struct.sqlite3_context*, align 8
  %2 = alloca i32, align 4
  %3 = alloca %struct.Mem**, align 8
  %zName = alloca i8*, align 8
  %in = alloca %struct._IO_FILE*, align 8
  %nIn = alloca i64, align 8
  %pBuf = alloca i8*, align 8
  store %struct.sqlite3_context* %context, %struct.sqlite3_context** %1, align 8
  store i32 %argc, i32* %2, align 4
  store %struct.Mem** %argv, %struct.Mem*** %3, align 8
  %4 = load %struct.Mem*** %3, align 8
  %5 = getelementptr inbounds %struct.Mem** %4, i64 0
  %6 = load %struct.Mem** %5, align 8
  %7 = call i8* @sqlite3_value_text(%struct.Mem* %6)
  store i8* %7, i8** %zName, align 8
  %8 = load i8** %zName, align 8
  %9 = icmp eq i8* %8, null
  br i1 %9, label %10, label %11

; <label>:10                                      ; preds = %0
  br label %43

; <label>:11                                      ; preds = %0
  %12 = load i8** %zName, align 8
  %13 = call %struct._IO_FILE* @fopen64(i8* %12, i8* getelementptr inbounds ([3 x i8]* @.str197, i32 0, i32 0))
  store %struct._IO_FILE* %13, %struct._IO_FILE** %in, align 8
  %14 = load %struct._IO_FILE** %in, align 8
  %15 = icmp eq %struct._IO_FILE* %14, null
  br i1 %15, label %16, label %17

; <label>:16                                      ; preds = %11
  br label %43

; <label>:17                                      ; preds = %11
  %18 = load %struct._IO_FILE** %in, align 8
  %19 = call i32 @fseek(%struct._IO_FILE* %18, i64 0, i32 2)
  %20 = load %struct._IO_FILE** %in, align 8
  %21 = call i64 @ftell(%struct._IO_FILE* %20)
  store i64 %21, i64* %nIn, align 8
  %22 = load %struct._IO_FILE** %in, align 8
  call void @rewind(%struct._IO_FILE* %22)
  %23 = load i64* %nIn, align 8
  %24 = call i8* @sqlite3_malloc64(i64 %23)
  store i8* %24, i8** %pBuf, align 8
  %25 = load i8** %pBuf, align 8
  %26 = icmp ne i8* %25, null
  br i1 %26, label %27, label %38

; <label>:27                                      ; preds = %17
  %28 = load i8** %pBuf, align 8
  %29 = load i64* %nIn, align 8
  %30 = load %struct._IO_FILE** %in, align 8
  %31 = call i64 @fread(i8* %28, i64 %29, i64 1, %struct._IO_FILE* %30)
  %32 = icmp eq i64 1, %31
  br i1 %32, label %33, label %38

; <label>:33                                      ; preds = %27
  %34 = load %struct.sqlite3_context** %1, align 8
  %35 = load i8** %pBuf, align 8
  %36 = load i64* %nIn, align 8
  %37 = trunc i64 %36 to i32
  call void @sqlite3_result_blob(%struct.sqlite3_context* %34, i8* %35, i32 %37, void (i8*)* @sqlite3_free)
  br label %40

; <label>:38                                      ; preds = %27, %17
  %39 = load i8** %pBuf, align 8
  call void @sqlite3_free(i8* %39)
  br label %40

; <label>:40                                      ; preds = %38, %33
  %41 = load %struct._IO_FILE** %in, align 8
  %42 = call i32 @fclose(%struct._IO_FILE* %41)
  br label %43

; <label>:43                                      ; preds = %40, %16, %10
  ret void
}

; Function Attrs: nounwind uwtable
define internal void @writefileFunc(%struct.sqlite3_context* %context, i32 %argc, %struct.Mem** %argv) #0 {
  %1 = alloca %struct.sqlite3_context*, align 8
  %2 = alloca i32, align 4
  %3 = alloca %struct.Mem**, align 8
  %out = alloca %struct._IO_FILE*, align 8
  %z = alloca i8*, align 8
  %rc = alloca i64, align 8
  %zFile = alloca i8*, align 8
  store %struct.sqlite3_context* %context, %struct.sqlite3_context** %1, align 8
  store i32 %argc, i32* %2, align 4
  store %struct.Mem** %argv, %struct.Mem*** %3, align 8
  %4 = load %struct.Mem*** %3, align 8
  %5 = getelementptr inbounds %struct.Mem** %4, i64 0
  %6 = load %struct.Mem** %5, align 8
  %7 = call i8* @sqlite3_value_text(%struct.Mem* %6)
  store i8* %7, i8** %zFile, align 8
  %8 = load i8** %zFile, align 8
  %9 = icmp eq i8* %8, null
  br i1 %9, label %10, label %11

; <label>:10                                      ; preds = %0
  br label %39

; <label>:11                                      ; preds = %0
  %12 = load i8** %zFile, align 8
  %13 = call %struct._IO_FILE* @fopen64(i8* %12, i8* getelementptr inbounds ([3 x i8]* @.str355, i32 0, i32 0))
  store %struct._IO_FILE* %13, %struct._IO_FILE** %out, align 8
  %14 = load %struct._IO_FILE** %out, align 8
  %15 = icmp eq %struct._IO_FILE* %14, null
  br i1 %15, label %16, label %17

; <label>:16                                      ; preds = %11
  br label %39

; <label>:17                                      ; preds = %11
  %18 = load %struct.Mem*** %3, align 8
  %19 = getelementptr inbounds %struct.Mem** %18, i64 1
  %20 = load %struct.Mem** %19, align 8
  %21 = call i8* @sqlite3_value_blob(%struct.Mem* %20)
  store i8* %21, i8** %z, align 8
  %22 = load i8** %z, align 8
  %23 = icmp eq i8* %22, null
  br i1 %23, label %24, label %25

; <label>:24                                      ; preds = %17
  store i64 0, i64* %rc, align 8
  br label %34

; <label>:25                                      ; preds = %17
  %26 = load i8** %z, align 8
  %27 = load %struct.Mem*** %3, align 8
  %28 = getelementptr inbounds %struct.Mem** %27, i64 1
  %29 = load %struct.Mem** %28, align 8
  %30 = call i32 @sqlite3_value_bytes(%struct.Mem* %29)
  %31 = sext i32 %30 to i64
  %32 = load %struct._IO_FILE** %out, align 8
  %33 = call i64 @fwrite(i8* %26, i64 1, i64 %31, %struct._IO_FILE* %32)
  store i64 %33, i64* %rc, align 8
  br label %34

; <label>:34                                      ; preds = %25, %24
  %35 = load %struct._IO_FILE** %out, align 8
  %36 = call i32 @fclose(%struct._IO_FILE* %35)
  %37 = load %struct.sqlite3_context** %1, align 8
  %38 = load i64* %rc, align 8
  call void @sqlite3_result_int64(%struct.sqlite3_context* %37, i64 %38)
  br label %39

; <label>:39                                      ; preds = %34, %16, %10
  ret void
}

declare i8* @sqlite3_value_text(%struct.Mem*) #2

declare i8* @sqlite3_value_blob(%struct.Mem*) #2

declare i64 @fwrite(i8*, i64, i64, %struct._IO_FILE*) #2

declare i32 @sqlite3_value_bytes(%struct.Mem*) #2

declare void @sqlite3_result_int64(%struct.sqlite3_context*, i64) #2

declare i32 @fseek(%struct._IO_FILE*, i64, i32) #2

declare i64 @ftell(%struct._IO_FILE*) #2

declare void @rewind(%struct._IO_FILE*) #2

declare i64 @fread(i8*, i64, i64, %struct._IO_FILE*) #2

declare void @sqlite3_result_blob(%struct.sqlite3_context*, i8*, i32, void (i8*)*) #2

declare void @sqlite3_result_text(%struct.sqlite3_context*, i8*, i32, void (i8*)*) #2

declare void @sqlite3_interrupt(%struct.sqlite3*) #2

; Function Attrs: nounwind uwtable
define internal void @shellLog(i8* %pArg, i32 %iErrCode, i8* %zMsg) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i8*, align 8
  %p = alloca %struct.ShellState*, align 8
  store i8* %pArg, i8** %1, align 8
  store i32 %iErrCode, i32* %2, align 4
  store i8* %zMsg, i8** %3, align 8
  %4 = load i8** %1, align 8
  %5 = bitcast i8* %4 to %struct.ShellState*
  store %struct.ShellState* %5, %struct.ShellState** %p, align 8
  %6 = load %struct.ShellState** %p, align 8
  %7 = getelementptr inbounds %struct.ShellState* %6, i32 0, i32 27
  %8 = load %struct._IO_FILE** %7, align 8
  %9 = icmp eq %struct._IO_FILE* %8, null
  br i1 %9, label %10, label %11

; <label>:10                                      ; preds = %0
  br label %22

; <label>:11                                      ; preds = %0
  %12 = load %struct.ShellState** %p, align 8
  %13 = getelementptr inbounds %struct.ShellState* %12, i32 0, i32 27
  %14 = load %struct._IO_FILE** %13, align 8
  %15 = load i32* %2, align 4
  %16 = load i8** %3, align 8
  %17 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([9 x i8]* @.str468, i32 0, i32 0), i32 %15, i8* %16)
  %18 = load %struct.ShellState** %p, align 8
  %19 = getelementptr inbounds %struct.ShellState* %18, i32 0, i32 27
  %20 = load %struct._IO_FILE** %19, align 8
  %21 = call i32 @fflush(%struct._IO_FILE* %20)
  br label %22

; <label>:22                                      ; preds = %11, %10
  ret void
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readonly "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }
attributes #6 = { nounwind readnone "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readonly }
attributes #8 = { noreturn nounwind }
attributes #9 = { nounwind readnone }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.0 (tags/RELEASE_350/final)"}
