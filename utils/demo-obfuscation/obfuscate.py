#!/bin/python
import sys, os
from llvm import *
from llvm.core import *
import networkx as nx
import capybara_utils as cutils
sys.path.append(os.path.join(sys.path[0], 'Pseudoloop')) # 'Pseudoloop' is relative path to package Pseudoloop
import Pseudoloop.pseudoloop as ps

def print_usage():
    print "Usage: %s input.ll output.ll" % [sys.argv[0]]

def obfuscate_function(function):
    name_length = 8
    #new_block_name  = get_random_string(name_length)
    #new_trash_block = function.append_basic_block(new_block_name)
    #insert_block_between(function.basic_blocks[0], function.basic_blocks[1], new_trash_block)


def obfuscate_module(module):
    #integer_type = Type.int()
    #variable = module.add_global_variable(integer_type, "gv1")
    #target_function = module.get_function_named("compare")
    #obfuscate_function(target_function)

    f = module.get_function_named("xor")
    print(f)
    print "---------------"
    b = f.basic_blocks[4]
    print(b)
    print "---------------"
    print ([i.opcode_name for i in cutils.get_local_variables(f, b)])

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print_usage()
        sys.exit(0)

    llfile = file(sys.argv[1])
    if llfile is None:
        print "File open error. Check input filename."
        sys.exit(0)

    crackme_module = Module.from_assembly(llfile)
    obfuscate_module(crackme_module)
    obfuscated_bitcode_file = file(sys.argv[2], "w")
    crackme_module.to_bitcode(obfuscated_bitcode_file)